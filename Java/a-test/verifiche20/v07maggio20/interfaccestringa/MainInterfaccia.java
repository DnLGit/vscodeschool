package verifiche20.v07maggio20.interfaccestringa; // modificato percorso package 29.05.20

public class MainInterfaccia {
    public static void main(String[] args) {
        
        Maiuscolo ma = new Maiuscolo();
        Minuscolo mi = new Minuscolo();

        System.out.println(ma.scrivi("scrivimi TUTTO maiuscolo"));
        System.out.println();

        System.out.println(mi.scrivi("SCRIVIMI tutto MINUSCOLO")); 
    }
    
}