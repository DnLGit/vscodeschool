/*  Gli yacht, infine, sono caratterizzati dal numero di cavalli del motore, dal numero di posti letto e 
    data di iscrizione all’elenco nazionale degli yacht  */

package verifiche20.v07maggio20.noleggiobarche; // modificato percorso package 29.05.20
import java.util.*;

public class Yacht extends Barche {

    private int cavalli, numeroPostiLetto;
    private String dataIscrizione;

    public Yacht() {                                                // Attenzione: aggiungere solo le variabili di super!!!
    }

    public Yacht(int lunghezza, int larghezza, int costoOrario, int valoreBarca, String nomeBarca, int numeroPosti, boolean conMotore, double altezzavela, double larghezzavela, int eta, int cavalli, int numeroPostiLetto, String dataIscrizione) {
        super(lunghezza, larghezza, costoOrario, valoreBarca, nomeBarca);
        this.cavalli = cavalli;                                         
        this.numeroPostiLetto = numeroPostiLetto;
        this.dataIscrizione = dataIscrizione;
    }

    public String stampaInfo(){                                         // 1° errore, era: public String stampaInfoString()
        return super.stampaInfo()+ "| cavalli: " + cavalli+"| numeroPostiLetto: " + numeroPostiLetto+"| dataIscrizione: " + dataIscrizione;
    }



/*     public boolean equals(Object o) {                                // metodo equals dell'esercizio consegnato
        if (o == this)
            return true;
        if (!(o instanceof Yacht)) {
            return false;
        }
        Yacht yacht = (Yacht) o;
        return cavalli == yacht.cavalli && numeroPostiLetto == yacht.numeroPostiLetto && Objects.equals(dataIscrizione, yacht.dataIscrizione);
    } */

    public boolean equals(Object obj) {                                   // metodo equals rifatto
        if (!super.equals(obj))                                          // e anche qui ... se ...??? se cosa???
            return true;
        if (!(obj instanceof Yacht)) {
            return false;
        }
        Yacht yt = (Yacht) obj;
        return (this.cavalli == yt.cavalli && 
        this.numeroPostiLetto == yt.numeroPostiLetto &&
        this.dataIscrizione.equals(yt.dataIscrizione));
    }


    // metodi Getter&Setter
    public int getCavalli() {
        return this.cavalli;
    }
    public void setCavalli(int cavalli) {
        this.cavalli = cavalli;
    }
    public int getNumeroPosti() {
        return this.numeroPostiLetto;
    }
    public void setNumeroPosti(int numeroPosti) {
        this.numeroPostiLetto = numeroPosti;
    }
    public String getDataIscrizione() {
        return this.dataIscrizione;
    }
    public void setDataIscrizione(String dataIscrizione) {
        this.dataIscrizione = dataIscrizione;
    }
   
}