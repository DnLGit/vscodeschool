/*  Le caratteristiche in comune a tutte e 3 le tipologie di imbarcazioni sono lunghezza in metri, larghezza in metri, 
    costo all’ora per il noleggio e descrizione ed il valore in euro.    */

    package verifiche20.v07maggio20.noleggiobarche; // modificato percorso package 29.05.20

import java.util.*;

public class Barche {

    protected int lunghezza, larghezza, costoOrario, valoreBarca;
    protected String nomeBarca;


    public Barche() {                                               // costruttori: questo di default
    }

    public Barche(int lunghezza, int larghezza, int costoOrario, int valoreBarca, String nomeBarca) {
        this.lunghezza = lunghezza;
        this.larghezza = larghezza;
        this.costoOrario = costoOrario;
        this.valoreBarca = valoreBarca;
        this.nomeBarca = nomeBarca;
    }


     // metodo toString                                              // 3° errore: mancava qui sotto la variabile "nomebarca"
     public String stampaInfo(){                            
        return "L'imbarcazione " +nomeBarca+": |> Lunghezza: "+lunghezza+ "| larghezza: " + larghezza + " | costoOrario: "+ costoOrario+" | valoreBarca: " +valoreBarca;               // cambiata posizione di "nomeBarca"                   
    }


/*      public boolean equals(Object o) {               // metodo equals dell'esercizio consegnato
        if (o == this) 
            return true;
        if (!(o instanceof Barche)) {
            return false;
        }
        Barche barche = (Barche) o;
        return lunghezza == barche.lunghezza && larghezza == barche.larghezza && costoOrario == barche.costoOrario && valoreBarca == barche.valoreBarca && Objects.equals(nomeBarca, barche.nomeBarca);
    } */

    public boolean equals(Object obj) {                 // metodo equals rifatto
        if (obj == null) 
            return false;
        if (!(obj instanceof Barche)) {
            return false;
        }
        Barche br = (Barche) obj;
        return (this.lunghezza == br.lunghezza && 
        this.larghezza == br.larghezza && 
        this.costoOrario == br.costoOrario && 
        this.valoreBarca == br.valoreBarca && 
        this.nomeBarca.equals(br.nomeBarca));
    }

    
  


    // metodi Getter&Setter
    public int getLunghezza() {
        return this.lunghezza;
    }
    public void setLunghezza(int lunghezza) {
        this.lunghezza = lunghezza;
    }
    public int getLarghezza() {
        return this.larghezza;
    }
    public void setLarghezza(int larghezza) {
        this.larghezza = larghezza;
    }
    public int getCostoOrario() {
        return this.costoOrario;
    }
    public void setCostoOrario(int costoOrario) {
        this.costoOrario = costoOrario;
    }
    public int getValoreBarca() {
        return this.valoreBarca;
    }
    public void setValoreBarca(int valoreBarca) {
        this.valoreBarca = valoreBarca;
    }
    public String getNomeBarca() {
        return this.nomeBarca;
    }
    public void setNomeBarca(String nomeBarca) {
        this.nomeBarca = nomeBarca;
    }
}