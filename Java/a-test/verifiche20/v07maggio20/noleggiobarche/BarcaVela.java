/*  Le barche a vela, oltre alle caratteristiche in comune, sono caratterizzate da altezza e larghezza della vela ed età
    minima per poterle noleggiare.     */

    package verifiche20.v07maggio20.noleggiobarche; // modificato percorso package 29.05.20
import java.util.*;

public class BarcaVela extends Barche {

    private double altezzavela, larghezzavela;
    private int eta;



    public BarcaVela() {
    }


    public BarcaVela(int lunghezza, int larghezza, int costoOrario, int valoreBarca, String nomeBarca, int numeroPosti, boolean conMotore, double altezzavela, double larghezzavela, int eta) {
        super(lunghezza, larghezza, costoOrario, valoreBarca, nomeBarca);
        this.altezzavela = altezzavela;
        this.larghezzavela = larghezzavela;
        this.eta = eta;
    }

    public String stampaInfo(){                                         // 1° errore, era: public String stampaInfoString()
        return super.stampaInfo() + "| altezzavela: " + altezzavela+"| larghezzavela: " + larghezzavela+"| eta: " + eta;
    }


 
/*     public boolean equals(Object o) {                    // metodo equals dell'esercizio consegnato
        if (o == this)
            return true;
        if (!(o instanceof BarcaVela)) {
            return false;
        }
        BarcaVela barcaVela = (BarcaVela) o;
        return altezzavela == barcaVela.altezzavela && larghezzavela == barcaVela.larghezzavela && eta == barcaVela.eta;
    } */

     public boolean equals(Object obj) {                    // metodo equals rifatto
        if (!super.equals(obj))                             // anche qui ... se ...??? se cosa???
            return false;                                   // controllare questo "false" con es. Andrea
        if (!(obj instanceof BarcaVela)) {
            return false;
        }
        BarcaVela bv = (BarcaVela) obj;                     // Perchè qui il "Downcasting"????
        return (this.altezzavela == bv.altezzavela && 
        this.larghezzavela == bv.larghezzavela && 
        this.eta == bv.eta);
    }


    // metodi Getter&Setter
    public double getAltezzavela() {
        return this.altezzavela;
    }
    public void setAltezzavela(double altezzavela) {
        this.altezzavela = altezzavela;
    }
    public double getLarghezzavela() {
        return this.larghezzavela;
    }
    public void setLarghezzavela(double larghezzavela) {
        this.larghezzavela = larghezzavela;
    }
    public int getEta() {
        return this.eta;
    }
    public void setEta(int eta) {
        this.eta = eta;
    }

    
}