/*  Un gommone inoltre possiede un numero posti ed un flag che permette di capire se dotato di motore o meno.   */

package verifiche20.v07maggio20.noleggiobarche; // modificato percorso package 29.05.20
import java.util.*;

public class Gommone extends Barche {

    protected int numeroPosti;
    private boolean conMotore;
    private String tempM;



    public Gommone() {
    }


    public Gommone(int lunghezza, int larghezza, int costoOrario, int valoreBarca, String nomeBarca, int numeroPosti, boolean conMotore) {
        super(lunghezza, larghezza, costoOrario, valoreBarca, nomeBarca);
        this.numeroPosti = numeroPosti;
        this.conMotore = conMotore;
    }

    public String stampaInfo(){                                           // 1° errore, era: public String stampaInfoString()
        if(conMotore) tempM = "con motore"; else tempM = "senza motore";
        return super.stampaInfo()+ "| numeroPosti: " + numeroPosti+" e"+tempM;
    }


   /*  public boolean equals(Object o) {                        // metodo equals dell'esercizio consegnato
        if (o == this)
            return true;
        if (!(o instanceof Gommone)) {
            return false;
        }
        Gommone gommone = (Gommone) o;
        return numeroPosti == gommone.numeroPosti && conMotore == gommone.conMotore && Objects.equals(tempM, gommone.tempM);
    } */

    public boolean equals(Object obj) {                             // metodo equals rifatto
        if (!super.equals(obj))                                     // se ...??? se cosa???
            return false;
        if (!(obj instanceof Gommone)) {
            return false;
        }
        Gommone gm = (Gommone) obj;
        return (this.numeroPosti == gm.numeroPosti && 
        this.conMotore == gm.conMotore);
    }




    // metodi Getter&Setter
    public int getNumeroPosti() {
        return this.numeroPosti;
    }
    public void setNumeroPosti(int numeroPosti) {
        this.numeroPosti = numeroPosti;
    }
    public boolean isConMotore() {
        return this.conMotore;
    }
    public boolean getConMotore() {
        return this.conMotore;
    }
    public void setConMotore(boolean conMotore) {
        this.conMotore = conMotore;
    }


    
}