/*
Bergamasco Daniele IV As - Verifica maggio 2020:  Esercizio 1 - 28.05.2020  [4 punti]

Per conto di una compagnia di noleggio imbarcazioni si intende realizzare una gerarchia di classi per rappresentare e gestire un parco barche composto da gommoni, barche a vela e yacht. 
Le caratteristiche in comune a tutte e 3 le tipologie di imbarcazioni sono lunghezza in metri, larghezza in metri, costo all’ora per il noleggio e descrizione ed il valore in euro. 
Un gommone inoltre possiede un numero posti ed un flag che permette di capire se dotato di motore o meno. 
Le barche a vela, oltre alle caratteristiche in comune, sono caratterizzate da altezza e larghezza della vela ed età minima per poterle noleggiare. 
Gli yacht, infine, sono caratterizzati dal numero di cavalli del motore, dal numero di posti letto e data di iscrizione all’elenco nazionale degli yacht. 
Scrivere il codice java necessario per l’implementazione di tale gerarchia fornendo per tutte le classi i relativi costruttori che permettono di settare tutte le caratteristiche (anche di quelle in comune) ed i metodi che permettono di visualizzare/modificare le caratteristiche citate. 
Inoltre per ogni classe sovrascrivere un metodo stampaInfo che permette di visualizzare la stampa di tutte le caratteristiche della classe ed un metodo isEqual che accetta in input un oggetto della stessa classe e restituisce vero o falso a seconda del fatto che i valori di tutti gli attributi dell’oggetto passato come parametro siano uguali a quelli dell’oggetto chiamante. [30 min]

.      */

package verifiche20.v07maggio20.noleggiobarche; // modificato percorso package 29.05.20

public class MainNoleggio {
    public static void main(String[] args) {

        System.out.println("\n ***** Esercizio 1/2 - 28.05.2020  [4 punti] ****** \n");             // riga aggiunta 29.05.20

        // Barche br = new Barche(10, 2, 10, 5000, "Imbarcadero");                  // non serve inizializzare la superclasse

        Gommone gm = new Gommone(5, 2, 6, 1000, "ElGommon", 4, false);
        Gommone gmm = new Gommone(3, 1, 4, 900, "ElGommin", 2, true);               // modificati valori (erano uguali a gm)
        Gommone gmmm = new Gommone(9, 3, 8, 1200, "ElGomMax", 5, true);                 // aggiunto terzo gommone per equals
        Gommone gmmmclone = new Gommone(9, 3, 8, 1200, "ElGomMax", 5, true);                        // aggiunto clone gommone

        BarcaVela bv = new BarcaVela(7, 3, 20, 20000, "ElVelier", 4, false, 4, 2, 8);   
        BarcaVela bv1 = new BarcaVela(4, 1, 15, 12000, "ElVelin", 3, false, 3, 1, 6);   // aggiunte varianti per fare equals
        BarcaVela bv2 = new BarcaVela(12, 5, 35, 34000, "ElVelaz", 6, false, 7, 3, 16);

        // forse non ci andavano i parametri della vela ...
        Yacht yt = new Yacht(30, 9, 50, 100000, "Riccon", 20, true, 20, 10, 2, 35, 18, "2020"); // + varianti per equals     
        Yacht yt1 = new Yacht(20, 6, 40, 80000, "MancoRiccon", 10, true, 5, 3, 25, 45, 8, "2020");
        Yacht yt2 = new Yacht(40, 15, 200, 450000, "SuperRiccon", 25, true, 20, 11, 25, 150, 25, "2020");


/*                              **** stampa come da esercizio consegnato **** 
        System.out.println(br.stampaInfo());                        // 2° errore, era: System.out.println(xx.toString());
        System.out.println();
        System.out.println(gm.stampaInfo());
        System.out.println(gmm.stampaInfo());
        System.out.println();
        System.out.println(bv.stampaInfo());
        System.out.println(yt.stampaInfo());
        System.out.println();
                               *********************************************** 
*/
        Barche [] br = new Barche[9];               // aggiunta dichiarazione di array di Barche (per poi stamparle)
        br[0] = gm;
        br[1] = gmm;
        br[2] = gmmm;
        br[3] = bv;
        br[4] = bv1;
        br[5] = bv2;
        br[6] = yt;
        br[7] = yt1;
        br[8] = yt2;

        for(int i =0; i<br.length; i++)                                         // aggiunto ciclo for per stampa imbarcazioni
        System.out.println(br[i].stampaInfo());

        System.out.println();

        // System.out.println(br.equals(br1));                                      // commentato perche' tolto anche sopra
        System.out.println("equals gm/gmmm: "+gm.equals(gmmm));                     // aggiunto testo di intro
        System.out.println("equals gmm/gm: "+gmm.equals(gm));
        System.out.println("equals gmmm/gmmmclone: "+gmmm.equals(gmmmclone));       // dovrebbe darmi "true"
        System.out.println("equals bv/bv1: "+bv.equals(bv1));
        System.out.println("equals bv/bv2: "+bv.equals(bv2));
        System.out.println("equals bv1/bv2: "+bv1.equals(bv2));
    }
}