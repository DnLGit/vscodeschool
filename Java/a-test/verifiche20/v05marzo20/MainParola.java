/*
Bergamasco Daniele IV As - Verifica Informatica 26.03.2020

5. [1,5 punti] Scrivere un programma in Java che permetta all’utente di inserire da tastiera una parola, dopo di che si dovrà stampare la parola al contrario usando i metodi delle stringhe. [10 min]  */

package verifiche20.v05marzo20;
import java.util.Scanner;


public class MainParola {

    public static void main(String[] args) {

        System.out.println("\n******** Main Classe Parola ********\n");

        Scanner ins = new Scanner(System.in);

        String parola = ins.nextLine();

        System.out.println("\nstampa la stringa diritta: " +parola);

        System.out.print("\nstampa Reverse con subs: ");
        for(int i=0; i<parola.length(); i++){                           // correzioni da casa 27.03.2020
            System.out.print(parola.substring((parola.length()-1-i), parola.length()-i)+" ");// soluz. con substring 
        }
        System.out.print("\nstampa Reverse con char: ");
        for(int i=0; i<parola.length(); i++){                           // correzioni da casa 27.03.2020
            System.out.print(parola.charAt(parola.length()-1-i)+" ");   // con parola.charAt funziona, ma vale in verifica??
        }
        System.out.println("\n");
  

    }        
}