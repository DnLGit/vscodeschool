package verifiche20.v06profmarzo20;


public class Canzone {

    //variabili d’istanza
    private String titolo;
    private String autore;
    private int annoLancio;

    public Canzone(){}                   // mancava il costruttore di default

    
//costruttori
    public Canzone(String titolo, String autore, int annoLancio){
    this.titolo = titolo;
    this.autore = autore;
    this.annoLancio = annoLancio;
    }
//metodi
    public String stampa(){
    return "Titolo: "+this.titolo+", autore: "+this.autore+", anno di lancio: "+this.annoLancio;   // mancava questo ultimo ;
    }

    public String compareAnno(Canzone altraCanzone){
    if (this.annoLancio > altraCanzone.annoLancio)
    return "La canzone "+this.titolo+" è più recente di "+altraCanzone.titolo;
    else if (this.annoLancio < altraCanzone.annoLancio)
    return "La canzone "+this.titolo+" è meno recente di "+altraCanzone.titolo;
    else
    return "le canzoni sono dello stesso anno";
    }

//metodi get set
    public void setTitolo(String titolo){
    this.titolo = titolo;
    }
    public String getTitolo(){
    return this.titolo;
    }
    public void setAutore(String autore){
    this.autore = autore;
    }
    public String getAutore(){
    return this.autore;
    }
    public void setAnnoLancio(int annoLancio){
    this.annoLancio = annoLancio;
    }
    public int getAnnoLancio(){
    return this.annoLancio;
    }

    
}