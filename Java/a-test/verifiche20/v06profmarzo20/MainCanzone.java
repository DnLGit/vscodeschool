package verifiche20.v06profmarzo20;
import java.util.*;



public class MainCanzone {
    public static void main(String[] args) {
        
    Canzone canz1 = new Canzone("canzone 1","autore 1",2010);
    Canzone canz2 = new Canzone();
    canz2.setTitolo("canzone2");
    canz2.setAutore("autore2");
    canz2.setAnnoLancio(2000);

    System.out.println(canz1.stampa());
    System.out.println("Autore: "+canz2.getAutore());
    System.out.println("Titolo: "+canz2.getTitolo());

    System.out.println("Anno di lancio: "+canz2.getAnnoLancio());   // mancava il get: era canz2.AnnoLancio()
    System.out.println(canz1.compareAnno(canz2));
    }
}