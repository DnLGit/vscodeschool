/*
Bergamasco Daniele IV As - Verifica Informatica 26.03.2020

1. [2,5 punti] Scrivere una classe Java chiamata Canzone i cui oggetti rappresentano canzoni. 
Ogni canzone deve avere le seguenti caratteristiche: titolo, autore, anno di lancio. 
Tale classe dovrà avere i seguenti metodi: 
- costruttore per inizializzare le caratteristiche della canzone 
- i metodi getter e setter per tutte le caratteristiche 
- il metodo stampa che restituisce una stringa con tutti i dati dell’oggetto su cui è invocato 
- il metodo compareAnnoLancio che permette di confrontare l’anno di lancio di una canzone con un’altra.  */

package verifiche20.v01marzo20;


public class Canzone {

    private String titolo, autore;
    private int anno;

    public Canzone (String titolo, String autore, int anno){
        this.titolo = titolo;
        this.autore = autore;
        this.anno = anno;
    }
    public Canzone(Canzone copyCanzone){
        this.titolo = copyCanzone.titolo;
        this.autore = copyCanzone.autore;
        this.anno = copyCanzone.anno;
    }

    public String getTitolo() {
        return this.titolo;
    }
    public String getAutore() {
        return this.autore;
    }
    public int getAnno() {
        return this.anno;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }
    public void setAutore(String autore) {
        this.autore = autore;
    }
    public void setAnno(int anno) {
        this.anno = anno;
    }


    public String stampa(){
        return "Titolo: "+this.titolo+" | Autore: "+autore+" | Anno: "+anno;
    }

    public boolean compareAnnolancio(Canzone copyCanzone){        // comparazione fra i vari anni di lancio.
        return this.anno < copyCanzone.anno;
    }
}