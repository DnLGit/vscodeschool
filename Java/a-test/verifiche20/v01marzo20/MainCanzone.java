/*
Bergamasco Daniele IV As - Verifica Informatica 26.03.2020

2. [1,5 punti] Scrivere una classe contenente un main che permette di utilizzare la classe dell’esercizio precedente, tramite tale classe si dovranno testare tutti i metodi implementati. [15 min]  */

package verifiche20.v01marzo20;


public class MainCanzone {

    public static void main(String[] args) {

        System.out.println("\n******** Main Classe Canzone ********\n");

        Canzone brano1 = new Canzone("Senza Parole", "Vasco Rossi", 1999);
        Canzone brano2 = new Canzone("Siamo solo Noi", "Vasco Rossi", 1981);
        
        System.out.println(brano1.getTitolo());
        System.out.println(brano2.getAutore());

        brano1.setTitolo("Ogni Volta");
        brano1.setAnno(1981);

        System.out.println(brano1.stampa());
        System.out.println(brano2.stampa());

        if(brano1.compareAnnolancio(brano2))                            
            System.out.println("\nquesta canzone "+brano1.getAutore()+" viene prima");
        else if(brano2.compareAnnolancio(brano1))
        System.out.println("\nquesta canzone "+brano2.getAutore()+" viene dopo");

        else
        System.out.println("\nle due canzoni sono dello stesso anno");


       
    }
}