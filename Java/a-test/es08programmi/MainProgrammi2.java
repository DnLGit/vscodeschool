/*

Bergamasco Daniele IV As - Esercizio 4:  Programmi con classi e costruttori/copia  11.02.2020

Definire in linguaggio Java una classe che rappresenti programmi per computer. 
Ogni oggetto deve avere i seguenti attributi: denominazione, produttore, versione, sistema operativo, anno. 
La classe deve avere i seguenti metodi e costruttori:
- costruttore che ha come parametri "denominazione, produttore, versione, sistema operativo, anno";
- costruttore di copia;
- "getDenominazione, getProduttore, getVersione, getSistema" e "getAnno" che restituiscono i valori degli attributi relativi;
- "setDenominazione, setProduttore, setVersione, setSistema" e "setAnno" che modificano i valori degli attributi relativi;
- toString che restituisce una stringa con tutti i dati dell'oggetto su cui è invocato;
- compareAnno che consente di confrontare l'anno di rilascio del programma con l'anno di rilascio di un altro programma.
Scrivere un main per testare la correttezza.

 */
/*  *******************   Attenzione >>> da implementare!!!!   *************        */

package es08programmi;

public class MainProgrammi2 {
    public static void main(String[] args) {

    System.out.println("\n******** Classe di un Programma ********\n");

    Programmi2 soft = new Programmi2();
    Programmi2 soft1 = new Programmi2(soft);

    System.out.println("E' stato rilasciato il software "+soft.getDenominazione()+" nel "+soft.getAnno()+"\n");

    soft.toStringa();      // il metodo di riferimento funziona ma è "public void" e non Stringa e non ritorna nulla, stampa!

    System.out.println("\n2) Il software ha le seguenti caratteristiche: "+ soft.toString());

    soft1.setAnno(2020);

    System.out.println(soft1.compareAnno2(soft1));
    

    }

}