/**

Bergamasco Daniele IV As - Esercizio 4:  Programmi con classi e costruttori/copia  11.02.2020

 */

package es08programmi;
 
public class Programmi2 {

// variabili d'istanza:
    private String denominazione, produttore, versione, sistemaOperativo;
    private int anno;

// costruttori
    public Programmi2(){
        this.denominazione = "DomoTech"; this.produttore = "BiMar"; 
        this.versione = "0.1.18"; this.sistemaOperativo = "Win10"; 
        this.anno = 2018;
    }
 
    public Programmi2(Programmi2 newPro1){
                
        this.denominazione = newPro1.denominazione;
        this.produttore = newPro1.produttore;
        this.versione = newPro1.versione;
        this.sistemaOperativo = newPro1.sistemaOperativo;
        this.anno = newPro1.anno;
    }

/* Metodi   */

    // Metodi Getter:
    public String getDenominazione(){
        return denominazione; 
    }
    public String getProduttore(){
        return produttore;
    }
    public String getVersione(){
        return versione;
    }
    public String getSistemaOperativo(){
        return sistemaOperativo;
    }
    public int getAnno(){
        return anno;
    }

    // Metodi Setter:
    public void setDenominazione(String denominazione){
        this.denominazione = denominazione; 
    }
    public void setProduttore(String produttore){
        this.produttore = produttore;
    }
    public void setVersione(String versione){
        this.versione = versione;
    }
    public void setSistemaOperativo(String sistemaOperativo){
        this.sistemaOperativo = sistemaOperativo;
    }
    public void setAnno(int anno){
        this.anno = anno;
    }

    // Metodi altri:
    public void toStringa(){                                        // stampa nel main tutti i dati
        System.out.println("1) L'oggetto ha i seguenti attributi: ");
        System.out.println("Denominazione: "+this.denominazione+"; Prodotto da: "+this.produttore+"; ");
        System.out.print("nella versione: "+this.versione+"; su Sistema Operativo: "+this.sistemaOperativo);
        System.out.println(" nell'anno: "+this.anno);
    }

    public String toString(){
        return  "nome: "+ denominazione +" Prodotto da: "+ produttore + " con versione " + versione 
        + " su Os " + sistemaOperativo + " rilasciato nell'anno "+ anno;

    }

 
    public void compareAnno(Programmi2 newPro1){         // sembra non esserci errori ma non so come invocare questo metodo
        if(this.anno > newPro1.anno) 
        System.out.println("kO");
        else
        if(this.anno < newPro1.anno) 
        System.out.println("Ok");
    }

    
    public String compareAnno2(Programmi2 newPro1){  // qui mi indica errori di "Tipo"
        if(this.anno > newPro1.anno)                // This method must return a result of type String
            return "Test kO";
        else
            return "Test Ok";
    }


}

