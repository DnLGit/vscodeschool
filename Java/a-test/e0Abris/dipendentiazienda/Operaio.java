package e0Abris.dipendentiazienda;
//package dipendentiazienda;

public class Operaio extends Dipendente {
	//Variabili d'istanza
	protected String sector;
	private int workedHours;
	private double hoursCost;
	
	//Costruttori
	public Operaio() {
		
	}	
	public Operaio(String id, String name, String surname, String sector, int workedHours) {
		super(id,name,surname);
		this.sector = sector;
		this.workedHours = workedHours;
		this.hoursCost = 7.50;
		this.setSalary();
	}
	
	//Metodi
	public String toString() {
		String temp;
		temp = super.toString();
		temp += "\nSettore: "+sector+"\nOre annue lavorate: "+this.workedHours+"\nRetribuzione orarira: "+this.hoursCost;
		temp += "\nStipendio mensile: "+this.salary/12+"\nStipendio annuo: "+this.salary;
		return temp;
	}
	public void setSalary() {
		this.salary = this.workedHours*this.hoursCost;
	}
	
	public void saluto() {
		System.out.println("Ciao sono un Operaio");
	}
	
	//Metodi get Set
	public void setWorkedHours(int workedHours) {
		this.workedHours = workedHours;
		this.setSalary();
		
	}
	public int getWorkedHours() {
		return this.workedHours;
	}
	
	public void setHoursCost(int hoursCost) {
		this.hoursCost = hoursCost;
		this.setSalary();
		
	}
	public double getHoursCost() {
		return this.hoursCost;
	}

}
