package e0Abris.dipendentiazienda;
//package dipendentiazienda;

public class Azienda {
	//variabili d'istanza
	protected Dipendente [] dipendenti;
	
	//costruttore
	public Azienda(Dipendente[] arrayDip) {
		this.dipendenti = arrayDip;
	}
	
	//metodi
	public void ElencoNomiDip() {
		System.out.println("--------Elenco nomi dipendenti----------");
		
		try {
			for (int i=0;i<dipendenti.length;i++) 
				System.out.println("nome: "+dipendenti[i].name);
				
		}
		catch (NullPointerException ex){
				System.out.println(">dato mancante..");
		}
		catch (ArrayIndexOutOfBoundsException ex) {
				System.out.println(">fuori indice..");
		}
		
		System.out.println("-----------------FINE-------------------\n\n");
		
	}
	
	public void AggiungiElemento(Dipendente dipendente, int pos) {
		this.dipendenti[pos] = dipendente;
	}
	
	public void EliminaElemento(int pos) {
		this.dipendenti[pos] = null;
	}
	
	
}
