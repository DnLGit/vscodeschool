package e0Abris.dipendentiazienda;
//package dipendentiazienda;;

public class Dirigente extends Impiegato {
	//Variabili d'istanza
	protected String targaAuto;
	
	
	//Costruttori
	public Dirigente() {
		
	}
	public Dirigente(String id, String name, String surname, int officeNumber, int phoneNumber,String targaAuto) {
		super(id,name,surname,officeNumber, phoneNumber);
		this.targaAuto = targaAuto;
		this.setSalary();
	}
	
	
	//Metodi
	public String toString() {
		String temp;
		temp = super.toString();
		temp += "\nAuto aziendale: "+targaAuto;
		return temp;
	}
	
	public void setSalary() {
		this.salary = 4000;
	}
	
	public void saluto() {
		System.out.println("Ciao sono un Dirigente");
	}
}
