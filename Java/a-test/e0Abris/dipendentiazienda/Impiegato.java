package e0Abris.dipendentiazienda;
//package dipendentiazienda;;

public class Impiegato extends Dipendente{
	//Variabili d'istanza
	protected int officeNumber;
	protected int phoneNumber;
	
	//Costruttori
	public Impiegato() {
		
	}
	
	public Impiegato(String id, String name, String surname, int officeNumber, int phoneNumber) {
		super(id,name,surname);
		this.phoneNumber = phoneNumber;
		this.officeNumber = officeNumber;
		this.setSalary();
	}
	
	//Metodi
	public String toString() {
		String temp;
		temp = super.toString();
		temp += "\nUfficio numero: "+this.officeNumber+"\nTelefono: "+this.phoneNumber;
		temp += "\nStipendio mensile: "+this.salary+"\nStipendio annuo: "+this.salary*12;
		return temp;
	}
	
	public boolean equals(Object ogg) {
		
		if(!super.equals(ogg)) return false;
			//System.out.println("equals...primo if");
		if(!(ogg instanceof Impiegato)) return false;
			//System.out.println("equals...secondo if");
		Impiegato imp = (Impiegato)ogg;
			//System.out.println(imp.officeNumber+" "+imp.phoneNumber);
		
		return (
				(this.officeNumber == imp.officeNumber) &&
				(this.phoneNumber == imp.phoneNumber)
				
				);
		
	}
	
	public void setSalary() {
		this.salary = 1800;
	}
	
	public void saluto() {
		System.out.println("Ciao sono un Impiegato");
	}
	
	//Get Set
	public int getPhoneNumber() {
		return this.phoneNumber;
	}
	
	public int getOfficeNumber() {
		return this.officeNumber;
	}
}
