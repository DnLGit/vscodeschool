package e0Abris.dipendentiazienda;
//package dipendentiazienda;

public class Dipendente {
	
	//Variabili d'istanza
	protected String id;
	protected String name;
	protected String surname;
	protected double salary;
	
	
	
	//Costruttori
	public Dipendente() {
		
	}	
	public Dipendente(String id, String name, String surname) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.salary = 0;
	}
	
	//Metodi
	public String toString() {
		 
		return "\nID: "+this.id+"\nNome: "+this.name+"\nCognome: "+this.surname;
	}
	
	public boolean equals(Object oggetto) {
		if (oggetto == null) return false;
		if (!(oggetto instanceof Dipendente)) return false;
		Dipendente dip = (Dipendente) oggetto;
		return (
				this.id.equalsIgnoreCase(dip.id) &&
				this.name.equalsIgnoreCase(dip.name) &&
				this.surname.equalsIgnoreCase(dip.surname) &&
				this.salary == dip.salary
			
				);
	}
	
	public void saluto() {
		System.out.println("Ciao sono un Dipendente");
	}
	
	//Get Set
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getSurname() {
		return this.surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	

}
