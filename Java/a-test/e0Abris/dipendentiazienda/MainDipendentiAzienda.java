/*	esercizio n° 33 pag.10 dispensa esercizi java - Esercizio su dipendenti azienda con EREDITARIETA’	*/

package e0Abris.dipendentiazienda;
//package dipendentiazienda;;

public class MainDipendentiAzienda {

	public static void main(String[] args) {
		
		Dipendente dip1 = new Dipendente("ID1", "Andrea", "Brisotto");
		//System.out.println(dip1.toString());
		
		Operaio operaio = new Operaio("ID2","Mario", "Bianchi","settore A",1800);
		//System.out.println(operaio.toString());
		
		Impiegato imp = new Impiegato("ID3", "Elia", "Brisotto", 12, 36668557);
		//System.out.println(imp.toString());
		
		Dirigente dir = new Dirigente("ID4", "Angelica", "Serra", 12, 36668557, "AT567XD");
		//System.out.println(dir.toString));
		
		
		
		
		//******Upcasting
		System.out.println("\n\n------------------- Upcasting  --------------------\n");
		Dipendente[] azienda = new Dipendente[4]; //Azienda è un array di dipendenti e accetta tutte le sottoclassi
		azienda[0] = dip1;
		azienda[1] = operaio;
		azienda[2] = imp;
		azienda[3] = dir;
		
		Azienda brisotto = new Azienda(azienda);
		brisotto.ElencoNomiDip();
		
		Operaio ope2 = new Operaio("ID5","Serra", "Susy","Insegnante",1600);
		Dirigente dir2 = new Dirigente("ID6", "Mario", "Mascia", 1, 36668, "Multipla");
		
		brisotto.EliminaElemento(2);
		brisotto.ElencoNomiDip();
		brisotto.AggiungiElemento(ope2, 2);
		brisotto.ElencoNomiDip();
		
		Dirigente diri = new Dirigente(); //istanzio un sottoclasse
		Dipendente dipe = (Dipendente) diri; //la inserisco in un riferimento alla sopraclasse
		
		dipe.saluto();
		
		System.out.println("\n\n--------------------  Downcasting  --------------------\n");
		//((Dirigente)imp).saluto(); //non doveva usare il metodo di dipendente?
		//*******downcast
		//Impiegato impx = (Impiegato)dip1; //ma non funziona
		
		//impx.saluto();
		
		//creo riferimento a superclasse e istanzio sottoclasse
		Dipendente dipx = new Impiegato("xxx", "Prova", "Downcasting", 12, 36668557);
		
		//creo riferimento sottoclasse e assegno l'oggetto precedente esplicitando il downcasting(cioè alla class a cui deve arrivare)
		Impiegato impx = (Impiegato) dipx; //
		
		dipx.saluto();
		impx.saluto();
		impx.getOfficeNumber();
		//dipx.getOfficeNumber();
		
		
		
		
		
		
		//******toString
		System.out.println("\n\n-------------------- toString  --------------------\n");
		for (int i = 0; i<azienda.length;i++) System.out.println(brisotto.dipendenti[i].toString());
		//******instance of
		System.out.println("\n\n-------------------  Instanceof  --------------------\n");
		System.out.println("dip1 è un'istanza di Dipendente? "+ ((dip1 instanceof Dipendente)? "SI" : "NO"));
		System.out.println("dip1 è un'istanza di Impiegato? "+ ((dip1 instanceof Impiegato)? "SI" : "NO"));
		System.out.println("imp è unn'istanza di Dipendente? "+ ((imp instanceof Dipendente)? "SI" : "NO"));
		System.out.println("imp è unn'istanza di Dirigente? "+ ((imp instanceof Dirigente)? "SI" : "NO"));
		System.out.println("dir2 è unn'istanza di Dirigente? "+ ((dir2 instanceof Dirigente)? "SI" : "NO"));
		System.out.println("dir2 è unn'istanza di Dipendente? "+ ((dir2 instanceof Dipendente)? "SI" : "NO"));
		System.out.println("dir2 è unn'istanza di Impiegato? "+ ((dir2 instanceof Impiegato)? "SI" : "NO"));
		
		//*******equals
		System.out.println("\n\n-------------------- equals  --------------------\n");
		Dipendente uno = new Dipendente("01", "Andrea", "Brisotto");
		Dipendente due = new Dipendente("02", "Susy", "Serra");
		Dipendente tre = new Dipendente("01", "Andrea", "Brisotto");
		Impiegato quattro = new Impiegato("01", "Andrea", "Brisotto",488112,6854432);
		Impiegato cinque = new Impiegato("01", "Andrea", "Brisotto",488112,6854432);
		Impiegato sei = new Impiegato("01", "Elia", "Brisotto",000000,00000);
		
		System.out.println(uno.getName()+" è uguale a "+due.getName()+"? : "+((uno.equals(due))? "SI":"NO"));
		System.out.println(uno.getName()+" è uguale a "+tre.getName()+"? : "+((uno.equals(tre))? "SI":"NO"));
		
		System.out.println(quattro.getName()+" è uguale a "+uno.getName()+"? : "+((quattro.equals(uno))? "SI":"NO"));
		System.out.println(quattro.getName()+" è uguale a "+cinque.getName()+"? : "+((quattro.equals(cinque))? "SI":"NO"));
		System.out.println(quattro.getName()+" è uguale a "+sei.getName()+"? : "+((quattro.equals(sei))? "SI":"NO"));
		
	}

}
