//package e0Abris.programmandbris;
package e0Abris.programmandbris;


public class EsercizioProgrammi {

	public static void main(String[] args) {


		Programma prog1 = new Programma ("Word","Microsoft","10.5","Windows",2018);
		System.out.println("originale:\t"+prog1.toString());
		Programma prog2 = new Programma(prog1);
		System.out.println("copia:\t\t"+prog2.toString());
		prog2.setVersione("13");
		System.out.println("copia:\t\t"+prog2.toString());
		System.out.println(prog1.compareAnno(prog2));
		prog2.setAnno(2000);
		System.out.println(prog1.compareAnno(prog2));
		

	}

}
