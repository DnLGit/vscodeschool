//package e0Abris.programmandbris;
package e0Abris.programmandbris;

public class Programma {
	
	//Variabili d'istanza
	private String denominazione;
	private String produttore;
	private String versione;
	private String sistemaOperativo;
	private int anno;
	
	
	//Costruttori
	public Programma(String denominazione, String produttore, String versione, String sistemaOperativo, int anno) {
		this.denominazione = denominazione;
		this.produttore = produttore;
		this.versione = versione;
		this.sistemaOperativo = sistemaOperativo;
		this.anno = anno;
	}
	public Programma (Programma altroProgramma) {
		this.denominazione = altroProgramma.denominazione;
		this.produttore = altroProgramma.produttore;
		this.versione = altroProgramma.versione;
		this.sistemaOperativo = altroProgramma.sistemaOperativo;
		this.anno = altroProgramma.anno;
				
	}
	
	//Metodi
	
	public String toString() {
		return denominazione+" "+produttore+" "+versione+" "+sistemaOperativo+" "+anno;
	}
	
	public String compareAnno(Programma altroProg) {
		if (this.anno == altroProg.anno) return "Stesso anno";
		else return "Anno diverso";

	}
	
	
	//Metodi Get e Set
	public String getDenominazione() {
		return denominazione;
	}
	public void setDenominazione(String denominazione) {
		this.denominazione = denominazione;
	}
	public String getProduttore() {
		return produttore;
	}
	public void setProduttore(String produttore) {
		this.produttore = produttore;
	}
	public String getVersione() {
		return versione;
	}
	public void setVersione(String versione) {
		this.versione = versione;
	}
	public String getSistemaOp() {
		return sistemaOperativo;
	}
	public void setSistemaOp(String sistemaOperativo) {
		this.sistemaOperativo=sistemaOperativo;
	}
	public int getAnno() {
		return anno;
	}
	public void setAnno(int anno) {
		this.anno = anno;
	}
}
