package e0Abris.interfacce_2;

public class Somma extends Numeri implements Operazione {
	
	public double operazione(double n1,double n2) {
		return n1+n2;
	}
	
	public Somma (double n1, double n2) {
		super(n1,n2);
	}

	@Override
	public void stampa() {
		System.out.println(n1+" + "+n2+"= "+this.operazione(this.n1, this.n2));
		
	}
	
	
}
