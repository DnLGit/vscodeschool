package e0Abris.interfacce_2;

public abstract class Numeri {
	
	protected double n1;
	protected double n2;
	
	public Numeri (double n1, double n2) {
		this.n1 = n1;
		this.n2 = n2;
	}
	
	public abstract void stampa();
	
	

}
