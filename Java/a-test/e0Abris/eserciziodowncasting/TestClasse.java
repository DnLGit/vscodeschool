package e0Abris.eserciziodowncasting;
//package eserciziodowncasting;

public class TestClasse {

    private int valore;
    private String parola;


    public void stampami(){
        System.out.println("parola e': "+parola+valore+" volte");
    }
    /*  ****** esercizio "personalizzato con Andrea via Skype 22.05.20 ****** */
    public boolean equals(Object roba){         // metodo che accetta un oggetto "Object" di nome "roba".
        if(roba == null)                        // controllo se l'oggetto "roba" e' nullo [qiuindi non sarà istanziato!]  
            return false;                       // e chiaramente mi torna un false
        if(!(roba instanceof TestClasse))       // controllo che l'oggetto "roba" non sia un'istanza di questa classe [ ! ]
            return false;                       // e chiaramente mi torna un false
        TestClasse cla = (TestClasse)roba;      // Downcasting: da Object (roba) alla classe (TestClasse) 
        return(this.valore == cla.valore && this.parola.equals(cla.parola));    // confronto delle variabili se sono perfettamente uguali.

    }


    public int getValore() {
        return this.valore;
    }
    public void setValore(int valore) {
        this.valore = valore;
    }
    public String getParola() {
        return this.parola;
    }
    public void setParola(String parola) {
        this.parola = parola;
    }

    
}