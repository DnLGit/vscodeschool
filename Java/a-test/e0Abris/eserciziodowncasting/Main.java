/*  Esercizio Spiegazione downcasting con Andrea - 22.05.2020 */

package e0Abris.eserciziodowncasting;
//package eserciziodowncasting;

public class Main {
    public static void main(String[] args) {

        TestClasse cl = new TestClasse();
        cl.stampami();

        TestClasse c2;

        cl.setParola("eccola");
        cl.setValore(10);

        cl.stampami();
        

        
    }

    
}