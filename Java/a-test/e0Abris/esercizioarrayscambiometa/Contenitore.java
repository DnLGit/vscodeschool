package e0Abris.esercizioarrayscambiometa;
//package esercizioarrayscambiometa;

import java.util.Arrays;

public class Contenitore {
	//variabili d'istanza
	private int[] contenitore;
	
	//costruttori
	public Contenitore() {
		int[] temp = {2,3,4,7,8,9};
		this.contenitore = Arrays.copyOf(temp, temp.length);
	}
	public Contenitore (int[] contenitore) {
		this.contenitore = Arrays.copyOf(contenitore, contenitore.length);
	}
	
	//Metodi
	public void scambiaMeta() {
		// trovo la quantita di elementi da spostare sia per la prima che per la seconda meta
		int elementi = contenitore.length/2;
		//se il numero elementi � pari l'indice �uguale alla meta altrimenti l'indice � incrementati di uno
		//cos� il numer centrale non viene toccato
		int indice;
		if (contenitore.length%2==0) indice = elementi;
		else indice = elementi+1;
		//ciclo per spostare gli elementi
		//creo un arrray temporaneo che contiene la seconda meta
		int []temp = Arrays.copyOfRange(contenitore, indice, contenitore.length);
		for (int i = 0;i<elementi;i++) {		
			this.contenitore[indice+i] = this.contenitore[i]; //metto il vaolre della prima meta nella seconda meta
			this.contenitore[i] = temp[i];//metto prima meta i valori dell seconda contenuti nell'array temporaneo
		}
	}
	
	public void visualizzaContenitore() {
		System.out.println("\n");
		for(int temp:this.contenitore) {
			System.out.print(temp+" ");
		}
		System.out.println("\n");
	}
	
	//Get Set
	public int[] getContenitore(){
		return this.contenitore;
	}
	public void setContenitore(int[] contenitore){
		this.contenitore = Arrays.copyOf(contenitore, contenitore.length);
	}

}
