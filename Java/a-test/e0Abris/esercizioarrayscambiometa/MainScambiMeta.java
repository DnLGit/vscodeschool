package e0Abris.esercizioarrayscambiometa;
//package esercizioarrayscambiometa;

public class MainScambiMeta {

	public static void main(String[] args) {
		
		Contenitore box = new Contenitore();
		box.visualizzaContenitore();
		box.scambiaMeta();
		box.visualizzaContenitore();
		System.out.println("**************************");
		int [] temp = {1,2,3,9,3,2,1,9};
		Contenitore bx2 = new Contenitore(temp);
		bx2.visualizzaContenitore();
		bx2.scambiaMeta();
		bx2.visualizzaContenitore();

	}

}
