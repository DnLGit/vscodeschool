package e0Abris.dipendentiscuola;



public class Docente extends Persona{
	//Variabili d'istanza
	
	protected int oreDocenza;
	protected double stipendio;
	
	//Costruttori
	public Docente() {
		
	}
	
	public Docente(String nome, String cognome, String sex, int anno, int mese, int giorno, int oreDocenza, double stipendio) {
		super(nome,cognome,sex,anno,mese,giorno);
		this.oreDocenza = oreDocenza;
		this.stipendio = stipendio;
		
		
	}
	
	//Metodi
	public boolean equals(Object oggetto) {
		if(!super.equals(oggetto)) return false;
		if(!(oggetto instanceof Docente)) return false;
		
		Docente doc = (Docente) oggetto;
		
		return (
				this.oreDocenza == doc.oreDocenza &&
				this.stipendio == doc.stipendio
				);
	}
	
	public String toString() {
		String temp = super.toString();
		temp += "\nOre docenza: "+oreDocenza+"\nStipendio: "+stipendio;
		return temp;
	}
	
	

}
