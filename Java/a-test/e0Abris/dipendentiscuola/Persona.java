package e0Abris.dipendentiscuola;
import java.text.SimpleDateFormat;
import java.util.*;

public class Persona {
	//Variabili d'istanza
	protected String nome;
	protected String cognome;
	protected String sex;
	protected GregorianCalendar dataNascita;
	
	//Costruttori
	public Persona() {
		
	}
	
	public Persona(String nome, String cognome, String sex, int anno, int mese, int giorno) {
		this.nome = nome;
		this.cognome = cognome;
		this.sex = sex;
		this.dataNascita = new GregorianCalendar(anno, mese-1, giorno);
	}
	//Metodi
	public boolean equals(Object ogg) {
		if (ogg == null) return false;
		//System.out.println("if null");
		if (!(ogg instanceof Persona)) return false;
		//System.out.println("if instanceof");
		Persona p = (Persona) ogg;
		boolean data = false;
		if (this.dataNascita.compareTo(p.dataNascita) == 0) data = true;
		return (
				this.cognome.equalsIgnoreCase(p.cognome) &&
				this.nome.equalsIgnoreCase(p.nome) &&
				this.sex.equalsIgnoreCase(p.sex) && 
				data
				
				);
	}
	
	public String toString() {
		String temp;
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		String dataN = formato.format(dataNascita.getTime());
		temp = "\nNome: "+nome+"\nCognome: "+cognome+"\nSesso: "+sex+"\nData: "+dataN;			
		return temp;
	}
	//Get Set

}
