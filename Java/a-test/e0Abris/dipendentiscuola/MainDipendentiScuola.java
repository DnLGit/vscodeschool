/*	esercizio n° 32 pag.10 dispensa esercizi java - Esercizio su gestione dipendenti scuola su EREDITARIETA’	*/
package e0Abris.dipendentiscuola;
//import java.util.*;


public class MainDipendentiScuola {

	public static void main(String[] args) {
		Persona pers1 = new Persona("Andrea", "Brisotto", "M", 1975, 2, 18);
		System.out.println(pers1.toString());
		
		Docente susy = new Docente("Susy", "Serra", "F", 1972, 3,4,200,1300);
		System.out.println(susy.toString());
		
		Impiegato imp1 = new Impiegato("Pinco", "Pallino", "Dubbio", 1985, 5, 24, 1, 600);
		System.out.println(imp1.toString());
		
		ImpiegatoExtra imp2 = new ImpiegatoExtra("Pinca","Pallina","F",1815,5,15,2,700,10);
		System.out.println(imp2.toString());
		
		// >>>>>> EQUALS <<<<<<<
		Persona pers2 = new Persona("Andrea", "Brisotto", "M", 1975, 2, 18);
		System.out.println(pers2.toString());
		
		System.out.println(pers1.equals(pers2));
		
		Docente maria = new Docente("Susy", "Serra", "F", 1972, 3,4,200,1300);
		System.out.println(maria.toString());
		System.out.println(maria.equals(susy));
		
		// >>>>>>> UPCASTING <<<<<<<
		
		Persona ix = new ImpiegatoExtra("Mr","Upcast","M",1915,5,15,2,700,10);
		
		System.out.println(ix.toString());
		
		// >>>>>>> DOWNCAST <<<<<<<<
		
		Persona impx2 = new Impiegato("Miss", "DownCast", "mah...", 1985, 5, 24, 1, 600);
		
		Impiegato docx2 = (Impiegato) impx2;
		
		System.out.println(docx2.toString());
		
		System.out.println("******* ARRAY");
		
		Persona [] tutti = new Persona [8];
		try{
			tutti[0] = pers1;
			tutti[1] = susy;
			tutti[2] = imp1;
			tutti[3] = imp2;
			tutti[4] = pers2;
			tutti[5] = maria;
			tutti[6] = ix;
			tutti[7] = impx2;
			tutti[8] = docx2;
		}
		catch(ArrayIndexOutOfBoundsException ex) {
			System.out.println(">>>>>Limite array superato...");
		}
		System.out.println("******* ARRAY stampa toString");
		try {
			for(int i = 0; i<tutti.length; i++) {
				System.out.println(tutti[i].toString());
			}
		}
		
		catch(ArrayIndexOutOfBoundsException ex) {
			System.out.println(">>>>>Limite array superato...");
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
