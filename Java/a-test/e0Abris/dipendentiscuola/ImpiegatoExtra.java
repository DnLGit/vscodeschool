package e0Abris.dipendentiscuola;

public class ImpiegatoExtra extends Impiegato{
	//Variabili d'istanza

	protected int oreExtra;
	protected double retribOra;
	
	//Costruttori
	public ImpiegatoExtra() {
		
	}
	
	public ImpiegatoExtra(String nome, String cognome, String sex, int anno, int mese, int giorno, int livello, double mensile, int oreExtra) {
		super(nome,cognome,sex,anno,mese,giorno,livello,mensile);
		this.oreExtra = oreExtra;
		this.retribOra = 7.50;
		this.integraStipendio();
		
		
	}
	
	//Metodi
	public void integraStipendio() {
		double integrazione = this.oreExtra*this.retribOra;
		this.mensile += integrazione;
	}
	
	public String toString() {
		String temp = super.toString();
		temp += "\nOre straordinarie: "+this.oreExtra+"\nRetribuzione oraria: "+retribOra;
		return temp;
	}
}
