package e0Abris.dipendentiscuola;

public class Impiegato extends Persona{
	//Variabili d'istanza
	
	protected int livello;
	protected double mensile;
		
	//Costruttori
	public Impiegato() {
		
	}
	
	public Impiegato(String nome, String cognome, String sex, int anno, int mese, int giorno, int livello, double mensile) {
		super(nome,cognome,sex,anno,mese,giorno);
		this.livello = livello;
		this.mensile = mensile;
		
		
	}
	
	//Metodi
	public String toString() {
		String temp = super.toString();
		temp += "\nLivello: "+livello+"\nStipendio: "+mensile;
		return temp;
	}

}
