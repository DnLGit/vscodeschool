package e0Abris.esercizioarray2universita;
//package esercizioarray2universita;

public class Docente {
	// variabili d'istanza
	private String nome;
	private String cognome;
	private int codice;
	private int eta;
	//costruttori
	public Docente() {
		
	}
	public Docente(int codice) {
		this.codice = codice;
		
	}
	public Docente(String nome, String cognome,int codice, int eta) {
		this.nome = nome;
		this.cognome = cognome;
		this.codice = codice;
		this.eta = eta;
	}
	
	//Metodi
	
	//Metodi Get Set
	public String getNome() {
		return this.nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return this.cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public int getCodice() {
		return this.codice;
	}
	public void setCodice(int codice) {
		this.codice = codice;
	}
	public int getEta() {
		return this.eta;
	}
	public void setEta(int eta) {
		this.eta = eta;
	}
}
