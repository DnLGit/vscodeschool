package e0Abris.esercizioarray2universita;
//package esercizioarray2universita;
import java.util.*;

public class EsercizioArrayUniversita {

	public static void main(String[] args) {
		int numDoc;
		String strTmp;
		int intTmp;
		Docente arrayDoc[];

		Scanner in = new Scanner(System.in);
		System.out.print("Numero docenti: ");
		numDoc = Integer.parseInt(in.nextLine()) ;
		
		
		arrayDoc = new Docente[numDoc];
		for(int i=0; i<arrayDoc.length; i++) {
			arrayDoc[i] = new Docente();
			System.out.print((i+1)+"nome: ");
			strTmp = in.nextLine();
			arrayDoc[i].setNome(strTmp);
			System.out.print((i+1)+"cognome: ");
			strTmp = in.nextLine();
			arrayDoc[i].setCognome(strTmp);
			System.out.print((i+1)+"codice: ");
			intTmp = Integer.parseInt(in.nextLine());
			arrayDoc[i].setCodice(intTmp);
			System.out.print((i+1)+"eta: ");
			intTmp = Integer.parseInt(in.nextLine());
			arrayDoc[i].setEta(intTmp);
		}
		
		Universita uniPadova = new Universita(arrayDoc);
		
		for(int i=0; i<arrayDoc.length; i++) {
			System.out.println(arrayDoc[i].getNome());
			
		}
		
		System.out.println("Eta minima: "+uniPadova.etaMinima()+" eta media: "+uniPadova.getEtaMedia());
		in.close();

	}

}
