package e0Abris.esercizioarray2universita;
//package esercizioarray2universita;

public class Universita {
	//Variabili d'idtanza
	private Docente arrayDocenti[];
	
	//Costruttore
	public Universita() {
		
	}
	public Universita(Docente arrayDocenti[]) {
		this.arrayDocenti = arrayDocenti;
	}
	
	//Metodi
	public float getEtaMedia() {
		float media=0;
		for(int i=0;i<this.arrayDocenti.length;i++) {
			media += this.arrayDocenti[i].getEta();
		}
		media /= 2;
		return media;
	}
	public int etaMinima() {
		int etaMin = this.arrayDocenti[0].getEta();
		for(int i=0;i<this.arrayDocenti.length;i++) {
			if(etaMin > this.arrayDocenti[i].getEta()) etaMin = this.arrayDocenti[i].getEta();
		}
		return etaMin;
	}
	
	
	//Metodi Get Set

}
