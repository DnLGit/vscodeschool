package e0Abris.interfacce_1;

public class Cubo implements Operazione {
	
	//Variabili d'istanza
	private int numero;
	
	// costruttori
	public Cubo(int numero) {
		this.numero = numero;
	}
	
	//metodi
	public int operazione() {
		return numero*numero*numero;
	}
	
	//Getter e Setter
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}

}
