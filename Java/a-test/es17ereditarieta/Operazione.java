/*   Overloading/Override --> 07.04.2020  Video 9a timeliner alle 19:35 circa   */

package es17ereditarieta;

public class Operazione {

    private int x, y;
    private double a, b;

    /* public int somma(double c, int y){
        return c + y;    // (return x+y) mi da errore:  [ Type mismatch: cannot convert from double to int ]
    } */    
    
    /**
     * 
     * @param x
     * @param y
     * @return prodotto int (metodo scritto nella superclasse "Operazione")
     */
    public int prodotto(int x, int y){
        return x * y;
    }

    /**
     * 
     * @param a
     * @param b
     * @return prodotto double (metodo scritto nella superclasse "Operazione")
     */
    public int prodotto(double a, double b){   // Overloading metodo prodotto: non solo si può effettuare sulla stessa classe
        return x * y;                           // ... ma anche sulla classe "linkata ..."!!
    }
    
    /**
     * 
     * @param x
     * @param y
     * @return somma standard (metodo scritto nella superclasse "Operazione")
     */
    public int somma(int x, int y){     // da provare override nella sottoclasse
        return x + y;
    }

    /**
     * 
     * @param c
     * @return potenza con double (metodo scritto nella superclasse "Operazione")
     */
    public double potenza(double c){
        return c*c;
    }    
}