/*  Videolezione del 07.04.2020   (time 2:30 - video 9a con VLC)  */

package es17ereditarieta;

public class MainOperazione {
    
    public static void main(String[] args) {
        
        Operazione o = new Operazione();        // istanzazione della superclasse
        ExOperazione eo = new ExOperazione();   // istanzazione delle sottoclasse
        

        System.out.println();                                                               // solo per ottenere uno spazio.

        System.out.println("medoto prodotto superclasse " + o.prodotto(5, 6));
        System.out.println("overloading metodo prodotto superclasse " + o.prodotto(6, 7));  // Risulta 42

        System.out.println("metodo somma superclasse " + o.somma(5, 6));                     // Risulta 11
        System.out.println();
        System.out.println("metodo somma sottoclasse (override) " + eo.somma(5, 6));         // Risulta 22        
        
        System.out.println("overloading metodo prodotto sottoclasse " + eo.prodotto(8, 3));  // Risulta 24
    }
}