
package es17ereditarieta;

public class ExOperazione extends Operazione{       // Importante: pe linkare questa classe bisogna "estenderla"!!!

    private int x, y;
    private double c, d;

    //in questa sottoclasse possiamo utilizzare sia l'Overloading che l'Override del metodo somma della classe "Operazioni":

    /* public int somma(double a, int x){     // partiamo dall'Overloading.
        return a + x;                         // (return a+x) mi da errore:  Type mismatch: cannot convert from double to int
    } */

  
    /**
     * 
     * @param a
     * @param b
     * @return prodotto double doppio (metodo scritto nella superclasse "Operazione")
     */
    public int prodotto(double c, double d){  // Override metodo prodotto (nella sottoclasse)
        return (x * y)*2;                        
    }

    /**
     * 
     * @param x
     * @param y
     * @return somma standard modificato!!!! (metodo scritto nella superclasse "Operazione")
     */
    public int somma(int x, int y){         // override metodo "somma" che ritorna "il doppio" rispetto ...
        return (x+y)*2;                     // ... il metodo della superclasse.
    }


    /**
     * 
     * @param x
     * @return potenza con int (metodo scritto nella classe "ExOperazione")
     */
    public int potenza(int x){      // overloading del metodo "potenza" della superclasse
        return x*x;
    }

    public static void main(String[] args) {    // per testare creo un main [ lo posso creare anche in una sottoclasse? si ]
    
    }
}