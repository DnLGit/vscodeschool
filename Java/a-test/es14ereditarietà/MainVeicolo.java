/*
Esempio concetto di Ereditarietà  [pag. 44 dispensa aggiornata al 23.04.20 ]
Vediamo un esempio:               [Videolezione time 38:53 del 02.04.20 ]
Consideriamo la classe Veicolo:
*/

package es14ereditarietà;

public class MainVeicolo {
    public static void main(String[] args) {

        /*  Nel momento in cui istanziamo la sottoclasse Automobile nel main 
            in pratica chiamiamo anche il costruttore di default della super classe. */

            Automobile a = new Automobile();    // istanza sottoclasse Automobile.
            
        /*  [vengono richiamati entrambi i costruttori di default, con metodi e attributi:
            sia della sottoclasse "Automobile" che anche della superclasse "Veicolo"]  */
            

        /*  Se istanzio invece la superclasse "Veicolo" sarà visibile solo ciò che c'è in quella classe.    */
            Veicolo v = new Veicolo();          // istanza superclasse Veicolo.
        /*  [viene richiamato solo il costruttore di default di se stesso, con metodi e attributi:
             quindi solo della superoclasse "Veicolo" ]  */
        
            

        
    }

}