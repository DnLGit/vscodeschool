package es14ereditarietà;

public class Veicolo {

    private double velocità;        // queste due variabili "private" sono visibili solo all'interno della clasee!!
    private double accelerazione;

    public double getVelocità(){    // questi due metodi "public" sono visibili anche nella sottoclasse!!
        return velocità;

    }

    public double getAccelarazione(){
        return accelerazione;
    }

}