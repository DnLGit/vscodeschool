package es14ereditarietà;

public class Automobile extends Veicolo{        // sottoclasse Automobile --> estesa alla superclasse Veicolo

    private boolean avviata;                    // variabile booleana che mi dirà se il veicolo è avviato o non ...

    public void avvia(){
        //this.                 // con this. vedo entrambi i metodi della superclasse Veicolo ma non le variabili "private".
        
    }

}