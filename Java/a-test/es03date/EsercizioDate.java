/*
@author daniele.bergamasco -> LabJava:   20.02.2020  IV/V ora
Esercizio su input da tastiera e stringhe e date (n° 01 a pag. 6 dispensa esercizi Java ... probabile j19stringdate)

Scrivere un programma Java che chieda in input due date in formato dd/mm/AAAA salvandole in due variabili di tipo string. 
Estrarre anno mese e giorno per entrambe e darle in input per la creazione di due date con la classe GregorianCalendar. 
Poi aggiungere alla prima data un anno, sottrarre alla seconda data un mese. 
E poi stampare la differenza in numero di giorni tra le due date. Verificare quale data è antecedente. 
Verificare se la seconda data inserita è antecedente a quella attuale, qualora lo fosse stampare il numero di giorni trascorsi da quella data fino ad oggi.
Stampare le due date in formato yyyy/mm/dd.
 */
package es03date;

//import java.util.GregorianCalendar;                     // importare questa classe per utilizzare GregorianCalendar
import java.util.*;                                       // così importo tutte le classi "until" ... anche GregorianCalendar
//import java.text.SimpleDateFormat;                      // importare questa classe per utilizzare SimpleDateFormat
import java.text.*;                                       // così importo tutte le classi "until" ... anche SimpleDateFormat

public class EsercizioDate {

    /*  Esempio Scanner:                                // per provare il metodo scanner
        Scanner input = new Scanner(System.in);
        System.out.println("Introduci nome e cognome:");
        String s = input.nextLine();
        System.out.println("Ho letto: " + s);   */
   
    public static void main(String[] args) {

        System.out.println("\n\n***** Esercizi con Stringhe e Date *****");
    /*
        Scanner inserisci = new Scanner(System.in);                 // Warning [Resource leak: 'inserisci' is never closed]
        System.out.print("\nInserisci una prima data: ");
        String data1 = inserisci.nextLine();
        System.out.print("\nInserisci la seconda data: ");
        String data2 = inserisci.nextLine();
        inserisci.close();                                          // con questa riga di chiusura risolvo il warning sopra
        
        System.out.println("\nTest variabili data1: "+data1+" e data2: "+data2);        // verifico le variabili se corrette
    */
        String data1 = "20.02.2020";        // variabili temporanee al posto di inserimento da tastiera
        String data2 = "25.02.2020";
        
                
        String data1gg = data1.substring(0,2);                                          // estraggo la substringa del giorno
        int data1Gg = Integer.parseInt(data1gg);                                        // converto stringa in intero
        String data1mm = data1.substring(3,5);                                          // estraggo la substringa del mese
        int data1Mm = Integer.parseInt(data1mm);                                        // converto stringa in intero
        String data1yy = data1.substring(6,10);                                          // estraggo la substringa dell'anno
        int data1Yy = Integer.parseInt(data1yy);                                        // converto stringa in intero

        System.out.println("\nprova stampa giorno mese e anno data1: "+data1gg+"/"+data1mm+"/"+data1yy);
        
        String data2gg = data2.substring(0,2);                                // come sopra estraggo la substringa del giorno
        int data2Gg = Integer.parseInt(data2gg);                              // come sopra converto stringa in intero
        String data2mm = data2.substring(3,5);                                // come sopra estraggo la substringa del mese
        int data2Mm = Integer.parseInt(data2mm);                              // come sopra converto stringa in intero
        String data2yy = data2.substring(6,10);                                // come sopra estraggo la substringa dell'anno
        int data2Yy = Integer.parseInt(data2yy);                              // come sopra converto stringa in intero
        System.out.println("\nprova stampa giorno mese e anno data2: "+data2gg+"/"+data2mm+"/"+data2yy);

        System.out.println("\nprova stampa giorno mese e anno data1 Int: "+data1Gg+"/"+data1Mm+"/"+data1Yy);
        System.out.println("\nprova stampa giorno mese e anno data2 Int: "+data2Gg+"/"+data2Mm+"/"+data2Yy);

        /*  Oggetti di GregorianCalendar    */
        GregorianCalendar dataAttuale=new GregorianCalendar();      // oggetto "dataAttuale" della classe "GregorianCalendar"

        GregorianCalendar date1=new GregorianCalendar(data1Yy, data1Mm-1, data1Gg);
        GregorianCalendar date2=new GregorianCalendar(data2Yy, data2Mm-1, data2Gg);
        GregorianCalendar aaaa=new GregorianCalendar(2020, 0, 31);
        GregorianCalendar bbbb=new GregorianCalendar(2020, 1, 29);
        GregorianCalendar cccc=new GregorianCalendar(2020, 2, 31);
        GregorianCalendar dddd=new GregorianCalendar(2020, 3, 30);

 

        /* proviamo a verificare le informazioni dell'oggetto "dataAttuale" */

        // utilizzo del Metodo "get" di "GregorianCalendar"
        int yearNow = dataAttuale.get(GregorianCalendar.YEAR);
        int monthNow = dataAttuale.get(GregorianCalendar.MONTH)+1;
        int dayNow = dataAttuale.get(GregorianCalendar.DATE);
        int hourNow = dataAttuale.get(GregorianCalendar.HOUR_OF_DAY);
        int minuteNow = dataAttuale.get(GregorianCalendar.MINUTE);
        int secondNow = dataAttuale.get(GregorianCalendar.SECOND);

        // Prova stampa data e ora corrente.
        System.out.print("\nProva stampa data attuale, oggi è: ");
        System.out.println(dayNow+"."+monthNow+"."+yearNow+" ore: "+hourNow+":"+minuteNow+":"+secondNow);

        // Prova stampa conversioni date da Stringhe a interi
        System.out.println("\n(data1gg) giovedì scorso era il: "+data1Gg+"-"+data1Mm+"-"+data1Yy);
        System.out.println("(data2gg) mentre carnevale finì il: "+data2Gg+"-"+data2Mm+"-"+data2Yy);

         /* Utilizzo dei Metodi di GregorianCalendar    */
        //  add e getTimeMillis

        date1.add(GregorianCalendar.YEAR, +0);                          //.add ->           aggiungo  1 anno alla data1
        date2.add(GregorianCalendar.MONTH, -1);                         //.add ->           sottraggo 1 mese alla data1
        long ms1 = date1.getTimeInMillis();                             //.getTimeMillis -> determina in ms data1
        long ms2 = date2.getTimeInMillis();                             //.getTimeMillis -> determina in ms data2
        long diff = ms1-ms2;                                            // sommatoria fra le due date in ms
        long diffDay = diff/(24*60*60*1000);                            // conversione dei ms in giorni

        System.out.print("\nDopo le modifiche la differenza fra le due date è di "+diffDay+" giorni.");


        //  Metodi before, after e equals

        if( date1.after(date2) )
        System.out.println("\ne la prima data precede la seconda!");
        else if( date1.equals(date2) )
        System.out.println("\n e le date sono uguali!");
        else
        System.out.println("\ne la seconda data precede la prima!!!");


        System.out.print("\nAl confronto con ora risulta che: ");
        if( date2.before(dataAttuale) ){
            long ms3 = dataAttuale.getTimeInMillis();                              //.getTimeMillis -> determina in ms data1
            long diff3 = ms3-ms2;                                                  // sommatoria fra le due date in ms
            long diffDay3 = diff3/(24*60*60*1000);                                 // conversione dei ms in giorni
            System.out.println("La seconda data precede quella attuale di "+diffDay3+" giorni!!!");
        }
        
        else if( date2.equals(dataAttuale) )
        System.out.println("Le date sono uguali!");
        else
        System.out.println("La seconda data è al futuro!!!");

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println(sdf.format(date1.getTime()));
        System.out.println(sdf.format(date2.getTime()));
        System.out.println(sdf.format(aaaa.getTime()));
        System.out.println(sdf.format(bbbb.getTime()));
        System.out.println(sdf.format(cccc.getTime()));
        System.out.println(sdf.format(dddd.getTime()));
    }
}
