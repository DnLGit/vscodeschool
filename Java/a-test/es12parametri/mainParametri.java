/*  
Esercizio esempio Parametri Formali e Attuali Java     */

package es12parametri;


public class mainParametri {

    public static void main(String[] args) {

        System.out.println("\n**** prova Parametri Formali e Attuali ****\n\n");

        Parametri par = new Parametri(5, 15);


        System.out.println("L'addizione è: "+par.addizione());

        System.out.println("la somma è: "+par.somma(5, 8));

        System.out.println("\n"); 
        

    }
}