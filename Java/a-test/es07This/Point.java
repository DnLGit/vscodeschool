/*  Parola chiave this (es. pag. 15 dispensa Java) - 2° caso    */

package es07This;


public class Point {

    //  Altro esempio:
    public int x = 0; 
    public int y = 0;

   /*  public Point(int a, int b) {
        x = a;
        y = b; 
    } */

    //  Possiamo scriverla anche così:
    //  constructor 
    public Point(int x, int y) {
        this.x = x; this.y = y;
    }

    /*  In questo secondo caso, l'uso di this è necessario nel corpo del costruttore per distinguere i membri di istanza
        nominati 'x' e 'y' dalle variabili locali del costruttore, nominate allo stesso modo.   */
}