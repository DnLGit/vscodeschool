/*  Parola chiave this (pag. 14 dispensa Java)
 Il riferimento this in Java viene utilizzato per fare riferimento, all’interno di un metodo o di un costruttore, agli attributi o metodi locali. Questo tipo di riferimento non fa altro che puntare all’oggetto a cui appartiene risolvendo possibili problemi di ambiguità. Vediamo subito un esempio:
*/

package es07This;


public class MainThis {
    public static void main(String[] args) {

        System.out.println("\n******* Prove con la parola chiave This *******\n");
        System.out.println("\nProve stampa Classe PersonaA: \n");

        // Avendo il seguente codice (classe PersonaA):

        PersonaA pa = new PersonaA("Giustino", "Verdicchio");

    /*  verrà chiamato il costruttore con due parametri in ingresso, il quale a sua volta chiamerà il costruttore 
        con un solo parametro, che infine chiamerà il costruttore senza parametri e stamperà: 
        "Sono il costruttore di default" */

        PersonaA pa2 = new PersonaA();
        PersonaA pa3 = new PersonaA("Faustino");

        
        System.out.println("\n");
        System.out.println("\nProve stampa Classe PersonaB: \n");

        // Sempre avendo il seguente codice (classe PersonaB):

        PersonaB pb = new PersonaB("Gatto", "Volpe");

        PersonaB pb2 = new PersonaB();
        PersonaB pb3 = new PersonaB("Pinocchio");


        // Invece per la classe Rectangle:
        System.out.println("\n");
        System.out.println("\nProve stampa Classe Rectangle: \n");

        Rectangle rec1 = new Rectangle();
        System.out.println("Il perimetro di rec1 è "+rec1.getPerimRect()+" e l'area è "+rec1.getAreaRect());

        Rectangle rec2 = new Rectangle(5, 4);
        System.out.println("Il perimetro di rec1 è "+rec2.getPerimRect()+" e l'area è "+rec2.getAreaRect());

        Rectangle rec3 = new Rectangle(15, 8, 6, 5);
        System.out.println("Il perimetro di rec1 è "+rec3.getPerimRect()+" e l'area è "+rec3.getAreaRect());



    }
}