/*  Parola chiave this (es. pag. 15 dispensa Java) - 3° caso    */

package es07This;


public class Rectangle {

    //  Altro esempio:

    private int x, y; 
    private int width, height;

    

    public Rectangle() {                            // In pratica se istanziamo la classe con 0 parametri 
        this(0, 0, 0, 0);                           // invoca il costruttore passando tutti 0 come parametri.  
    }     
        
    public Rectangle(int width, int height) {   //  Se istanziamo la classe con 2 parametri invoca il costruttore
        this(0, 0, width, height);              //  con i 4 parametri dove abbiamo "0, 0, width, height"
    } 
    
    public Rectangle(int x, int y, int width, int height) { //  Se istanziamo la classe con 4 parametri invece
        this.x = x; this.y = y;                             //  si hanno i semplici aggiornamenti
        this.width = width; this.height = height; 
    } 


/*  In pratica se istanziamo la classe con 0 parametri invoca il costruttore passando tutti 0 come parametri.
    Se istanziamo la classe con 2 parametri allora invoca il costruttore con i 4 parametri dove abbiamo sequenzialmente 0,0,larghezza ed altezza. 
    Se istanziamo la classe con 4 parametri invece si hanno i semplici aggiornamenti. 
    Se dentro il corpo di un costruttore di una classe X si vuole eseguire un'invocazione esplicita ad un altro costruttore di X, tale invocazione deve comparire come prima istruzione nel corpo del costruttore. 
    La motivazione? Secondo voi?   */


    // [DnL] inseriamo dei metodi da richiamare nel main:

    // qui Getter&Setter

    public int getX() {
        return this.x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return this.y;
    }
    public void setY(int y) {
        this.y = y;
    }
    public int getWidth() {
        return this.width;
    }
    public void setWidth(int width) {
        this.width = width;
    }
    public int getHeight() {
        return this.height;
    }
    public void setHeight(int height) {
        this.height = height;
    }


    public int getPerimRect(){
        return 2*(width+height);
    }
    public int getAreaRect(){
        return (width*height);
    }
}