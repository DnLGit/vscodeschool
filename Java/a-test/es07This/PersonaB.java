/*  Parola chiave this (pag. 14 dispensa Java) - 1° caso variante   */

package es07This;


public class PersonaB {

    /*
    This può essere usato anche per richiamare i costruttori in altri costruttori, vediamo un esempio:
    */
    
    private String nome;
    private String cognome;

    public PersonaB() {
        System.out.println("Sono il costruttore di default."); 
    } 
    
    public PersonaB(String nome) {                          // Chiama il costruttore senza parametri
        this();                                              
        this.nome=nome;
        System.out.println(nome); 
    } 
    
    public PersonaB(String nome, String cognome) {          // Chiama il costruttore con un parametro
        //this(nome);                                          
        this.cognome=nome;   this.cognome=cognome;
        System.out.println(nome+"&"+cognome);
    }
}