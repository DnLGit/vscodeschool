/*  Parola chiave this (pag. 14 dispensa Java) - 1° caso    */

package es07This;


public class PersonaA {

    private String nome1;                            // variabile d'istanza

    /*
    In questo caso non possiamo chiamare il parametro di setNome con “nome” 
    perchè già c’è la variabile di istanza nome     */
    public void setNome1(String nome1_pers){      // 
        nome1 = nome1_pers; 
    }

    //  invece con il riferimento this potremo scrivere:
    private String nome2; 
    public void setNome2(String nome2) {
        this.nome2=nome2; 
    }

    /*
    This può essere usato anche per richiamare i costruttori in altri costruttori, vediamo un esempio:
    */
    private String nome;
    private String cognome;

    public PersonaA() {                                                   // costruttore di default
        System.out.println("Sono il costruttore di default."); 
    } 
    
    public PersonaA(String nome) {                                        // costruttore a 1 parametro
        this();                                                           // this che chiama il costruttore senza parametri 
        this.nome=nome; 
    } 
    
    public PersonaA(String nome, String cognome) {                        // costruttore a 2 parametro
        this(nome);                                                       // this che chiama il costruttore con un parametro 
        this.cognome=cognome; 
    }
}