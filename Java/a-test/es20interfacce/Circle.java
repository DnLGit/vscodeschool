package es20interfacce;

public class Circle extends Shape  {          // classe Circle estesa dalla classe astratta Shape [copiata da esercizio es18]

    public double rad;                              // variabile d'istanza (raggio del cerchio)

    public Circle(double rad){                      // costruttore
        this.rad = rad;
    }

    public double area(){                           // implementazione metodo "area" della classe astratta
        return Math.PI*rad*rad;                     // area cerchio: pigrecoErreQuadro ...
    }
    public double perimeter(){                      // implementazione metodo "perimetro" della classe astratta
        return Math.PI*2*rad;                       // perimetro cerchio: duePigrecoErre ....
    }

    public double getRadius(){
        return rad;
    }

}