package es20interfacce;

public class DrawableRectangle extends Rectangle implements Drawable{           // sottoclasse dell'interfaccia "Drawable"

    private int color; 
    private double x, y;

    public DrawableRectangle(double width, double height){              // Costruttore che richiama però anche "Rectangle"
        super(width, height); 
    }

                /*  ******  Contratto fra Interfaccia e la classe che la Implementa  ****** */
/* Le classi che implementano l'interfaccia "devono" implementare "tutti" i metodi dell'interfaccia!!  */
/* Se non implemento uno solo dei metodi [per esempio public void setColor] mi da questo errore qui sotto: */
/* The type DrawableRectangle must implement the inherited abstract method Drawable.setColor(int) */
    
    public void setColor(int color) {                           // se "commento" questo metodo ricevo l'errore sopra.   
        this.color = color; 
    } 
    public void setPosition(double x, double y){
        this.x = x; this.y = y;
    } 
    
    public void draw() {

    }
}