package es20interfacce;

public abstract class Shape {                       // classe astratta "Shape" [copiata da esercizio es18]

    protected int test;
    
    public abstract double area();                  // metodi astratti
    public abstract double perimeter();
    
    
    protected String getInfo(){                     // metodo standard
        return "Test";
    }
    
}