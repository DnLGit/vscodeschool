package es20interfacce;             // Interfaccia:  i metodi verranno implementati dalle sottoclassi 

public interface Drawable {

    public void setColor(int color);                    // dichiarazione di metodi "senza" implementazione!!
    public void setPosition(double x, double y); 
    public void draw();
}