/*  
Limiti delle classi astratte ed Interfacce. in Java [ pag. 62 dispensa]
Tornando all’esempio delle figure geometriche, immaginiamo ora di voler ampliare il nostro lavoro. 
Vogliamo implementare forme geometriche che possono anche essere disegnate sullo schermo (quindi devono avere caratteristiche come il valore sull’asse delle x e sull’asse delle y ed un possibile colore per colorarle). 
Potremmo definire una classe astratta DrawableShape da cui far estendere DrawableCircle e DrawableRectangle e quindi queste ultime due dovrebbero estendere anche rispettivamente da Circle e Rectangle per poter sfruttare i metodi area e perimetro. Quindi dovremmo trovarci di fronte ad una situazione del genere:
    - class DrawableCircle extends Circle, DrawableShape.
    - class DrawableRectangle extends Rectangle, DrawableShape.
Tale operazione non è possibile! Java non supporta l’ereditarietà multipla(come in questo caso).
Questo è un problema: DrawableCircle non può discendere contemporaneamente da Circle e da DrawableShape.
Come si risolve il problema? Si usano le Interfacce   */

package es20interfacce;

public class MainDraw {
    public static void main(String[] args) {

        DrawableCircle dc1 = new DrawableCircle(2);

        System.out.println(dc1.area());
 
        
    }
    
}