/*  Le classi astratte in Java [ pag. 61 dispensa] - Esercizio svolto su classi astratte
Costruiamo la classe astratta Solido che contiene come variabile il pesoSpecifico
e tre metodi:
- uno che calcola la superficie del solido
- uno che calcola il volume
- uno che calcola il peso.
NB: Ps= P/V. Quindi costruiamo la classe Cubo e la classe Sfera estensione della classe astratta Solido.    */

package es19abstractsolid;

public class MainSolid {
    public static void main(String[] args) {

        System.out.println("\n****************** Le classi astratte in Java - pag 61 ******************\n");

        Sfera sf1 = new Sfera(20, 1.2);
        Sfera sf2 = new Sfera(30, 1.3);

        Cubo cb1 = new Cubo(20, 1.2);
        Cubo cb2 = new Cubo(30, 1.3);

        System.out.println("La prima sfera pesa kg "+sf1.peso());           // invocato il metodo "comune" del peso
        System.out.println("La seconda pesa kg "+sf2.peso());
        System.out.println("Il primo cubo pesa kg "+cb1.peso());
        System.out.println("Mentre il secondo pesa kg "+cb2.peso());
        System.out.println();

        Solido[] solids = new Solido[4];                                    // dichiarazione array di solidi
        solids[0] = new Sfera(25, 1.2);                                     // creazione oggetti Sfera
        solids[1] = new Sfera(35, 1.3);
        solids[2] = new Cubo(25, 1.2);                                      // creazione oggetti Cubo
        solids[0] = new Cubo(35, 1.3);

        System.out.println("I dati sono: ");
        for(int i=0; i<solids.length; i++)
        System.out.println("\nSolido "+(i+1)+"\npeso: "+solids[i].peso()+"\nvolume: "+solids[i].volume()+"\nsuperficie: "+solids[i].superficie());
    }
}