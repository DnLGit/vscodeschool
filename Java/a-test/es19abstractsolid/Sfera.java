package es19abstractsolid;

public class Sfera extends Solido{                          // classe derivata dalla classe astratta Solido

    private double raggio;

    public Sfera(double raggio, double pesoSpecifico){      // costruttore
        this.raggio = raggio;
        this.pesoSpecifico = pesoSpecifico;
    }

    public double volume(){             // Math.pow è il metodo che calcola la potenza: primo parametro elevato il secondo
        return (4/3 * Math.PI*Math.pow(raggio, 3));         // formula volume sfera: (4pir^3)/3 - Math.PI vale 3.14(P greco)
    }

    public double superficie(){
        return (4*Math.PI*Math.pow(raggio, 2));             // formula superficie sfera: 4pir^2
    }
}