package es19abstractsolid;

public class Cubo extends Solido {                          // classe derivata dalla classe astratta Solido
    private double lato;

    public Cubo(double lato, double pesoSpecifico){         // costruttore
        this.lato = lato;
        this.pesoSpecifico = pesoSpecifico;
    }

    public double volume(){                                 // implementazioni dei metodi astratti 
        return lato*lato*lato;
    }
    public double superficie(){
        return 6*lato*lato;
    }
}