package es19abstractsolid;

public abstract class Solido {                  // classe astratta "Solido"

    protected double pesoSpecifico;             // variabile d'istanza

    protected double peso(){                    // metodo "NON" astratto [ottimo per entrambi le classi]  
        return volume()*pesoSpecifico;          // che mi calcola il peso ricavandolo dal prodotto di volume e ps
    }

    public abstract double volume();
    public abstract double superficie();
}