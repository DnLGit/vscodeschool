package es16protectd.pkg2; // Classe4test è in un altro package!!!!

import es16protectd.pkg1.Classe1;

public class Classe4Test {              // classe3Test " non estesa "!!!


/*  proviamo innanzitutto a testare un metodo per capire la visibilità di attributi  */
    public void get(){
       //Classe1 c1 = new Classe1();
        //c1.
        //this.

        /*  --> in questo caso non vediamo gli attributi di classe1 perchè manca
        sia " extends " alla Classe1 ma è necessaio anche " importare il secondo package pkg1" !! 
        --> vediamo esempio nella classe4 "  */ 

    }
}