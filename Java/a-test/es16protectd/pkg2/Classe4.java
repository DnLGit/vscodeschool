package es16protectd.pkg2; // Classe4 è in un altro package [ pkg2 ]!! Importiamo pkg1:

import es16protectd.pkg1.Classe1;

/*  testare ora un metodo per capire in questo caso la visibilità di attributi  */
public class Classe4 extends Classe1 {
    public void get(){
        //this.
/*  --> in questo caso vediamo gli attributi di classe1 perchè la classe4 è "estesa" alla Classe1
    ed è stato anche importartato il secondo package pkg2" !!   */ 

    }
}