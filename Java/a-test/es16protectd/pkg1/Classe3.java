package es16protectd.pkg1;

public class Classe3 extends Classe1 {  // creazione di una terza classe per testare l'Ereditarietà tramite "extends".

/*  supponiamo che si vogliono utilizzare attributi e metodi della Classe1 ... 
    creiamo un metodo con il quale richiamare i suoi attributi */
    public void get(){
        // this.
        // this.       --> tramite il " this. " accedo agli attrributi di classe1.

    }
}