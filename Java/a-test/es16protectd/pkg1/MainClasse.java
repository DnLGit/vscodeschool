/*
Accedere a metodi ed attributi della superclasse.
Modificatore "protected"  [pag. 45 dispensa aggiornata al 23.04.20 ]
Con un esempio chiariamo le idee, eseguiamo i seguenti passi [Videolezione 9b dalle 22:49 del 02.04.20 ]:

*/

package es16protectd.pkg1;       // pkg1 =  package 1 (poi se ne farà un altro)




public class MainClasse {
    public static void main(String[] args) {
        
        Classe1 c1 = new Classe1();         
        /*  richiamandole con " c1. " su questa istanza sono visibili le variabili public e protected   */
        //c1.
    }
}