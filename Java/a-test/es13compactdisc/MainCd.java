/*

Bergamasco Daniele IV As - Esercizio 4:  Programmi con classi e costruttori/copia  18.02.2020

Implementare in Java una classe CD, i cui oggetti rappresentano CD audio. 
Ogni oggetto CD deve avere almeno le seguenti caratteristiche: titolo, autore, numero brani, durata. 
La classe deve avere i seguenti metodi:
- costruttore che ha come parametri "titolo, autore, numeroBrani, durata";
- costruttore di copia;
- "getTitolo, getAutore, getNumeroBrani e getDurata" che restituiscono i valori degli attributi relativi;
- "setTitolo, setAutore, setNumeroBrani e setDurata" che modificano i valori degli attributi relativi;
- toString che restituisce una stringa con tutti i dati dell'oggetto su cui è invocato;
- compareDurata che consente di confrontare la durata complessiva del cd con la durata complessiva di un altro cd.
Scrivere un main per testare la correttezza.

 */

package es13compactdisc;
 
 public class MainCd {
     public static void main(String[] args) {

        Cd disc1 = new Cd("Angel Dust", "Faith No More", 22, 59);               // creazione primo oggetto
        Cd disc0 = new Cd(disc1);                                               // creazione secondo oggetto (copia)

        disc1.setAutore("Green Day");                                           // modifica valori tramite Setter
        disc1.setTitolo("Dookie");
        disc1.setNBrani(14);
        disc1.setDurata(38);

        System.out.println("\nCd di riferimento -> "+disc0.toString());   // Stampa valori oggetto 1 (cd dei "Faith No More")

        System.out.println("\nCd di riferimento -> "+disc1.toString());   // Stampa valori oggetto 1 (cd dei "Green Day")


        if(disc0.compareDurata(disc1))                                    // invocazione Metodo di comparazione tempi cd
            System.out.println("\nil cd dei "+disc1.getAutore()+" dura di pù");
        else
            System.out.println("\nil cd dei "+disc0.getAutore()+" dura di pù");
    }
}