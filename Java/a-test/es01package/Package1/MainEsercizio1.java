package es01package.Package1;               // forse meglio tutto minuscolo????   provapackage.package1;

import es01package.Package2.Esercizio2;     // prova importazione del package2

public class MainEsercizio1 {

    public static void main(String[] args) {
        
        Esercizio1 es1 = new Esercizio1();
        Esercizio2 es2 = new Esercizio2();

        System.out.println("\necco il main");
        System.out.println("ed ecco il valore "+es1.getValoreTest1());

        es1.setValoreTest1(50);

        System.out.println("ed ecco il valore adesso "+es1.getValoreTest1());

        System.out.println("ed ecco il valore dell'altro package "+es2.getValoreTest2());

        es2.setValoreTest2(100);

        System.out.println("e cambiando il valore dell'altro package "+es2.getValoreTest2());

        System.out.println("e duplicando il valore dell'altro package "+es2.getProdottoTest2());

        //System.out.println("e duplicando il valore del primo package?? "+es2.getProdottoTest1());  // errore: non si può!!!



    }

}