/*  java.util.Arrays 
    Java mette a disposizione la classe java.util.Arrays con numerosi algoritmi per operare sugli array:
     ricerca; 
     sorting (ordinamento); 
     copia; 
    e altri metodi per la manipolazione degli array

Esercizio esempio a pag. 32 della dispensa Java     */

package es05pag32;
import java.util.*;


public class Esempio1 {
    public static void main(String[] args) {

        int array [] = {2,5,7,8,2,1,8,4};       // array per i test: dichiarato ed inizializzato

/*  Ordinamento:  metodo Arrays.sort() */
        Arrays.sort(array);                 // ordina l'array in modo crescente
        for(int tmp:array)
            System.out.print(tmp+" ");      // stampo l'array con foreach

/*  Ricerca:  metodo Arrays.binarySearch() -> l'array DEVE PRIMA essere Ordinato*/
    System.out.println("\n");    
    System.out.println(Arrays.binarySearch(array, 9));  // l'elemento cercato non c'è: ritorno di un numero negativo (-7)

    System.out.println("\n");    
    System.out.println(Arrays.binarySearch(array, 5));  // l'elemento cercato 5 c'è: ritorno della posizione (3)

    System.out.println("\n");    
    System.out.println(Arrays.binarySearch(array, 2));  // l'elemento cercato 2 c'è: ritorno della posizione ultima (2)

/*  Copia:  metodo Arrays.copy() -> mettendo come dimensione un numero minore, l'array viene troncato*/
        /* int arrayCopia [] = Arrays.copyOf(array, 4);
            for(int tmp:arrayCopia)
                System.out.print(tmp+" "); */
        
        int arrayCopia [] = Arrays.copyOf(array, 7);    // con dimensione maggiore, inizializza a zero le celle aggiunte
            for(int tmp:arrayCopia)
                System.out.print(tmp+" ");


    
        

    }

    
}