package es05pag32;

public class Esempio2b {

    /*
        private int dim;             ---> Non serve la variabile d'istanza, la dimensione viene passata tramite il metodo.
    */

    /* 
        public Esempio2b(int dim){   ---> E Non ci va qui neanche il costruttore, non ho assegnazioni particolari
        this.dim = dim;
        } 
    */

    public int[] creaTabellaQuadrati (int n){               // Metodo per creare i quadrati di un array "v" di lunghezza "n"
        int[] v = new int[n];                       // array "v" iterato dal for sotto per produrre il quadrato di ogni cifra
        for (int i=0; i < v.length; i++){
            v[i] = i*i;
        }
        return v;                                       // ritorno dell'array dei quadrati
    }
}

