/*  Array e passaggio di parametri ai metodi
Nei parametri formali di un metodo di passa con le [], invece nei parametri attuali solo il nome.
Gli array Java possono essere restituiti come risultato di funzioni, come qualunque altro oggetto e vengono passati per riferimento, quindi qualsiasi modifica che effettuiamo ad un array passato come parametro si ripercuote anche quando è terminata la chiamata.(vediamo dettagli fra poco)

Esercizio esempio a pag. 32/33 della dispensa Java     */
/*  int creaTabellaQuadrati [] (int n)
            – crea un array di interi di opportuna dimensione
            – lo riempie con i quadrati richiesti
            – lo restituisce al chiamante    */ 

package es05pag32;

public class Esempio2a {    

    public static void main(String[] args) {

       // ???????????? //
       // int n = 5;

       System.out.println("\n **** Tabella Quadrati ***\n");

       int elevatoQ [] = creaTabellaQuadrati (5);

       for(int q:elevatoQ){
           System.out.print(q+"\t");
       }
       System.out.println("\n");
 
    }
    static int[] creaTabellaQuadrati (int n){
        int[] v = new int[n];
        for (int i=0; i < v.length; i++){
            v[i] = i*i;
        }
    return v;
    }
}