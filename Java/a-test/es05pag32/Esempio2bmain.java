/*  
Esercizio esempio a pag. 32/33 della dispensa Java     */
/*  int creaTabellaQuadrati [] (int n)
            – crea un array di interi di opportuna dimensione
            – lo riempie con i quadrati richiesti
            – lo restituisce al chiamante    */

package es05pag32;

public class Esempio2bmain {
    public static void main(String[] args) {

        System.out.println("\n **** Tabella Quadrati con 2 classi ***\n");

        Esempio2b es = new Esempio2b();             // Creo oggetto "es"

        for(int q : es.creaTabellaQuadrati(5))      // invoco il metodo creaTabellaQuadrati() inizializzando a 5 "n" 
            System.out.print(q+"\t");               // all'interno di un "foreach" che lo stamperà.

        System.out.println("\n");
        
    }
}














/*
Array e passaggio di parametri ai metodi
Nei parametri formali di un metodo di passa con le [], invece nei parametri attuali solo il nome.
Gli array Java possono essere restituiti come risultato di funzioni, come qualunque altro oggetto e vengono passati per riferimento, quindi qualsiasi modifica che effettuiamo ad un array passato come parametro si ripercuote anche quando è terminata la chiamata.(vediamo dettagli fra poco)
*/