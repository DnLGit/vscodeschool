/*
Bergamasco Daniele IV As - Esercizio per testare la classe Scanner - 21.03.2020
*/

package es09testscanner;
import java.util.*;


public class MainScanner {
    public static void main(String[] args) {
        
        System.out.println("\n***** Esercizio per testare la classe Scanner *****\n");

        System.out.print("Inserire il numero di Docenti ");

        Scanner ins = new Scanner(System.in);                                   // Dichiarazione e istanza dell'oggetto ins 
        int N = ins.nextInt();                                  // inizializzazione variabile dimensione array, da tastiera
        SnalsProf sprof[] = new SnalsProf[N];                                    // Dichiarazione e istanza dell'array sprof 
        



        System.out.println("\nInserisci in sequenza nome, cognome, codice e età dei "+N+" insegnanti");

        for(int i=0; i<N; i++){                     // ciclo per istanziare tutti gli oggetti dell'array prof[] da tastiera

            sprof[i] = new SnalsProf ("", "", "", "");
            System.out.println("\n"+(i+1)+"^ insegnante ");
            System.out.print("nome: ");
            sprof[i].setNome(ins.nextLine());
            System.out.print("cognome: ");
            sprof[i].setCognome(ins.nextLine());
            System.out.print("codice: ");
            sprof[i].setCodice(ins.nextLine());
            System.out.print("eta: ");
            sprof[i].setAnni(ins.nextLine());
        }
    }
}