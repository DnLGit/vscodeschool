package es15super;

public class Impiegato {            // superclasse

    public String nome;
    double stipendio;

    public Impiegato (){            // ho aggiunto quello di default per non avere errore nel main
        //this.nome = "Pinotto";
        //this.stipendio = 990;
    } 

    public Impiegato (String n){    // costruttore ad un parametro String
        this.nome = n;
        this.stipendio = 1000;
    }
}