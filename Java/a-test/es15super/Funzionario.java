package es15super;

public class Funzionario extends Impiegato {    // sottoclasse

    public Funzionario(){
        super();
        this.nome = "Pinotto";
        this.stipendio = 800;
    }

    public Funzionario (String m){
        super(m);
        stipendio = 1010;
    }

}