/*
Parola chiave ... Costrutto "super"  [pag. 44 dispensa aggiornata al 23.04.20 ]
Vediamo un esempio:
Abbiamo la seguente classe impiegato con il costruttore di default(non scritto) e quello fatto manualmente.
*/

package es15super;

public class MainImpiegato {
    public static void main(String[] args) {

        Funzionario funz0 = new Funzionario();   // funzionario di default (mi dava errore se non lo dichiaravo nella classe)

        Funzionario funz1 = new Funzionario("Gianni");  // richiama il costrutore " public Funzionario (String m) "

        System.out.println("\nIl funzionario "+funz0.nome + " guadagna " + funz0.stipendio + " euro/mese\n");
        System.out.println("\nIl funzionario "+funz1.nome + " guadagna " + funz1.stipendio + " euro/mese\n");
    }
}