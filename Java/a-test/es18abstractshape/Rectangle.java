package es18abstractshape;

public class Rectangle extends Shape{                   // classe Rectangle estesa dalla classe astratta Shape

    public double width, height;                        // variabile d'istanza (larghezza e altezza rettangolo)

    public Rectangle(double width, double height){      // costruttore
        this.width = width;
        this.height = height;
    }

    public double area() {                              // implementazione metodo "area" della classe astratta
        return width*height;                            // area rettangolo: basePerAltezza ...
    }
    public double perimeter(){                          // implementazione metodo "perimetro" della classe astratta
        return 2*(width+height);                        // perimetro rettangolo: doppiaBasePiu'Altezza ....
    }
    
}