package es18abstractshape;

public abstract class Shape {                       // classe astratta "Shape"

    protected int test;
    
    public abstract double area();                  // metodi astratti
    public abstract double perimeter();
    
    protected String getInfo(){                     // metodo standard
        return "Test";
    }
    
}