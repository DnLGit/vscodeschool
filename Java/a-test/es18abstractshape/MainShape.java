/*  Le classi astratte in Java [ pag. 58/60 dispensa]
    Vediamo un altro esempio con le figure geometrice(shape) per capire l’intercambialità tra classi. 
    Vogliamo implementare una classe astratta Shape che contiene area e perimetro come metodi astratti;    */

package es18abstractshape;

public class MainShape {
    public static void main(String[] args) {

        System.out.println("\n****************** Le classi astratte in Java - pag 60 ******************\n");

        //Shape sh = new Shape();                 // errore: Cannot instantiate the type Shape [1° regola classi astratte]

        Circle c1 = new Circle(2);                  // istanze di cerchi
        Circle c2 = new Circle(5);

        Rectangle r1 = new Rectangle(2.5, 3.0);     // istanze di rettangoli
        Rectangle r2 = new Rectangle(5.5, 2.3);

        System.out.println("L'area totale dei cerchi c1 e c2 e': "+(c1.area()+c2.area()));
        System.out.println("Il perimetro totale dei cerchi c1 e c2 e': "+(c1.perimeter()+c2.perimeter()));
        System.out.println();
        System.out.println("L'area totale dei rettangoli r1 e r2 e': "+(r1.area()+r2.area()));
        System.out.println("Il perimetro totale dei rettangoli r1 e r2 e': "+(r1.perimeter()+r2.perimeter()));

        
    /* Grazie all’uso della classe astratta possiamo costruire un array che contiene indifferentemente cerchi e rettangoli */
        
        Shape[] shapes = new Shape[4];              // array di 4 oggetti forme "shapes"

        shapes[0] = new Circle(2.5);
        shapes[1] = new Circle(4.5);
        shapes[2] = new Rectangle(3.5, 4.0);
        shapes[3] = new Rectangle(6.6, 3.3);

        System.out.println();

        System.out.println("Area del cerchio zero. "+shapes[0].area());             // stampa standard di area e perimetro
        System.out.println("Perimetro del cerchio zero. "+shapes[0].perimeter());
        System.out.println();

         /*  Si potra' calcolare quindi l’area/perimetro totali trattando uniformemente cerchi e rettangoli.    */

        for(int i =0; i<shapes.length; i++){
            System.out.println("Area della "+(i+1)+"^ forma e': "+shapes[i].area());
            System.out.println("mentre il suo perimetro e': "+shapes[i].perimeter()+"\n");
        }

        double areaTot = 0, perimeterTot =0;
        for(int i=0; i<shapes.length; i++){
            areaTot += shapes[i].area();
            perimeterTot += shapes[i].perimeter();
        }
        System.out.println("L'area totale e': "+areaTot);
        System.out.println("Il perimetro totale e': "+perimeterTot);
    }
}