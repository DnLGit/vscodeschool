/*
Bergamasco Daniele IV As - Esercizio per testare la classe Scanner - 21.03.2020
*/

package es10scanner;
import java.util.*;


public class MainScanner {
    public static void main(String[] args) {
        
        System.out.println("\n***** Esercizio per testare la classe Scanner *****\n");

        
        Scanner ins = new Scanner(System.in);                                   // Dichiarazione e istanza dell'oggetto ins 

        SnalsProf sprof = new SnalsProf();                                      // Dichiarazione e istanza dell'array sprof 
        
     

        System.out.println("\nInserisci i dati del prof iscritto allo Snals: ");
        
        System.out.print("nome: ");
        sprof.setNome(ins.nextLine());
        System.out.print("cognome: ");
        sprof.setCognome(ins.nextLine());
        System.out.print("codice: ");
        sprof.setCodice(ins.nextLine());
        System.out.print("eta: ");
        sprof.setEta(ins.nextInt());
     
        ins.close();
    }
}