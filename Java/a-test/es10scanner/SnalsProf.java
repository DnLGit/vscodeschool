package es10scanner;


public class SnalsProf {


        private String nome, cognome, codice, anni;           // Variabili d'istanza
        private int eta;

           
    // Costruttore senza parametri
        public SnalsProf(){
            
        }
    
    
    // Costruttori a 4 parametri
         public SnalsProf(String nome, String cognome, String codice, int eta){
            this.nome = nome; this.cognome = cognome; this.codice = codice; 
            this.eta = eta; 
        }
        public SnalsProf(String nome, String cognome, String codice, String anni){
            this.nome = nome; this.cognome = cognome; this.codice = codice; this.anni = anni; 
        }
    
    // Metodi Getter & Setter
        public String getNome(){
            return this.nome;
        }
        public void setNome(String nome){
            this.nome = nome;
        }
    
        public String getCognome(){
            return this.cognome;
        }
        public void setCognome(String cognome){
            this.cognome = cognome;
        }
    
        public String getCodice(){
            return this.codice;
        }
        public void setCodice(String codice){
            this.codice = codice;
        }
    
        public int getEta(){
            return this.eta;
        }
        public void setEta(int eta){
            this.eta = eta;
        }
        public int getYyy(){
            return this.eta;
        }
        public void setAnni(String anni){       // Attenzione: metodo setter degli anni ma di Tipo String ... 
            this.anni = anni;                   // ... per il test della classe Scanner!!!
        }
}