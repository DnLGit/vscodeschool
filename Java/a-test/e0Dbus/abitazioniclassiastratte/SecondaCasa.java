/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e0Dbus.abitazioniclassiastratte;

/**
 *
 * @author dorin
 */
public class SecondaCasa extends Abitazione{
    
     private double imu;
     private double tasse;
     private double tassaRinnovoAffitto;
     
     public SecondaCasa(double imu, double tasse, double tassaRinnovoAffitto){
         this.imu = imu;
         this.tassaRinnovoAffitto = tassaRinnovoAffitto;
         this.tasse = tasse;
        
    }
    
     public double getImu(){
         return imu;
     }
     public double getTasse(){
        return tasse;
     }
     public double getRinnovo(){
         return tassaRinnovoAffitto;
     }
     public void setImu(double imu){
         this.imu = imu;
     }
     public void setTasse(double tasse){
         this.tasse = tasse;
     }
     public void setRinnovo(double tassaRinnovoAffitto){
         this.tassaRinnovoAffitto = tassaRinnovoAffitto;
     }
     
    public double calcoloCostoMensile(){
       
        return imu+tasse+tassaRinnovoAffitto;
    }
}
