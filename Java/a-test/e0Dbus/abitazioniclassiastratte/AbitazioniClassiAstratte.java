/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*  ************* (DnL:  e' l'esercizio n° 35 della dispensa)   */

package e0Dbus.abitazioniclassiastratte;

/**
 *
 * @author dorin
 */
public class AbitazioniClassiAstratte {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        
       
        
        Abitazione [] ab = new Abitazione [2];
        ab[0] = new PrimaCasa(200, 500);
        ab[1] = new SecondaCasa(400, 600, 100);
        
        System.out.println("la somma delle spese della prima e seconda casa e': "+(ab[0].calcoloCostoMensile()+ab[1].calcoloCostoMensile()));
        
        
              
    }
    
}
