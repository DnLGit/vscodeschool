/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e0Dbus.abitazioniclassiastratte;

/**
 *
 * @author dorin
 */
public class PrimaCasa extends Abitazione{
    private double condominio;
    private double utenze;
    
    public PrimaCasa(double condominio, double utenze){
        this.condominio = condominio;
        this.utenze = utenze;
        
    }
    
    public double getCondominio(){
        return condominio;
    }
    public double getUtenze(){
        return utenze;
    } 
    public void setCondominio(double condominio){
        this.condominio = condominio;
    }
    public void setUtenze(double utenze){
        this.utenze = utenze;
    }
            
    public double calcoloCostoMensile(){
         return condominio + utenze;
    }
    
}
