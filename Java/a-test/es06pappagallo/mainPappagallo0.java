/*
Bergamasco Daniele IV As - Esercizio 12:  Pappagallo - 01.03.2020

Scrivere una classe Pappagallo che 
chiede continuamente all’utente di inserire una stringa e la ripete (stampandola) immediatamente. 
L’inserimento delle stringhe deve terminare quando l’utente inserisce la stringa vuota.   */

package es06pappagallo;

import java.util.Scanner;

public class mainPappagallo0 {

    public static void main(String[] args) {

        System.out.println("\n***** Esercizi con Stringhe:  Pappagallo *****\n");

        Scanner ripeti = new Scanner(System.in);

        System.out.print("\nInserisci una frase: ");
        String frase = ripeti.nextLine();
        System.out.println("e ristampa: "+frase);

        boolean uguali = frase.equals("");

        while(!uguali) {
            System.out.print("\nInserisci un'altra frase: ");
            frase = ripeti.nextLine();
            System.out.println("e ristampa: "+frase);
            uguali = frase.equals("");
        } 
        System.out.println("Maaaa ... non Ti avevo detto di inserire una frase!!!");
    }
}



//System.out.print("\nInserisci la prima frase: ");
        //String frase = insertReply.nextLine();
        //System.out.println("stampa: "+frase);
/*
        boolean uguali = frase.equals("");

        while(!uguali){
            System.out.print("\nInserisci un'altra frase: ");
            frase = insertReply.nextLine();
            System.out.println("e ristampa: "+frase);            
        }
        System.out.println("Maaaa ... non Ti avevo detto di inserire ...");
*/
/*
        String frase;

        do {System.out.print("\nInserisci la prima frase: ");
            frase = insertReply.nextLine();
            System.out.println(frase);
            
        } while (frase !="");

        System.out.println("Maaaa ... non Ti avevo detto di inserire ...");
*/