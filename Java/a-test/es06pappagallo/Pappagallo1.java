package es06pappagallo;

import java.util.Scanner;                                           // Libreria per utilizzare la classe "Scanner".

public class Pappagallo1 {

    Scanner ripeti = new Scanner(System.in);

    private String domanda, risposta;                               // variabili d'istanza

    public Pappagallo1(String domanda, String risposta){             // costruttore
        this.domanda = domanda; this.risposta = risposta;
    }

    public void repeat(){                                           // Metodo inizio ciclo

        while(!risposta.equals("")) {
            System.out.print(domanda);
            risposta = ripeti.nextLine();
            System.out.println(risposta);
        } 
        System.out.println("Maaaa ... non Ti avevo detto di inserire una frase!!!");
    } 
}