/*
Bergamasco Daniele IV As - Esercizio 12:  Pappagallo - 01.03.2020

Scrivere una classe Pappagallo che 
chiede continuamente all’utente di inserire una stringa e la ripete (stampandola) immediatamente. 
L’inserimento delle stringhe deve terminare quando l’utente inserisce la stringa vuota.   */

package es06pappagallo;

public class mainPappagallo1 {
    public static void main(String[] args) {

        System.out.println("\n***** Esercizi con Stringhe:  Pappagallo *****\n");

        Pappagallo1 parlante = new Pappagallo1("\nInserisci una frase: ", "\nfunky");     // costruzione oggetto

        parlante.repeat();                                                              // invocazione metodo inizio ciclo
    }
}