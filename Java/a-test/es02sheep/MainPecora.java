/*
Appunti di Java pag. 17/18:

Costruttore di copia 
Sono quelli che ricevono come parametro un'altra istanza della stessa classe e quindi copiano tutte le proprietà di quell'istanza. 
I costruttori di copia vengono definiti tali in quanto possono effettuare cloni di oggetti come nell’esempio di seguito:

*/
package es02sheep;


public class MainPecora {

    public static void main(String[] args) {
        
        Pecora pecora1 = new Pecora ("Dolly", 20);      // creazione oggetto pecora1
        Pecora pecora2 = new Pecora (pecora1);          // creazione oggetto pecora2 (clonazione oggetto pecora1)
                                                        // dolly.nome è "Dolly" e dolly.peso è 20


    }

    
}