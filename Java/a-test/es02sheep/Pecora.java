package es02sheep;


public class Pecora {

    private String nome;
    private int peso;

    public Pecora(String nome, int peso){       // costruttore 1
        this.nome = nome;
        this.peso = peso;
    }

    public Pecora(Pecora copyPecora){           // costruttore di copia
        this.nome = copyPecora.nome;
        this.peso = copyPecora.peso;

    }
    

    
}