/*  Input da file [ pag. 75/76 dispensa]
• Per leggere dati da un file presente sul disco, la classe Scanner si affida ad un’altra classe, File, 
che gestisce file e directory presenti in un file-system. Esso richiede l’importazione della libreria:
import java.io.File;    */

package es21inoutfile;
import java.io.File;                                    // importazione libreria .io.File [gestione file e directory]
import java.io.FileNotFoundException;
import java.util.Scanner;                               // importazione libreria .util [gestione file e directory]

public class MainFile {                                 // "throws FileNotFoundException" aggiunto da VsCode
    public static void main(String[] args) /* throws FileNotFoundException */ {   
        System.out.println("\n******  Input da file [ pag. 75/76 dispensa]  *******\n");

        try{
            File file1 = new File("FileEs21Testo3.txt");        // costruzione oggetto "file1" di tipo File
            if(file1.exists()){
                Scanner scan = new Scanner(file1);
                System.out.println(scan.nextLine());        // così mi stampa solo la prima riga
                while(scan.hasNextLine())                   // con il while ed il metodo hasNetLine
                    System.out.println(scan.nextLine());        // invece mi stampa fino all'ultima riga
            }                    
            /* else
            System.out.println("manca il file"); */

        }
        catch (FileNotFoundException exception){
            System.out.println("Aiutooo");
        }
        
/*         File file1 = new File("FileTesto3.txt");        // costruzione oggetto "file1" di tipo File
        Scanner scan = new Scanner(file1);
        while(scan.hasNextLine())
            System.out.println(scan.nextLine()); */

    }
    
}