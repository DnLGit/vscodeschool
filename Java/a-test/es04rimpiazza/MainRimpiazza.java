/*
Bergamasco Daniele IV As - Esercizio 15:  Rimpiazza - 04.03.2020

Scrivere un programma Rimpiazza che legge una stringa e due caratteri, e poi stampa la stringa in cui tutte le occorrenze del primo carattere sono sostituite dal secondo.   */

package es04rimpiazza;

import java.util.*;


public class MainRimpiazza {

    public static void main(String[] args) {

        System.out.println("\n***** Esercizi con Stringhe:  Rimpiazza *****\n");

        System.out.print("\nHei, inserisci una frase: ");

        Scanner leggi = new Scanner(System.in);                      // Warning [Resource leak: 'inserisci' is never closed]

        String frase = "trottola";  //leggi.nextLine();
        String unodueChar = "oa";  //leggi.next();

        String unoChar = unodueChar.substring(0,1);
        String dueChar = unodueChar.substring(1);

        System.out.println(frase);
        System.out.println("uno è: "+unoChar);
        System.out.println("due è: "+dueChar);

        String fraseReplace = frase.replaceAll(unoChar, dueChar);
        System.out.println(fraseReplace);


        
    }
}