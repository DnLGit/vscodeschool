/*
 * Esercizi sugli array (serie 1)
 * https://andbin.it/blog/java/esercizi-array-serie-1
 *
 * Andrea Binello ("andbin")
 *
 * =============================================================================
 *
 * Esercizio 01 - Rotazione in avanti di un array int[]
 *
 * Realizzare il seguente metodo:
 *
 * public static void ruotaAvanti(int[] array)
 *
 * Il metodo deve modificare l’array ricevuto in ingresso facendo “ruotare” in
 * avanti di una posizione tutti i valori. L’ultimo valore dovrà quindi essere
 * spostato all’inizio dell’array.
 */

package es11ruota;

public class Esercizio01RotazioneAvanti {
    public static void main(String[] args) {
        System.out.println("Esercizio 01 - Rotazione in avanti di un array int[]");
        prova(new int[] { 4, 9 });
        prova(new int[] { 4, 9, 3 });
        prova(new int[] { 4, 9, 3, 7 });
    }

    private static void prova(int[] array) {
        String strArray = EserciziUtil.toString(array);
        ruotaAvanti(array);
        String strArrayRis = EserciziUtil.toString(array);

        System.out.println(strArray + "  --->  " + strArrayRis);
    }

    public static void ruotaAvanti(int[] array) {
        if (array.length > 1) {
            int ultimo = array[array.length - 1];

            for (int i = array.length - 1; i > 0; i--) {
                array[i] = array[i-1];
            }

            array[0] = ultimo;
        }
    }
}
