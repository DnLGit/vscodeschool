
/*
15.02.2020 
*/

package e0Devis.rubrica;                      // DnL:  aggiunto package per evitare errori su Vscode
class Contatto{
    
    private String nome;
    private String cognome;
    private String numTel;//numero di telefono del contatto
    private String email;
    private int eta;

    //funzionalità del costruttore di default: inizializza tutte le variabili ai suoi valori di default
    public Contatto(){
        nome = null;
        cognome = null;
        numTel = null;
        email = null;
        eta = 0;
    }

    public Contatto(String nome){
        this.nome = nome;
    }

    public Contatto(String nome, String cognome, String numTel, String email, int eta){
        this.nome = nome;
        this.cognome = cognome;
        this.numTel = numTel;
        this.email = email;
        this.eta = eta;
    }


    public String getNome(){
        return nome;
    }

    public void setNome(String nome){
       this.nome = nome;
    }

    public String getCognome(){
        return cognome;
    }

    public void setCognome(String cognome){
        this.cognome = cognome;
     }

}