/*
Bergamasco Daniele IV As - Esercizio 20:  Leggi gli Elementi - 06.03.2020

Esercizio su array ed input da tastiera
Scrivere un programma che legge da input n elementi interi(n deciso da tastiera), li memorizza in un array e stampa l’elemento con occorrenze maggiore.   */

package e0Devis.eserciziodizionario;

import java.util.HashMap;
import java.util.Scanner;                                                       // libreria per utilizzare la classe Scanner

public class MainLeggiElementi {

    public static void main(String[] args) {

        System.out.println("\n***** Esercizi con Stringhe:  Legggi gli Elementi *****\n");

        Scanner leggi = new Scanner(System.in);                     // Warning [Resource leak: 'inserisci' is never closed]
        
        System.out.print("Inserisci la dimensione dell'array: ");
        int N = leggi.nextInt();
        int harry [] = new int[N];                                          // dichiarazione e creazione dell'array "harry"!
     
        System.out.println("Ora riempi l'array di "+ N +" elementi interi");
        
        for(int i=0; i<N; i++){                                                             // ciclo for per riempire l'array
            System.out.print(i+1+"^ elemento: ");
            harry[i]=leggi.nextInt();
        }

        System.out.println("\nQuesto è harry: ");

        HashMap<Integer, Integer> contatori = new HashMap<>();
        
        for(int el:harry){
            if(contatori.containsKey(el)){
                //chiave trovata, incrementa il suo valore

                int valore = contatori.get(el);//recupero il valore per la chiave el e lo salvo in una variabile temp
                valore++;//incremento il valore corrente
                contatori.put(el, valore);//aggiorno il valore nel dizionario

            }else{
                //chiave non trovata, inserisci nel dizionario contatori entry (el, 1)
                
                contatori.put(el, 1);//aggiungo il valore nel dizionario

            }
        }

        //itero su tutte le possibili chiavi del dizionario contatori per trovare quella con valore maggiore
        // quella con valore maggiore == numero con maggiori occorrenze nell'array originale
        int maxKey = -1;//numero con maggiori occorrenze
        int maxOcc = 0;//massimo numero di occorrenze
        for(int key : contatori.keySet()){
            int occ = contatori.get(key);
            if(occ > maxOcc){
                //trovato una chiave (chiave=elemento nell'array iniziale) con valore (valore=occorrenze nell'array iniziale)
                //maggiore rispetto al massimo trovato finora
                maxOcc = occ;//salvo il massimo numero di occorrenze trovate finora per un valore
                maxKey = key;//salvo il valore (elemento interessato all'utente da tornare alla fine)
            }
        }
        System.out.println("Il valore con massimo numero di occorrenze e' " + maxKey + " (si presenta " + maxOcc + " volte) ");


    






        leggi.close();                                           // il metodo "leggi.close()"" elimina il Warning sopra



}
    
}