/*
Bergamasco Daniele IV As - Esercizio 25:  InputTastiera - 25.03.2020

Esercizio sugli array ed input da tastiera.
Realizzare il seguente metodo: 
public int[] trovaPosizioniMassimo(int[] array) 
Il metodo deve restituire un "nuovo" array che contiene le posizioni (indici) degli elementi con valore massimo. 
Il valore massimo può trovarsi infatti anche in più posizioni all’interno dell’array. Esempi:
 array                        Risultato
 a) { 4, 2, 6, 5 }             { 2 }
 b) { 2, 9, 4, 3, 9, 5 }       { 1, 4 }
 c) { 7, 2, 7, 5, 1, 7 }       { 0, 2, 5 }   

Creare un main per testarlo con input da tastiera.  */

package j25trovamax;

public class MainMax {
    public static void main(String[] args) {

        System.out.println("\n***** Classe Trova indici con valore Max *****");

        int arrayA[] = {4, 2, 6, 5};               // Dichiarazione e inizializzazione di tre array
        int arrayB[] = {2, 9, 4, 3, 9, 5};
        int arrayC[] = {7, 2, 7, 5, 1, 7};

        Max mx = new Max();                                     // Istanza della classe Max (creazione oggetto mx)
 
        System.out.print("\narray A)   ");                  
        for (int temp : mx.trovaPosizioniMassimo(arrayA)) {     //  foreach che chiama il metodo "trovaPosizioniMassimo"   
            System.out.print(temp+" ");                         //  che si trova nella classe Max alla quale vengono passati
        }                                                       //  gli array "A, B e C" dichiarati nel main e poi stampati
        System.out.print("\narray B)   ");
        for (int temp : mx.trovaPosizioniMassimo(arrayB)) {     // qui infatti gli viene passato l'array B
            System.out.print(temp+" ");
        }
        System.out.print("\narray C)   ");
        for (int temp : mx.trovaPosizioniMassimo(arrayC)) {     // mentre qui viene passato al metodo l'array C
            System.out.print(temp+" ");
        }
    }
}