package j25trovamax;

public class Max {

    
    public int[] trovaPosizioniMassimo(int[] array){        // metodo per trovare le posizioni dei valori massimi di un array
        int cont = 0, index=0,  max = array[0];
        
        for(int i=1; i<array.length; i++){                  // Primo for: scorre l'array da esaminare ...
            if(array[i]>max){                               // trova valore massimo e lo memorizza in max
                max = array[i];
            }
        }
        for(int i =0; i<array.length; i++){                 // Secondo for: scorre di nuovo l'array da esaminare ...
            if(array[i]==max){                              // e conta quante volte max compare nell'array incrementando
                cont++;                                     // "cont", che diventerà la dimensione del nuovo array
            }
        }

        int [] arrayIndex = new int [cont];                 // dichiarazione dell'array nuovo ... da popolare

        for(int i =0; i<array.length; i++){                 // Terzo for: scorre ancora l'array da esaminare ...
            if(array[i]==max){                              // e quando ritrova i valori max memorizza la loro posizione
                arrayIndex[index]=i;                        // nella prima cella libera del nuovo array 
                index++;                                    // e incrementando index faccio avanzare l'indice dell'array.
            }
        }
        return arrayIndex;
    }
}