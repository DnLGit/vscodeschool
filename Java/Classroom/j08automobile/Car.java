/* Bergamasco Daniele IV As - Esercizio 8:  Automobili con costruttori e metodi particolari - 27.02.2020

- classe Car(automobile) con le proprietà seguenti. 
- resa del carburante in litri/chilometri (specificata dal costruttore)
- quantità di carburante nel serbatoio. -> livello iniziale del carburante è a zero. 
Fornire questi metodi: 
- metodo drive per simulare il percorso di un’automobile per una certa distanza, 
  riducendo il livello di carburante nel serbatoio; 
- metodo getGas, per ispezionare il livello corrente del carburante; 
- metodo addGas per far rifornimento.   */

package j08automobile;

public class Car {
    private float fuelKm, fuelTank;                                 // fuelKm: resa (20km/lt)     fuelTank: livello serbatoio

    // Costruttori
    public Car(float fuelKm, float fuelTank){
        this.fuelKm = fuelKm;
        this.fuelTank = fuelTank;
    }

    // Metodi Getter e Setter
    public float getFuelKm() {
        return this.fuelKm;
    }
    public void setFuelKm(float fuelKm) {
        this.fuelKm = fuelKm;
    }
    public float getFuelTank() {                                // per ispezionare il livello corrente del carburante
        return this.fuelTank;
    }
    public void setFuelTank(float fuelTank) {
        this.fuelTank = fuelTank;
    }
    
    // altri Metodi:
    public void drive(float giro){                              // consumo carburante in base al percorso 
        this.fuelTank -= giro*fuelKm;                           // meglio forse mettere una condizione di minimo serbatoio???
    }
    public void addGas(float fuel){                             // aggiunge carburante in litri
        this.fuelTank += fuel;
    }
    public void full(){                                         // fa il pieno considerando il livello esistente
        this.fuelTank += (60.00f - fuelTank);
    }
}