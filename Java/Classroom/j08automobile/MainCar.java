/*
Bergamasco Daniele IV As - Esercizio 8:  Automobili con costruttori e metodi particolari - 27.02.2020

Progettare e realizzare una classe Car(automobile) con le proprietà seguenti. 
Un’automobile ha una determinata resa del carburante (misurata in miglia/galloni o in litri/chilometri: 
scegliete il sistema che preferite) e una certa quantità di carburante nel serbatoio. 
La resa è specificata dal costruttore e il livello iniziale del carburante è a zero. 
Fornire questi metodi: 
un metodo drive per simulare il percorso di un’automobile per una certa distanza, 
riducendo il livello di carburante nel serbatoio; 
un metodo getGas, per ispezionare il livello corrente del carburante; 
un metodo addGas per far rifornimento.
*/

 package j08automobile;
 
 
 public class MainCar {
     public static void main(String[] args) {

        System.out.println("\n******** Classe Car (automobile) ********");

        Car jeep = new Car(0.05f, 0.00f);               // Oggetto jeep della classe Car (in java, float vuole la "f" finale)

        System.out.println("\nAuto nuova ... ma senza benzina, il sebatoio ha "+jeep.getFuelTank()+" litri");

        System.out.println("\nFacciamo rifornimento!!");
        jeep.full();                                                        // chiamata metodo full: fa il pieno alla Jeep.
        System.out.print("\nOra il serbatoio ha "+jeep.getFuelTank()+" litri ed è pieno!!");

        System.out.println("\ne facciamo un primo giro!!");
        jeep.drive(50.00f);              // chiamata metodo drive: simulazione di percorso di 50km e riduzione serbatoio
        System.out.print("\nDopo 50km nel serbatoio ci sono ancora "+jeep.getFuelTank()+" litri");

        System.out.println("\ne facciamo un secondo giro, un po' più lungo!!");
        jeep.drive(800.00f);                                    // chiamata metodo drive: altri 800km e riduzione serbatoio
        System.out.println("\nAndata e ritorno: 400km. Riserva: nel serbatoio rimangono "+jeep.getFuelTank()+" litri");

        System.out.println("\nMeglio aggiungere comunque del carburante");
        jeep.addGas(40.00f);                                    // chiamata metodo addGas: aggiunge 40 litri di carburante
        System.out.println("Ora il display dell'auto mi indica che ci sono "+jeep.getFuelTank()+" litri nel sebatoio!!");
        System.out.println("\n... ma domani meglio partire col pieno");
        jeep.full();                                                        // chiamata metodo full: rifa il pieno alla Jeep.
        System.out.println("Ora il display mi indica "+jeep.getFuelTank()+" litri tondi!!");
    }
}