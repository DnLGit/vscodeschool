/*
Bergamasco Daniele IV As - Esercizio 23:  Scambio - 24.03.2020

Realizzare il seguente metodo: public void scambiaMeta(int[] array).
Il metodo deve modificare l’array ricevuto in ingresso scambiando la prima e la seconda metà dell’array. 
I valori in ciascuna metà devono mantenere l’ordine originale. 
Se un array ha lunghezza dispari, il valore centrale deve rimanere inalterato al suo posto. 
Esempi:
prima                     dopo
{2, 3, 4, 7, 8, 9}        {7, 8, 9, 2, 3, 4}
{4, 2, 6, 5, 3, 3, 9}     {3, 3, 9, 5, 4, 2, 6}

Creare un main per testarlo con input da tastiera.  */

package j23scambio;

public class MainReverse {

    public static void main(String[] args) {
        
        System.out.println("\n***** Classe Reverse 1° soluzione *****\n");

        int array1[] = {2, 3, 4, 7, 8, 9};                                  // Dichiarazione e inizializzazione di due array
        int array2[] = {4, 2, 6, 5, 3, 3, 9};   

        Reverse rv1 = new Reverse(array1);                            // Dichiarazione e inizializzazione dei due oggetti rv
        Reverse rv2 = new Reverse(array2);

        ReverseFurbo rf1 = new ReverseFurbo(array1);                   // Dichiarazione e inizializzazione dei due oggetti rf
        ReverseFurbo rf2 = new ReverseFurbo(array2);

        System.out.print("Array d'origine:  ");                                 // stampa dei due array originali con foreach
        for(int print:array1)
            System.out.print(print+" ");
        System.out.print(" / ");
        for(int print:array2)
            System.out.print(print+" ");
        System.out.println("\n");

        System.out.print("Array Scambiati:  ");                                     // Chaimata dei due metodi "ScambiaMeta"
        rv1.ScambiaMeta();
        System.out.print(" / ");
        rv2.ScambiaMeta();
        System.out.println("\n");


        System.out.print("Array Furbi:  ");                                         // Chaimata dei due metodi "ScambioFurbo"
        rf1.ScambioFurbo();
        System.out.print(" / ");
        rf2.ScambioFurbo();
        System.out.println("\n");
    }
}