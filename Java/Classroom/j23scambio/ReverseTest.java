package j23scambio;

import java.util.Arrays;

public class ReverseTest {

    private int array[];


    public ReverseTest(int array[]){
        this.array = array;
    }

    public void ScambiaMeta(){
        int temp1[] = Arrays.copyOf(this.array, this.array.length/2);
        int temp2[] = Arrays.copyOfRange(this.array, (this.array.length)-(this.array.length/2), this.array.length);

        for(int i = 0; i < array.length; i++){
            if(i < array.length/2){
                array[i] = temp2[i];
            }
            else if(i > array.length/2 & (array.length)%2!=0)
                array[i] = temp1[i - (array.length/2)-1];

            else if(i >= array.length/2 & (array.length)%2==0)
                array[i] = temp1[i - array.length/2];

            /* else array[i] = temp1[i-array.length/2]; */
        }
        
        for(int tmp:array)
            System.out.print(tmp+" ");
        

        // test per verificare la lunghezza di length/2: "
        System.out.println("\n\ntest lunghezza di length/2: ");
        System.out.println("array.length/2 è di "+array.length/2 +" elementi ");

        // test per verificare la lunghezza dei due mezziArray: "
        System.out.println("\n\ntest lunghezza dei due mezziArray: ");
        System.out.println("temp1: "+temp1.length+" elementi \ntemp2: "+temp2.length+" elementi ");
        
        // test per verificare le stampe dei due mezziArray: "
        System.out.println("\n\ntest stampa dei due mezziArray: ");
        for(int tmp1:temp1)
            System.out.print(tmp1+" ");
        System.out.print(" / ");
        for(int tmp2:temp2)
            System.out.print(tmp2+" ");
    }
}