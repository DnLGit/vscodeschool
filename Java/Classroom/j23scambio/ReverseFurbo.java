package j23scambio;

import java.util.Arrays;

public class ReverseFurbo {

    private int arrayFurbo[];                           // variabile d'istanza

    public ReverseFurbo(int arrayFurbo[]){              // Costruttore
        this.arrayFurbo = arrayFurbo;
    }

    public void ScambioFurbo(){                         // estraggo le due metà dell'array salvandole in due variabili temp
        int temp1[] = Arrays.copyOf(this.arrayFurbo, this.arrayFurbo.length/2);
        int temp2[] = Arrays.copyOfRange(this.arrayFurbo, this.arrayFurbo.length/2, this.arrayFurbo.length);
        
        for(int tmp:arrayFurbo)             // Stampo l'array finale (deframmentato sopra in temp1 e temp2)
            System.out.print(tmp+" ");
    }
}