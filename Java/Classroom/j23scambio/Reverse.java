package j23scambio;

import java.util.Arrays;

public class Reverse {

    private int array[];                                // variabile d'istanza

    public Reverse(int array[]){                        // Costruttore
        this.array = array;
    }

    public void ScambiaMeta(){                            // estraggo le due metà dell'array salvandole in due variabili temp
        int temp1[] = Arrays.copyOf(this.array, this.array.length/2);
        int temp2[] = Arrays.copyOfRange(this.array, (this.array.length)-(this.array.length/2), this.array.length);

        for(int i = 0; i < array.length; i++){                      //  ciclo per ri-assegnare i valori scambiati all'array 
            if(i < array.length/2){                                 //  1) fino a metà array ...
                array[i] = temp2[i];                                //  2) assegno la seconda metà dell'array d'origine
            }
            else if(i > array.length/2 & (array.length)%2!=0)   // 3a) se array dispari e superata la metà ... 
                array[i] = temp1[i - (array.length/2)-1];       // 4a) assegno la prima metà dell'array dispari d'origine 

            else if(i >= array.length/2 & (array.length)%2==0)  // 3b) se array pari e dalla metà in poi ... 
                array[i] = temp1[i - array.length/2];           // 4b) assegno la prima metà dell'array pari d'origine
        }
        for(int tmp:array)                                      // Stampo l'array finale
            System.out.print(tmp+" ");
    }
}