package j27testcondizioni;

public class Grade {

    public double getNumericGrade(String grade){        // metodo per convertire i voti da lettere a numeri.
        double convertGrade = 0.0;                      // inizializzazione a zero della variabile

        if(grade.substring(0, 1).equals("A")){          // confronto tra il primo carattere della stringa inserita con "A"
            convertGrade = 4;                           // istruzione che mi rende il rispettivo valore numerico 
        }
        else if(grade.substring(0, 1).equals("B")){     // idem
            convertGrade = 3;
        }
        else if(grade.substring(0, 1).equals("C")){     // idem
            convertGrade = 2;
        }
        else if(grade.substring(0, 1).equals("D")){     // idem
            convertGrade = 1;
        }
        else if(grade.substring(0, 1).equals("F")){     // idem
            convertGrade = 0;
        }
        /*  incremento di 0.3 i rispettivi valori numerici tranne nel caso di inserimento di "A" e "F" */
        if(grade.substring(1).equals("+") && !grade.substring(0, 1).equals("A") && !grade.substring(0, 1).equals("F")){
            convertGrade = convertGrade + 0.3;          // ... forse meglio creare una costante ...!!
        }
        /*  decremento di 0.3 i rispettivi valori numerici tranne nel caso di inserimento di "F" */
        else if(grade.substring(1).equals("-") && !grade.substring(0, 1).equals("F"))
            convertGrade = convertGrade - 0.3;          

        return convertGrade;
    }
}

/*
    public double getNumericGrade(char[] grade){
        double convertGrade=0, finalGrade=0;
        if(grade[0] == 'A'){
            convertGrade = 4;
            if(grade[0] == 'B')
            convertGrade = 3;
            if(grade[0] == 'C')
            convertGrade = 2;
            if(grade[0] == 'D')
            convertGrade = 1;
            if(grade[0] == 'F')
            convertGrade = 0;
        }
        if(grade[1] =='+')
            finalGrade=convertGrade+0.3;
        else if(grade[1] =='-')
            finalGrade=convertGrade-0.3;
        return finalGrade;
*/