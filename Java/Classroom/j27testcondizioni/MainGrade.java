/*
Bergamasco Daniele IV As - Esercizio 27:  Test Condizioni - 11.05.2020

Esercizio per testare le condizioni if-else etc
Scrivete un programma per convertire la lettera di un voto scolastico nel numero corrispondente. 
Le lettere sono A, B, C, D e F, eventualmente seguite dai segni + o -. 
I loro valori numerici sono 4, 3, 2, 1 e 0. F+ e F- non esistono. 
Un segno + o – incrementa o decrementa il valore numerico di 0.3. 
Tuttavia, A+ è uguale a 4.0. 
Usate una classe Grade con un metodo getNumericGrade.  */

package j27testcondizioni;
import java.util.*;                                                     // libreria per utilizzare la classe Scanner

public class MainGrade {

    public static void main(String[] args) {

        System.out.println("\n****************** Classe Grade ******************\n");

        Scanner insGrade = new Scanner(System.in);                      // Istanza dell'oggetto Scanner "insGrade".  
        Grade gr = new Grade();                                         // Istanza dell'oggetto Grade "gr".
        
        System.out.print("Proviamo a convertire\nImmetti una lettera dalla A alla F [U per uscire]\n\n");
        
        boolean entrata = true;                                         // variabile booleana per "entrare" nel doWhile
        do {System.out.print("Inserisci carattere: ");                  // doWhile per ripetere gli inserimenti da tastiera
            String gradeInsert = insGrade.next();

            if(gradeInsert.equals("U"))                                 // all'inserimento di "U" esco dal ciclo ...
                entrata = false;                                        // entrata > false, altrimenti continuo ad inserire
            else System.out.println("-------->  "+gradeInsert+" vale  "+gr.getNumericGrade(gradeInsert)+"\n");
            
        } while (entrata);                                              // fino a quando sarà valida la condizione!
        insGrade.close();
    }
}