package j26cancminmax;

public class MinMax {

    public static int[] eliminaMinimoMassimo(int[] array){  // metodo per trovare i valori minimi e massimi in un array
        int max = array[0], min = array[0];                 // inizializzati min&max al valore della prima cella dell'array
        int countMax = 0, countMin = 0, k=0;                // inizializzate a zero variabili contatori di min&max

        for (int i =1; i<array.length; i++){                // Primo for: scorre l'array da esaminare ...
            if(array[i] > max)                              // trova valore massimo e lo memorizza in max
            max = array[i];
            if(array[i] < min)                              // trova valore minimo e lo memorizza in min
            min = array[i];
        }
        for(int i = 0; i<array.length; i++){                // Secondo for: scorre di nuovo l'array da esaminare ...
            if(array[i] == max)                             // conta quante volte max compare nell'array 
            countMax++;                                     // incrementando la variabile countMax
            if(array[i] == min)                             // conta quante volte min compare nell'array
            countMin++;                                     // incrementando la variabile countMin
        }
        int newDim = array.length - (countMax+countMin);    // dimenione del nuovo array
        int newArray[] = new int [newDim];                  // dichiarazione nuvo array

        for(int i = 0; i < array.length; i++){              // Terzo for: popola il nuovo array
            if(array[i] != max && array[i] != min){         // escludendo valori min e max
                newArray[k] = array[i];                     // memorizzando gli altri valori nel nuovo array
                k++;                                        // incrementando separatamente l'indice
            }
        }
        return newArray;
    }
}