/*
Bergamasco Daniele IV As - Esercizio 26:  InputTastiera - 27.04.2020

Esercizio sugli array ed input da tastiera.
Realizzare il seguente metodo: 
public static int[] eliminaMinimoMassimo(int[] array) Il metodo deve restituire un nuovo array che contiene tutti i valori dell’array in ingresso escludendo però i valori minimo e massimo (che possono ripetersi anche più volte nell’array). Esempi:
 array                                     Risultato
 a) { 8, 16, 5, 6, 12, 5, 10, 5, 16, 9 }   { 8, 6, 12, 10, 9 }
 b) { 3, 10, 4, 10, 8, 5, 2 }              { 3, 4, 8, 5 }
 c) { 3, 8,6, 3, 6, 3, 3, 8 }              { 6, 6 }   

Creare un main per testarlo con input da tastiera.  */

package j26cancminmax;

public class MainMinMax {

    public static void main(String[] args) {

        System.out.println("\n***** Classe Elimina Min & Max *****");

        int arrayA[] = { 8, 16, 5, 6, 12, 5, 10, 5, 16, 9 };                // Dichiarazione e inizializzazione di tre array
        int arrayB[] = { 3, 10, 4, 10, 8, 5, 2 };
        int arrayC[] = { 3, 8,6, 3, 6, 3, 3, 8 };

        System.out.println();

        System.out.print("Array A originale con MIn&Max: ");                // foreach stampa dell'array A iniziale
        for (int print : arrayA) {                                          
            System.out.print(print+" ");
        }
        System.out.print("\nArray B originale con MIn&Max: ");                // foreach stampa dell'array B iniziale
        for (int print : arrayB) {                                          
            System.out.print(print+" ");
        }
        System.out.print("\nArray C originale con MIn&Max: ");                // foreach stampa dell'array C iniziale
        for (int print : arrayC) {                                          
            System.out.print(print+" ");
        }
        System.out.println();
        System.out.print("\nArray A nuovo e senza Min&Max: ");              // foreach stampa dell'array A di return
        for (int print : MinMax.eliminaMinimoMassimo(arrayA)) {             // chiamando il metodo static (non serve istanza)
            System.out.print(print+" ");
        }
        System.out.print("\nArray B nuovo e senza Min&Max: ");              // foreach stampa dell'array B di return
        for (int print : MinMax.eliminaMinimoMassimo(arrayB)) {             // chiamando il metodo static (non serve istanza)
            System.out.print(print+" ");
        }
        System.out.print("\nArray C nuovo e senza Min&Max: ");              // foreach stampa dell'array C di return
        for (int print : MinMax.eliminaMinimoMassimo(arrayC)) {             // chiamando il metodo static (non serve istanza)
            System.out.print(print+" ");
        }
        System.out.println("\n\n***** Soluzione con Classe MinMax2 (ottimizzata) *****");

        System.out.print("\nArray A nuovo e senza Min&Max: ");              // foreach stampa dell'array A di return
        for (int print : MinMax2.eliminaMinimoMassimo(arrayA)) {            // chiamando il metodo static (non serve istanza)
            System.out.print(print+" ");
        }
        System.out.print("\nArray B nuovo e senza Min&Max: ");              // foreach stampa dell'array B di return
        for (int print : MinMax2.eliminaMinimoMassimo(arrayB)) {            // chiamando il metodo static (non serve istanza)
            System.out.print(print+" ");
        }
        System.out.print("\nArray C nuovo e senza Min&Max: ");              // foreach stampa dell'array C di return
        for (int print : MinMax2.eliminaMinimoMassimo(arrayC)) {            // chiamando il metodo static (non serve istanza)
            System.out.print(print+" ");
        }
        System.out.println();
    }
}