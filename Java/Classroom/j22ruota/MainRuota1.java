/*
Bergamasco Daniele IV As - Esercizio 20:  Docente - 22.03.2020

Esercizio sugli array ed input da tastiera - Realizzare il seguente metodo: public void ruotaAvanti(int[] array) 
Il metodo deve modificare l’array ricevuto in ingresso facendo “ruotare” in avanti di una posizione tutti i valori. 
L’ultimo valore dovrà quindi essere spostato all’inizio dell’array. Esempi:   
prima         dopo
{4 9}         {9, 4}
{4, 9, 3}     {3, 4, 9}
{4, 9, 3, 7}  {7, 4, 9, 3}
Creare un main per testarlo con input da tastiera.  */

package j22ruota;
import java.util.*; 


public class MainRuota1 {
    public static void main(String[] args) {

      System.out.println("\n***** Classe Ruota 1° soluzione *****\n");

      int array1[] = {4, 9};                                                // Dichiarazione e inizializzazione di tre array
      int array2[] = {4, 9, 3};
      int array3[] = {4, 9, 3, 7};

      Ruota rt1 = new Ruota(array1);                                      // Dichiarazione e inizializzazione di tre oggetti
      Ruota rt2 = new Ruota(array2);
      Ruota rt3 = new Ruota(array3);

      System.out.print("Array origine:  ");                                     // stampa con foreach dei tre array d'origine
      for(int a1:array1)
        System.out.print(a1+" ");
      System.out.print("/ ");
      for(int a2:array2)
        System.out.print(a2+" ");
      System.out.print("/ ");
      for(int a3:array3)
        System.out.print(a3+" "); 
      
      System.out.println("\n");

      rt1.ruotaAvanti();                                                // chiamata del metodo "ruotaAvanti" per ogni oggetto
      rt2.ruotaAvanti();
      rt3.ruotaAvanti();
      System.out.print("Array ruotati:  ");           // stampa con foreach dei tre array ruotati con il metodo "ruotaAvanti"
      for(int a1:array1)
        System.out.print(a1+" ");
      System.out.print("/ ");
      for(int a2:array2)
        System.out.print(a2+" ");
      System.out.print("/ ");
      for(int a3:array3)
        System.out.print(a3+" ");

      System.out.println("\n");
    }
}