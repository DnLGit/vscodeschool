package j22ruota;

public class Ruota {

    private int array [];
    
    public Ruota(int array[]){
        this.array = array;
    }

    public void ruotaAvanti(){                      // Metodo per spostare in avanti di una posizione gli elementi dell'array
        int tmp = this.array[array.length-1];       // memorizzo l'ultimo elemento in un variabile temporanea
        for(int i=array.length-1; i>0; i--){        // ciclo for che, dall'ultimo elemento dell'array scorre all'inizio ...
            this.array[i]=this.array[i-1];          // ... memorizzando il valore dell'indice sull'elemento successivo
        }
        this.array[0] = tmp;                        // inizializzazione del primo elemento al valore dell'elemento ultimo. 
    }
}