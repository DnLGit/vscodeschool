package j22ruota;
import java.util.*;                                                 // Libreria per utilizzare la classe Scanner e Arrays.


public class MainRuota2 {
    public static void main(String[] args) {

        System.out.println("\n***** Classe Ruota 2° soluzione *****\n");

        Scanner ins = new Scanner(System.in);

        System.out.print("Inserisci prima la dimensione dell'array: ");
        int N = ins.nextInt();                            // inizializzazione variabile dimensione array, input da tastiera.

        int arrayDinamic[] = new int[N];                                // Dichiarazione e istanza dell'array "arrayDinamic"
        
        Ruota rt = new Ruota(arrayDinamic);                                     // Dichiarazione e istanza dell'oggetto "rt"
         

        System.out.println("\nora i " + N + " valori dell'array: ");                  // popolamento dell'array con ciclo for

        for(int i=0; i<N; i++){
            arrayDinamic[i] = ins.nextInt();
        }

        System.out.print("Array origine:  ");                                   // stampa dell'array inserito con foreach
        for(int aD:arrayDinamic)
            System.out.print(aD+" ");
        System.out.println("\n");

        rt.ruotaAvanti();                                             // chiamata del metodo "ruotaAvanti" dalla classe Ruota

        System.out.print("Array ruotato:  ");                                           // Ri-stampa dell'array con foreach
        for(int aD:arrayDinamic)
            System.out.print(aD+" ");
        System.out.println("\n");

        ins.close();
    }
}