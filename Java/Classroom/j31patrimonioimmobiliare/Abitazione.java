/*  Le caratteristiche di un’abitazione sono il numero di stanze, la superficie, l’indirizzo e la città.
    costruttori ed i metodi che permettono di visualizzare le caratteristiche citate
    sovrascrivere un metodo toString che permette di visualizzare la stampa di tutte le caratteristiche
    Realizzare un costruttore che permette di settare tutte le caratteristiche */

package j31patrimonioimmobiliare;

public class Abitazione {                       // superclasse

    protected int stanze, superficieAb;           // variabili d'istanza [protected per le sottoclassi]
    protected String indirizzo, città;

    public Abitazione(){                        // costruttori: questo di default
    }

    public Abitazione(int stanze, int superficieAb, String indirizzo, String città) {
        this.stanze = stanze;
        this.superficieAb = superficieAb;
        this.indirizzo = indirizzo;
        this.città = città;
    }

    // metodo toString
    @Override                   
    public String toString() {
        return " stanze: " + stanze + " |" + " superficie: " + superficieAb + " |" + " indirizzo: " + indirizzo + " |" + " città: " + città;
    }
    

    // metodi Getter&Setter
    public int getStanze() {
        return this.stanze;
    }
    public void setStanze(int stanze) {
        this.stanze = stanze;
    }
    public int getSuperficie() {
        return this.superficieAb;
    }
    public void setSuperficie(int superficieAb) {
        this.superficieAb = superficieAb;
    }
    public String getIndirizzo() {
        return this.indirizzo;
    }
    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }
    public String getCittà() {
        return this.città;
    }
    public void setCittà(String città) {
        this.città = città;
    }
}