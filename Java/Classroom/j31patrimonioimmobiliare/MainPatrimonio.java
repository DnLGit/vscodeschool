/*
Bergamasco Daniele IV As - Esercizio 31:  Patrimonio Immobiliare su Ereditarieta' - 25.05.2020

Si intende realizzare una gerarchia di classi per rappresentare e gestire un patrimonio immobiliare composto da abitazioni, ville e appartamenti. 
Le caratteristiche di un’abitazione da impostare in fase di inizializzazione sono il numero di stanze, la superficie, l’indirizzo e la città. 
Le ville sono caratterizzate inoltre dal numero di piani, dalla superficie del giardino e dal fatto di avere o meno la piscina. 
Un appartamento è caratterizzato dal piano a cui è situato, dal fatto che sia raggiungibile o meno tramite ascensore e dal numero di terrazzi. 
Scrivere il codice java necessario per l’implementazione di tale gerarchia di classi fornendo per Abitazioni, Ville ed Appartamenti i relativi costruttori ed i metodi che permettono di visualizzare le caratteristiche citate. 
Per le classi sovrascrivere un metodo toString che permette di visualizzare la stampa di tutte le caratteristiche della classe. 
Realizzare un costruttore che permette di settare tutte le caratteristiche e scrivere un main che permette di testare i metodi.  */

package j31patrimonioimmobiliare;

public class MainPatrimonio {
    public static void main(String[] args) {

        System.out.println("\n****************** Esercizio 31:  Patrimonio Immobiliare ******************\n");
        

        Abitazione abit = new Abitazione();
        Abitazione abit1 = new Abitazione(4, 120, "via Pompei", "Bolzano");

        Villa vil1 = new Villa(8, 230, "via Ercolano", "Trento", 3, 150, true);
        Appartamento app1 = new Appartamento(5, 80, "via vesuvio", "Torino", 3, 2, true);
        Villa vil2 = new Villa(8, 230, "via Marmolada", "Trento", 2, 150, false);
        Appartamento app2 = new Appartamento(5, 80, "via MonteBianco", "Torino", 9, 2, false);

        System.out.println("abit: "+abit.toString());
        System.out.println("abit1: "+abit1.toString());
        System.out.println();
        System.out.println("vil1: "+vil1.toString());
        System.out.println();
        System.out.println("app1: "+app1.toString());
        System.out.println();
        System.out.println("vil2: "+vil2.toString());
        System.out.println("app2: "+app2.toString());
        System.out.println();

        abit1.setCittà("Firenze");
        abit1.setIndirizzo("via Amalfi, 25");
        abit1.setSuperficie(65);

        vil1.setCittà("Bologna");
        vil1.setIndirizzo("via Aquila, 65");
        vil1.setNumeroPiani(8);

        app1.setCittà("Perugia");
        app1.setStanze(7);
        app1.setIndirizzo("via Trasimeno, 45B");
        app1.setSuperficie(250);

        System.out.println("abit1: "+abit1.toString());
        System.out.println();
        System.out.println("vil1: "+vil1.toString());
        System.out.println();
        System.out.println("app1: "+app1.toString());
    }
}