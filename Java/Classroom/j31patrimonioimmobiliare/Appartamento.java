/*  Un appartamento è caratterizzato dal piano a cui è situato, da avere o meno l'ascensore e dal numero di terrazzi.
    costruttori ed i metodi che permettono di visualizzare le caratteristiche citate
    sovrascrivere un metodo toString che permette di visualizzare la stampa di tutte le caratteristiche
    Realizzare un costruttore che permette di settare tutte le caratteristiche  */

package j31patrimonioimmobiliare;

public class Appartamento extends Abitazione {      // sottoclasse

    private int piano, numeroTerrazzi;              // variabili d'istanza 
    private boolean ascensore;
    private String tempA;                        // variabile temporanea da usare in toString

    public Appartamento() {                         // costruttori: questo di default
    }

    public Appartamento(int stanze, int superficieAb, String indirizzo, String città, int piano, int numeroTerrazzi, boolean ascensore){
        super(stanze, superficieAb, indirizzo, città);
        this.piano = piano;
        this.numeroTerrazzi = numeroTerrazzi;
        this.ascensore = ascensore;
    }

    // metodo toString che richiama con super quello della superclasse 
    public String toString(){
        if(ascensore) tempA = "con ascensore"; else tempA = "senza ascensore";  // condizione per decisione presenza piscina
        return super.toString() + "| e' al "+piano+"° piano | ha "+numeroTerrazzi+" terrazzi | e "+tempA;
    }

    // metodi Getter&Setter
    public int getPiano() {
        return this.piano;
    }
    public void setPiano(int piano) {
        this.piano = piano;
    }
    public int getNumeroTerrazzi() {
        return this.numeroTerrazzi;
    }
    public void setNumeroTerrazzi(int numeroTerrazzi) {
        this.numeroTerrazzi = numeroTerrazzi;
    }
    public boolean isAscensore() {
        return this.ascensore;
    }
    public boolean getAscensore() {
        return this.ascensore;
    }
    public void setAscensore(boolean ascensore) {
        this.ascensore = ascensore;
    }
}