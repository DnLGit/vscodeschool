/*  Le ville sono caratterizzate inoltre dal numero di piani, dalla superficie del giardino e di avere o meno la piscina.
    costruttori ed i metodi che permettono di visualizzare le caratteristiche citate
    sovrascrivere un metodo toString che permette di visualizzare la stampa di tutte le caratteristiche
    Realizzare un costruttore che permette di settare tutte le caratteristiche */

package j31patrimonioimmobiliare;

public class Villa extends Abitazione {         // sottoclasse

    private int numeroPiani, superficieGreen;   // variabili d'istanza 
    private boolean piscina;
    private String tempP;                        // variabile temporanea da usare in toString

    
    

    public Villa() {                             // costruttori: questo di default
    }


    public Villa(int stanze, int superficieAb, String indirizzo, String città, int numeroPiani, int superficieGreen, boolean piscina){
        super(stanze, superficieAb, indirizzo, città);
        this.numeroPiani = numeroPiani;
        this.superficieGreen = superficieGreen; 
        this.piscina = piscina;
    }
    
    // metodo toString che richiama con super quello della superclasse
    public String toString(){
        if(piscina) tempP = "con piscina"; else tempP = "senza piscina";  // condizione per decisione presenza piscina
        return super.toString() + "| numero dei piani: "+numeroPiani+" | giardino di mq: "+superficieGreen+" | e "+tempP;
    }


    // metodi Getter&Setter
    public int getNumeroPiani() {
        return this.numeroPiani;
    }
    public void setNumeroPiani(int numeroPiani) {
        this.numeroPiani = numeroPiani;
    }
    public int getSuperficieGreen() {
        return this.superficieGreen;
    }
    public void setSuperficieGreen(int superficieGreen) {
        this.superficieGreen = superficieGreen;
    }
    public boolean isPiscina() {
        return this.piscina;
    }
    public boolean getPiscina() {
        return this.piscina;
    }
    public void setPiscina(boolean piscina) {
        this.piscina = piscina;
    }   
}