/*
Bergamasco Daniele IV As - Esercizio 9:  Persona con References - 27.02.2020

Scrivere un programma con i seguenti requisiti. 
Utilizzare una classe Persona che dichiara le variabili nome, cognome, età. 
Si dichiari inoltre un metodo dettagli() che restituisce in una stringa le informazioni sulla persona in questione. Utilizzare una classe Principale che, nel metodo main(), istanzia due oggetti chiamati persona1 e persona2 della classe Persona, inizializzando per ognuno di essi i relativi campi. 
Dichiarare un terzo riferimento (persona3) che punti ad uno degli oggetti già istanziati. 
Controllare che effettivamente persona3 punti allo oggetto voluto, stampando i campi di persona3.
*/
package j09personaref;

public class MainPersonaRef {

    public static void main(String[] args) {

        System.out.println("\n******** Classe Persona con References ********\n");

        PersonaRef persona1 = new PersonaRef("Sonia", "Bianchi", 39);                           // creazione oggetto persona1
        PersonaRef persona2 = new PersonaRef("Mario", "Neri", 48);                              // creazione oggetto persona2
        PersonaRef persona3 = persona1;                                                         // creazione oggetto persona3


        System.out.print("\nLa persona 1 è: "+persona1.dettagli());         // chiamata del modulo "dettagli" per i 3 oggetti
        System.out.print("\nLa persona 2 è: "+persona2.dettagli());
        System.out.println("\nAnche la persona 3 (che punta alla persona1) è: "+persona3.dettagli());
        
        System.out.println("\nModifico il nome di persona 3 ... e poi verifico che: "); // chiamata modulo set per modifica
        persona3.setNome("Bianca");

        System.out.print("\nLa persona 3 (che punta alla persona1) ora è: "+persona3.dettagli());
        System.out.println("\nma anche persona 1 (puntata da persona3) ora è: "+persona1.dettagli());

        System.out.println("\nOra modifico il cognome di persona 1 ... e poi verifico che: ");
        persona1.setCognome("Rossi");

        System.out.print("\nLa persona 1 (puntata da persona3) ora è: "+persona1.dettagli());
        System.out.println("\nma anche persona 3 (che punta alla persona1) diventa: "+persona3.dettagli());
    }
}