/*
Bergamasco Daniele IV As - Esercizio 9:  Persona con References - 27.02.2020

- classe Persona con variabili nome, cognome, età. 
- metodo dettagli() che restituisce in una stringa le informazioni sulla persona in questione. 
- classe Principale che, nel metodo main(), istanzia 
- due oggetti chiamati persona1 e persona2 della classe Persona, inizializzando per ognuno di essi i relativi campi. 
- terzo riferimento (persona3) che punti ad uno degli oggetti già istanziati. 
Controllare che effettivamente persona3 punti allo oggetto voluto, stampando i campi di persona3.
*/
package j09personaref;

public class PersonaRef {

    // Variabili d'istanza
    private String nome, cognome;
    private int eta;

    // Costruttori
    public PersonaRef(String nome, String cognome, int eta){
        this.nome = nome;   this.cognome = cognome;
        this.eta = eta; 
    }


    // Metodi Getter e Setter
    public String getNome() {
        return this.nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCognome() {
        return this.cognome;
    }
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    public int getEta() {
        return this.eta;
    }
    public void setEta(int eta) {
        this.eta = eta;
    }

    // altri Metodi                dettagli: ritorna una stringa con tutti gli attributi dell'oggetto!
    public String dettagli(){
        return nome+" "+cognome+" di anni "+eta;
    }


    
}