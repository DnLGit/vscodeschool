/*

Bergamasco Daniele IV As - Esercizio 7:  Cellulare con costruttori e metodi particolari - 20.02.2020

Progettare una classe di nome Cellulare, per rappresentare un telefono cellulare con contratto a ricarica. 
Tale classe prevede due variabili d'istanza. 
La prima variabile d'istanza è definita come private double carica, e rappresenta il quantitativo di euro disponibile 
per le chiamate. 
La seconda variabile d'istanza è definita come private int numeroChiamate, e rappresenta il numero di chiamate effettuate con il cellulare. 
La classe deve implementare un costruttore public Cellulare(double unaCarica), che prende come parametro la quantità di euro della ricarica iniziale. 
La classe deve inoltre implementare i seguenti metodi. 
Un metodo definito come public void ricarica(double unaRicarica), che ricarica il telefonino.
Un metodo definito come public void chiama(double minutiDurata), che effettua una chiamata di durata in minuti specificata dal parametro. Tale metodo dovrà aggiornare la carica disponibile, ed incrementare la memoria contenente il numero di chiamate effettuate dal telefonino. Si assuma un costo di 0.20 euro per ogni minuto di chiamata. 
Un metodo public double numero404(), che restituisce il valore della carica disponibile. 
Un metodo public int getNumeroChiamate(), che restituisce il valore della variabile d'istanza numeroChiamate. 
Infine, un metodo public void azzeraChiamate(), che azzera la variabile contenente il numero di chiamate effettuate dal telefonino.

 */

package j07cellulare;

public class MainCellulare {
    public static void main(String[] args) {
        System.out.println("\n******** Classe Cellulare ********");

        Cellulare samsungA40 = new Cellulare(25.00); // creazione oggetto samsungA40

        // Test telefono: chiamata e verifica credito e numero chiamate
        System.out.println("\nAcquisto nuovo cellulare, trovo però una carica di solo € " + samsungA40.getCarica());
        samsungA40.chiama(3);
        System.out.println("\nProviamo se funziona. \nPrima chiamata: ");
        System.out.print("leggo nel display, Chaimate: n° " + samsungA40.getNumeroChiamate());
        System.out.println(" e credito di € " + samsungA40.numero404());

        // Secondo Test telefono: chiamata più lunga, verifica credito e numero chiamate.
        System.out.println("\nRiproviamo con una chiamata più lunga: ");
        samsungA40.chiama(25);
        System.out.print("Ora leggo nel display, Chaimate: n° " + samsungA40.getNumeroChiamate());
        System.out.println(" e credito di € " + samsungA40.numero404());

        // Terzo Test telefono: chiamata di due minuti, verifica credito e numero
        // chiamate.
        System.out.println("\nRiproviamo con un'altra ancora più lunga: ");
        samsungA40.chiama(120);
        System.out.print("e ora leggo, Chaimate: n° " + samsungA40.getNumeroChiamate());
        System.out.println(" e credito di € " + samsungA40.numero404()); // qui il credito diventa negativo.

        // Quarto Test telefono: ricarica Sim e azzeramento della memoria numero
        // chiamate.
        samsungA40.ricarica(50.00);
        System.out.println("\nAccidenti sono 'sotto', meglio ricaricare la sim, ora è di € " + samsungA40.numero404());
        samsungA40.azzeraChiamate();
        System.out.println("... e meglio anche azzerare le chiamate.");

        // Ultimo Test telefono: verifica credito e numero chiamate.
        System.out.print("\nA questo punto abbiamo la sim con €" + samsungA40.numero404());
        System.out.println(" e " + samsungA40.getNumeroChiamate() + " chiamate");
    }
}