package j07cellulare;

public class Cellulare {
    
    private double carica;                              // quantitativo di euro disponibile
    private int numeroChiamate;                         // numero di chiamate effettuate

    // costruttori
    public Cellulare(double unaCarica){                 // quantità di euro della ricarica iniziale
        this.carica = unaCarica;                        // ----> [in questo caso potrei anche fare a meno del this???]
    }
    
    // Metodi Getter e Setter
    public double getCarica(){
        return this.carica;                             // ----> [anche qui: potrei fare a meno del this???]
    }
    public void setCarica(double carica){
        this.carica = carica;                   
    }
    public int getNumeroChiamate(){             // Metodo che restituisce il valore della variabile d'istanza numeroChiamate
        return this.numeroChiamate;
    }
    public void setNumeroChiamate(int numeroChiamate){
        this.numeroChiamate = numeroChiamate;
    }

    // altri Metodi
    public void ricarica(double unaRicarica){           // Metodo che ricarica il telefonino
        this.carica += unaRicarica;  
    }

    public void chiama(double minutiDurata){            // Metodo che effettua una chiamata 
        this.carica = carica-(minutiDurata*0.20);       // ... aggiorna la carica
        this.numeroChiamate++;                          // e incrementa le chiamate
    }

    public double numero404(){                          // Metodo che restituisce il valore della carica disponibile
        return this.carica;                             // ----> [questo metodo può essere evitato? (usando getCarica)]
    }
    
    public void azzeraChiamate(){                       // Metodo che azzera la variabile del numero di chiamate effettuate
        this.numeroChiamate = 0;
    }
}