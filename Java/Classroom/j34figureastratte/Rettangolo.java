/*  I metodi da gestire sono perimetro e area.  */

package j34figureastratte;

public class Rettangolo extends FigurePiane {

    private double lato1, lato2;


    public Rettangolo() {                                           // costruttori: questo e' quello di default.
    }

    public Rettangolo(double lato1, double lato2) {
        this.lato1 = lato1;
        this.lato2 = lato2;
    }

    public double perimetro(){                                      // implementazione dei metodi astratti per Rettangolo
        return lato1+lato2;
    }
    public double area(){
        return lato1*lato2;
    }

    // metodi Getter&Setter
    public double getLato1() {
        return this.lato1;
    }
    public void setLato1(double lato1) {
        this.lato1 = lato1;
    }
    public double getLato2() {
        return this.lato2;
    }
    public void setLato2(double lato2) {
        this.lato2 = lato2;
    }
}