/*
Bergamasco Daniele IV As - Esercizio 34:  Figure piane con CLASSI ASTRATTE - 28.05.2020

Realizzare una classe astratta per le Figure piane e due sottoclassi, la sottoclasse Quadrato e la sottoclasse Rettangolo. 
I metodi da gestire sono perimetro e area.      */

package j34figureastratte;

public class MainFigurePiane {
    public static void main(String[] args) {
        
        System.out.println("\n****************** Esercizio 34:  Figure piane con CLASSI ASTRATTE ******************\n");

        Quadrato qt = new Quadrato(5);
        Rettangolo rt = new Rettangolo(6, 9);

        System.out.println("Il prerimetro del quadrato è: "+qt.perimetro());
        System.out.println("L'area del quadrato è: "+qt.area());

        System.out.println("mentre");


        System.out.println("Il prerimetro del rettangolo è: "+rt.perimetro());
        System.out.println("L'area del rettangolo è: "+rt.area());
    }
}