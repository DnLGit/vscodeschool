/*  I metodi da gestire sono perimetro e area.  */

package j34figureastratte;

public abstract class FigurePiane {                                 // definita come Superclasse astratta

    public abstract double perimetro();                             // metodi astratti su classe astratta
    public abstract double area();    
}