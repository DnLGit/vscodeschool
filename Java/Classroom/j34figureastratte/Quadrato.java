/*  I metodi da gestire sono perimetro e area.  */

package j34figureastratte;

public class Quadrato extends FigurePiane {

    private double lato;


    public Quadrato() {                                             // costruttori: questo di default
    }

    public Quadrato(double lato) {
        this.lato = lato;
    }

    public double perimetro(){                                      // implementazione dei metodi astratti per Quadrato
        return lato*2;
    }
    public double area(){
        return lato*lato;
    }


    public String toString(){
        return "Il lato del quadrato e': "+lato;
    }

    // metodi Getter&Setter
    public double getLato() {
        return this.lato;
    }
    public void setLato(double lato) {
        this.lato = lato;
    }
}