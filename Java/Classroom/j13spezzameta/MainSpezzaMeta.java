/*
Bergamasco Daniele IV As - Esercizio 13:  Spezza a Metà - 04.03.2020

Scrivere una classe SpezzaMeta che chiede all’utente di inserire una stringa e stampa tale stringa divisa a metà su due righe (se la lunghezza della stringa è dispari una delle due parti sarà più lunga dell’altra di un carattere).   */

package j13spezzameta;
import java.util.*;                                     // importare questa libreria per utilizzare anche la classe Scanner

public class MainSpezzaMeta {
    public static void main(String[] args) {

        System.out.println("\n***** Esercizi con Stringhe:  Spezza a Metà *****\n");

        System.out.print("\nHei, inserisci una frase: ");
        Scanner scrivi = new Scanner(System.in);                      // Warning [Resource leak: 'inserisci' is never closed]
/*
        String frase;
        frase = scrivi.nextLine();
        int tutta = frase.length();                                                     // determino la lunghezza della frase
        String frase1 = frase.substring(0, tutta/2);                                    // ricavo la prima metà della frase
        String frase2 = frase.substring(tutta/2);                                       // ricavo la seconda metà della frase

        System.out.println("La frase è: "+frase);
        System.out.println("\nFrase lunga "+tutta+", di cui una metà è "+tutta/2);
        System.out.println("\nLa frase scissa diventa: \n"+frase1+"\n"+frase2); // stampo le due frasi a capo e concatenate
*/
        String frase = scrivi.nextLine();
        System.out.println("\nLa frase scissa diventa: ");
        System.out.println(frase.substring(0, frase.length()/2)+"\n"+frase.substring(frase.length()/2));
        
        scrivi.close();                                                             // metodo per ovviare al Warning sopra
    }
}