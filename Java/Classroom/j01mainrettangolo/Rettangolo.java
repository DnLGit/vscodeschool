/*
Bergamasco Daniele IV As - Esercizio 1:  
Rettangolo classi e costruttori  06.02.2020 (ultima ora)
 */
package j01mainrettangolo;

public class Rettangolo {

// Variabili d'istanza    
    private int base, altezza;          // dichiarazione variabili d'istanza
  
// Costruttori    
    public Rettangolo(){                // costruttore rettangolo fisso
        base = 5;
        altezza = 10;
    }
    
    public Rettangolo(int base, int altezza){     // costruttore rettangolo variabile
        this.base = base;
        this.altezza = altezza;
    }
    
    // Metodi
    public int getPerimetro(){          // metodo per calcolare il perimetro
        return 2*(base+altezza);
    }
    public int getArea(){               // metodo per calcolare l'area
        return base*altezza;         
    }
    
    public int getBase(){               // metodo per richiamare variabile base
        return base;                
    }
    public int getAltezza(){            // metodo per richiamare variabile altezza
        return altezza;
    }
    
    public void setBase(int base){         // metodo per settare variabile base
        this.base = base;
    }
    public void setAltezza(int altezza){      // metodo per settare variabile altezza
        this.altezza = altezza;
    }
}
