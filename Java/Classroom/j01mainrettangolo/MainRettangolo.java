/*

Bergamasco Daniele IV As - Esercizio 1:  Rettangolo classi e costruttori  06.02.2020 (ultima ora)

Creare una classe Java Rettangolo tale che un oggetto sia definito da: 
  • Un intero per la base 
  • Un intero per l’altezza 
  • Due costruttori: 
    uno che crea rettangoli con dimensioni fissate, 
    il secondo che permette all’utente di inizializzare i valori. 
- Metodi: li dividiamo in più categorie 
  - Metodi contenenti la dicitura get che restituiscono il valore della dimensione richiesta es. getAltezza…)
    oppure effettuano un’operazione specifica (ad es. per il calcolo dell’area o del perimetro) 
  - Metodi contenenti la dicitura set permettono di assegnare un nuovo valore alla variabile d’istanza (ad es. setAltezza)
Creare poi un main che crei un oggetto rettangolo e ne visualizzi perimetro e area.

 */
package j01mainrettangolo;

public class MainRettangolo {
  

  public static void main(String[] args) {

    System.out.println("\n******** Classe di un Rettangolo ********");
    
    // istanze alla classe Rettangolo
    Rettangolo retFix = new Rettangolo();     // oggetto Rettangolo fisso
    Rettangolo retVari = new Rettangolo(2,3); // oggetto Rettangolo variabile
        
    System.out.println("\nCon i valori fissi di base "+retFix.getBase()+" e altezza "+retFix.getAltezza());            
    System.out.println("\nIl perimetro del rettangolo fisso è: "+retFix.getPerimetro());
    System.out.println("L'area del rettangolo fisso è: "+retFix.getArea());
    
    System.out.println("\nCon i valori di base "+retVari.getBase()+" e altezza "+retVari.getAltezza());
    System.out.println("\nIl perimetro del rettangolo variabile è: "+retVari.getPerimetro());
    System.out.println("L'area del rettangolo variabile è: "+retVari.getArea());
    
    retVari.setBase(15);                      // prima modifica base rettangolo
    retVari.setAltezza(25);                   // prima modifica altezza rettangolo
      
    System.out.println("\nDopo il primo set la nuova base è: "+retVari.getBase());
    System.out.println("e la nuova altezza è: "+retVari.getAltezza());
      
    System.out.println("\n... e risulta quindi che: ");
    System.out.println("\nIl nuovo perimetro del rettangolo è: "+retVari.getPerimetro());
    System.out.println("L'area del rettangolo ora è: "+retVari.getArea());
      
    retVari.setBase(100);                   // seconda modifica base rettangolo
    retVari.setAltezza(250);                // seconda modifica altezza rettangolo
    
    System.out.println("\n... e settando nuovamente: ");
    System.out.println("\nL'ultimo valore di base è: "+retVari.getBase());
    System.out.println("L'ultimo valore dell'altezza è: "+retVari.getAltezza());
 
    }
    
}
