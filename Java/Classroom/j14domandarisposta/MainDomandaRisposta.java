/*
Bergamasco Daniele IV As - Esercizio 14:  Domanda e Risposta - 04.03.2020

Scrivere una classe DomandaRisposta che chiede all’utente di inserire una stringa, 
ricava l’ultimo carattere di tale stringa e 
• se tale carattere è un punto interrogativo stampa: Non saprei... 
• se tale carattere è un punto esclamativo stampa: Hai proprio ragione!
• altrimenti stampa Mmmm, non mi convince...   */

package j14domandarisposta;
import java.util.*;                                     // importare questa libreria per utilizzare anche la classe Scanner

public class MainDomandaRisposta {

    public static void main(String[] args) {

        System.out.println("\n***** Esercizi con Stringhe:  Spezza a Metà *****\n");

        System.out.print("\nHei, inserisci una frase: ");

        Scanner scrivi = new Scanner(System.in);                      // Warning [Resource leak: 'inserisci' is never closed]

        String frase = scrivi.nextLine();

        String ultimoCar = frase.substring(frase.length()-1);           // estrapolo l'ultimo carattere con la substringa 

        if(ultimoCar.equals("?"))
            System.out.println("\nNon saprei ...");
        else if(ultimoCar.equals("!"))
            System.out.println("\nHai proprio ragione!");
        else
            System.out.println("\nMmmm, non mi convince...");

        scrivi.close();                                                             // metodo per ovviare al Warning sopra
    }
}