/*
Bergamasco Daniele IV As - Esercizio 20:  Leggi gli Elementi - 06.03.2020

Esercizio su array ed input da tastiera
Scrivere un programma che legge da input n elementi interi(n deciso da tastiera), li memorizza in un array e stampa l’elemento con occorrenze maggiore.   */

package j20leggielementi;
import java.util.Scanner;                                                       // libreria per utilizzare la classe Scanner

public class MainLeggiElementi1 {

    public static void main(String[] args) {

        System.out.println("\n***** Esercizi con Stringhe:  Legggi gli Elementi v.01 *****\n");

        Scanner leggi = new Scanner(System.in);                     // Warning [Resource leak: 'inserisci' is never closed]
        
        System.out.print("Inserisci la dimensione dell'array: ");
        int N = leggi.nextInt();                                        // metodo .nextInt per inserire un intero da tastiera
        int harry [] = new int[N];                                          // dichiarazione e creazione dell'array "harry"!
        
        System.out.println("\nOra riempi l'array di "+ N +" elementi interi");

        int Nc = 0;                                                     // variabile temporanea per trovare il valore massimo
        for(int i=0; i<N; i++){                                                             // ciclo for per riempire l'array
            System.out.print(i+1+"^ elemento: ");
            harry[i]=leggi.nextInt();
            if(harry[i] > Nc)
                Nc = harry[i]+1;                       // Nc sarà la dimensione di un nuovo array per calcolare le occorrenze
        }
        
        int contador [] = new int[Nc];              // dichiarazione e creazione dell'array "contador" ... per le occorrenze!

        System.out.println("\nQuesto è l'array harry: ");

        for(int element:harry){                                             // uso foreach per la stampa dell'array "harry"
            System.out.print(element+"\t");
        }

        System.out.println("\n\ne questo è l'array contador (a zero!!): ");
        for(int element : contador){                                        // ... per la stampa dell'array "contador" vuoto
            System.out.print(element+"\t");
        }

        for(int i=0; i<N; i++){             // ciclo for per popolare l'array "contador" con le occorrenze dell'array "harry"
            contador[harry[i]] +=1;                 // inserisco le occorrenze al corrispondente indice dei valori di harry
        }

        System.out.println("\n\nora l'array contador ha le occorrenze: ");
        for(int element : contador){                            // ... e anche per la stampa dell'array "contador" riempito
            System.out.print(element+"\t");
        }

        int pos=0, occ=0;                           // ciclo for per determinare posizione e valore della occorrenza maggiore
        for(int i=0; i<Nc; i++){
            if(contador[i] > occ){
                pos = i;                                               // "pos" mi darà in realtà il valore delle occorrenze 
                occ = contador[i];                                          // e "occ" invece il massimo numero di occorrenze
            }
        }
        System.out.println("\n\nIl valore con occorrenze maggiori è "+pos+" ed è apparso "+occ+" volte!\n");
        leggi.close();                                                  // il metodo "leggi.close()" elimina il Warning sopra
    }
}