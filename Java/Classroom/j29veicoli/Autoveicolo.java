/*  - Mentre gli autoveicoli da: il numero delle porte.  */

package j29veicoli;

public class Autoveicolo extends Veicolo{                       // sottoclasse estesa

    private int numPorte;

    public Autoveicolo(String targa, String marca, String modello, int numPosti, int numPorte){
        super(targa, marca, modello, numPosti);                 // attributi della superclasse
        this.numPorte = numPorte;                               // aggiunto quello della sottoclasse
    }

    // Getter&Setter:
    public int getNumPorte() {
        return this.numPorte;
    }
    public void setNumPorte(int numPorte) {
        this.numPorte = numPorte;
    }
}