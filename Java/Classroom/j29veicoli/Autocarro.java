/*  - Gli autocarri sono caratterizzati inoltre da: la capacità massima di carico in quintali   */

package j29veicoli;

public class Autocarro extends Veicolo{                              // sottoclasse estesa

    private int portata;

    public Autocarro(String targa, String marca, String modello, int numPosti, int portata){
        super(targa, marca, modello, numPosti);                     // attributi della superclasse
        this.portata = portata;                                     // aggiunto quello della sottoclasse
    }


    // Getter&Setter:
    public int getPortata() {
        return this.portata;
    }
    public void setPortata(int portata) {
        this.portata = portata;
    }
}