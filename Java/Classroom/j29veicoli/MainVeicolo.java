/*
Bergamasco Daniele IV As - Esercizio 29:  Veicoli con ereditarietà - 22.05.2020

- Si vogliono realizzare una o più classi che permettano di modellare diverse tipologie di veicolo: autocarri e autoveicoli.
- Tutti i veicoli sono caratterizzati da:
    - targa
    - marca
    - modello
    - numero posti
- Gli autocarri sono caratterizzati inoltre da:
    - la capacità massima di carico in quintali
- Mentre gli autoveicoli da:
    - il numero delle porte.  */

package j29veicoli;

public class MainVeicolo {
    public static void main(String[] args) {

        System.out.println("\n****************** Classi Veicolo Automobili/Autocarri ******************\n");

        Veicolo veic = new Veicolo("DD547KK", "Giugizu", "street", 5);              // istanza di oggetto Veicolo/veic

        Autoveicolo auto1 = new Autoveicolo("GG598KK", "Isuzu", "China", 7, 5);     // istanza di oggetto Autoveicolo/auto1 
        Autoveicolo auto2 = new Autoveicolo("KK895GG", "Usizu", "Noky", 11, 5);     // istanza di oggetto Autoveicolo/auto2 

        Autocarro car1 = new Autocarro("TT55RR", "Bull", "Hill", 2, 35);            // istanza di oggetto Autocarro/car1 
        Autocarro car2 = new Autocarro("RR55TT", "Dozer", "Mont", 3, 35);           // istanza di oggetto Autocarro/car2

        System.out.println();

        veic.dammiTutto();                       // invocazione metodo per stampare tutte le variabili del Veicolo veic      
        System.out.println(veic.toString()+"\n");     // stampa variabili con toString

        auto1.dammiTutto();                // invocazione metodo per stampare tutte le variabili dell'AutoVeicolo auto1      
        System.out.println(auto1.toString()+"\n");     // stampa variabili con toString

        auto2.dammiTutto();                // invocazione metodo per stampare tutte le variabili dell'AutoVeicolo auto2      
        System.out.println(auto2.toString()+"\n");     // stampa variabili con toString

        car1.dammiTutto();                 // invocazione metodo per stampare tutte le variabili dell'Autocarro car1      
        System.out.println(car1.toString()+"\n");     // stampa variabili con toString

        car2.dammiTutto();                 // invocazione metodo per stampare tutte le variabili dell'Autocarro car2      
        System.out.println(car2.toString());     // stampa variabili con toString        
    }
}