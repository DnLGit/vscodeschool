/*  - Tutti i veicoli sono caratterizzati da: targa/marca/modello/numero posti  */

package j29veicoli;

public class Veicolo {                                                                              // superclasse

    private String targa, marca, modello;
    private int numPosti;


    public Veicolo(String targa, String marca, String modello, int numPosti){                       // costruttore
        this.targa = targa; this.marca = marca; this.modello = modello; this.numPosti = numPosti;
    }
    
    // metodo che mi stampa direttamente tutte le variabili all'interno dei parametri senza usare il System.out
    public void dammiTutto (){
        System.out.println("targa: " + targa + "| marca: "+marca +"| modello: "+ modello +"| numero dei posti "+ numPosti);
    }

    public String toString(){           // metodo toString: [override] per stampare nel main le variabili con System.out
        return "TO STRING --> targa: " + targa + "| marca: "+ marca +"| modello: "+ modello +"| numero dei posti "+ numPosti;
    }

    // metodi Getter&Setter:
    public String getTarga() {
        return this.targa;
    }
    public void setTarga(String targa) {
        this.targa = targa;
    }
    public String getMarca() {
        return this.marca;
    }
    public void setMarca(String marca) {
        this.marca = marca;
    }
    public String getModello() {
        return this.modello;
    }
    public void setModello(String modello) {
        this.modello = modello;
    }
    public int getNumPosti() {
        return this.numPosti;
    }
    public void setNumPosti(int numPosti) {
        this.numPosti = numPosti;
    }
}