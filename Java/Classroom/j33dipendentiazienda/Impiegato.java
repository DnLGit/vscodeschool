/*  - Impiegato: ogni impiegato lavora in un ufficio ed ha un numero di telefono    */

package j33dipendentiazienda;

public class Impiegato extends Dipendente{              // definita come Sottoclasse (ma superclasse di Dirigente)

    protected String ufficio, numTelefono;              // variabili d'istanza [protected per la sottoclasse Dirigente]


    public Impiegato() {                                // costruttori: questo di default
    }

    public Impiegato(String identificativo, String nome, String cognome, double stipendio, String ufficio, String numTelefono) {
        super(identificativo, nome, cognome, stipendio);
        this.ufficio = ufficio;
        this.numTelefono = numTelefono;
    }

    // metodo toString
    public String toString(){
        return "Impiegato con "+super.toString()+" | ufficio: " +ufficio+" | Telefono: " +numTelefono;
    }
    
    // metodi Getter&Setter
    public String getUfficio() {
        return this.ufficio;
    }
    public void setUfficio(String ufficio) {
        this.ufficio = ufficio;
    }
    public String getNumTelefono() {
        return this.numTelefono;
    }
    public void setNumTelefono(String numTelefono) {
        this.numTelefono = numTelefono;
    }
}