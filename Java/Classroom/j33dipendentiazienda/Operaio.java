/*  Operai: ogni operaio appartiene ad un settore   */

package j33dipendentiazienda;

public class Operaio extends Dipendente {                       // definita come Sottoclasse

    private String settore;                                     // variabile d'istanza

    public Operaio() {                                          // costruttori: questo di default
    }

    public Operaio(String identificativo, String nome, String cognome, double stipendio, String settore) {
        super(identificativo, nome, cognome, stipendio);        // costruttore della superclasse
        this.settore = settore;
    }

    // metodo toString
    public String toString(){
        return "Operaio con "+super.toString()+" | settore: " +settore;
    }


    // metodi Getter&Setter
    public String getSettore() {
        return this.settore;
    }
    public void setSettore(String settore) {
        this.settore = settore;
    }
}