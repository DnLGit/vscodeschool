/*  Ogni dipendente e’ caratterizzato da: un identificativo, un nome, un cognome
    ha uno stipendio che dipende dalla sua tipologia   */

package j33dipendentiazienda;

public class Dipendente {                               // definita come Superclasse

    protected String identificativo, nome, cognome;     // variabili d'istanza [protected per le sottoclassi]
    protected double stipendio;

    public Dipendente() {                               // costruttori: questo di default
    }

    public Dipendente(String identificativo, String nome, String cognome, double stipendio) {
        this.identificativo = identificativo;
        this.nome = nome;
        this.cognome = cognome;
        this.stipendio = stipendio;
    }

    // metodo toString
    public String toString(){
        return "ID: "+identificativo+ "| nome: " + nome + " | cognome: "+ cognome+" | stipendio: " +stipendio;
    }

    // metodi Getter&Setter
    public String getIdentificativo() {
        return this.identificativo;
    }
    public void setIdentificativo(String identificativo) {
        this.identificativo = identificativo;
    }
    public String getNome() {
        return this.nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCognome() {
        return this.cognome;
    }
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    public double getStipendio() {
        return this.stipendio;
    }
    public void setStipendio(double stipendio) {
        this.stipendio = stipendio;
    }
}