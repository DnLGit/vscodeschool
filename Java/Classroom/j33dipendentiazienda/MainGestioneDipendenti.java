/*
Bergamasco Daniele IV As - Esercizio 33:  Gestione dipendenti azienda con EREDITARIETA’ - 28.05.2020

Realizzare un insieme di classi in relazione gerarchica che modellino diversi tipi di “dipendenti” di un’azienda.
Ogni dipendente e’ caratterizzato da: un identificativo, un nome, un cognome.
I dipendenti possono essere:
- Operai: ogni operaio appartiene ad un settore
- Impiegato: ogni impiegato lavora in un ufficio ed ha un numero di telefono
- Dirigente: ogni dirigente ha un ufficio, un numero di telefono e una macchina aziendale
- Ogni dipendente ha uno stipendio che dipende dalla sua tipologia.
Realizzare un main per testare l’applicativo      */

package j33dipendentiazienda;

public class MainGestioneDipendenti {
    
    public static void main(String[] args) {

        System.out.println("\n****************** Esercizio 33:   Gestione dipendenti azienda ******************\n");

        Operaio op = new Operaio("OP555TEC", "Tony", "Gira", 1000, "Torneria");
        Operaio op1 = new Operaio("OP666LM", "Herry", "Potter", 950, "Laminatoio");
        Impiegato imp = new Impiegato("IM666TEC", "Imerio", "Picci", 2000, "Tecnico", "+39 011 56871 (int.02)");
        Dirigente dir = new Dirigente("DG999UK", "Jonny", "Stecchino", 3000, "Estero", "+044 589 69 (int.03)", "BMW", "HR689FG");

        System.out.println(op.toString());
        System.out.println(op1.toString());
        System.out.println(imp.toString());
        System.out.println(dir.toString());

        System.out.println();
        op1.setStipendio(1100);

        System.out.println(op.toString());
        System.out.println(op1.toString());
        System.out.println(imp.toString());
        System.out.println(dir.toString());
    }
}