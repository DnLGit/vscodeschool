/*  - Dirigente: ogni dirigente ha un ufficio, un numero di telefono e una macchina aziendale   */

package j33dipendentiazienda;

public class Dirigente extends Impiegato {                      // definita come Sottoclasse di altra derivata

    private String autoAziendale, targa;                        // variabile d'istanza


    public Dirigente() {                                        // costruttori: questo di default
    }

    public Dirigente(String identificativo, String nome, String cognome, double stipendio, String ufficio, String numTelefono, String autoAziendale, String targa) {
        super(identificativo, nome, cognome, stipendio, ufficio, numTelefono);
        this.autoAziendale = autoAziendale; this.targa = targa;
    }

// metodo toString
public String toString(){
    return "Dirigente "+super.toString()+" | con auto aziendale: " +autoAziendale+" | Targata: " +targa;
}
    

// metodi Getter&Setter
    public String getAutoAziendale() {
        return this.autoAziendale;
    }
    public void setAutoAziendale(String autoAziendale) {
        this.autoAziendale = autoAziendale;
    }
    public String getTarga() {
        return this.targa;
    }
    public void setTarga(String targa) {
        this.targa = targa;
    }
}