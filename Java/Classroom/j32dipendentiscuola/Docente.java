/*  - Ogni docente ha: nome, cognome, sesso, data di nascita, numero ore di docenza e stipendio. */

package j32dipendentiscuola;

public class Docente {                                  // definita come Superclasse
    protected String nome, cognome, dataNascita, sesso; // variabili d'istanza [protected per le sottoclassi]
    protected int numeroOre;
    protected double stipendio;

    public Docente() {                                  // costruttori: questo di default
    }

    public Docente(String nome, String cognome, String dataNascita, String sesso, int numeroOre, int stipendio) {
        this.nome = nome;
        this.cognome = cognome;
        this.dataNascita = dataNascita;
        this.sesso = sesso;
        this.numeroOre = numeroOre;
        this.stipendio = stipendio;
    }

    // metodo toString
    public String toString(){
        return "nome: " + nome + " | cognome: "+ cognome+" | data: " +dataNascita+" | sesso: "+sesso+" | ore: "+numeroOre+" | stipendio: "+stipendio;
    }

    // metodi Getter&Setter
    public String getNome() {
        return this.nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCognome() {
        return this.cognome;
    }
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    public String getDataNascita() {
        return this.dataNascita;
    }
    public void setDataNascita(String dataNascita) {
        this.dataNascita = dataNascita;
    }
    public String getSesso() {
        return this.sesso;
    }
    public void setSesso(String sesso) {
        this.sesso = sesso;
    }
    public int getNumeroOre() {
        return this.numeroOre;
    }
    public void setNumeroOre(int numeroOre) {
        this.numeroOre = numeroOre;
    }
    public double getStipendio() {
        return this.stipendio;
    }
    public void setStipendio(double stipendio) {
        this.stipendio = stipendio;
    }
}