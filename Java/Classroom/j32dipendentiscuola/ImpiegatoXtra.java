/*  Impiegato Xtra: ha ore di straordinario mensili e una retribuzione oraria (stessa per tutti gli impiegatiXtra).
    Lo stipendio integra il pagamento delle ore straordinarie effettuate.    */

package j32dipendentiscuola;

public class ImpiegatoXtra extends Docente{                                 // Sottoclasse

    protected double retribuzioneOra;                                   // variabile d'istanza [protected per il main]


    public ImpiegatoXtra() {                                                // costruttori: questo di default
    }

    public ImpiegatoXtra(String nome, String cognome, String dataNascita, String sesso, int numeroOre, int stipendio, double retribuzioneOra) {
        super(nome, cognome, dataNascita, sesso, numeroOre, stipendio);
        this.retribuzioneOra = retribuzioneOra;
        this.calcoloPagaMese(); 
    }

    // metodo toString
    public String toString(){
        return super.toString()+"| paga oraria: "+retribuzioneOra;
    }
    
    public void calcoloPagaMese(){                                          // metodo che modifica lo stipendio
        this.stipendio = (stipendio+(retribuzioneOra*numeroOre));
    }

    public double calcoloPagaAnno(){                                        // metodo che calcola lo stipendio
        return stipendio+(12*(stipendio+(retribuzioneOra*numeroOre)));
    }

    // metodi Getter&Setter
    public double getRetribuzioneOra() {
        return this.retribuzioneOra;
    }
    public void setRetribuzioneOra(double retribuzioneOra) {
        this.retribuzioneOra = retribuzioneOra;
    }
}