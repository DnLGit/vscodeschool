/*
Bergamasco Daniele IV As - Esercizio 32:  Gestione dipendenti scuola su EREDITARIETA’ - 27.05.2020

Si intende realizzare una gerarchia di classi per rappresentare e gestire i dipendenti di una scuola. 
Docenti, Impiegati ed Impiegati che effettuano straordinari. 
- Ogni docente ha: nome, cognome, sesso, data di nascita, numero ore di docenza e stipendio. 
    Tali info devono essere impostate in fase di inizializzazione di un oggetto Docente. 
- Ogni impiegato è caratterizzato: dal nominativi, dal sesso, dalla data di nascita, dal livello e dallo stipendio mensile.
    Queste informazioni devono essere impostate in fase di inizializzazione di un oggetto "Impiegato".
- Un Impiegato che effettua straordinari e' un impiegato a cui viene attribuito un certo numero di ore di straordinario
    mensili e una relativa retribuzione oraria da impostare in fase di inizializzazione: la retribuzione oraria e' sempre la stessa per tutti gli impiegati che effettuano straordinari e il loro stipendio integra il pagamento delle ore straordinarie effettuate.
Implementare le classi java specificando costruttori e metodi di accesso agli attributi sovrascrivendo il metodo toString che visualizza una stampa di tutte le caratteristiche di ogni classe.     */

package j32dipendentiscuola;

public class MainGestioneDipendenti {
    public static void main(String[] args) {

        System.out.println("\n****************** Esercizio 32:   Gestione dipendenti scuola ******************\n");

        Docente doc1 = new Docente();
        Docente doc2 = new Docente("Pinco", "Palla"," 6 nov 65", "M", 35, 1000);
        ImpiegatoXtra ix = new ImpiegatoXtra("Tony", "Manero", "01/01/01", "M", 25, 1200, 5);

        System.out.println(doc1.toString());
        System.out.println(doc2.toString());

        System.out.println();

        System.out.println(ix.toString());

        ix.calcoloPagaMese();

        System.out.println(ix.toString());
        System.out.print("\nVediamo quindi la paga mensile dell'impegato con straordiari: ");
        System.out.print(ix.stipendio);
        System.out.print("\ne simuliano la paga annuale dell'impegato con straordiari: ");
        System.out.println(ix.calcoloPagaAnno());

        ix.stipendio = (5*(3*(ix.retribuzioneOra*ix.numeroOre)));
        System.out.print("\nRiformulato lo stipendio rivediamo la paga dell'impegato con straordiari: ");
        System.out.println(ix.stipendio);

        System.out.println();
    }
}