/*  Ogni impiegato ha: nominativo, sesso, data di nascita, livello e stipendio mensile. */

package j32dipendentiscuola;

public class Impiegato extends Docente {                        // Sottoclasse

    private int livello;


    public Impiegato() {                                        // costruttori: questo di default
    }


    public Impiegato(String nome, String cognome, String dataNascita, String sesso, int numeroOre, int stipendio, int livello) {
        super(nome, cognome, dataNascita, sesso, numeroOre, stipendio);
        this.livello = livello;
    }

    // metodo toString
    public String toString(){
        return super.toString()+"| livello: "+livello;
    } 
 
    // metodi Getter&Setter
    public int getLivello() {
        return this.livello;
    }
    public void setLivello(int livello) {
        this.livello = livello;
    }
}