/*
Bergamasco Daniele IV As - Esercizio 15:  Rimpiazza - 04.03.2020

Scrivere un programma Rimpiazza che legge una stringa e due caratteri, e poi stampa la stringa in cui tutte le occorrenze del primo carattere sono sostituite dal secondo.   */

package j15rimpiazza;

import java.util.*;                                                     // libreria per utilizzare anche la classe Scanner


public class MainRimpiazza {

    public static void main(String[] args) {

        System.out.println("\n***** Esercizi con Stringhe:  Rimpiazza *****\n");

        System.out.print("\nHei, inserisci una frase: ");

        Scanner leggi = new Scanner(System.in);                      // Warning [Resource leak: 'inserisci' is never closed]

        String frase = leggi.nextLine();                                                            // per esempio "trottola"

        System.out.print("\nOra inserisci i due caratteri per lo scambio: ");
        String unodueChar = leggi.next();                                                               // per esempio "oa"

        String unoChar = unodueChar.substring(0,1);                             // estraggo e memorizzo il primo carattere
        String dueChar = unodueChar.substring(1);                               // estraggo e memorizzo il secondo carattere

        String fraseReplace = frase.replaceAll(unoChar, dueChar);                   // scambio il carattere uno con il due
        System.out.print("\nDopo lo scambio la nuova frase è: ");   // (avrei potuto anche fare a meno delle due variabili)
        System.out.println(fraseReplace);                           

        leggi.close();                                                  // Scanner viene chiuso e si elimina il Warning sopra
    }
}