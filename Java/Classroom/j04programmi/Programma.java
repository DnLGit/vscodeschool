/**

Bergamasco Daniele IV As - Esercizio 4:  Programmi con classi e costruttori/copia  11.02.2020

 */

package j04programmi;
 
public class Programma {

// variabili d'istanza:
    private String denominazione, produttore, versione, sistemaOperativo;
    private int anno;

// costruttori
    public Programma(String denominazione, String produttore,String versione, 
                                            String sistemaOperativo, int anno){
        this.denominazione = denominazione; this.produttore = produttore; 
        this.versione = versione; this.sistemaOperativo = sistemaOperativo; 
        this.anno = anno;
    }
 
    public Programma(Programma newPro1){                
        this.denominazione = newPro1.denominazione;
        this.produttore = newPro1.produttore;
        this.versione = newPro1.versione;
        this.sistemaOperativo = newPro1.sistemaOperativo;
        this.anno = newPro1.anno;
    }

/* Metodi   */

    // Metodi Getter:
    public String getDenominazione(){
        return denominazione; 
    }
    public String getProduttore(){
        return produttore;
    }
    public String getVersione(){
        return versione;
    }
    public String getSistemaOperativo(){
        return sistemaOperativo;
    }
    public int getAnno(){
        return anno;
    }

    // Metodi Setter:
    public void setDenominazione(String denominazione){
        this.denominazione = denominazione; 
    }
    public void setProduttore(String produttore){
        this.produttore = produttore;
    }
    public void setVersione(String versione){
        this.versione = versione;
    }
    public void setSistemaOperativo(String sistemaOperativo){
        this.sistemaOperativo = sistemaOperativo;
    }
    public void setAnno(int anno){
        this.anno = anno;
    }

    // Metodi altri:


    public String toString(){
        return  "nome: "+ denominazione +" Prodotto da: "+ produttore + "\n   la versione è la " + versione 
        + ", gira su Os " + sistemaOperativo + ", rilasciato nell'anno "+ anno;
    }

    public boolean confrontaAnno(Programma newPro1){        // comparazione con booleano
        return this.anno < newPro1.anno;
    }
    
    public String confrontaAnno3(Programma newPro1){        // comparazione con if/else
        if(this.anno < newPro1.anno)
            return "Il software è più recente\n";
        else
            return "Il software non è più recente\n";
    }
}

