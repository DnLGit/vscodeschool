/*

Bergamasco Daniele IV As - Esercizio 4:  Programmi con classi e costruttori/copia  11.02.2020

Definire in linguaggio Java una classe che rappresenti programmi per computer. 
Ogni oggetto deve avere i seguenti attributi: denominazione, produttore, 
versione, sistema operativo, anno. 
La classe deve avere i seguenti metodi e costruttori:
- costruttore che ha come parametri "denominazione, produttore, versione, sistema operativo, anno";
- costruttore di copia;
- "getDenominazione, getProduttore, getVersione, getSistema" e "getAnno" che restituiscono i valori degli attributi relativi;
- "setDenominazione, setProduttore, setVersione, setSistema" e "setAnno" che modificano i valori degli attributi relativi;
- toString che restituisce una stringa con tutti i dati dell'oggetto su cui è invocato;
- compareAnno che consente di confrontare l'anno di rilascio del programma con l'anno di rilascio di un altro programma.
Scrivere un main per testare la correttezza.

 */

package j04programmi;

public class MainProgramma {
    public static void main(String[] args) {

    System.out.println("\n******** Classe di un Programma ********\n");

    Programma soft = new Programma("DomoTech", "BiMar", "0.1.18", "Win10", 2017);
    
    Programma soft1 = new Programma(soft);

    // 1) esempio chiamata variabili d'istanza tramite metodi Getter
    System.out.println("1) Il software "+soft.getDenominazione()+" rilasciato invece nel "+soft.getAnno()+"\n");  
    soft.setAnno(2018);

    System.out.println("\n2) Il software ha le seguenti caratteristiche: "+ soft.toString());   // 2) qui con metodo toString
    
    soft1.setAnno(2019);                                                        // Getter e Setter della variabile "anno"
    System.out.println("\nUltimo aggiornamento nel " + soft1.getAnno());
    
    // assegnato alla variabile "boolPrint" il valore del metodo "confrontoAnno"  
    String boolPrint = soft.confrontaAnno(soft1) ? "\nanno minore" : "\nanno maggiore";  // operatore ternario (suggerimento)
    System.out.println(boolPrint);
    
    if(soft.confrontaAnno(soft1))
        System.out.println("\nè la prima relase");
    else
        System.out.println("\nè una relase successiva");
    
    
    System.out.println("\nconfronto Anno 3 risulta: "+soft.confrontaAnno3(soft1));
   
    }
}