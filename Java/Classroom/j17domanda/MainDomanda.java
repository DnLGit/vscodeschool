/*
Bergamasco Daniele IV As - Esercizio 17:  Domanda - 05.03.2020

Esercizio su input da tastiera e uguaglianza di stringhe
Scrivere un programma che stampi la domanda “Vuoi continuare?” e che attenda dati dall’utente. 
Se l’utente immette “S”, “Sì”, “Ok”, “Certo” o “Perché no?”, stampare “OK”. 
Se l’utente scrive “N” o “No”, stampare “Fine”. 
Negli altri casi, stampare “Dato non corretto”. 
Non considerare differenze tra maiuscolo e minuscolo, quindi anche “s” e “sì” sono validi.   */

package j17domanda;
import java.util.*;                                                 // libreria per utilizzare anche la classe Scanner


public class MainDomanda {
    public static void main(String[] args) {

        System.out.println("\n***** Esercizi con Stringhe:  Parole quasi Uguali *****\n");
        Scanner rispondi = new Scanner(System.in);                   // Warning [Resource leak: 'inserisci' is never closed]
        
        System.out.println("\nVuoi continuare?");
        String risposta = rispondi.nextLine();

        if(risposta.equalsIgnoreCase("s")||risposta.equalsIgnoreCase("si")||risposta.equalsIgnoreCase("ok")||risposta.equalsIgnoreCase("certo")||risposta.equalsIgnoreCase("perche' no?")) // Scanner non "prende" gli accenti, è corretto?
        System.out.println("Ok!!!");
        else if(risposta.equalsIgnoreCase("n")||risposta.equalsIgnoreCase("no"))
        System.out.println("Fine!!!");
        else System.out.println("Dato non corretto!");

        rispondi.close();                                           // il metodo "rispondi.close()"" elimina il Warning sopra
    }
}