/*
Bergamasco Daniele IV As - Esercizio 11:  ParoleUguali - 01.03.2020

Esercizio banale su stringhe ed input da tastiera
Scrivere una classe ParoleUguali che chiede all’utente di inserire due singole parole 
e stampa "Sono uguali!"" se le due parole sono uguali, o "Non sono uguali!" altrimenti.   */

package j11paroleuguali;

import java.util.Scanner;                       // Libreria per utilizzare la classe "Scanner".

public class MainParoleUguali {

    public static void main(String[] args) {

        System.out.println("\n***** Esercizi con Stringhe e inPut da tastiera *****\n");
        
        Scanner input = new Scanner(System.in);                       // Warning [Resource leak: 'input' is never closed]
        System.out.print("\nIniserisci la prima parola: ");
        String parola1 = input.nextLine();
        System.out.print("\nInserisci la seconda parola: ");
        String parola2 = input.nextLine();
        input.close();                                                              // metodo per ovviare al warning sopra

        System.out.println("\nTEST -> parola1 è: "+parola1+" -> parola2 è: "+parola2);        // test stampa delle due parole

        boolean uguali = parola1.equals(parola2);               // potrei inserire forse il boolean già all'interno dell'if??

        if(uguali)
        System.out.println("\nLe parole sono uguali!!");
        else System.out.println("\nLe parole NON sono uguali!!");
    }
}