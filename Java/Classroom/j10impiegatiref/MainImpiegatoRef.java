/*
Bergamasco Daniele IV As - Esercizio 10:  Impiegati [con References] - 27.02.2020

Scrivere un programma con i seguenti requisiti. 
Utilizzare una classe Impiegato che dichiara le variabili nome, cognome, salario. 
Si dichiari un metodo dettagli() che restituisce in una stringa le informazioni sulla persona in questione. 
Si dichiari un metodo aumentasalario() che aumenti lo stipendio secondo una certa percentuale. 
Utilizzare una classe Principale che, nel metodo main(), istanzia due oggetti chiamati impiegato1 e impiegato2 della classe Impiegato, inizializzando per ognuno di essi i relativi campi. 
Si aumenti il salario di impiegato1 del 10% e 
poi si verifichi se il salario di impiegato 1 è maggiore di quello di impiegato2.   */

package j10impiegatiref;

public class MainImpiegatoRef {
    public static void main(String[] args) {


        System.out.println("\n******** Classe Impiegato [con References] ********\n");

        ImpiegatoRef impiegato1 = new ImpiegatoRef("Edoardo", "Bennato", 1850.00f);         // creazione oggetto impiegato1
        ImpiegatoRef impiegato2 = new ImpiegatoRef("Enzo", "Avitabile", 1650.00f);          // creazione oggetto impiegato2

        System.out.println(impiegato1.getNome()+" prende al mese "+ impiegato1.getSalario()+" $");
        System.out.println("L'amico suo, "+impiegato2.getNome()+", prende al mese "+ impiegato2.getSalario()+" $");

        impiegato2.aumentasalario(10);                                                // chiamata del modulo "aumentasalario"
        System.out.println("\nEnzo riceve un aumento del 10% e si ritrova con "+impiegato2.getSalario()+" $ al mese!!");

        if(impiegato1.getSalario() > impiegato2.getSalario()){                          // chiamata del modulo "getSsalario"
            System.out.println("\nLa paga di "+impiegato1.getNome()+" è però più alta di quella di "+impiegato2.getNome());
        }
        else System.out.println("\nLa paga di "+impiegato1.getNome()+" NON è più alta di quella di "+impiegato2.getNome());

        System.out.println("\nEnzo allora chiede un ulteriore aumento del 5%!");
        impiegato2.aumentasalario(5);
        System.out.println("e ora si ritrova con "+impiegato2.getSalario()+" $ al mese!!");

        if(impiegato1.getSalario() > impiegato2.getSalario()){
            System.out.println("\nLa paga di "+impiegato1.getNome()+" è ancora più alta di quella di "+impiegato2.getNome());
        }
        else System.out.println("\nLa paga di "+impiegato1.getNome()+" ORA è più alta di quella di "+impiegato2.getNome());

                                                                            // chiamata del modulo "dettagli" per i 2 oggetti
        System.out.println("\nIn definitiva "+impiegato1.dettagli()+" e "+impiegato2.dettagli());
    }
}