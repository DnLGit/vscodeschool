/*
Bergamasco Daniele IV As - Esercizio 10:  Impiegati con References - 27.02.2020

- classe Impiegato che dichiara le variabili nome, cognome, salario. 
- metodo dettagli() che restituisce in una stringa le informazioni sulla persona in questione. 
- metodo aumentasalario() che aumenti lo stipendio secondo una certa percentuale. 
- classe Principale che, nel metodo main(), istanzia due oggetti chiamati impiegato1 e impiegato2 della classe Impiegato, inizializzando per ognuno di essi i relativi campi. 
Si aumenti il salario di impiegato1 del 10% e poi si verifichi se 
il salario di impiegato 1 è maggiore di quello di impiegato2.
*/
package j10impiegatiref;


public class ImpiegatoRef {

    // Variabili d'istanza
    private String nome, cognome;
    private float salario;

    // Costruttori
    public ImpiegatoRef(String nome, String cognome, float salario){
        this.nome = nome; this.cognome = cognome;
        this.salario = salario;
    }

    // Metodi Getter e Setter
    public String getNome() {
        return this.nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCognome() {
        return this.cognome;
    }
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    public float getSalario() {
        return this.salario;
    }
    public void setSalario(float salario) {
        this.salario = salario;
    }

    // altri Metodi:                                dettagli: ritorna una stringa con tutti gli attributi dell'oggetto!
    public String dettagli(){
        return nome+" "+cognome+" guadagna al mese "+salario+" $";
    }

    public void aumentasalario(float perc){         // calcola la percentuale di aumento del salario
        this.salario += salario*(perc/100) ;
    }
}