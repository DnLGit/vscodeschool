/*

Bergamasco Daniele IV As - Esercizio 6:  Libro su classi e costruttori  19.02.2020

Esercizio su Libro con costruttori di copia e metodi - Scrivere una classe java che rappresenti un libro. 
Esso deve avere le seguenti caratteristiche: 
- titolo, autore, data di pubblicazione, un autore, un numero di pagina. 
- Un costo fisso messo staticamente. 
- Un costo per pagina sempre inserito staticamente. 
Implementare un costruttore che accetti gli attributi dinamici, un costruttore di copia, i metodi getter e setter 
ed un metodo che calcoli il costo dell’intero libro in base al costo fisso del libro ed il costo per ogni pagina. 
Inoltre un metodo che consenta di modificare il costo per pagina. 
Scrivere un main per testare il programma..

 */

package j06libro;

public class MainLibro {

    public static void main(String[] args) {
        
        System.out.println("\n******** Classe Libro ********");

        // Creazione degli oggetti
        Libro book1 = new Libro ("Fear and Loathing in LasVegas", "Hunter S. Thomson", 1972, 204, 23.02f, 0.05f);
        Libro book0 = new Libro(book1);                             
        Libro book2 = new Libro(book0);
        Libro book3 = new Libro(book0);

        book1.setCostoPag(0.06f);                                    // modifica dati invocando il metodo Setter  
        book1.setData(1982);

        book2.setCostoPag(0.07f);
        book2.setData(1990);

        book3.setCostoFix(book2.getCostoFix()/2);   // chiamata del metodo Setter con modulo Getter come parametro
        book3.setData(1998);
        
        System.out.println("\nPresentazione del libro:  "+book0.getTitolo());

        System.out.print("\nIl costo di questo libro, di "+book0.getAutore());
        System.out.println(", nel "+book0.getData()+" era di € "+ book0.costoLib());

        System.out.println("\nAlla prima ristampa (era il "+book1.getData()+") Il costo diventò di € "+ book1.costoLib());

        System.out.println("\nNel "+book2.getData()+" ci fu la terza ristampa che portò il costo a € "+ book2.costoLib());

        System.out.println("\nOtto anni dopo, nel "+book3.getData()+" uscì l'omonimo film, con Johnny Depp");
        System.out.println("A titolo di promozione, venne vendutò ad un prezzo di € "+book3.costoLib());
    }
}