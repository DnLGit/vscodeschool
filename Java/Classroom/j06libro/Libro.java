/*
Bergamasco Daniele IV As - Esercizio 6:  Libro su classi e costruttori  19.02.2020

- titolo, autore, data di pubblicazione, un autore, un numero di pagina. 
- Un costo fisso messo staticamente. 
- Un costo per pagina sempre inserito staticamente. 
Implementare un costruttore che accetti gli attributi dinamici, un costruttore di copia, i metodi getter e setter 
ed un metodo che calcoli il costo dell’intero libro in base al costo fisso del libro ed il costo per ogni pagina. 
Inoltre un metodo che consenta di modificare il costo per pagina. 

 */

package j06libro;

public class Libro {

    private String titolo, autore;
    private int data, pagine;
    private float costoFix, costoPag;  // Nota: aggiungere la "f" a fine valori assegnati (per far digerire il float a Java) 

    // costruttori
    public Libro(String titolo, String autore, int data, int pagine, float costoFix, float costoPag){
        this.titolo = titolo; this.autore = autore;
        this.data = data; this.pagine = pagine;
        this.costoFix = costoFix; this.costoPag = costoPag;
    }

    public Libro(Libro copyLibro){                                                      // costruttore di copia
        this.titolo = copyLibro.titolo; this.autore = copyLibro.autore;
        this.data = copyLibro.data; this.pagine = copyLibro.pagine;
        this.costoFix = copyLibro.costoFix; this.costoPag = copyLibro.costoPag;
    }

    // Metodi Getter e Setter
    
    public String getTitolo(){
        return this.titolo;
    }
    public void setTitolo(String titolo){
        this.titolo = titolo;
    }
    public String getAutore(){
        return this.autore;
    }
    public void setAutore(String autore){
        this.autore = autore;
    }
    public int getData(){
        return this.data;
    }
    public void setData(int data){
        this.data = data;
    }
    public int getPagine(){
        return this.pagine;
    }
    public void setPagine(int pagine){
        this.pagine = pagine;
    }
    public float getCostoFix() {
        return this.costoFix;
    }
    public void setCostoFix(float costoFix){
        this.costoFix = costoFix;
    }
    public float getCostoPag(){
        return this.costoPag;
    }
    public void setCostoPag(float costoPag){
        this.costoPag = costoPag;
    }

    // Altri Metodi
    public float costoLib(){
        return costoFix+(costoPag*pagine);
    }
}

