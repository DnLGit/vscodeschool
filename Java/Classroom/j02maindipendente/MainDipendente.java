/*

Bergamasco Daniele IV As - Esercizio 2:  Dipendente su classi e costruttori  09.02.2020

Progettare una classe di nome Dipendente. Tale classe prevede due variabili di istanza 
una definita come nome, di tipo String e una definita come stipendio di tipo double. 
Scrivere un costruttore senza parametri che inizializza le variabili di istanza, un costruttore con due parametri 
(nome e stipendio) che ne setta i valori con quelli in input. 
La classe deve inoltre implementare i seguenti metodi: 
- Un metodo definito come public String getNome() che restituisce il nome del dipendente. 
- Un metodo definito come public double getStipendio() che restituisce lo stipendio del dipendente. 
- Un metodo public void setStipendio(double nuovoStipendio) che modifica il valore dello stipendio. 
- Un metodo public void setNome(String nuovoNome) che cambia il nome al dipendente. 
- Un metodo aumento(double percentuale), che incrementi lo stipendio del dipendente secondo una certa percentuale. 
Scrivere un main per testare il tutto.

 */

package j02maindipendente;

public class MainDipendente {
    public static void main(String[] args) {
        
        System.out.println("\n******** Classe di un Dipendente ********");

        Dipendente dati = new Dipendente();                             // prima istanza, oggetto con parametri fissi
        Dipendente datiMody = new Dipendente();                         // seconda istanza, oggetto con parametri variabili

        System.out.println("\nQui di seguito i dati del dipendente: ");
        System.out.print("nome: "+dati.getNome());
        System.out.println(" ed un stipendio attuale di "+dati.getStipendio()+" €/mese");

        System.out.println("\n"+dati.getNome()+" gioca spesso con i nomi");
        datiMody.setNome("Jane");
        System.out.println("prima "+dati.getNome()+", poi fino a ieri era "+datiMody.getNome());
        datiMody.setNome("MaryJane");
        System.out.println("e oggi si fa chiamare "+datiMody.getNome());
        
        System.out.println("\nFortuna vuole che le hanno dato un aumento del 10% ");
        datiMody.getAumento(10.00);
        System.out.println("così dal prossimo mese prenderà "+datiMody.getStipendio()+" euro al mese");
        datiMody.setStipendio(2300.00);
        System.out.println("... anche se lei ambiva ad almeno "+datiMody.getStipendio()+" euro tondi!");

    }
}