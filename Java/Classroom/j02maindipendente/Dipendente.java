/**
 /*

Bergamasco Daniele IV As - Esercizio 2:  Dipendente su classi e costruttori  09.02.2020

 */
package j02maindipendente;

public class Dipendente{

// variabili d'istanza:
    private String nome;
    private double stipendio;

// costruttori:
    public Dipendente(){                                // primo costruttore: inizializza le variabili d'istanza
        this.nome = "Mary";
        this.stipendio = 1850.50;
    }

    public Dipendente(String nome, double stipendio){   // secondo costruttore, da usare per l'input valori
        this.nome = nome;
        this.stipendio = stipendio;
    }

// Metodi:
    public String getNome() {                           // restituisce il nome del dipendente
        return nome;
    }
    public double getStipendio() {                      // restituisce lo stipendio del dipendente
        return stipendio;
    }
    public void setNome(String nome) {                  // modifica il nome del dipendente
        this.nome = nome;               
    }
    public void setStipendio(double stipendio){         // modifica lo stipendio del dipendente
        this.stipendio = stipendio;
    }
    public void getAumento(double perc){                // incrementa percentualmente lo stipendio del dipendente 
        this.stipendio = stipendio+(stipendio/perc);
    }
}