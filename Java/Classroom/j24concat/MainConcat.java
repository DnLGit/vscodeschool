/*
Bergamasco Daniele IV As - Esercizio 24:  Concatenazione - 25.03.2020

Esercizio sugli array ed input da tastiera. Realizzare il seguente metodo: 
    public int[] concatenaArray(int[] array1, int[] array2) 
Il metodo deve restituire un nuovo array della “giusta” e corretta dimensione 
che contiene la concatenazione dei due array in ingresso. Esempi:
    array1     array2          Risultato
 a) {12, 7,}  { }             {12, 7}
 b) {12, 7 }  { 10, 5, 16 }   {12, 7, 10, 5, 16 }
 c) { }       { 10, 5, 16 }   { 10, 5, 16 }   

Creare un main per testarlo con input da tastiera.  */

package j24concat;


public class MainConcat {
    public static void main(String[] args) {

        System.out.println("\n***** Classe Concat *****\n");

        int harry1a[] = {12, 7};                                 // Dichiarazione e inizializzazione di tre coppie di array
        int harry2a[] = {};

        int harry1b[] = {12, 7};
        int harry2b[] = {10, 5, 16};

        int harry1c[] = {};
        int harry2c[] = {10, 5, 16};

        Concat tak = new Concat();                              // Dichiarazione e inizializzazione dell'oggetto tak

        System.out.print("A) Prima coppia di array:   ");       // stampa delle tre coppie di array originali con foreach
        for(int hr1a:harry1a)
            System.out.print(hr1a+" ");
        System.out.print(" / ");
        for(int hr2a:harry2a)
            System.out.print(hr2a+" ");

        System.out.print("\nB) Seconda coppia di array: ");
        for(int hr1b:harry1b)
            System.out.print(hr1b+" ");
        System.out.print(" / ");
        for(int hr2b:harry2b)
            System.out.print(hr2b+" ");

        System.out.print("\nC) Terza coppia di array:   ");
        for(int hr1c:harry1c)
            System.out.print(hr1c+" ");
        System.out.print(" / ");
        for(int hr2c:harry2c)
            System.out.print(hr2c+" ");
        System.out.print("\n");


        int harryA[] =  tak.concatenaArray(harry1a, harry2a);                   // Chaimata dei tre metodi "concatenaArray"
        int harryB[] =  tak.concatenaArray(harry1b, harry2b);
        int harryC[] =  tak.concatenaArray(harry1c, harry2c);

        System.out.print("\nRisultato di A:   ");                           // stampa dei risultati di ogni coppia di array
        for(int hr: harryA)
            System.out.print(hr+" ");

        System.out.print("\nRisultato di B:   ");
        for(int hr: harryB)
            System.out.print(hr+" ");

        System.out.print("\nRisultato di C:   ");
        for(int hr: harryC)
            System.out.print(hr+" ");
        System.out.print("\n");
    }
}
