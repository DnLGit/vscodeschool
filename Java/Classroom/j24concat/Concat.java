package j24concat;

public class Concat {

    public int[] concatenaArray(int[] array1, int[] array2){
        int nn = array1.length+array2.length;
        int array[] = new int[nn];
        for(int i=0; i<nn; i++){
            if(i<array1.length){
                array[i] = array1[i];
            }else array[i] = array2[i-array1.length];
        }return array;
    }
}