package j36quadratocubo;

public abstract class Dati {                    // classe astratta con variabile comune

    protected int lato;

    public Dati(int lato){
        this.lato = lato;
    }
}