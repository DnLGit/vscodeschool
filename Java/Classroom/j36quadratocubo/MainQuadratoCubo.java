/*
Bergamasco Daniele IV As - Esercizio 36:  Quadrato e Cubo classi Interfacce - 02.06.2020

Scrivere un programma che dato un numero intero in ingresso vengano restituiti
un oggetto rappresentativo del quadrato del numero dato
e un oggetto rappresentativo del cubo del numero assegnato.
Implementa l'algoritmo attraverso l'uso di una interfaccia comune..      */

package j36quadratocubo;
import java.util.*; 

public class MainQuadratoCubo {

    public static void main(String[] args) {

        System.out.println("\n****************** Esercizio 36:  Quadrato e Cubo classi Interfacce ******************\n");

        System.out.print("Inserisci un numero intero: ");

        Scanner insert = new Scanner(System.in);
        int numero = insert.nextInt();

        Quadrato qt = new Quadrato(numero);
        Cubo cb = new Cubo(numero);

        System.out.println("\nInserendo il numero "+numero+" Risulta che: ");
        System.out.println("Il quadrato e' pari a: "+qt.formula());
        System.out.println("Il cubo e' pari a: "+cb.formula());

        insert.close();
    }
}