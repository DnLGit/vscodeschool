package j36quadratocubo;

public class Cubo extends Dati implements Formulazione{

    public Cubo(int lato){                          // costruttore da superclasse
        super(lato);
    }

    public int formula(){                           // implementazione metodo da interfaccia
        return lato*lato*lato;
    }    
}