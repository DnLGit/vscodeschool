package j36quadratocubo;

public class Quadrato extends Dati implements Formulazione{

    public Quadrato(int lato){              // costruttore da superclasse
        super(lato);
    }
        
    public int formula(){                   // implementazione metodo da interfaccia
        return lato*lato;
    }
}