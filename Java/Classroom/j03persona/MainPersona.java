/*

Bergamasco Daniele IV As - Esercizio 3:  Persona su classi e costruttori  11.02.2020

Definire una classe Java "Persona" che abbia come attributi età, nome, sesso, professione. 
Oltre ad implementare i metodi getter e setter per ogni attributo, implementare:
- un costruttore che permetta di istanziare oggetti di tipo "Persona" definendo valori specifici per i vari attributi
- il metodo "chiSei" che restituisca, quando invocato, una stringa del tipo 
  << Sono una persona di nome: "nome", sesso: "sesso", età: "età", professione: "professione" >> 
  dove "nome, sesso, età e professione" sono i valori assunti dagli attributi della classe;
- il metodo "main" che preveda la creazione di un oggetto di tipo "Persona" e l'invocazione del metodo "chiSei" 
  per visualizzarne il risultato restituito.
Scrivere un main per testare la correttezza della classe.

 */

 package j03persona;

 public class MainPersona {

    public static void main(String[] args) {
        
        System.out.println("\n******** Classe di una Persona ********");

        Persona people = new Persona();   // istanza della classe Persona: creazione oggetto

        people.chiSei();                  // invocazione del metodo "chiSei"
    }
    
 }

 