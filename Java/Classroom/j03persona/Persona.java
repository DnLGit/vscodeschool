/**
 /*

Bergamasco Daniele IV As - Esercizio 3:  Persona su classi e costruttori  11.02.2020

 */
package j03persona;

public class Persona{

// variabili d'istanza:
    private int eta;
    private String nome, sesso, professione;

// costruttori
    public Persona(){
        this.eta = 32;
        this.nome = "MaryJane"; this.sesso = "Femminile"; this.professione = "Designer";
    }


// Metodi:
    public int getEta(){                                    // metodo Getter: restituisce l'età della persona
        return eta;
    }
    public String getNome(){                                // metodo Getter: restituisce il nome della persona
        return nome;
    }
    public String getSesso(){                               // metodo Getter: restituisce il sesso della persona
        return sesso;
    }
    public String getProfessione(){                         // metodo Getter: restituisce la professione della persona
        return professione;
    }

    public void setEta(int eta){                            // metodo Setter: modifica l'età della persona
        this.eta = eta;
    }
    public void setNome(String nome){                       // metodo Setter: modifica il nome della persona
        this.nome = nome;
    }
    public void setSesso(String sesso){                     // metodo Setter: modifica il sesso della persona
        this.sesso = sesso;
    }
    public void setProfessione(String professione){         // metodo Setter: modifica la professione della persona
        this.professione = professione;
    }

    public void chiSei(){                                              // mi ritorna e stampa la stringa se invocato nel main
        System.out.print("\nSono una persona di nome: "+this.nome+ ", sesso: "+this.sesso); 
        System.out.println(", età: "+this.eta +" e professione: "+this.professione);
    }
}