/*
Bergamasco Daniele IV As - Esercizio 19 (Il primo a pag. 6 esercizi Java):  Input, Stringhe e Date - 20/27.02.2020

Scrivere un programma Java che chieda in input due date in formato dd/mm/AAAA salvandole in due variabili di tipo string. 
Estrarre anno mese e giorno per entrambe e darle in input per la creazione di due date con la classe GregorianCalendar. 
Poi aggiungere alla prima data un anno, sottrarre alla seconda data un mese. 
E poi stampare la differenza in numero di giorni tra le due date. Verificare quale data è antecedente. 
Verificare se la seconda data inserita è antecedente a quella attuale, qualora lo fosse stampare il numero di giorni trascorsi da quella data fino ad oggi. Stampare le due date in formato yyyy/mm/dd.     */

package j19stringdata;

//import java.util.GregorianCalendar;                     // importare questa classe per utilizzare GregorianCalendar
import java.util.*;                                       // così importo tutte le classi "until" ... anche GregorianCalendar
//import java.text.SimpleDateFormat;                      // importare questa classe per utilizzare SimpleDateFormat
import java.text.*;                                       // così importo tutte le classi "until" ... anche SimpleDateFormat

public class StringheDate {

    public static void main(String[] args) {
        System.out.println("\n\n***** Esercizi con Stringhe e Date *****");
    
        Scanner inserisci = new Scanner(System.in);                 // Warning [Resource leak: 'inserisci' is never closed]
        System.out.print("\nInserisci una prima data: ");
        String data1 = inserisci.nextLine();
        System.out.print("\nInserisci la seconda data: ");
        String data2 = inserisci.nextLine();
        inserisci.close();                                          // con questa riga di chiusura risolvo il warning sopra
        
        System.out.println("\nTest variabili data1: "+data1+" e data2: "+data2);        // verifico le variabili se corrette
    /*
        String data1 = "20.02.2020";        // variabili temporanee al posto di inserimento da tastiera (durante i test)
        String data2 = "25.02.2033";        // variabili temporanee al posto di inserimento da tastiera (durante i test)
    */    
                
        String data1gg = data1.substring(0,2);                                          // estraggo la substringa del giorno
        int data1Gg = Integer.parseInt(data1gg);                                                // converto stringa in intero
        String data1mm = data1.substring(3,5);                                          // estraggo la substringa del mese
        int data1Mm = Integer.parseInt(data1mm);                                                // converto stringa in intero
        String data1yy = data1.substring(6,10);                                         // estraggo la substringa dell'anno
        int data1Yy = Integer.parseInt(data1yy);                                            // converto stringa in intero

                
        String data2gg = data2.substring(0,2);                                // come sopra estraggo la substringa del giorno
        int data2Gg = Integer.parseInt(data2gg);                                // come sopra converto stringa in intero
        String data2mm = data2.substring(3,5);                                // come sopra estraggo la substringa del mese
        int data2Mm = Integer.parseInt(data2mm);                                // come sopra converto stringa in intero
        String data2yy = data2.substring(6,10);                               // come sopra estraggo la substringa dell'anno
        int data2Yy = Integer.parseInt(data2yy);                                // come sopra converto stringa in intero

        System.out.println("\nprova stampa Stringa data1: "+data1gg+"/"+data1mm+"/"+data1yy);
        System.out.println("\nprova stampa Stringa data2: "+data2gg+"/"+data2mm+"/"+data2yy);

        System.out.println("\nprova stampa Intero data1: "+data1Gg+"/"+data1Mm+"/"+data1Yy);
        System.out.println("\nprova stampa Intero data2: "+data2Gg+"/"+data2Mm+"/"+data2Yy);

        /*  Oggetti di GregorianCalendar (ridurre il numero dei mesi perchè partono dallo zero!!!)    */
        GregorianCalendar dataAttuale=new GregorianCalendar();      // oggetto "dataAttuale" della classe "GregorianCalendar"

        GregorianCalendar date1=new GregorianCalendar(data1Yy, data1Mm-1, data1Gg);                 // nuovo oggetto date1
        GregorianCalendar date2=new GregorianCalendar(data2Yy, data2Mm-1, data2Gg);                 // nuovo oggetto date2
 

        /* Verifica informazioni dell'oggetto "dataAttuale" */

        // utilizzo dei Metodi "get" di "GregorianCalendar"
        int yearNow = dataAttuale.get(GregorianCalendar.YEAR);
        int monthNow = dataAttuale.get(GregorianCalendar.MONTH)+1;          // aggiunto +1 perchè i mesi partono dallo zero
        int dayNow = dataAttuale.get(GregorianCalendar.DATE);
        int hourNow = dataAttuale.get(GregorianCalendar.HOUR_OF_DAY);
        int minuteNow = dataAttuale.get(GregorianCalendar.MINUTE);
        int secondNow = dataAttuale.get(GregorianCalendar.SECOND);

        // Prova stampa data e ora corrente.
        System.out.print("\nProva stampa data attuale, oggi è: ");
        System.out.println(dayNow+"."+monthNow+"."+yearNow+" ore: "+hourNow+":"+minuteNow+":"+secondNow);

        // Prova stampa conversioni date da Stringhe a interi
        System.out.println("\n(data1gg) giovedì scorso era il: "+data1Gg+"-"+data1Mm+"-"+data1Yy);
        System.out.println("(data2gg) il carnevale finì il: "+data2Gg+"-"+data2Mm+"-"+data2Yy);

         /* Utilizzo dei Metodi di GregorianCalendar    */
        //  add e getTimeMillis

        date1.add(GregorianCalendar.YEAR, +1);                          //.add ->           aggiungo  1 anno alla data1
        date2.add(GregorianCalendar.MONTH, -1);                         //.add ->           sottraggo 1 mese alla data1
        long ms1 = date1.getTimeInMillis();                             //.getTimeMillis -> determina in ms data1
        long ms2 = date2.getTimeInMillis();                             //.getTimeMillis -> determina in ms data2
        long diff = ms1-ms2;                                            // sommatoria fra le due date in ms
        long diffDay = diff/(24*60*60*1000);                            // conversione dei ms in giorni

        System.out.print("\nDopo le modifiche la differenza fra le due date è di "+diffDay+" giorni.");

        //  Metodi before, after e equals

        if( date1.after(date2) )
        System.out.println("\ne Data1 è dopo di Data2!");
        else if( date1.equals(date2) )
        System.out.println("\n e le date sono uguali!");
        else
        System.out.println("\ne Data1 è prima di Data2!!!!");


        System.out.print("\nAl confronto con ora, dopo la modifica, risulta che: ");
        if( date2.before(dataAttuale) ){                      // solo se la seconda data è antecedente all'attuale eseguo ...
            long ms3 = dataAttuale.getTimeInMillis();                               //.getTimeMillis -> determina in ms data1
            long diff3 = ms3-ms2;                                                   // sommatoria fra le due date in ms
            long diffDay3 = diff3/(24*60*60*1000);                                  // conversione dei ms in giorni
            System.out.println("\nLa seconda data precede quella attuale di "+diffDay3+" giorni!!!");
        }
        else if( date2.equals(dataAttuale) )
        System.out.println("Le date sono uguali!");
        else
        System.out.println("La seconda data è al futuro!!!");

        System.out.println("\nStampe finali con SimpleDateFormat: ");

        SimpleDateFormat now = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        System.out.println("La data di Oggi è        "+now.format(dataAttuale.getTime()));
        System.out.println("Data1 dopo la modifica è "+sdf.format(date1.getTime()));
        System.out.println("Data2 dopo la modifica è "+sdf.format(date2.getTime()));
      
    }
}