/*
Bergamasco Daniele IV As - Esercizio 18:  Richiesta Operazioni - 06.03.2020
Esercizio su input da tastiera e stringhe
Scrivere un programma Java che effettua le seguenti operazioni:
• Fa inserire all’utente una stringa s;
• Visualizza all’utente il numero totale di caratteri di s;
• Chiede all’utente di inserire due posizioni intere a e b, tali che a < b e comprese tra 0 e la lunghezza di s meno 1 
    (il programma assume che l’utente inserisca correttamente i dati, cioè non deve eseguire controlli di correttezza);
• Visualizza all’utente la sottostringa di s compresa tra a (incluso) e b (escluso).
• Sostituisce il carattere a all’interno della stringa con –
• Estrarre il carattere o i caratteri al centro della stringa   */

package j18richiestaoperazioni;
import java.util.*;                                                 // libreria per utilizzare anche la classe Scanner

public class RichiestaOperazioni {

    public static void main(String[] args) {

        System.out.println("\n***** Esercizi con Stringhe:  Richiesta Operazioni *****\n");

        System.out.print("\nInserisci una stringa: ");
        Scanner inserire = new Scanner(System.in);                    // Warning [Resource leak: 'inserisci' is never closed]
        String s = inserire.nextLine();                                             // metodo .nextLine per inserire stringhe

        System.out.println("\nStringa: "+s+" | Lunghezza: "+s.length()+" caratteri");  // lunghezza data dal metodo s.lenght.

        System.out.println("\nInserisci due indici (a, b con a < b) tra 0 e "+(s.length()-1));
        System.out.print("\na: -> ");
        int a = inserire.nextInt();                                                 // metodo .nextInt per inserire interi
        System.out.print("\nb: -> ");
        int b = inserire.nextInt();

        System.out.println("\nLa substring è: "+s.substring(a, b+1));

        System.out.println("\nCambiando nella stringa le 'a' con il segno '-' ");
        String s1 = s.replaceAll("a", "-");                                 // memorizzo in una nuova stringa il risultato.
        System.out.println("La nuova stringa diventa: "+s1);

        System.out.println("\nEstraendo i caratteri centrali della stringa: ");

        if(s.length()%2==0)                             // se la lunghezza della stringa è pari, avrò due caratteri centrali
        System.out.println("Il centro della stringa è: "+s.substring((s.length()/2)-1, (s.length()/2)+1));

        else                                                                      // altrimentri ne avrò solo uno al centro  
        System.out.println("\nIl centro della stringa è: "+s.substring((s.length()/2), (s.length()/2)+1));

        inserire.close();                                           // il metodo "inserire.close()"" elimina il Warning sopra
    }
}