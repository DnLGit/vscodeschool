/*  I dipendenti sono caratterizzati, oltre che dalle informazioni precedentemente indicate per tutto il personale, 
    anche da un codice fiscale e da una base di retribuzione.   */

package j30gestionepersonale;

public class Dipendente extends Persona{

    private String codiceFiscale;
    //private double pagaBase;

    public Dipendente(String nome, String indirizzo, String tel, double pagaBase, String codiceFiscale){
        super(nome, indirizzo, tel, pagaBase);
        this.codiceFiscale = codiceFiscale;
    }

    public String toString(){ 
        return super.toString()+" [ C.F.: "+ codiceFiscale+" ]";

    }

    public String getCodiceFiscale() {
        return this.codiceFiscale;
    }
    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

}