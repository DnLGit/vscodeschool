/*
Bergamasco Daniele IV As - Esercizio 30:  Gestione del Personale con ereditarietà - 22.05.2020

Si vuole realizzare un insieme di classi per la gestione del personale di un’istituzione. 
Ogni persona che lavora nell’istituzione è caratterizzata da un nome, un indirizzo ed un numero di telefono. 
Deve essere possibile in qualsiasi momento ottenere una rappresentazione di un membro del personale sotto forma di stringa o calcolare la sua paga. 
Nell’istituzione esistono due tipologie di personale, i volontari e i dipendenti. 
I dipendenti sono caratterizzati, oltre che dalle informazioni precedentemente indicate per tutto il personale, anche da un codice fiscale e da una base di retribuzione. 
Un volontario percepisce una paga pari a 0, un dipendente percepisce una paga pari alla base di retribuzione. 
I dipendenti a loro volta possono essere, giornalieri o impiegati. 
I giornalieri sono caratterizzati anche dalle giornate lavorative svolte e la loro paga è calcolata moltiplicando la loro base di retribuzione per il numero di giorni in cui hanno lavorato. 
Gli impiegati sono caratterizzati dal numero di bonus maturati e la loro paga è calcolata sommando i bonus maturati alla base di retribuzione.  */

package j30gestionepersonale;

public class MainPersonale {
    public static void main(String[] args) {
        
        System.out.println("\n****************** Esercizio 30:  Gestione del Personale ******************\n");

        Volontario vol1 = new Volontario("Dario", "via Agratis", "87 77 ho 22", 0);
        Giornaliero day1 = new Giornaliero("Pippo", "vie de qua'", "+44 558 806", 1000, "PPOD11F154M", 20);
        Impiegato imp1 = new Impiegato("Filippo", "via de la'", "+36 751 872", 1500, "FPON02G254S", 15);

        vol1.dammiTutto();
        day1.dammiTutto();
        imp1.dammiTutto();

        System.out.println(vol1.toString());
        System.out.println(day1.toString());
        System.out.println(imp1.toString());
        
        System.out.println();
        System.out.println(vol1.getNome() + " ora guadagna " + vol1.conteggioPaga()+"€/mese");
        System.out.println(day1.getNome() + " ora guadagna " + day1.conteggioPaga()+"€/mese");
        System.out.println(imp1.getNome() + " ora guadagna " + imp1.conteggioPaga()+"€/mese");
    }
}