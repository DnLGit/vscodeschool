/*  I giornalieri sono caratterizzati anche dalle giornate lavorative svolte e la loro paga è calcolata moltiplicando 
    la loro base di retribuzione per il numero di giorni in cui hanno lavorato.   */

package j30gestionepersonale;

public class Giornaliero extends Dipendente {

    private int dayJob;

    public Giornaliero(String nome, String indirizzo, String tel, double pagaBase, String codiceFiscale, int dayJob){
        super(nome, indirizzo, tel, pagaBase, codiceFiscale);
        this.dayJob = dayJob;
    }


/*     public void dammiTutto(){               // per stampare tutto ho provato così ma non mi sembra una bella cosa ... 
        super.dammiTutto();
        System.out.println("prova");
    } */

    public String toString(){                   // con il toString invece mi viene: dove sbaglio?
        return super.toString()+" lavorando "+dayJob+" giorni";
    }
        

    public double conteggioPaga(){
        return super.pagaBase * this.dayJob;
    }


    // Metodi Getter&Setter
    public int getDayJob(){
        return dayJob;                
    }
    public void setDayJob(int dayJob){
        this.dayJob = dayJob;    
    }
}