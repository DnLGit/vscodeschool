/*  Gli impiegati sono caratterizzati dal numero di bonus maturati e la loro paga è calcolata sommando 
    i bonus maturati alla base di retribuzione.  */

package j30gestionepersonale;

public class Impiegato extends Dipendente{

    private double bonus;


    public Impiegato(String nome, String indirizzo, String tel, double pagaBase, String codiceFiscale, double bonus){
        super(nome, indirizzo, tel, pagaBase, codiceFiscale);
        this.bonus = bonus;
    
    }

    /* public double conteggioPaga(){
        return super.pagaBase + this.bonus;
    } */

    public String toString(){                   // con il toString invece mi viene: dove sbaglio?
        return super.toString()+" e bonus di "+bonus+" €";

    }

    public double conteggioPaga(){
        return super.pagaBase + this.bonus;
    }
    


    public double getBonus() {
        return this.bonus;
    }
    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
}