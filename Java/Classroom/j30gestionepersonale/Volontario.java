/*  Un volontario percepisce una paga pari a 0, un dipendente percepisce una paga pari alla base di retribuzione.   */

package j30gestionepersonale;

public class Volontario extends Persona{

    //private double pagaVolontario;

    public Volontario(String nome, String indirizzo, String tel, double pagaBase){
        super(nome, indirizzo, tel, pagaBase);
    }

    

    public double conteggioPaga(){
        return super.pagaBase + 600;
    }
}