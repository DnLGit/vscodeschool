/*  Ogni persona che lavora nell’istituzione è caratterizzata da un nome, un indirizzo ed un numero di telefono. 
    Deve essere possibile in qualsiasi momento ottenere una rappresentazione di un membro del personale 
    sotto forma di stringa o calcolare la sua paga  */

package j30gestionepersonale;

public class Persona {

    protected String nome, indirizzo, tel;
    protected double pagaBase;

    public Persona(String nome, String indirizzo, String tel, double pagaBase) {        // Costruttore
        this.nome = nome; this.indirizzo = indirizzo; this.tel = tel;
        this.pagaBase = pagaBase;
    }

    // Metodo toString [override] per stampare nel main le variabili con System.out
    public String toString(){
        return "(toSting) Er " + nome + " vive in " + indirizzo + ", e tel.: " + tel + ", guadagna " + pagaBase + "€/mese";
    }

    public void dammiTutto(){   // così stampo tutto nel main invocando solo il metodo senza usare il System.out
        System.out.println("Mr " + nome + " vive in " + indirizzo + " con tel.: " + tel + " guadagna " + pagaBase + "€/mese\n");
    }

            
    public double conteggioPaga(){
        return this.pagaBase;
    }


    // Metodi Getter&Setter
    public String getNome() {
        return this.nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getIndirizzo() {
        return this.indirizzo;
    }
    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }
    public String getTel() {
        return this.tel;
    }
    public void setTel(String tel) {
        this.tel = tel;
    }
    public double getPagaBase() {
        return this.pagaBase;
    }
    public void setPagaBase(double pagaBase) {
        this.pagaBase = pagaBase;
    }
}