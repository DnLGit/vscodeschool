/*
Bergamasco Daniele IV As - Esercizio 16:  Parole quasi Uguali - 05.03.2020

Scrivere un programma ParoleQuasiUguali che chiede all’utente di inserire due singole parole e 
stampa Sono uguali! se le due parole sono uguali, 
Sono quasi uguali! se le due parole differiscono solo per l’uso di maiuscolo e minuscole, 
Non sono uguali! altrimenti.   */

package j16parolequasiuguali;
import java.util.*;                                                 // libreria per utilizzare anche la classe Scanner


public class MainParoleQuasiUguali {

    public static void main(String[] args) {

        System.out.println("\n***** Esercizi con Stringhe:  Parole quasi Uguali *****\n");
        Scanner inserisci = new Scanner(System.in);                   // Warning [Resource leak: 'inserisci' is never closed]

        System.out.print("\nInserisci gentilente la prima parola: ");
        String parola1 = inserisci.nextLine();                                            // per esempio "MAXISCHERMO"
        System.out.print("\nInserisci gentilente la seconda parola: ");
        String parola2 = inserisci.nextLine();                                            // per esempio "maxischermo"

        System.out.println("\n\nLe parole "+parola1+" e "+parola2);
        if(parola1.equals(parola2))                           // metodo .equals:  ritorna true se le due stringhe sono uguali
        System.out.println("sono perfettamente Uguali!!");    
        else if(parola1.equalsIgnoreCase(parola2))          // metodo .equalsIgnoreCase:  come sopra eliminando CaseSensitive
        System.out.println("sono quasi uguali.");
        else System.out.println("sono differenti!!!");

        inserisci.close();                                                  // inserisci.close() elimina il Warning sopra
    }
}