/*
Bergamasco Daniele IV As - Esercizio 12:  Pappagallo - 04.03.2020

Scrivere una classe Pappagallo che 
chiede continuamente all’utente di inserire una stringa e la ripete (stampandola) immediatamente. 
L’inserimento delle stringhe deve terminare quando l’utente inserisce la stringa vuota.   */

package j12pappagallo;

import java.util.Scanner;                                                   // Libreria per utilizzare la classe "Scanner".

public class MainPappagallo {
    public static void main(String[] args) {

        System.out.println("\n***** Esercizi con Stringhe:  Pappagallo *****\n");
        Scanner ripeti = new Scanner(System.in);                        // Warning [Resource leak: 'input' is never closed]
        String frase;

        do {
            System.out.print("\nHei, inserisci una frase: ");                               // entro nel ciclo la prima volta
            frase = ripeti.nextLine();              // inserisco una frase chiamando il metodo nextLine della classe Scanner
            if(!frase.equals(""))                           // aggiunto un if per non stampare anche eventuale frase vuota
                System.out.println("e io te la stampo: "+frase);            
        } while (!frase.equals(""));            // rimango nel ciclo fino a quando la condizione sarà vera (frase NON nulla)
        
        System.out.println("\nHei, ma non ti avevo detto di inserire un frase?");

        ripeti.close();                                                             // metodo per ovviare al Warning sopra
    }
}