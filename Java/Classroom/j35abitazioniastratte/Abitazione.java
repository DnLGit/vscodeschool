/*  classe astratta Abitazione che contenga il metodo astratto calcoloCostoMensile  */

package j35abitazioniastratte;

public abstract class Abitazione {                              // definita come Superclasse astratta
    
    public abstract double calcoloCostoMensile();               // metodo astratto su classe astratta
}