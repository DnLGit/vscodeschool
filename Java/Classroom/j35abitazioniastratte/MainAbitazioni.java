/*
Bergamasco Daniele IV As - Esercizio 35:  Abitazioni con classi Astratte - 30.05.2020

Realizzare una classe astratta Abitazione che contenga il metodo astratto calcoloCostoMensile. 
Creare due sottoclassi, una chiamata PrimaCasa, l’altra chiamata SecondaCasa. 
L’implementazione del metodo nella prima classe è: costo condominio + utenze. 
L’implementazione del metodo della seconda classe è imu + tasse + costo rinnovo contratto – Affitto.      */

package j35abitazioniastratte;

public class MainAbitazioni {
    public static void main(String[] args) {

        System.out.println("\n****************** Esercizio 35:  Abitazioni con classi Astratte ******************\n");
        
        Abitazione abit [] = new Abitazione [2];            // istanza superclasse astratta

        abit[0] = new PrimaCasa(50, 200);
        abit[1] = new SecondaCasa(75, 180, 450);

        System.out.println("La prima casa ha un costo di: "+abit[0].calcoloCostoMensile());
        System.out.println("La seconda invece spende : "+abit[1].calcoloCostoMensile());
        System.out.println();
        System.out.println("In totale sono: "+(abit[0].calcoloCostoMensile()+abit[1].calcoloCostoMensile()));
    } 
}