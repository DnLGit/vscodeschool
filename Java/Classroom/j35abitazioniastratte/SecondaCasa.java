/*  SecondaCasa - metodo:  imu + tasse + costo rinnovo contratto – Affitto. */

package j35abitazioniastratte;

public class SecondaCasa extends Abitazione {                            // definita come Sottoclasse di superclasse astratta

    private double imu, tasse, rinnovoAffitto;


    public SecondaCasa() {                                              // costruttori: questo di default
    }

    public SecondaCasa(double imu, double tasse, double rinnovoAffitto) {
        this.imu = imu;
        this.tasse = tasse;
        this.rinnovoAffitto = rinnovoAffitto;
    }

    public double calcoloCostoMensile(){                               // implementazione del metodo astratto per secondaCasa
        return imu+tasse+rinnovoAffitto;
    }


    public double getImu() {
        return this.imu;
    }
    public void setImu(double imu) {
        this.imu = imu;
    }
    public double getTasse() {
        return this.tasse;
    }
    public void setTasse(double tasse) {
        this.tasse = tasse;
    }
    public double getRinnovoAffitto() {
        return this.rinnovoAffitto;
    }
    public void setRinnovoAffitto(double rinnovoAffitto) {
        this.rinnovoAffitto = rinnovoAffitto;
    }
}