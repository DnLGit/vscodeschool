/*  PrimaCasa - metodo: costo condominio + utenze   */

package j35abitazioniastratte;

public class PrimaCasa extends Abitazione {                             // definita come Sottoclasse di superclasse astratta

    private double costoCondominio, costoUtenza;                        // variabili d'istanza


    public PrimaCasa() {                                                // costruttori: questo di default
    }

    public PrimaCasa(double costoCondominio, double costoUtenza) {
        this.costoCondominio = costoCondominio;
        this.costoUtenza = costoUtenza;
    }

    public double calcoloCostoMensile(){                                // implementazione del metodo astratto per primaCasa
        return costoCondominio+costoUtenza;
    }


    // metodi Getter&Setter
    public double getCostoCondominio() {
        return this.costoCondominio;
    }
    public void setCostoCondominio(double costoCondominio) {
        this.costoCondominio = costoCondominio;
    }
    public double getCostoUtenza() {
        return this.costoUtenza;
    }
    public void setCostoUtenza(double costoUtenza) {
        this.costoUtenza = costoUtenza;
    }
}