/*
Bergamasco Daniele IV As - Esercizio 20:  Docente - 10.03.2020

Esercizio su array di oggetti
Scrivere una classe Docente che rappresenti le seguenti informazioni relative ad un docente: nome, cognome, codice ed età, e che contenga il costruttore parametrizzato(quello che prende in input tutte le variabili di istanza) ed i metodi getCodice, getCognome e getEta che restituiscono rispettivamente il codice, il cognome e l’età del docente. Scrivere poi una classe Universita, che rappresenti un insieme di docenti universitari tramite un array di tipo Docente, e che contenga un costruttore che accetta l’array di Docenti ed un metodo etaMinima che restituisce la minima età tra i docenti universitari.
Scrivere un main per testare il tutto.   */

package j21docente;
import java.util.*;                                                 // Libreria per utilizzare la classe Scanner e ... altro!

public class MainDocente6 {

    public static void main(String[] args) {

        System.out.println("\n***** Classe Docente & Università 6° soluzione *****\n");
        System.out.print("Inserire il numero di insegnanti di questa Università: ");

        Scanner ins = new Scanner(System.in);                                   // Dichiarazione e istanza dell'oggetto ins

        int N = Integer.parseInt(ins.nextLine());          // inizializzazione variabile dimensione array, da tastiera [cast]

        Docente prof[] = new Docente[N];                                        // Dichiarazione e istanza dell'array prof 
        Universita UWC = new Universita(prof);                                  // Dichiarazione e istanza dell'oggetto UWC
        
        System.out.println("\nInserisci i dati dei "+N+" insegnanti");

        for(int i=0; i<N; i++){            // ciclo per istanziare e popolare tutti gli oggetti dell'array prof[] da tastiera
            prof[i] = new Docente ();
            System.out.println("\n"+(i+1)+"^ insegnante: ");
            System.out.print("nome: ");
            prof[i].setNome(ins.nextLine());
            System.out.print("cognome: ");
            prof[i].setCognome(ins.nextLine());
            System.out.print("codice: ");
            prof[i].setCodice(ins.nextLine());
            System.out.print("eta: ");
            prof[i].setEta(Integer.parseInt(ins.nextLine()));       // conversione di intero per utilizzare lo stesso Scanner
        }

        // Stampa risultato tramite Chiamata del metodo "etaMinima()" da Classe Università
        System.out.println("\nL'età minima dei docenti della UWC è di anni " + UWC.etaMinima());

        ins.close();
    }
}