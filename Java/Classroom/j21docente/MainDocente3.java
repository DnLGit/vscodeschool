/*
Bergamasco Daniele IV As - Esercizio 20:  Docente - 10.03.2020

Esercizio su array di oggetti
Scrivere una classe Docente che rappresenti le seguenti informazioni relative ad un docente: nome, cognome, codice ed età, e che contenga il costruttore parametrizzato(quello che prende in input tutte le variabili di istanza) ed i metodi getCodice, getCognome e getEta che restituiscono rispettivamente il codice, il cognome e l’età del docente. Scrivere poi una classe Universita, che rappresenti un insieme di docenti universitari tramite un array di tipo Docente, e che contenga un costruttore che accetta l’array di Docenti ed un metodo etaMinima che restituisce la minima età tra i docenti universitari.
Scrivere un main per testare il tutto.   */

package j21docente;
import java.util.*;                                                 // Libreria per utilizzare la classe Scanner e ... altro!

public class MainDocente3 {

    public static void main(String[] args) {

        System.out.println("\n***** Classe Docente & Università 3° soluzione *****\n");
        System.out.print("Inserire il numero di insegnanti di questa Università: ");

        Scanner ins = new Scanner(System.in);                                   // Dichiarazione e istanza dell'oggetto ins 
        int N = ins.nextInt();                                  // inizializzazione variabile dimensione array, da tastiera
        Docente prof[] = new Docente[N];                                        // Dichiarazione e istanza dell'array prof 
        Universita UWC = new Universita(prof);                                  // Dichiarazione e istanza dell'oggetto UWC

        
        System.out.println("\nInserisci in sequenza nome, cognome, codice e età dei "+N+" insegnanti");

        for(int i=0; i<N; i++){                     // ciclo per istanziare tutti gli oggetti dell'array prof[] da tastiera
            System.out.println("\n"+(i+1)+"^ insegnante: inserisci e premi invio: ");
            prof[i] = new Docente (ins.next(), ins.next(), ins.next(), ins.nextInt());
        }
        
        System.out.print("\nTabella delle età: ");            // ciclo for: chiamate di prova metodo getEta da classe Docente
        for (Docente docente : prof) {
            System.out.print(+docente.getEta() + "\t");                             // stampa di tutte le età dei docenti
        }

        // Stampa risultato tramite Chiamata del metodo "etaMinima()" da Classe Università
        System.out.println("\nL'età minima dei docenti della UWC è di anni " + UWC.etaMinima());

        ins.close();
    }
}