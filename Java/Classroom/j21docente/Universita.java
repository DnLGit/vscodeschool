/*
Bergamasco Daniele IV As - Esercizio 20:  Docente - 10.03.2020

classe Universita, che rappresenti un insieme di docenti universitari tramite un array di tipo Docente, e che contenga un costruttore che accetta l’array di Docenti 
ed un metodo etaMinima che restituisce la minima età tra i docenti universitari.    */

package j21docente;


public class Universita {

    private Docente prof[];                                                 // variabile d'istanza: array di docenti/prof  
    

    public Universita(Docente prof[]){                                  // costruttore per inizializzare i "prof" nel main
        this.prof = prof;
    }
    public int etaMinima(){                                 // Metodo per trovare il valore più piccolo fra le età dei "prof"
        int etaMin = this.prof[0].getEta();     // inizializzazione variabile di confronto al primo valore "eta" dell'array
        for(int i=0; i<this.prof.length; i++) {     // ciclo che percorre l'array, di lunghezza "length", di tutte le "età" 
            if(etaMin > this.prof[i].getEta())              // confronto della variabile "etaMin" con tutte le età iterate
                etaMin = this.prof[i].getEta();                         // se ne trova una minore la memorizza in etaMin ...
        }
        return etaMin;                                                      // ... e poi la ritorna come risultato al metodo
    }
}














// Metodo "etaMinima" da inserire successivamente quando risolvo con uno più elementare!!!

   


/*   public void stampaNomi(){                       // Questo metodo non centra con la richiesta dell'esercizio
    System.out.println("prova stampa nomi");    // l'ho creato solo per test, una prova di stampa nel main ... 
}                                               // ... e che non va!!!  */





/*  da prof. Celotto Skype 11.03.20

    public int etaMinima() {
		int etaMin = this.arrayDocenti[0].getEta();
		for(int i=0;i<this.arrayDocenti.length;i++) {
			if(etaMin > this.arrayDocenti[i].getEta()) etaMin = this.arrayDocenti[i].getEta();
		}
		return etaMin;
	}   */