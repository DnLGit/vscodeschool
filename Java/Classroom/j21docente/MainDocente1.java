/*
Bergamasco Daniele IV As - Esercizio 20:  Docente - 10.03.2020

Esercizio su array di oggetti
Scrivere una classe Docente che rappresenti le seguenti informazioni relative ad un docente: nome, cognome, codice ed età, e che contenga il costruttore parametrizzato(quello che prende in input tutte le variabili di istanza) ed i metodi getCodice, getCognome e getEta che restituiscono rispettivamente il codice, il cognome e l’età del docente. Scrivere poi una classe Universita, che rappresenti un insieme di docenti universitari tramite un array di tipo Docente, e che contenga un costruttore che accetta l’array di Docenti ed un metodo etaMinima che restituisce la minima età tra i docenti universitari.
Scrivere un main per testare il tutto.   */

package j21docente;

public class MainDocente1 {

    public static void main(String[] args) {

        System.out.println("\n***** Classe Docente & Università 1° soluzione *****\n");

        int N = 5;                                                                          // variabile dimensione array

        Docente prof[] = new Docente[N];                                        // Dichiarazione e istanza dell'array prof 
        Universita UWC = new Universita(prof);                                  // Dichiarazione e istanza dell'oggetto UWC

        prof[0] = new Docente ("Emilio", "Celotto", "EM01", 33);                                // istanza oggetto prof[0]  
        prof[1] = new Docente ("Pasquale", "Sergi", "PS02", 36);                                // istanza oggetto prof[0]
        prof[2] = new Docente ("Federica", "Marcuzzi", "FM03", 29);                             // istanza oggetto prof[0]
        prof[3] = new Docente ("Massimo", "DaRos", "MD04", 45);                                 // istanza oggetto prof[0]
        prof[4] = new Docente ("Silvana", "Frattoluso", "HX05", 37);                            // istanza oggetto prof[0]


        System.out.print(prof[0].getEta() + "\t");                              // chiamate di prova metodo da classe Docente
        System.out.print(prof[1].getEta() + "\t");                              // chiamate di prova metodo da classe Docente
        System.out.print(prof[2].getEta() + "\t");                              // chiamate di prova metodo da classe Docente
        System.out.print(prof[3].getEta() + "\t");                              // chiamate di prova metodo da classe Docente
        System.out.print(prof[4].getEta() + "\t\n\n");                          // chiamate di prova metodo da classe Docente

        // Stampa risultato tramite Chiamata metodo da Classe Università
        System.out.println("L'età minima dei docenti della UWC è di anni " + UWC.etaMinima());
    }
}