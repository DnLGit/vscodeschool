/*
Bergamasco Daniele IV As - Esercizio 20:  Docente - 10.03.2020

classe Docente che rappresenti le seguenti informazioni relative ad un docente: 
nome, cognome, codice ed età, 
e che contenga il costruttore parametrizzato (quello che prende in input tutte le variabili di istanza) 
ed i metodi getCodice, getCognome e getEta che restituiscono rispettivamente il codice, il cognome e l’età del docente. */

package j21docente;


public class Docente {

    private String nome, cognome, codice, anni;           // Variabili d'istanza
    private int eta;


// Costruttore senza parametri
    public Docente(){
        
    }


// Costruttori a 4 parametri
     public Docente(String nome, String cognome, String codice, int eta){
        this.nome = nome; this.cognome = cognome; this.codice = codice; 
        this.eta = eta; 
    }
    public Docente(String nome, String cognome, String codice, String anni){
        this.nome = nome; this.cognome = cognome; this.codice = codice; 
        this.anni = anni; 
    }

// Metodi Getter & Setter
    public String getNome(){
        return this.nome;
    }
    public void setNome(String nome){
        this.nome = nome;
    }

    public String getCognome(){
        return this.cognome;
    }
    public void setCognome(String cognome){
        this.cognome = cognome;
    }

    public String getCodice(){
        return this.codice;
    }
    public void setCodice(String codice){
        this.codice = codice;
    }

    public int getEta(){
        return this.eta;
    }
    public void setEta(int eta){
        this.eta = eta;
    }
    public int getYyy(){
        return this.eta;
    }
    public void setAnni(String anni){
        this.anni = anni;
    }
}