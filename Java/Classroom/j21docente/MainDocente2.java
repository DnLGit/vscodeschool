/*
Bergamasco Daniele IV As - Esercizio 20:  Docente - 10.03.2020

Esercizio su array di oggetti
Scrivere una classe Docente che rappresenti le seguenti informazioni relative ad un docente: nome, cognome, codice ed età, e che contenga il costruttore parametrizzato(quello che prende in input tutte le variabili di istanza) ed i metodi getCodice, getCognome e getEta che restituiscono rispettivamente il codice, il cognome e l’età del docente. Scrivere poi una classe Universita, che rappresenti un insieme di docenti universitari tramite un array di tipo Docente, e che contenga un costruttore che accetta l’array di Docenti ed un metodo etaMinima che restituisce la minima età tra i docenti universitari.
Scrivere un main per testare il tutto.   */

package j21docente;
import java.util.*;                                                 // Libreria per utilizzare la classe Scanner e ... altro!

public class MainDocente2 {

    public static void main(String[] args) {

        System.out.println("\n***** Classe Docente & Università 2° soluzione *****\n");
        System.out.print("In questo istituto ci sono 5 insegnati\n");
        
        int N = 5;                                                              // variabile dimensione array

        Scanner ins = new Scanner(System.in);                                   // Dichiarazione e istanza dell'oggetto ins 
        Docente prof[] = new Docente[N];                                        // Dichiarazione e istanza dell'array prof 
        Universita UWC = new Universita(prof);                                  // Dichiarazione e istanza dell'oggetto UWC

        
        System.out.println("\nInserisci in sequenza nome, cognome, codice e età dell'insegnante di Informatica: ");
        prof[0] = new Docente (ins.next(), ins.next(), ins.next(), ins.nextInt());  // istanza oggetto prof[0] da tastiera

        System.out.println("\nInserisci in sequenza i dati dell'insegnante di Inglese: ");
        prof[1] = new Docente (ins.next(), ins.next(), ins.next(), ins.nextInt());  // istanza oggetto prof[1] da tastiera

        System.out.println("\nInserisci in sequenza ora quelli dell'insegnante di Laboratorio: ");
        prof[2] = new Docente (ins.next(), ins.next(), ins.next(), ins.nextInt());  // istanza oggetto prof[2] da tastiera

        System.out.println("\nOra i dati dell'insegnante di Telecomunicazioni: ");
        prof[3] = new Docente (ins.next(), ins.next(), ins.next(), ins.nextInt());  // istanza oggetto prof[3] da tastiera

        System.out.println("\ned in fine, inserisci i dati dell'insegnate, di Matematica: ");
        prof[4] = new Docente (ins.next(), ins.next(), ins.next(), ins.nextInt());  // istanza oggetto prof[2] da tastiera


        System.out.print("\n"+prof[0].getEta()+"\t"+prof[1].getEta()+"\t"+prof[2].getEta()+"\t"+ prof[3].getEta()+"\t"+prof[4].getEta() + "\t\n");                                              // chiamate di prova metodo, da classe Docente

        // Stampa risultato tramite Chiamata metodo da Classe Università
        System.out.println("\nL'età minima dei docenti della UWC è di anni " + UWC.etaMinima());

        ins.close();
    }
}