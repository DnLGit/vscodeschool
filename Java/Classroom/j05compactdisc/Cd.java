/*

Bergamasco Daniele IV As - Esercizio 5:  Programmi con classi e costruttori/copia  18.02.2020

- costruttore che ha come parametri "titolo, autore, numeroBrani, durata";
- costruttore di copia;
- "getTitolo, getAutore, getNumeroBrani e getDurata" che restituiscono i valori degli attributi relativi;
- "setTitolo, setAutore, setNumeroBrani e setDurata" che modificano i valori degli attributi relativi;
- toString che restituisce una stringa con tutti i dati dell'oggetto su cui è invocato;
- compareDurata che consente di confrontare la durata complessiva del cd con la durata complessiva di un altro cd.

*/

package j05compactdisc;


public class Cd {

    private String titolo, autore;
    private int nBrani, durata;

	
    // costruttori
    public Cd(String titolo, String autore, int nBrani, int durata){    // costruttore principale
        this.titolo = titolo; this.autore = autore;
        this.nBrani = nBrani; this.durata = durata;
    }
    public Cd(Cd copyCd){                                               // costruttore di copia
        this.titolo = copyCd.titolo; this.autore = copyCd.autore;
        this.nBrani = copyCd.nBrani; this.durata = copyCd.durata;
    }

    // Metodi Getter e Setter
    public String getTitolo(){
        return titolo;
    }
    public String getAutore() {
        return autore;
    }
    public int getNBrani(){
        return nBrani;
    }
    public int getDurata(){
        return durata;
    }

    public void setTitolo(String titolo){
        this.titolo = titolo;
    }
    public void setAutore(String autore){
        this.autore = autore;
    }
    public void setNBrani(int nBrani){
        this.nBrani = nBrani;
    }
    public void setDurata(int durata){
        this.durata = durata;
    }


    // Altri Metodi
    public String toString(){                       // restituisce una stringa con tutti i valori degli oggetti
        return "Title: "+titolo+" | Artist: "+autore+" | Traks: "+nBrani+" | Length: "+durata+" minuti";
    }
    
    public boolean compareDurata(Cd copyCd){        // comparazione fra i tempi di durate dei due cd
        return this.durata < copyCd.durata;
    }
}