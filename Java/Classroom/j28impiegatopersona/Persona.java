
/*  Classe Persona ha le variabili nome, cognome, età. 
    Ha i metodi GetNome, GetCognome, GetEtà e 
    un metodo dettagli() che restituisce in una stringa le informazioni sulla persona in questione. */

package j28impiegatopersona;

public class Persona {

    private String nome, cognome;
    private int eta;

    /**
     * metodo dettagli della superclasse Persona
     * @return nome, cognome ed eta'
     */
    public String dettagli(){
        return  "il sig. "+nome+" "+cognome+" ha "+eta+" anni.";
    }

    // metodi Getter e Setter:
    public String getNome() {
        return this.nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCognome() {
        return this.cognome;
    }
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    public int getEta() {
        return this.eta;
    }
    public void setEta(int eta) {
        this.eta = eta;
    }
}