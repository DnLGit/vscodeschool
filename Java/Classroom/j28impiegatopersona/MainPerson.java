/*
Bergamasco Daniele IV As - Esercizio 28:  Impiegato e Persona su ereditarietà - 12.05.2020

Create il tipo di dato Impiegato come estensione del tipo di dato Persona. 
- Dove una classe Persona ha le variabili nome, cognome, età. 
  Ha i metodi GetNome, GetCognome, GetEtà e 
  un metodo dettagli() che restituisce in una stringa le informazioni sulla persona in questione.
- Dove una classe Impiegato ha le variabili nome, cognome, età, salario. 
  Ha un metodo dettagli() che restituisce in una stringa le informazioni sulla persona in questione.
  Ha un metodo aumentasalario() che aumenti lo stipendio secondo una certa percentuale.
Scrivere un main per testare il tutto.  */

package j28impiegatopersona;

public class MainPerson {
    public static void main(String[] args) {

      System.out.println("\n****************** Classi Persona&Impiegato ******************\n");

      Persona man1 = new Persona();                           // Istanza dell'oggetto Persona di nome man1
      Persona woman1 = new Persona();                         // Istanza dell'oggetto Persona di nome woman1
      Impiegato emp1M = new Impiegato();                      // Istanza dell'oggetto Persona di nome emp1M
      Impiegato emp1F = new Impiegato();                      // Istanza dell'oggetto Persona di nome emp1F

      System.out.println(man1.dettagli());                    // Test stampa valori dei costruttori di default
      System.out.println(woman1.dettagli());
      System.out.println(emp1M.dettagli());
      System.out.println(emp1F.dettagli());

      man1.setNome("Gomez");                                  // Invocazione dei vari metodi setter
      man1.setCognome("Addams");
      man1.setEta(55);      

      woman1.setNome("Morticia");
      woman1.setCognome("Addams");
      woman1.setEta(45);

      emp1M.setNome("GomezImpiegato");
      emp1M.setCognome("AddamsImpiegato");
      emp1M.setEta(56);
      emp1M.setSalario(1500);

      emp1F.setNome("MorticiaImpiegata");
      emp1F.setCognome("AddamsImpiegata");
      emp1F.setEta(46);
      emp1F.setSalario(1600);

      System.out.println();

      System.out.println(man1.dettagli());                    // Invocazione del metodo "dettagli" dai vari oggetti
      System.out.println(woman1.dettagli());
      System.out.println(emp1M.dettagli());
      System.out.println(emp1F.dettagli());

      emp1M.aumentaSalario(10);                               // invocazione metodo salario della sottoclasse impiegato
      emp1F.aumentaSalario(20);

      System.out.println("\nmentre ora\n"+emp1M.dettagli());
      System.out.println("e "+emp1F.dettagli());

      System.out.println("\nRipeto, il nuovo salario di "+emp1M.getNome()+" e' di "+emp1M.getSalario()+" euro.");
    }
}