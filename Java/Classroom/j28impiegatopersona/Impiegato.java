/*  ... Dove una classe Impiegato ha le variabili nome, cognome, età, salario. 
    Ha un metodo dettagli() che restituisce in una stringa le informazioni sulla persona in questione.
    Ha un metodo aumentasalario() che aumenti lo stipendio secondo una certa percentuale.   */

package j28impiegatopersona;

public class Impiegato extends Persona {

    private String nome, cognome;
    private int salario;

    
   /**
     * metodo dettagli della sottoclassse Impiegato
     * @return nome, cognome e salario
     */
    public String dettagli(){
        return  "il sig. "+nome+" "+cognome+" guadagna "+salario+" €/mese.";
    }
    /**
     * metodo della classe Impiegato
     * @param percento (double >> int non lo considera!)
     */
    public void aumentaSalario (double percento){
        salario += salario*(percento/100); 
    }

    // Metodi Getter e Setter:
    public String getNome() {
        return this.nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCognome() {
        return this.cognome;
    }
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    public int getSalario() {
        return this.salario;
    }
    public void setSalario(int salario) {
        this.salario = salario;
    }
}