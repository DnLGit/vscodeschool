#include <stdio.h>
#include <string.h>


/*  Test di codice C 26.12.2019 */

/* ------------------------ 
----------------------------------- */

int main(){

/*char leggo;
printf ("\nInserire un carattere : ");
leggo=getchar ();
printf ("\n E' stato inserito il carattere: %c",leggo);*/

/* ------------------------ 

char lettera = 'a';
char lettera2 = 'b';
//putchar (lettera);                    //stampa la lettera a

printf ("%c, %c\n", lettera, lettera2);   //stampa tutto ciò che viene indicato

int x = 10; 
printf("Il numero è %d \n", x);


char nomeCognome[80];
printf("Inserisci nome e cognome: %s ", nomeCognome);
//scanf(" %[^\n]", nomeCognome);                        // stampa stringa con spazi (senza usare gets())
//gets(nomeCognome);                                    // stampa stringa con spazi

fgets(nomeCognome, 80, stdin);                          // (aggiunta libreria string.h)
nomeCognome [strlen(nomeCognome)-1] = '\0';             

printf("Il nome e cognome e': %s ", nomeCognome);

----------------------------------- */


/* --------  strlen(s)  ---------------- 

char saluta[80] = "Ciao";
char nomeCognome[80]  =  "Devis Dal Moro";

strlen(saluta);
strlen(nomeCognome);
int leng1 = strlen(saluta);
int leng2 = strlen(nomeCognome);
printf("saluta è lunga: %d e nomeCognome è lungo: %d:  ", leng1, leng2);

----------------------------------- */


/* -------  strcpy(s1, s2)  ----------------- 

    char saluta[80] = "Ciao";
    char nomeCognome[80] = "Devis Dal Moro";


    char nomeCopiato[80] = "Niente";
        // strcpy(s1, s2) copia s2 in s1
        // adesso nomeCopiato vale “Niente”
    printf("nomeCopiato ora è: %s\n", nomeCopiato);
    strcpy( nomeCopiato, "Daniele"); 
        // adesso nomeCopiato vale “Daniele”
    printf("nomeCopiato ora è: %s\n", nomeCopiato);
    strcpy ( nomeCopiato, nomeCognome);
        // adesso nomeCopiato vale “Devis Dal Moro”
    printf("nomeCopiato ora è: %s\n", nomeCopiato);
    strcpy ( nomeCopiato, saluta);
        // adesso nomeCopiato vale “Ciao Devis Dal Moro”
    printf("nomeCopiato ora è: %s\n", nomeCopiato);

----------------------------------- */


/* ------   strcat(s1, s2)   [concatena 2 stringhe] ------------------ 

    char saluta[80] = "Ciao";
    char nomeCognome[80] = "Devis Dal Moro";


    // strcat(s1, s2) concatena s2 a s1, mettendo il risultato in s1

    // adesso saluta vale “Ciao”
    printf( " adesso saluta vale: %s\n", saluta);

    strcat( saluta, ", "); 
    // adesso saluta vale “Ciao ”
    printf( " adesso saluta vale: %s\n", saluta);

    strcat( saluta, nomeCognome);
    // adesso saluta vale “Ciao Devis Dal Moro”
    printf( " adesso saluta vale: %s\n", saluta);

----------------------------------- */



/* ------  strcmp  ------------------ 

char saluta[80] = "Ciao";
char nomeCognome[80]  =  "Devis Dal Moro";


strcmp( nomeCognome, "Devis Dal Moro");     // ritorna 0
//strcmp( nomeCognome, "Daniele");  // ritorna valore pos.
//strcmp("Daniele", nomeCognome);   // ritorna valore neg.

int risultato = strcmp("Devis Dal Moro", nomeCognome);
printf("risultato è: %d ", risultato);

----------------------------------- */












}