#include <string>
#include <cstring>                              // dovrebbe sostituire <string.h> e permettere di usare le funzioni di C in C++
#include<iostream>
using namespace std;

/* Bergamasco Daniele  01.01.20  - Stringhe - Esercizio n° 5

    Ricerca sottostringa 
    
    Si scriva un programma in linguaggio C che riceva in ingresso due parole inserite da tastiera. 
    Si consideri che ciascuna parola può contenere al massimo 30 caratteri. 
    Il programma deve verificare se la seconda parola inserita è contenuta almeno una volta all’interno della prima parola 
    (ossia se la seconda parola è una sottostringa della prima parola).  */


#define CharMax 30       // direttiva per il preProcessore  // ** ma se la richiesta è  di 30, CharMax dovrebbe essere 30+1??

int main()                                          // ** = domande per Devis!!
{
    cout << " \n***** Ricerca sottostringa ****** \n " << endl;

    char parola1[CharMax];                                                // dichiarazione 1^ frase, da inserire da tastiera
    char parola2[CharMax];                                                // dichiarazione 2^ frase,  da costruire successivamente

    cout << "Inserisci due parole\n " << endl;

    cin.getline(parola1, CharMax);                 // ** essendo la richiesta di "parole" potrebbe essere sufficiente il cin?
    cin.getline(parola2, CharMax);                 // ** essendo la richiesta di "parole" potrebbe essere sufficiente il cin?

    int long1 = strlen(parola1);
    int long2 = strlen(parola2);

    cout << "La prima parola: " << parola1 << " e' di " << long1 << " caratteri" <<endl;  // portofino
    cout << "La seconda parola: " << parola2 << " e' di " << long2 << " caratteri" <<endl;  // fino

/*  ************************    proviamo con array già inizializzati    *********************************    */

    int long3 = 10;
    int long4 = 5;
    int long5 = 12;
    int long6 = 6;

    char parola1 [long3] = "portofino";
    char parola2 [long4] = "fino";
    char parola3 [long5] = "sbarramento";
    char parola4 [long6] = "barra";

/*    for(int i=0; i+(5-1)<10-1; i++)
    {
        for(int j=0; j<5-1; j++)
        {
            if(parola1[i] == parola2[j])
            {
                cout <<"ok, funzia: " <<parola1[i]<<endl;
            }
        }
    }
*/
/*    int pos =0;
    for(int i=0; i<long1; i++)  // qui volevo trovare la "f" di inizio ... ma se ce n'è più di una risulta e' inutile!!!
    {
        if(parola2[0] == parola1[i])
        {
            pos = i;
        }
    }

    int count =0;
    for(int j=0; j<long2; j++)  // qui invece trovo si tutta la parola ma ... anche altre lettere uguali ...
    {
        for(int i=0; i<long1; i++)
        {
            if(parola2[j] == parola1[i])
            {
                cout <<"ok, funzia: " <<parola2[j]<<endl;
                count+=1;
            }
        }
    }

    cout <<" count e': " << count <<endl;
*/

/* ************** proviamo con soluzione "con cicli" di esercizio simile trovato in rete ***************** */

//  ciclo for esterno che scorre parola1
    int finito = 0;                                     // indica la conclusione della ricerca 
    for(int i=0; i+(long2-1)<long1 && finito==0; i++)
    {
        int uguali =1;
        for(int j=0; j<long2 && uguali==1; j++)
        {
            if(parola1[i+j] != parola2[j])
                uguali = 0;
        }
        if(uguali ==1)
            finito = 1;
    }

    cout <<endl;
    if(finito == 1)
        cout << "parola2: " << parola2 <<" e' contenuta in parola1: " << parola1 <<endl;
    else
    {
        cout << "parola2: " << parola2 <<" NON e' contenuta in parola1: " << parola1 <<endl;
    }

    /*  Funzione maaaaaa .... non ho capito!!!!!    */



//  ciclo for esterno che scorre parola3
    int finito2 = 0;                                     // indica la conclusione della ricerca 
    for(int i=0; i+(long4-1)<long3 && finito2==0; i++)
    {
        int uguali =1;
        for(int j=0; j<long4 && uguali==1; j++)
        {
            if(parola3[i+j] != parola4[j])
                uguali = 0;
        }
        if(uguali ==1)
            finito2 = 1;
    }

    cout <<endl;
    if(finito2 == 1)
        cout << "parola4: " << parola4 <<" e' contenuta in parola3: " << parola3 <<endl;
    else
    {
        cout << "parola4: " << parola4 <<" NON e' contenuta in parola3: " << parola3 <<endl;                                                               
    }


    return 0;
}