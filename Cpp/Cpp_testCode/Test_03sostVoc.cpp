#include <string>
#include <cstring>  // dovrebbe sostituire <string.h> e permettere di usare le funzioni di C in C++
#include<iostream>
using namespace std;

/* Bergamasco Daniele  28.12.19  - Stringhe - Esercizio n° 3

    Codifica di una parola

    Scrivere un programma in linguaggio C che legga una frase introdotta da tastiera. 
    La frase è terminata dall’introduzione del carattere di invio. 
    La frase contiene sia caratteri maiuscoli che caratteri minuscoli, e complessivamente al più 100 caratteri. 
    Il programma deve svolgere le seguenti operazioni: 
    * visualizzare la frase inserita; 
    * costruire una nuova frase tale che ogni lettera vocale presente nella frase di partenza sia seguita 
        dalla lettera ’f’ (se la vocale è minuscola) o 
        dalla lettera ’F’ (se la vocale è maiuscola) nella nuova frase. 
        Il programma deve memorizzare la nuova frase in una opportuna variabile. 
    * visualizzare la nuova frase.*/

#define CharMax 100                                                             // direttiva per il preProcessore

int main()
{
    cout << " \n***** Sostituisci vocali con f&F ****** \n " << endl;

    char fraseC[CharMax];                                                       // dichiarazione 1^ frase, da inserire da tastiera
    char fraseC2[CharMax];                                                      // dichiarazione 2^ frase,  da costruire successivamente

    cout << "Inserisci una frase: \n";
    
    cin.getline(fraseC, CharMax);                                                // acquisizione di un array di caratteri con spazi (fgets prende anche \n)
    int longC = strlen(fraseC);                                                  // calcolo lunghezza della 1^ frase
    cout << "La frase inserita e': " << fraseC << ", lunga " << longC <<endl;

    
    int vocm = 0;                                                               // contatore vocali minuscole
    int vocM = 0;                                                               // contatore vocali maiuscole
    
    for(int i=0; i<longC; i++)                                                  // ciclo for: scansione 1^ frase
    {
        fraseC2[i+vocm] = fraseC[i];                                            // assegnazione valori 1^ frase alla 2^ frase
        if(fraseC[i] == 'i' || fraseC[i] == 'u' || fraseC[i] == 'o' || fraseC[i] == 'a' || fraseC[i] == 'e')
        {
            fraseC2[i+vocm+1] ='f';                                             // se trovo una vocale minuscola aggiungo una f
            vocm++;                                                             // incremento contatore delle vocali minuscole
        }
         if(fraseC[i] == 'I' || fraseC[i] == 'U' || fraseC[i] == 'O' || fraseC[i] == 'A' || fraseC[i] == 'E')
        {
            fraseC2[i+vocM+1] ='F';                                                // se trovo una vocale maiuscola aggiungo una F 
            vocM++;                                                                // incremento contatore delle vocali maiuscole
        }                   // Attenzione:  unificare vocm e vocM!!!!!
         
    }

    int longC2 = longC+vocm;
    
    cout << "longC2 vale: " <<longC2<<endl;
    cout << "La lunghezza del nuovo array e': "<< longC2 <<endl;
    cout << "L'array nuovo e': ";
    cout <<fraseC2[0] <<fraseC2[1] <<fraseC2[2] <<fraseC2[3] <<fraseC2[4] <<fraseC2[5] <<fraseC2[6] <<endl;
   
    cout << "La frase nuova e': ";
        for(int i=0; i<10; i++)
    {
        cout <<fraseC2[i];          // Attenzione:  e' sufficiente un "cout <<fraseC2;" ... non serve il for!!
    }


/*
    string fraseCpp;
    string fraseCpp2;
    cout << "Inserisci una frase: \n";
    getline(cin, fraseCpp, '\n');
    int Ns = fraseCpp.length();
    cout << " questa e' la fraseCpp: " << fraseCpp << endl;
    cout << " ed e' lunga " << Ns << " caratteri! " << endl;

    int Ns2=Ns;
    int j;
    for(int i=0; i<Ns; i++)
    {
        fraseCpp2[j] = fraseCpp[i];
        if(fraseCpp2[j] == 'i')
        {
            for( j=0; j<10; j++)
            {
                fraseCpp2[j+1] = 'Z';
                
            }
        }
        
    }



    for(int i=0; i<10; i++)
    {
        cout <<fraseCpp2[i];
    }
*/

    // cout << " questa e' la fraseCpp2 ora: " << fraseCpp2 << endl;

    /*        for( j=0; j<Ns2; j++)
        {   
                   
            if(fraseCpp[j] == 'i' || fraseCpp[j] == 'u' || fraseCpp[j] == 'o' || fraseCpp[j] == 'a' || fraseCpp[j] == 'e')
            {
                fraseCpp2[j+1] = 'f';
            }
            
        }*/
        










    return 0;
}