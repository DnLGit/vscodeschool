#include <string>
#include<iostream>
using namespace std;
#include <cstring>  // dovrebbe sostituire <string.h> e permettere di usare le funzioni di C in C++ 
/*  **********************************************************************************************
    
    Vedi link: https://stackoverflow.com/questions/12824595/difference-between-cstring-and-string

    In C++, you wouldn't use #include <somefile.h>, but instead #include <somefile>. 
    Now C++ has its string classes in <string>, but the c-string functions are also available, which would be in <string.h>. 
    C++ uses for 'traditional' c- include files. Therefore, <cstring> and <string>

    http://www.cplusplus.com/reference/clibrary/cstring/   
    ***********************************************************************************************    */

/*  Test di codice C++ 27.12.2019 */

int main(){

/* ------------------------ 
----------------------------------- */

/* ------------------------ 

char lettera = 'a';
char lettera2 = 'b';

int x = 10; 

cout <<lettera<<lettera2<<endl;

char nome1[6] = "Devis";
cout << nome1<<endl;

char carattere3;
cout << "inserisci carattere: ";
cin >> carattere3;

cout <<"Il carattere inserito e': "<<carattere3<<endl;

char frase1[] = "Daniele è qui";
cout << frase1<<endl;

char frase2[40];
cout << "inserisci frase2: ";
cin >> frase2;
cout << frase2<<endl;

string frase3;
cout << "inserisci frase3: ";
cin >> frase3;
cout << frase3<<endl;


char nomeCognome[80];
cout << "Inserisci il tuo nome: "; 
cin.getline(nomeCognome, 80); 
cout << "inserito: " <<nomeCognome<<endl;
----------------------------------- */


/*------- strlen  e  s.length   ----------------------------

char saluti[80] = "Ciao";                               // con array di caratteri
char nomiCognomi[80]  =  "Devis Dal Moro";
string saluta = "Ciao";                                 // con stringhe
string nomeCognome = "Devis Dal Moro";

strlen(saluti);                                         // con strlen()
strlen(nomiCognomi);
int leng1 = strlen(saluti);
int leng2 = strlen(nomiCognomi);
cout << "saluti è lunga: " << leng1 << " e nomiCognomi è lungo: " << leng2 <<endl;

saluta.length();
nomeCognome.length();
int leng3 = saluta.length();
int leng4 = nomeCognome.length();
cout << "saluta è lunga: " << leng3 << " e nomeCognome è lungo: " << leng4 <<endl;

----------------------------------- */


/* -------   copia stringa (s1=s2)  e  strcpy    ----------------- 

    string saluta = "Ciao";
    string nomeCognome = "Devis Dal Moro";


    string nomeCopiato = "Niente";
    // s1 = s2 copia s2 in s1
    // adesso nomeCopiato vale “Niente”
    cout << "ora nomeCopiato vale: " << nomeCopiato <<endl;
    nomeCopiato = "Daniele"; 
    // adesso nomeCopiato vale “Daniele”
    cout << "ora nomeCopiato vale: " << nomeCopiato <<endl;
    nomeCopiato = nomeCognome;
    // adesso nomeCopiato vale “Devis Dal Moro”
    cout << "ora nomeCopiato vale: " << nomeCopiato <<endl;
    nomeCopiato = saluta;
    // adesso nomeCopiato vale “Ciao Devis Dal Moro”
    cout << "ora nomeCopiato vale: " << nomeCopiato <<endl;


    char saluta[80] = "Ciao";
    char nomeCognome[80] = "Devis Dal Moro";

    char nomeCopiato[80] = "Niente";
        // strcpy(s1, s2) copia s2 in s1
        // adesso nomeCopiato vale “Niente”
    cout << "nomeCopiato ora è: " << nomeCopiato <<endl;
    strcpy( nomeCopiato, "Daniele"); 
        // adesso nomeCopiato vale “Daniele”
    cout << "nomeCopiato ora è: " << nomeCopiato <<endl;
    strcpy ( nomeCopiato, nomeCognome);
        // adesso nomeCopiato vale “Devis Dal Moro”
    cout << "nomeCopiato ora è: " << nomeCopiato <<endl;
    strcpy ( nomeCopiato, saluta);
        // adesso nomeCopiato vale “Ciao Devis Dal Moro”
    cout << "nomeCopiato ora è: " << nomeCopiato <<endl;

----------------------------------- */



/* -------  s1 += s2 (concatena 2 stringhe)  ----------------- 

string saluta = "Ciao";
    string nomeCognome = "Devis Dal Moro";


    // s1 += s2 concatena s2 a s1, mettendo il risultato in s1

    // adesso saluta vale “Ciao”
    cout << "adesso saluta vale: " << saluta <<endl;

    saluta += ", "; 
    // adesso saluta vale “Ciao ”
    cout << "adesso saluta vale: " << saluta <<endl;

    saluta += nomeCognome;
    // adesso saluta vale “Ciao, Devis Dal Moro”
    cout << "adesso saluta vale: " << saluta <<endl;

----------------------------------- */



/* -----  s.compare  e  strcmp  ------------------- 


strcmp( nomiCognomi, "Devis Dal Moro");     // ritorna 0
strcmp( nomiCognomi, "Daniele");  // ritorna valore pos.
strcmp( "Daniele", nomiCognomi);  // ritorna valore neg.
int resC = strcmp( nomiCognomi, "Devis Dal Moro");

nomeCognome.compare("Devis Dal Moro");                       // ritorna 0
nomeCognome.compare("Daniele");                    // ritorna valore pos.
nomeCognome.compare("Daniele", nomiCognomi);       // ritorna valore neg.

int risultato = nomeCognome.compare("Devis Dal Moro");

cout <<"resC e': "<<resC<<endl;
cout <<"risultato e': "<<risultato<<endl; 

----------------------------------- */

char saluti[10] = "a12666c";                               // con array di caratteri

int alfa=0;
int num =0;

for(int i=0; i<10 && saluti[i]!='\0'; i++)
{

    if(isdigit(saluti[i]))
        num++;
}

/*
int i=0;
while(saluti[i]!='\0')
{
    if(isdigit(saluti[i]))
    num++;
    if(isalpha(saluti[i]))
    alfa++;
    i++;
}
*/

cout <<alfa<<endl;
cout << num<<endl;




return 0;

}