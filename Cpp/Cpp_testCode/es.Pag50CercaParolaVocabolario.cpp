/*  C++     */
#include <string>
#include <cstring>  // dovrebbe sostituire <string.h> e permettere di usare le funzioni di C in C++
#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 04.01.2020 >>>>>>>>>>>> Esercizi con Matrici

    Pag. 50 Dispensa Celotto: Cerca parole in un Vocabolario
    Array di stringhe
    Una applicazione interessante delle matrici è l’array di stringhe. La stringa è un array di caratteri e quindi potremmo dire che un array di stringhe è un array di array di caratteri. Per chiarire la gestione di questo tipo di array si propone un programma che si occupa di cercare delle parole in un vocabolario e riferisce, per ognuna, se è contenuta nel vocabolario stesso     */

int main()
{
    cout << "\n********** Cerca parole in un Vocabolario *********** \n\n";

    int row = 10;               // dichiarazione e inizializzazione righe della matrice
    int col = 20;               // dichiarazione e inizializzazione colonne della matrice
    char vocab[row][col];       // dichiarazione della matrice "vocabolario" che sara' di max 10 parole di 19 caratteri max.
    char parCercata[col];       // dichiarazione dell'array "parola da cercare"
    bool trovata;               // flag di parola trovata nel for di ricerca della parola (solo per C++, in C: int 0/1)

/*  Inserimento parole nella matrice "vocabolario"  */
    cout << "Inserire le parole nel vocabolario: \n";
    for(int i=0; i<row; i++)     // ciclo for per inserire le parole (array) nel vocabolario nelle "righe" della matrice
    {
        cin >> vocab[i];
    }

/*  Inserimento parola da cercare nella matrice "vocabolario"  */
    cout << "Inserire la parola da cercare nel vocabolario: ";
    cin >> parCercata;

    
    trovata = false;
    for(int i=0; i<row; i++)
    {
        if(strcmp(parCercata, vocab[i]) == 0)
            trovata = true;
    }

    if(trovata)
        cout << "La parola "<< parCercata << " e' nel vocabolario\n";
        cout <<endl;


    








    return 0;
}