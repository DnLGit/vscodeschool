#include <string>
#include <cstring>  // dovrebbe sostituire <string.h> e permettere di usare le funzioni di C in C++
#include<iomanip>       /* Libreria per la funzione setw(n)*/
#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 21.01.2020 >>>>>>>>>>>> prova Switch */

int main()
{
    cout << "\n******** Prova switch ********\n\n";
    int scelta;
    
    do
    {
        cout << "inserisci scelta: ";
        cin >> scelta;
        if(scelta <0 || scelta >2)
        cout << "Attenzione: scelta non valida, riprova:\n";
        
        switch (scelta)
        {
        case 1:
            cout << "Prova switch 1" <<endl;
            break;
                    
        case 2:
            cout << "Prova switch 2" <<endl;
            break;
        
        case 0:
            cout << "stai per uscire, bye bye";
            break;

        default:
            break;
        }
    } 
    while (scelta !=0);
 
return 0;
}
