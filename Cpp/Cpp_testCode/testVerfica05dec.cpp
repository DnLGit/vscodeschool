#include<iostream>
using namespace std;

/*  Test codici relativi alla Verifica del 05.12.2019 */
/*  copiati / controllati e riscritti */


int main()
{

/*  ****  Test con array "Vect1" già inizializzato  ****    */

    const int N = 10;
    int Vect1[10]={2,2,2,2,0,3,3,3,3,0};        

    for(int i=0; i<N; i++)
    {
        cout << Vect1[i]<<"\t";
    }
    cout << endl;

/*  c)  Calcolare la media degli elementi pari e dispari */

    int sommaP1=0, sommaD1=0, countP1=0, countD1=0;
    for(int i=0; i<N; i++){
        
        if(Vect1[i]%2==0 && Vect1[i]!=0){
            sommaP1+=Vect1[i];
            countP1++;
        }
        if(Vect1[i]%2!=0 && Vect1[i]!=0){
            sommaD1+=Vect1[i];
            countD1++;
        }
    }
    cout <<"sommaP1: " <<sommaP1<<endl;
    cout <<"countP1: " <<countP1<<endl;
    cout <<"sommaD1: " <<sommaD1<<endl;
    cout <<"countD1: " <<countD1<<endl;
    cout << endl;
    cout << "Media Pari e' " <<sommaP1/countP1<< " e Media Dispari e' "<<sommaD1/countD1<<endl;
    cout << endl;

/*  ****  Test con array "Vect2" inserito da tastiera  ****    */

    int Vect2[N];
    for(int i=0; i<N; i++){
        cout <<"Inserisci il "<<i+1<<" ^ valore: ";     // aggiunti: duepunti e spazio
        cin >>Vect2[i];
    }

    for(int i=0; i<N; i++)
    {
        cout << Vect2[i]<<"\t";
    }
    cout << endl;

    int sommaP2=0, sommaD2=0, countP2=0, countD2=0;
    for(int i=0; i<N; i++){
        
        if(Vect2[i]%2==0 && Vect2[i]!=0){
            sommaP2+=Vect2[i];
            countP2++;
        }
        if(Vect2[i]%2!=0 && Vect2[i]!=0){
            sommaD2+=Vect2[i];
            countD2++;
        }
    }
    cout <<"sommaP2: " <<sommaP2<<endl;
    cout <<"countP2: " <<countP2<<endl;
    cout <<"sommaD2: " <<sommaD2<<endl;
    cout <<"countD2: " <<countD2<<endl;
    cout << endl;
    cout << "Media Pari e' " <<sommaP2/countP2<< " e Media Dispari e' "<<sommaD2/countD2<<endl;






    return 0;
}