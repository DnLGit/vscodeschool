#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 15.01.2020 >>>>>>>>>>>> Esercizi con Funzioni e Matrici

    Esercizio libero in Laboratorio C (bunker III ora) -  Calcolo media diagonale.
    Realizzare un programma che calcoli la media dei valori della prima diagonale 
    sotto quella centrale di una matrice quadrata 4x4. */


    int const N = 4;
    //float mediaDiagonaleK(int M[][N], int N, int k, bool top);

int main()
{   
    cout << "\n********* Calcolo media valori diagonale 'k-1.0' di una matrice 4x4 ***********\n";


    int k;                                              // variabile per modifica indice diagonale
    float media;
    int matrix[N][N] = {{3,5,7,9}, {10,12,13,1},        // matrice di 4 righe e 4 colonne
                        {-5,7,8,9}, {6,7,11,12}};

    for(int i=0; i<N; i++)
    {
        for(int j=0; j<N; j++)
        {
            cout << matrix[i][j] << " ";
        }cout <<endl;
    }

    k = 3;
    media = 0;
    for(int i=0; i<N-k; i++)
    {
        media+=matrix[i+k][i];
    }

    cout << "la somma e': " << media << " e la media e': " << media/(N-k);





    return 0;
}