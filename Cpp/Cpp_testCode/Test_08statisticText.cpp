#include <iostream>
#include<string>

#include <string.h>

using namespace std;

/*  Bergamasco Daniele 4As - 02.12.2019 >>>>>>>>>>>> Esercizi con Funzioni e Matrici

    Esercizio n° 8 -  Statistiche Testo
        Si scriva un programma in C che acquisisca da tastiera un testo libero, composto da più righe (max 1000) di un numero di caratteri non superiore a 100 ciascuna. 
        L’inserimento termina quando l’utente inserirà una riga uguale a FINE. 
        Al termine dell’acquisizione del testo, il programma dovrà stampare le seguenti statistiche: 
        1. il numero totale di righe inserite;
        2. il numero totale di caratteri inseriti;
        3. il numero totale di caratteri alfanumerici inseriti;
        4. il numero totale di parole inserite. */

int main()
{
    cout <<"\n******** 8 - Statistiche Testo ********\n\n ";

    cout <<"Inserisci del testo libero:\n\n";

    int R = 1000;                       // dichiarazione variabile inizializzata RigheMax della matrice 
    int C = 100;                        // dichiarazione variabile inizializzata ColonneMax della matrice
    char Text[R][C];                    // dichiarazione matrice (array di ... array di caratteri)

    bool flag = false;
    for(int i=0; i<R && flag!=true; i++)
    {
        cin.getline(Text[i], C);        // acquisizione del testo

        if(strcmp(Text[i], "FINE")==0)  // fino a quando la funzione strcmp ritorna zero trovando la parola FINE
            flag = true;                // uscita dal ciclo //** ho sostituito il break con un flag (funziona: e' ok cosi'?)
    }

    cout << "\n\n Stampa Matrice:" << endl;

    int righe = 0;
    int caratteri = 0;
    int parole = 0;
    for(int i=0; i<R; i++)
    {   
        int j;
        for(j=0; j<C && Text[i][j] != '\0'; j++)    // j<C ma solo fino "al tappo"
            cout << Text[i][j];

        righe=i;                        // ad ogni uscita dal for interno inizia una nuova riga
        caratteri+=j;                   // ed una nuova sommatoria di caratteri   
        cout << endl;
        if(strcmp(Text[i], "FINE")==0)  // la stampa termina all'incontro di "FINE"
            break;
    }

    int longF = strlen(Text[righe]);    // calcolo la lunghezza dell'ultima parola (FINE)
    Text[righe][longF+1] = '\0';        // e ci metto il tappo (se mancante)
    cout<<"Tappo"<<Text[righe][longF+1]<<"Tappo"<<endl<<endl;   // verifico!!

/*
    for(int i=0; i<R; i++)
    {
        for(int j=0; j<C && Text[i][j] !='\0'; j++)    (input solo "FINE":  perche' mi stampa un sacco di "eh no"??)
        {
            cout << "eh no!";
        }

    }
*/


        
    cout <<endl<<endl;
    cout << "ci sono " << righe+1 << " righe in questo testo" <<endl;   // +1 perche' righe parte da zero!
    cout << "escludendo 'FINE' ci sono " << righe << " righe, in questo testo" <<endl<<endl;
    cout << "ci sono " << caratteri << " caratteri in questo testo" <<endl;
    cout << "escludendo 'FINE'ci sono " << caratteri-longF << " caratteri in questo testo" <<endl<<endl;
    cout << "ci sono " << parole << " parole in questo testo" <<endl; 

return 0;
}


/*  codice Devis: ............ ----->>>>> i commenti dopo //** sono domande per te!
cout << "\n\nTirato su" << endl;
    for(int i=0; i<R; i++)
    {
        for(int j=0; j<C && Text[i][j] != '\0'; j++)
            cout << Text[i][j] << "\t";
        cout << endl;
        if(strcmp(Text[i], "FINE")==0)
            break;
    }
*/

/*   && Text[i][j-1] != ' ' && Text[i][j+1] == ' ' && Text[i][j] == '\0'    */


/*
    Ho visto

    Ho visto le menti migliori della mia generazione distrutte da un piatto di pesce persico in una pizzeria di viale Palmanova. 
    Ho visto giovani promettenti rovinati dalle buone compagnie; li ho visti rubare droga per comprarsi un'autoradio. 
    Ho visto i teorici della coppia aperta devastati dagli spifferi! Ho visto donne manifestare in piazza contro l'aborto e poi avere dei figli a cui piace Masini. 
    Ho visto... ho visto coppie copulare ferocemente in oscuri pied-à-terre, talmente al buio che copulando al buio cascavan dal letto, continuando a copulare al buio sul pavimento, 
    nell'atrio, in corridoio, giù per le scale al buio, finché arrivavano in piazza sotto un lampione e alla luce si guardavano in faccia: non si erano mai visti prima! 
    Avevano sbagliato pied-à-terre. Era troppo buio. Non avevano mai copulato così bene! 
    Ho visto gente che per paura dell'Aids si faceva presentare il nuovo partner solo se aveva un certificato di sana costituzione fisica e se non l'aveva doveva venire all'appuntamento
    con il vecchio partner che facesse da garante. Ma il vecchio partner doveva venire all'appuntamento col nuovo e il vecchio partner. E il nuovo e il vecchio partner dovevano venire 
    all'appuntamento col nuovo e il vecchio partner. E il nuovo e il vecchio partner dovevano venire con il nuovo e il vecchio partner... così fino alla quarta generazione! 
    Li ho visti che non scopavano più... Ma conoscevano un casino di gente! 
    Ho visto... ho visto e ho sentito Maradona parlare in Tv e subito dopo ho sentito parlare Cossiga. E mi sono chiesto: ma chi è dei due qui che tira la coca? 
    Ho visto Beppe Marzullo. Io ho visto Beppe Marzullo alla televisione chiedere a centinaia di invitati: «Che cos'è per lei la notte?»... e ognuno di questi cento dare una risposta 
    diversa... Ma se ad ognuno di questi cento noi avessimo chiesto: «Che cos'è per lei Marzullo?» avrebbero dato tutti l'identica risposta: «Un coglione!» 
    Ho visto... ho visto... ho visto un giovane bellissimo uscire da una discoteca di Rimini, bello come un nuovo dio pagano, i capelli ingelatinati che sembrava Jovanotti, 
    i piedi simili a Schwarzenegger, il tronco di Barbareschi... perfetto... uscire dalla discoteca di Rimini, generoso: «Extasi, extasi per tutti!... Facciamoci... facciamoci!» ... 
    «Eh, ma ci siamo già fatti!»... «Rifacciamoci!»... L'ho visto saltare sulla sua Ferrarina Testarossa, partire sgommando a 280 all'ora e schiantarsi contro il primo albero!...
    E ho pianto, perché era un tiglio bellissimo! 
    FINE
    Ho visto durante la guerra del Golfo parlare i generali e mi sono chiesto: «Ma dove li trovano così imbecilli? Così idioti dove li trovano?» Poi ho sentito parlare un colonnello 
    e ho capito tutto: i generali li trovano tra i colonnelli! 
    Ho visto uomini politici invasati dalle proiezioni elettorali, dai termini Doxa, dalle statistiche, che non puoi dirgli neanche più: «Ma come sei diventato stronzo!» 
    che ti rispondono: «Sì, rispetto all'80, ma rispetto all'85 molto meno»... 
    Ho visto... ho visto il papa, io ho visto il papa. Io ho avuto un'educazione cattolica anche se mi sono confessato due volte sole. 
    La seconda sono andato dal parroco e il parroco mi ha detto: «Da quanto tempo è che non ti confessi figliolo?» «Dieci anni» «Ah! Sei venuto a costituirti?» 
    A me il personaggio che piaceva di più della Bibbia e di tutti gli altri compact, era san Giuseppe, il falegname. Perché san Giuseppe ha a che fare con noi. 
    Io me lo vedevo tutte le sere, prima di coricarsi con Maria: «Maria scusa, riparliamone... non voglio menarla, ma riparliamone.. . 
    C'è stata una gran luce... è apparso un angelo... ma esattamente cosa ti ha detto?» «Mi ha detto: donna tu domani farai un figlio e tuo marito sarà suo padre protettore, 
    lo so, è una storia un po' incasinata, lui non ci crederà subito, tu cerca di convincerlo, se lo convinci entra anche lui nell'immaginetta, se no morta lì!» 
    Io sono pieno di dubbi sul papa... Il papa, quando fa la pipì... si scrolla come noi (fa il gesto)... oppure si scrolla così (fa il gesto di spargere l'acqua benedetta). 
    Tutto, io ho visto tutto nella mia vita. 
    Io ho visto donne comportarsi da uomini e uomini comportarsi da donne, le prime trattate quasi sempre meglio dei secondi! 
    Ho visto fabbriche scambiarsi operai, padroni, azioni, ma non ho mai visto operai che dicevano: «Ci scambiamo la fabbrica!» 
    Ho visto uomini mettersi profilattici ai piedi per paura delle Adidas! 
    Ho visto... ho visto Craxi chiedere scusa! Io un giorno ho visto Craxi chiedere scusa... poi mi sono avvicinato, ho guardato meglio, non era Craxi. 
    Ho visto tutto nella mia vita, io ho visto tutto. 
    Io ho visto Trussardi!... Io ho visto Trussardi apparirmi in televisione e dire: «Voi non mi conoscete, ma io sono Trussardi, e con questo tesserino dell'American Express 
    posso andare dappertutto». Allora sono andato all'American Express e gli ho detto: «Voi non mi conoscete. 
    Sono Trussardi, ho perso 'sto cazzo di tesserino, devo andare solo a Treviso che mi han rubato 26 mila lire...» 
    Ho visto tutto... ho visto tutto... Io nella mia vita ho visto tutto e son contento! 
    Tre cose sole io non ho mai visto: 
    Primo. Non ho mai visto chi cazzo stava sulla Y 10. 
    Secondo. Non ho mai visto un tornitore andare a cena con una mannequin, pagando lei. 
    E non ho mai visto, dico mai, un pensionato schiantarsi alla guida di una Volvo. 
    Ma terzo, e dico terzo. Non ho mai visto, e Dio sa se lo vedrò un giorno, ma non l'ho mai visto per ora, un democristiano dire:
    «Va bè, dai, basta! Son stato io! Dai, son stato io, basta!»
    Ho visto di tutto, anche le cose più agghiaccianti; 
    ho visto perfino uomini che assomigliavano alla foto sul loro passaporto. 
    Ho visto donne innamorate contemporaneamente di molti uomini, poi venire da me a dire: «Basta, sono stanca di farvi soffrire tutti! 
    Scelgo te!» «Cazzo, solo a me mi devi far soffrire!» 
    Ho visto Craxi guardare un socialista magro e piangendo domandarsi: «Dove ho sbagliato?» 
    Ho visto Berlusconi così convinto che coi soldi si può tutto che quando va a pescare, come esca usa l'American Express. 
    Ho visto un primo ministro andare in televisione e dire: «Svalutare sarebbe una grande sconfitta». Poi dopo una settimana tornare in televisione e annunciare: «Abbiamo svalutato? 
    È una grande vittoria!» E mi sono chiesto: «Ma non sarebbe meglio mettere come primo ministro Trapattoni? Direbbe le stesse cazzate, ma almeno sa cos'è la media inglese». 
    Ho visto decine di talkshow in Tv e mi sono sempre chiesto: «Ma il dialogo di due deficienti è uguale al monologo di quattro semideficienti?» 
    Ho visto delle compagne amare talmente il proprio compagno che per non sciuparlo prendevano quello delle loro amiche. 
    Ho visto per anni e anni uomini sudare e raddoppiare gli sforzi per raggiungere obiettivi che nel frattempo si erano dimenticati. 
    Ho visto bravi compagni finire nei cellulari tanto tempo fa, e adesso ho visto gli stessi compagni passare ai telefoni cellulari... E poi dai telefoni cellulari tornare ai cellulari.
    Ho visto dei modelli di Armani che sembravano dei giovani nazisti. Ma poi ho guardato meglio e ho capito: non erano affatto giovani. 
    Ho visto Ligresti che sta ancora dentro, in galera. E questo mi sembra crudele, disumano... Soprattutto nei confronti del tossico che divide la cella con lui. 
    Che stava uscendo dal tunnel e ha trovato un muro di mattoni. 
    Ho visto Bobo Craxi venire a un mio spettacolo e dire: quello lì è un drogato, prima o poi finisce in galera... Non augurare mai agli altri quello che non vorresti succedesse mai a te e alla tua famiglia. 
    Grazie alla televisione ho visto poveri e infelici ribaltare completamente la propria vita. Adesso sono infelici e poveri. 
    Fine!!!
    
*/