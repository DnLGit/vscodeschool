#include<iostream>
using namespace std;

/*  Test codici relativi alla Verifica del 05.12.2019 */
/*  copiati com'erano da file originale */

float media(int somma, int contatore);      // dimenticata dichiarazione del prototipo

int main()
{
    int N;                                              // attenzione, avevo dichiarato "const int N"
    cout << "\nInserire la dimensione di un array: ";   // tolto <<endl e aggiunti: duepunti, spazio e \n
    cin >> N;
    int Vect[N];
    
    for(int i=0; i<N; i++){
        cout <<"Inserisci il "<<i+1<<" ^ valore: ";     // aggiunti: duepunti e spazio
        cin >>Vect[i];                                  // array per prova:  int Vect1[10]={2,2,2,2,0,3,3,3,3,0};
    }

    int Vect2[N];

    int index = 0;
    int index2 = N-1;

    for(int i=0; i<N; i++){
        
        if(Vect[i]!=0){
            Vect2[index]=Vect[i];
            index++;
        }else
        {
            Vect2[index2]=Vect[i];
            index2--;
        }
    }
    for(int i=0; i<N; i++)
    {
        cout << Vect2[i]<<"\t";
    }
    cout << endl;


    int sommaP=0, sommaD=0, countP=0, countD=0;
        for(int i=0; i<N; i++){
            
            if(Vect[i]%2==0 && Vect[i]!=0){
                sommaP+=Vect[i];
                countP++;
            }
            if(Vect[i]%2!=0 && Vect[i]!=0){
                sommaD+=Vect[i];
                countD++;
            }
            
        }
    
    cout << "Media Pari e' " <<sommaP/countP<< " e Media Dispari e' "<<sommaD/countD<<endl;

    cout <<"sommaP: " <<sommaP<<endl;
    cout <<"countP: " <<countP<<endl;
    cout <<"sommaD: " <<sommaD<<endl;
    cout <<"countD: " <<countD<<endl;

return 0;
}

/* ***************** Implementazione delle funzioni **************************** */

float media(int a, int b)
{
    return(a/b);
}