#include<iostream>
#include <cstring>      // dovrebbe sostituire <string.h> e permettere di usare le funzioni di C in C++
using namespace std;

/* Bergamasco Daniele 4As - Verifica del 23.01.2020 */

//# define N 100                          // define dell'esercizio numero 2
int const N = 4;                        // costante dell'esercizio numero 3                            
int minimo (int array[][N]);            // prototipo dell'esercizio numero 3 (mancava il Tipo int)


int main()
{
    /* 1) Osservando la porzione di programma indicato, cosa verra' stampato? Motivare la risposta
        -> 1pt <-

    char str1[]="esempio";
    char str2[40], str3[40];
    char str4[40] = "prova ";
    strcpy(str2, str1);
    strcpy(str3, "copia riuscita");
    strcat(str4,str3);
    int appoggio = strlen(str4);

    cout <<"str1 = "<<str1<<endl;           // str1 = esempio
    cout <<"str2 = "<<str2<<endl;           // str2 = esempio
    cout <<"str3 = "<<str3<<endl;           // str3 = copia riuscita
    cout <<"str4 = "<<str4<<endl;           // str4 = prova copia riuscita
    cout <<"appoggio = "<<appoggio<<endl;   // appoggio = 20                ok = 1pt    */


/* 2)   Scrivere un programma in c/c++ che prende in input una serie di frasi da tastiera, su piu' righe,
        l'inserimento termina con la parola FINE scritta su una sola riga. Il programma deve determinare:
        a) Quante righe sono state inserite dall'utente
        b) Numero medio di caratteri per ogni stringa inserita
        c) inserire la concatenazione di ogni stringa inserita in un'unica stringa che abbia una dimensione
           adeguata (calcolate voi la dimensione) e stampare il contenuto.
        N.B.: Escludere l'ultima riga, ovvero quella in cui abbiamo inserito la parola FINE.
        PS: Si possono usare le funzioni di string.h.
        -> 3.5pt <-
        

    int lung = 0;
    char matrix [N][N];
    char stringa [N];
    int i = 0;
    int car = 0;
    int righe = 0;

    do
    {
        cout << "\nInserisci frasi: " <<endl;           // mancava \n (ed era meglio mettere questo cout prima del do-while)
        cin.getline(matrix[i], N);                      // mancava [i]
        i++;
        righe =i;

    } while (strcmp(matrix[i-1],"FINE")!=0);            // mancava !=0 -> era (matrix[i-1]!="FINE"))

    for(int i=0; i<righe-1; i++)
    {
        for(int j=0; j<N && matrix[i][j]!='\0'; j++)    // mancava for interno (j)
        {
            if(isalpha(matrix[i][j]))
            {
                car++;
                lung+=strlen(matrix[i]);                // non serviva
            }
        }
        strcat(stringa, matrix[i]);
    }

    cout << "Numero medio caratteri e': " << car/(righe-1) <<endl;  // errore:  era car/lung
    cout << "La stringa concatenata e': " << stringa <<endl;
    cout << "Il numero di righe e': " << righe <<endl;                                      */

/* 3)   Scrivere una funzione in C/C++ (scrivere sia prototipo che implementazione) che prende in input 
        un array bidimensionale di interi di dimensioni generiche  NumRighe, NumColonne che siano uguali, e che
        stampa il valore minimo degli elementi presenti nelle due diagonali. Dare per scontato che l'array sia gia'
        riempito.
        -> 3.5pt <-     

    int matrix[N][N] = {{-2, 5, 8, -7}, 
                        {3, 9, 2, 3},
                        {5, 4, 9, 3},
                        {8, 7, 6, 4}};
    int valoreMin;
    valoreMin = minimo(matrix);

    cout << "il valore minimo e': " << valoreMin <<endl;


    return 0;
}

int minimo(int array[][N])  // attenzione: mancava il Tipo int)
{
    int min = array[0][0];
    for(int i=1; i<N; i++)
    {
        if(array[i][i] < min)
        min = array[i][i];

        if(array[i-1][N-i] < min)   // errore primo indice: [N-i][N-i]
        min = array[i-1][N-i];
    }
    return min;
}                   */