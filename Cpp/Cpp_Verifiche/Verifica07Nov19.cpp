#include<iostream>
using namespace std;

/* Bergamasco Daniele 4As - Verifica del 07.11.2019 */

int main()
{

/* Esercizio 1, scrivere un programma in C/C++ che: 
    a)  Permette di inserire da tastiera un numero. Il numero inserito deve essere la dimensione di un array di interi
        che bisogna riempire tramite input da tastiera (effettuare il riempimento).
        Il numero inserito deve essere compreso tra 0 e 20. Qualora non fosse compreso in questo intervallo bisogna inserire nuovamente
        il numero fino a quando non si mette un numero corretto.
    b)  Dopo aver riempito l'array si deve restituire la dicitura "UGUALI" se l'array riempito e' uguale a quello letto al contrario, 
        altrimenti bisogna restituire la dicitura "DIVERSI". 
        Esempio: se l'array riempito contiene i seguenti elementi: 6,7,7,6 bisogna stampare "UGUALI" perche' letto al contrario e' uguale;
        invece se l'array inserito contiene i seguenti elementi: 9,8,7,5 bisogna stampare "DIVERSI" in quanto l'array al contrario e' 5,7,8,9
        quindi diverso da quello inserito. NB = Non sono ammessi array di appoggio.
    c)  Creare un secondo array della stessa dimensione e inserirvi prima gli elementi multipli di 3 e poi il resto dei numeri nello stesso ordine
        del primo array e poi stampare il contenuto del secondo array.
        Esempio: Se il primo array ordinato contiene 2,3,5,9,10,12, il secondo array dovra' contenere 3,9,12,2,5,10 perche' 3,9,12 sono multipli di 3.
        Il resto dei numeri no.  */

    /*

    int N1;
    bool uguale;                                        // dimenticata booleana

    do
    {
        cout<<"\nInserire N1: ";//<<endl;               // dimenticato il "\n" e meglio non mettere "endl;"
        cin>>N1;
    } 
    while (N1<1 || N1>=20);                             // dimenticato "punto&virgola"
    int Vect1[N1];                                      // dimenticato N1

    for(int i=0; i<N1; i++)
    {
        cout<<"Inserire "<<i+1<<"^ elemento ";//<<endl; // era meglio non mettere "endl;"
        cin>>Vect1[i];                                  // dimenticato il "\t"          
    }
    
    for(int i=0; i<N1; i++)
    {
        uguale = false;                                 // booleana dimenticata                     
        for(int j=N1-1; j>0; j--)
            if(Vect1[i]==Vect1[j])  
            //cout<<"Uguali"<<endl;                     // questo cout non funziona qui
            uguale = true;                              // booleana dimenticata
            break;                                      // dimenticato il break
    }

    if(uguale == true)                                  // dimenticato l'if
    {
        cout<<"\nUguali"<<endl;                         // dimenticato il "\n"
    }
    else
    {
        cout<<"\nDiversi"<<endl;                        // dimenticato il "\n"
    }

    int Vect2[N1];

        for(int i=0; i<N1; i++)
        {
            int counter =0;
            for(int j=0; j<N1; j++)                     // dimenticate graffe
            {
                if(Vect1[i]%3 == 0)
                Vect2[j] = Vect1[i];
                counter++;                              // dimenticato punto&vorgola
            }
            //else
            
                //for(int k=N1-counter; k<N1; k++)

            for(int j=N1-counter; j<N1; j++)            // dimenticato intero ciclo for
                if(Vect1[i]%3 !=0)
                {
                    Vect2[j]=Vect1[i];
                }
            cout<<Vect2[i]<<"\t";//<<endl;              // era meglio non mettere "endl;"
        }
        
    */

/*  Esercizio 2, Osservando la porzione di programma indicato, cosa contiene la variabile k
    alla fine del ciclo? Motivare la risposta.  */

/*
    int j = 7, k = 7;
    while (j>2)
    {
        j--;
        k++;
    }
    cout <<" kappa e': "<<k<<endl;                      // k sbagliato (scritto 11) corretto n° cicli (5 volte)
            
*/

/*  Esercizio 3, Se definiamo int s[10]; e' ammessa l'istruzione:  s++;?    */

/*
    int s[10];
    s++;                                                // Sembra corretta (No: xche' "s" e' il nome dell'array)
*/

/*  Esercizio 4, Alla fine della seguente porzione di codice, k vale 13? 
    Se no, quanto vale? Motivare la risposta   */

/*
    int i, k=7;
    for(i=17; i>2; i-=3)
        k++;
    cout<<"kappa vale: "<<k<<endl;                      // Corretto:  k vale 12!!
*/

/*  Esercizio 5, Determinare il valore visualizzato dai seguenti programmi nel caso in cui
    num=4 e per i seguenti valori della variabile conta:  conta=5, conta=0, conta=1, conta=-5   */

/*
    int conta, num;
    cout<<"conta: ";
    cin>>conta;
    cout<<"num: ";
    cin>>num;
    while (conta!=0)    // programma sx: 54, 4, 14 e -46 (errore: 14, 14, 14 e 14)
    //while (conta>0)    // programma dx: 54, 4, 14 e 4 (errore: 14, 4, 14 e 4)
    {
        num+=10;
        conta=conta-1;
    }
    cout<<"num e': "<<num;
*/    

/*  Esercizio 6, Data la seguente porzione di codice
    Quanto valgono "b" e "c" dopo la seconda istruzione?
    Quanto valgono "d" e "c" dopo la terza istruzione?
    Motivare la risposta    */

/*
    int c=2;
    float b=40/++c;
    int d=10+c--;

    cout<<"\nb vale: "<<b;      //b = 13    (errore: b=20 )
    cout<<"\n++c vale: "<<++c;  //++c = 3   (errore: c=2)
    cout<<"\nc-- vale: "<<c--;  //c-- = 3   (errore: c=1)
    cout<<"\nd vale: "<<d;      //d = 13    (errore. d=11)

*/

/*  Esercizio 7, A cosa servono le istruzioni break e continue?
    Fornire un esempio a supporto    */

/*
    int a=1;
    int b=2;
    if(a<b)
    {
        break;  // break può essere usata solo in un ciclo o un switch!!!
    }
    else
    {
        cout<<"Attenzione: Errore!";
    }
*/    






    return 0;
}