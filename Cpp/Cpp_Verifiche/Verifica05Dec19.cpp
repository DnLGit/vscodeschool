#include<iostream>
using namespace std;

/* Bergamasco Daniele 4As - Verifica del 05.12.2019 */

float media(int somma, int contatore);      // dimenticata dichiarazione del prototipo

int main()
{

/*  1)  k vale 5?       */

    int i, k=2;
    for(i=19; i>3; i-=5)
        k++;
    
    cout << "kappa vale " <<k<<endl;        // k = 6 (ok corretto)


/*  2) Cosa contiene k? */

    int j=11, k=11;
    while(j>6){
        j--;
        k++;
    }
    cout << "kappa vale " <<k<<endl;        // k = 16 (ok corretto)


/*  3) E' corretta la seguente istruzione?      */

    int s[5]={-1.5, -3.4, -6.4, -1, -9};

    for(int i=0; i<5; i++)    // stampando l'array mi da errore. Probabile risposta esatta.
    {
        cout <<s[i]<<"\t";
    }


/*  4) Scrivere un programma in c++ che:     

/*  a)  Permette di inserire da tastiera un numero. 
        Il numero deve essere positivo se l'utente inserisce un numero negativo si deve presentare all'utente 
        di nuovo la richiesta di inserimento del numero. Il numero inserito deve essere la dimensione di un array
        di interi che si deve riempire tramite input da tastiera (effettuare il riempimento)  */ 

    int N;                                                  // attenzione, avevo dichiarato "const int N"
    do
    {
        cout << "\nInserire la dimensione di un array: ";   // tolto <<endl e aggiunti: duepunti, spazio e \n
        cin >> N;
        if(N<0)
            cout <<"Attenzione, valore sbagliato, riprova"<<endl;
    } while (N<0);
    int Vect[N];
    for(int i=0; i<N; i++){
        cout <<"Inserisci il "<<i+1<<" ^ valore: ";         // aggiunti: duepunti e spazio
        cin >>Vect[i];
    }

/*  b)  Creare un altro array della stessa dimensione ed inserire al suo interno gli elementi
        del primo array nello stesso ordine del primo tranne gli zeri che vanno messi in fondo  */    


    int Vect2[N];

    int index = 0;
    int index2 = N-1;

    for(int i=0; i<N; i++){                                                                     // soluzione con un solo for
        
        if(Vect[i]!=0){
            Vect2[index]=Vect[i];
            index++;
        }else
        {
            Vect2[index2]=Vect[i];
            index2--;
        }
        
    }
/*    
    for(int i=0; i<N; i++){                                                                     // soluzione con due for
        
        if(Vect[i]!=0){
            Vect2[index]=Vect[i];
            index++;
        }
    }

    for(int i=0; i<N; i++)
    {
        if(Vect[i]==0){
            Vect2[index]=Vect[i];
            index++;
        
        }
    }
*/   
    
    for(int i=0; i<N; i++)
    {
        cout << Vect2[i]<<"\t";
    }
    cout << endl;


/*  c)  Calcolare la media degli elementi pari e dispari */

    int sommaP=0, sommaD=0, countP=0, countD=0;
    for(int i=0; i<N; i++){
        
        if(Vect[i]%2==0 && Vect[i]!=0){
            sommaP+=Vect[i];
            countP++;
        }
        if(Vect[i]%2!=0 && Vect[i]!=0){
            sommaD+=Vect[i];
            countD++;
        }
        
    }
        

    float mP = media(sommaP, countP);                                                               // soluzione con funzioni
    float mD = media(sommaD, countD);
    

    cout <<"sommaP: " <<sommaP<<endl;
    cout <<"countP: " <<countP<<endl;
    cout <<"sommaD: " <<sommaD<<endl;
    cout <<"countD: " <<countD<<endl;


/*  d)  Stampare il messaggio sulla media degli elementi pari e dispari
        Ad esempio se l'array inserito è 3,6,9,10,11 bisogna stampare il messaggio:
        Media elementi pari = 8. Media elementi dispari = 7.66 */

        //cout << "Media Pari e' " <<sommaP/countP<< " e Media Dispari e' "<<sommaD/countD<<endl;   // senza funzione
        cout << "Media Pari e' " <<mP<< " e Media Dispari e' "<<mD<<endl;
        cout << endl;


/*  5)  Data la seguente porzione di codice: 
        Quanto valgono b e c dopo la seconda istruzione?
        Quanto valgono d e c dopo la terza istruzione?      */

        int c = 3;
        float b = 40/--c;
        cout << "b = "<<b<<" e c = "<<c<<endl;      // b = 20 e c = 2
        int d = 5 + c++;
        cout << "d = "<<d<<" e c = "<<c<<endl;      // d = 7 e c = 3  (Avevo scritto 2 ... Attenzione -> l'istruzione 3 è già eseguita!!!)


        
/*  6)  A cosa serve il While e il Do While? Quali sono le differenze?
        Fornisci anche degli esempi.

*/  
    int x = 5;
    while (x>0)
    {
        cout << "While ciao" << "\n" << endl;
        x--;
    }


    int y;

/*
    do
    {
        cout << "Inserisci il valore: ";                        // forma scritta in verifica
        cin >> y;
    } while (y>0);
        cout << "ciao" << "\n" << endl;    
        y--;
*/    

    cout << "DoWhile -> Inserisci il valore: ";                 // forse meglio questa
    cin >> y;
    do
    {
        cout << "DoWhile ciao" << "\n" << endl;    
        y--;
        
    } while (y>5);

        
    return 0;
}

/* ***************** Implementazione delle funzioni **************************** */

float media(int a, int b)
{
    return(a/b);
}