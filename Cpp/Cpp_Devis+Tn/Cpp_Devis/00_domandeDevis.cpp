/******************************************************************************

                              Domande

*******************************************************************************/

/*  22.01.2020

-> esempiomenu.cpp (è il file che mi hai inviato ieri):  
    - C'e' un motivo per utilizzare sia "cout endl" che "\n"?
    - La condizione !=3 del while non esclude scelte non comprese nel menu', e' ok cosi'?
          (potrebbe essere buona norma mettere un IF di "attenzione, scelta sbagliata"?)

-> 09rubricaTelefonica.cpp (non credo ti serva il file ...):
    - Quando nei parametri di una funzione c'e' una matrice e' d'obbligo inserire il valore delle colonne;
      la soluzione migliore e': 
      a) utilizzare una costante inizializzata o un #define (entrambi fuori e prima del main),
      b) fare l'inizializzazione nell'implementazione della funzione fuori dal main, sotto il return 0;
      c) o c'e' un modo migliore?
    - I parametri dichiarati nel prototipo o nella funzione non sono gli stessi di quelli nella chiamata
      (c'e' il passaggio per valore) ... puo' essere un'idea dichiararli con lo stesso nome ma in MAIUSCOLO
      per differenziarli ma al contempo riconoscerli all'istante?
      Oppure e' usuale dichiararli allo stesso modo di quando la funzione viene invocata?

   ---------------- 
   
-> extra5_matrixStringhe.cpp
    - con il do-while se supero, anche solo nella prima parola, la dimensione "N" si esce dal ciclo!! (corretto??)
    - inserendo un simbolo dopo una parola (Gennaio!) l'apostrofo viene duplicato anche nella riga sotto.
*/


#include <iostream>
using namespace std;

int main()
{

    return 0;
}