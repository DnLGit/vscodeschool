#include <iostream>
#include <iomanip>
#include <cstring>

using namespace std;

int main (int argc, char* argv[])
{
    int G = 3, K = 5;
    string giudici[G] = {"Pippo", "Arno", "Luca"};
    string candidati[K] = {"Gianpiero", "Daniele", "Devis", "Mario", "Massimo"};
    int giudizi[G][K] = 
    {
        {4,3,4,3,5},
        {2,3,2,2,2},
        {3,1,5,5,1}
    };

    cout << left << setw(16) << "";
    for(int j=0; j < K; j++)
    {
        cout << left << setw(16) << candidati[j];
    }
    cout << endl;

    for(int i=0; i<G; i++)
    {
        cout << left << setw(16) << giudici[i];
        for(int j=0; j<K; j++)
        {
            cout << left << setw(16) << giudizi[i][j];
        }

        cout << endl;
    }

    cout << endl << endl;

    double lowestAvgG = 5;
    int strictestG = 0;
    for(int i=0; i<G; i++)
    {
        //cout << "I voti dati dal giudice " << i << ": ";
        cout << "I voti dati dal giudice " << giudici[i] << ":\t";
        double media = 0.0;
        for(int j=0; j<K; j++)
        {
            cout << giudizi[i][j] << "\t";
            media += giudizi[i][j];
        }
        media = media / K;
        
        cout << "\t (media=" << media << ")" <<  endl;

        if(media < lowestAvgG)
        {
            lowestAvgG = media;
            strictestG = i;
        }
    }

    cout << "RISULTATO: il giudice più severo è " << giudici[strictestG] << endl;
    cout << endl << endl;

    for(int j=0; j<K; j++)
    {
        //cout << "I voti presi dal candidato " << j << ": ";
        cout << "I voti presi dal candidato " << candidati[j] << ":\t";
        for(int i=0; i<G; i++)
            cout << giudizi[i][j] << "\t";
        cout << endl;
    }


    return 0;
}