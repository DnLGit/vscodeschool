#include<iostream>
using namespace std;
 
void swap(int*, int*);

int main()
{

    int x = 10, y = 12;
    cout << "Indirizzo di x: " << &x << "\nIndirizzo di y: " << &y << endl;
    cout << "[PRIMA DELLO SWAP]" << endl;
    cout << "x: " << x << "\ny: " << y << endl;

    swap(&x, &y);

    cout << "\n\n[DOPO DELLO SWAP]" << endl;
    cout << "x: " << x << "\ny: " << y << endl;   
    return 0;
}



/* ***************** Implementazione delle funzioni **************************** */

void swap(int* a, int* b)
{
    cout << "\n[DENTRO A SWAP]" << endl;
    cout << "Indirizzo di a: " << &a << "\nIndirizzo di b: " << &b << endl;
    cout << "a: " << a << "\nb: " << b << endl;
    cout << "*a: " << *a << "\n*b: " << *b << endl << "Swapping...\n";  
    int temp = *a;
    *a = *b;
    *b = temp;        
    cout << "*a: " << *a << "\n*b: " << *b << endl;                 
}
