#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#define MAXL 256

int main (int argc, char* argv[])
{
    printf("Hello World!\n");

    char nomeMio[MAXL];
    char nomeTuo[MAXL];
    
    printf("Inserisci il tuo nome: ");
    //fgets equivalente a gets(nomeMio), dove è implicito si legga da stdin, ma è un'operazione più "sicura" perché specifico il numero massimo di caratteri da leggere
    //NOTA fgets acquisisce anche \n nella stringa, mentre gets no
    
    fgets(nomeMio, MAXL, stdin); 
    nomeMio[strlen(nomeMio) - 1] = '\0';//NOTA usa con fgets per rimuovere \n se non lo vuoi
    
    //gets(nomeMio);

    printf("Hai inserito il nome: %s \n", nomeMio);
    printf("La stringa che hai inserito è di lunghezza: %lu \n", strlen(nomeMio)); //strlen non è altro che un ciclo che conta i passi da fare per arrivare al tappo '\0'

    printf("Inserisci il nome di un tuo amico: ");
    
    fgets(nomeTuo, MAXL, stdin); 
    nomeTuo[strlen(nomeTuo) - 1] = '\0';//NOTA usa con fgets per rimuovere \n se non lo vuoi
    
    //gets(nomeTuo);

    printf("Hai inserito il nome: %s \n", nomeTuo);
    printf("La stringa che hai inserito è di lunghezza: %lu \n", strlen(nomeTuo)); 

    int resultStrCmp = strcmp(nomeMio, nomeTuo);
    printf("\n\nstrcmp(nomeMio, nomeTuo) = %d\n", resultStrCmp);

    if(resultStrCmp == 0)
        printf("I nomi inseriti sono uguali\n");
    
    else if(resultStrCmp < 0)
        printf("Il tuo nome (%s) viene prima in ordine alfabetico del nome del tuo amico (%s)\n", nomeMio, nomeTuo);//lettere maiuscole precedono le minuscole

    else if(resultStrCmp > 0)
        printf("Il nome del tuo amico (%s) viene prima in ordine alfabetico del tuo nome (%s)\n", nomeTuo, nomeMio);//lettere maiuscole precedono le minuscole


    char nomeCopiato[MAXL];
    printf("\n\nPrima di effettuare la copia, nomeCopiato vale: %s \n", nomeCopiato);
    printf("strcpy(nomeCopiato, nomeMio) in corso \n");
    strcpy(nomeCopiato, nomeMio);//voglio copiare il contenuto di nomeMio in nomeCopiato
    printf("Hai copiato il tuo nome in nomeCopiato che ora vale: %s \n", nomeCopiato);
    //se adesso modifico il valore di nomeCopiato, naturalmente nomeMio NON viene toccato

    printf("\n\nPrima di effettuare la concatenazione, nomeCopiato vale: %s (len=%lu) \n", nomeCopiato, strlen(nomeCopiato));
    printf("strcat(nomeCopiato, \" \") in corso \n");
    strcat(nomeCopiato, " "); // concateno a nomeCopiato uno spazio
    printf("Hai concatenato uno spazio a nomeCopiato che ora vale: %s (len=%lu) \n", nomeCopiato, strlen(nomeCopiato));
    printf("strcat(nomeCopiato, nomeTuo) in corso \n");
    strcat(nomeCopiato, nomeTuo); // concateno a nomeCopiato il nome del mio amico (nomeTuo)
    printf("Hai concatenato il nome del tuo amico (nomeTuo) a nomeCopiato che ora vale: %s (len=%lu) \n", nomeCopiato, strlen(nomeCopiato));
    
    printf("\n\n nomeMio = \"%s\" (len=%lu)", nomeMio, strlen(nomeMio));
    printf("\n\n nomeTuo = \"%s\" (len=%lu)", nomeTuo, strlen(nomeTuo));
    printf("\n\n nomeCopiato = \"%s\" (len=%lu)\n", nomeCopiato, strlen(nomeCopiato));
    return 0;
}