#include<iostream>
#include<string.h>

using namespace std;

/*  Bergamasco Daniele 4As - 30.11.2019 >>>>>>>>>>>> Conta Vocali e Consonanti

    01. Scrivere un programma in linguaggio C che legga una frase introdotta da tastiera. 
        La frase è terminata dall’introduzione del carattere di invio. 
        La frase contiene sia caratteri maiuscoli che caratteri minuscoli, e complessivamente al più 100 caratteri. 
        Il programma dovrà stampare su schermo le seguenti informazioni: 
         • per ognuna delle lettere dell’alfabeto, il numero di volte che la lettera compare nella stringa 
         • il numero di consonanti presenti nella stringa 
         • il numero di vocali presenti nella stringa..  */

#define MAX 256

int main()
{
    char s[MAX];
    cout << "Inserisci una frase:";
    cin.getline(s, MAX); 
    const int N = strlen(s);

    cout << endl << "Inserita la frase: \"" << s << "\" di lunghezza EFFETTIVA " << N << endl;

    int countV = 0, countC = 0;
    int alfaCountM[('Z'-'A'+1)];
    int alfaCountm[('z'-'a'+1)];

    for(int i = 0; i < ('Z'-'A'+1); i++)
    {
        alfaCountM[i] = 0;
        alfaCountm[i] = 0;
    }

    for(int i = 0; i < N; i++)
    {
        if(s[i] >= 'A' && s[i] <= 'Z')
        {
            alfaCountM[s[i]-'A']++;
            if(s[i] == 'A' || s[i] == 'E' || s[i] == 'U' || s[i] == 'O' || s[i] == 'I')
            {
                countV++;
            }
            else
            {
                countC++;
            }
        }
        else if(s[i] >= 'a' && s[i] <= 'z')
        {
            alfaCountm[s[i]-'a']++;
            if(s[i] == 'a' || s[i] == 'e' || s[i] == 'u' || s[i] == 'o' || s[i] == 'i')
            {
                countV++;
            }
            else
            {
                countC++;
            } 
        }    


    }

    for(int i = 0; i < ('Z'-'A'+1); i++)
    {
        if(alfaCountM[i] > 0){
            cout << "La lettera " << (char)('A'+i) << " compare " << alfaCountM[i] <<" volte" << endl;
        }

        if(alfaCountm[i] > 0){
            cout << "La lettera " << (char)('a'+i) << " compare " << alfaCountm[i] <<" volte" << endl;
        }
        
    }
    
    cout << endl << "Nella frase ci sono " << countV << " vocali e " << countC << " consonanti" << endl;

    return 0;
}