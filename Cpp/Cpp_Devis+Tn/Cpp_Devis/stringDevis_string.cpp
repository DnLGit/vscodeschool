#include <iostream>

#include <cstring>

using namespace std;

int main (int argc, char* argv[])
{
    cout << "Hello World!" << endl;
    
    string nomeMio;//string non è altro un vettore di char di dimensione dinamica (non fissa)
    string nomeTuo;
    
    cout << "Inserisci il tuo nome: ";
    getline(cin, nomeMio, '\n'); //non mettendo il terzo parametro, ovvero il delimitatore a cui fermarsi nella lettura (nel caso '\n') di default questo vale '\n'
    // nomeMio[2] = 'f'; //NOTA: volendo puoi accedere ad una cella della stringa (oggetto di tipo string) così come se fosse un vettore
    cout << "Hai inserito il nome " << nomeMio << endl;
    cout << "La stringa che hai inserito è di lunghezza " << nomeMio.length() << endl; //length() è una funzione richiamabile su oggetti di tipo string (non su vettori di char) e ritorna la lunghezza della stringa 

    cout << "Inserisci il nome del tuo amico: ";
    getline(cin, nomeTuo, '\n');
    cout << "Hai inserito il nome " << nomeTuo << endl;
    cout << "La stringa che hai inserito è di lunghezza " << nomeTuo.length() << endl;

    /*
        NOTA: getline(...) è utilizzabie solo su oggetti di tipo string
    */
    
    int resultStrCmp = nomeMio.compare(nomeTuo); // compare, come length usato sopra, è un metodo della classe string, quindi lo richiamo con il punto su un oggetto di tipo string

    if(resultStrCmp == 0)
        cout << "\n\nI nomi inseriti sono uguali" << endl;
    
    else if(resultStrCmp < 0)// nota lettere maiuscole precedono le minuscole
        cout << "\n\nIl tuo nome (" << nomeMio << ") viene prima in ordine alfabetico del nome del tuo amico (" << nomeTuo << ")" << endl;

    else if(resultStrCmp > 0)
        cout << "\n\nIl nome del tuo amico (" << nomeTuo << ") viene prima in ordine alfabetico del tuo nome (" << nomeMio << ")" << endl;
    

    string nomeCopiato;
    cout << "\n\nPrima di effettuare la copia, nomeCopiato vale: " << nomeCopiato << endl;
    cout << "nomeCopiato = nomeMio in corso " << endl;
    nomeCopiato = nomeMio;
    cout << "Hai copiato il tuo nome in nomeCopiato che ora vale: " << nomeCopiato << endl;
    //se adesso modifico il valore di nomeCopiato, naturalmente nomeMio NON viene toccato

    cout << "\n\nPrima di effettuare la concatenazione, nomeCopiato vale: " << nomeCopiato << " (len=" << nomeCopiato.length() << ") " << endl;
    cout << "nomeCopiato += \" \" in corso" << endl;
    nomeCopiato += " "; // concateno a nomeCopiato uno spazio
    cout << "Hai concatenato uno spazio a nomeCopiato che ora vale: " << nomeCopiato << " (len=" << nomeCopiato.length() << ") " << endl;
    cout << "nomeCopiato += nomeTuo in corso" << endl;
    nomeCopiato += nomeTuo; // concateno a nomeCopiato uno spazio
    cout << "Hai concatenato il nome del tuo amico a nomeCopiato che ora vale: " << nomeCopiato << " (len=" << nomeCopiato.length() << ") " << endl;
    
    cout << "nomeMio = \"" << nomeMio << "\" " << " (len=" << nomeMio.length() << ") " << endl;
    cout << "nomeTuo = \"" << nomeTuo << "\" " << " (len=" << nomeTuo.length() << ") " << endl;
    cout << "nomeCopiato = \"" << nomeCopiato << "\" " << " (len=" << nomeCopiato.length() << ") " << endl;
    return 0;
}