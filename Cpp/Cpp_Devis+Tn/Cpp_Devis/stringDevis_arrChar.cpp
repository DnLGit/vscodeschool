#include <iostream>

#include <string.h>

using namespace std;

#define MAXL 256

int main (int argc, char* argv[])
{
    cout << "Hello World!" << endl;
    
    char nomeMio[MAXL];
    char nomeTuo[MAXL];
    
    cout << "Inserisci il tuo nome: ";
    cin.getline(nomeMio, MAXL); //estrae dallo stdin (mediante l'oggetto cin) al massimo MAXL caratteri mettendoli in nomeMio o fermandosi al primo \n
    cout << "Hai inserito il nome " << nomeMio << endl;
    cout << "La stringa che hai inserito è di lunghezza " << strlen(nomeMio) << endl;

    cout << "Inserisci il nome del tuo amico: ";
    cin.getline(nomeTuo, MAXL); //estrae dallo stdin (mediante l'oggetto cin) al massimo MAXL caratteri mettendoli in nomeMio o fermandosi al primo \n
    cout << "Hai inserito il nome " << nomeTuo << endl;
    cout << "La stringa che hai inserito è di lunghezza " << strlen(nomeTuo) << endl;

    /*
        NOTA: cin.getline() è un modo per leggere array di caratteri in C++, avrei tranquillamente potuto usare gets & fgets anche qui,
        ma NON avrei potuto invece usare cin.getline() in C, perché cin è un oggetto di iostream che esiste in C++ 
    */

    int resultStrCmp = strcmp(nomeMio, nomeTuo);
    cout << "\n\nstrcmp(nomeMio, nomeTuo) = " << resultStrCmp << endl;

    if(resultStrCmp == 0)
        cout << "I nomi inseriti sono uguali" << endl;
    
    else if(resultStrCmp < 0)// nota lettere maiuscole precedono le minuscole
        cout << "Il tuo nome (" << nomeMio << ") viene prima in ordine alfabetico del nome del tuo amico (" << nomeTuo << ")" << endl;

    else if(resultStrCmp > 0)
        cout << "Il nome del tuo amico (" << nomeTuo << ") viene prima in ordine alfabetico del tuo nome (" << nomeMio << ")" << endl;


    char nomeCopiato[MAXL];
    cout << "\n\nPrima di effettuare la copia, nomeCopiato vale: " << nomeCopiato << endl;
    cout << "strcpy(nomeCopiato, nomeMio) in corso " << endl;
    strcpy(nomeCopiato, nomeMio);//voglio copiare il contenuto di nomeMio in nomeCopiato
    cout << "Hai copiato il tuo nome in nomeCopiato che ora vale: " << nomeCopiato << endl;
    //se adesso modifico il valore di nomeCopiato, naturalmente nomeMio NON viene toccato

    cout << "\n\nPrima di effettuare la concatenazione, nomeCopiato vale: " << nomeCopiato << " (len=" << strlen(nomeCopiato) << ") " << endl;
    cout << "strcat(nomeCopiato, \" \") in corso" << endl;
    strcat(nomeCopiato, " "); // concateno a nomeCopiato uno spazio
    cout << "Hai concatenato uno spazio a nomeCopiato che ora vale: " << nomeCopiato << " (len=" << strlen(nomeCopiato) << ") " << endl;
    cout << "strcat(nomeCopiato, nomeTuo) in corso" << endl;
    strcat(nomeCopiato, nomeTuo); // concateno a nomeCopiato il nome del mio amico (nomeTuo)
    cout << "Hai concatenato il nome del tuo amico a nomeCopiato che ora vale: " << nomeCopiato << " (len=" << strlen(nomeCopiato) << ") " << endl;
    
    cout << "nomeMio = \"" << nomeMio << "\" " << " (len=" << strlen(nomeMio) << ") " << endl;
    cout << "nomeTuo = \"" << nomeTuo << "\" " << " (len=" << strlen(nomeTuo) << ") " << endl;
    cout << "nomeCopiato = \"" << nomeCopiato << "\" " << " (len=" << strlen(nomeCopiato) << ") " << endl;
    return 0;
}