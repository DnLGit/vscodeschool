using namespace std;
#include <iostream> // header

void printArray (int V[], int N)
{
  for(int i=0; i<N; i++)
    cout << V[i] << "\t";
  
  cout << endl;
}

int main (int argc, char* argv[]) // programma principale
{
  /*PUNTO a*/
  int N; //lunghezza del vettore
  do{
    
    cout << "Inserisci la lunghezza del vettore (tra 0 e 20):";
    cin >> N;
    cout << endl;

    if(N<0 || N>20)
      cout << "Input errato! Probabilmente non sai leggere" << endl << endl;

  }while(N<0 || N>20);//ripetere la domanda finché input non corretto

  int V[N];
  cout << "Ora procediamo con il riempimento del suddetto vettore di interi avente " << N << "elementi" << endl;
  for(int i=0; i<N; i++)
  {
    cout << "V[" << i << "]:";
    cin >> V[i];
  }

  cout << "Inserito il seguente vettore:\t";
  printArray(V, N);
  cout << endl << endl;

  /*Punto b*/
  bool uguali = true;//assumo che sia UGUALE se letto nei due sensi
  
  // i e j iteratori di V per confrontare mano a mano gli elementi V[i] e V[j], 
  // dove i parte da 0 e aumenta, mentre parallelamente j parte dall'ultimo elemento (in pos N-1) e diminuisce
  // NOTA BENE questo è essenzialmente il problema della stringa palindroma
  int i = 0, j = N-1;
  
  // (i<j) devo controllare mano a mano tutti gli elementi agli "opposti" (V[1] e V[N-1]), poi (V[2] e V[N-2]) e così via fino a che i e j si incrontrano 
  // (in tal caso l'array è palindromo,aka stampa UGUALI) (uguali) -> appena trovo due elementi agli opposti diversi so già che l'array non è palindromo
  while(i < j && uguali)
  {
    if(V[i] != V[j])
      uguali = false;//lo stesso si poteva fare evitando di mettere uguali nella condizione del while e con il break, ma il break fa SCHIFO xD
  
    i++; j--;
  }

  if(uguali)
    cout << "\n\n\nb: UGUALI\n\n\n";
  else
    cout << "\n\n\nb: DIVERSI\n\n\n";


  /*Punto c*/  
  int Vc[N];
  i = 0;//iteratore per V
  j = 0;//iteratore per Vc

  //primo scanning di V atto ad inserire prima i multipli di 3 in Vc
  while(i<N && j<N)
  {
    if(V[i]%3==0)//V[i] multiplo di 3
    {
      Vc[j] = V[i];
      j++;
    }
    i++;
  }

  //secondo scanning di V atto ad inserire i NON multipli di 3 in Vc
  //NOTA: resetto solo i perché j si trova nella prossima valida posizione in cui posso inserire numeri in Vc (avendo inserito da 0 a j-1 i multipli di 3 con il while sopra)
  i = 0;
  while(i<N && j<N)
  {
    if(V[i]%3!=0)//V[i] NON multiplo di 3
    {
      Vc[j] = V[i];
      j++;
    }
    i++;
  }

  cout << "Vettore per punto c:\t";
  printArray(Vc, N);
  cout << endl << endl;
  return 0;
}

