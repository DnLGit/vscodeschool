/******************************************************************************

                              Esempio MENU

*******************************************************************************/

#include <iostream>

using namespace std;

int main()
{
    int scelta;
    do
    {
        cout << "\n\nMENU: scegli fra le seguenti opzioni" << endl;
        cout << "1- Opzione 1" << endl;
        cout << "2- Opzione 2" << endl;
        cout << "3- Esci" << endl;
        cout << "\nscelta:";
        cin >> scelta;
        cout << "\n";
        
        switch(scelta)
        {
            case 1:
            {
                //codice opzione 1
                break;
            }
            case 2:
            {
                //codice opzione 2
                break;
            }
            case 3:
            {
                cout << "Il programma sta per chiudersi... Bye bye!" << endl;
                break;
            }
        }
        
    }while(scelta != 3);
    
    return 0;
}
