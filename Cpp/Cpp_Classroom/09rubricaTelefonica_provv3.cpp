#include <string>
#include <cstring>  // dovrebbe sostituire <string.h> e permettere di usare le funzioni di C in C++
#include<iomanip>       /* Libreria per la funzione setw(n)*/
#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 02.01.2020 >>>>>>>>>>>> Esercizi con Funzioni e Matrici

    Esercizio n° 9 -  Rubrica Telefonica.
    Si realizzi un programma in linguaggio C in grado di gestire una rubrica di nomi e numeri telefonici. 
    La rubrica deve contenere fino a 100 voci diverse. 
    Ciascuna voce è composta da un nome (max 40 caratteri) e da un numero di telefono (max 20 caratteri). 
    Il programma deve fornire all’utente un menù di scelta, con le seguenti voci: 
    1) Aggiungi nuova voce in rubrica 
    2) Ricerca esatta per nome 
    3) Ricerca approssimata per nome 
    4) Stampa completa rubrica 
    0) Esci dal programma 
    Una volta che l’utente ha scelto l’operazione desiderata (1-4), il programma acquisirà i dati necessari dall’utente ed eseguirà il comando. 
    Nota: nella rubrica non possono esistere due voci con lo stesso nome. */

#define vMax 100                                             // direttiva per il preProcessore: numero massimo di utenti
#define nMax 40                                              // direttiva per il preProcessore: lunghezza massima dei nomi
#define tMax 20                                              // direttiva per il preProcessore: lunghezza massima dei numeri

int main()
{
    cout << "\n******** 9 - Rubrica Telefonica ********\n\n";

    char nomi[vMax][nMax] = {{"Carabinieri"},               // dichiarazione e inizializzazione matrice dei nomi utenti
                                {"Polizia"},
                                {"Soccorso"},
                                {"Mare"}};
   
    char nTel[vMax][tMax] = {{"112"},                       // dichiarazione e inizializzazioni matrice dei numeri telefonici
                            {"113"},
                            {"118"},
                            {"1530"}};
    char nome[nMax];                                        // dichiarazione array nome utenti (da usare per la ricerca)
    char numero[tMax];                                      // dichiarazione array numero utente (da usare per la ricerca)

    int longN;                                              // variabile in cui memorizzare la lunghezza del nome più lungo
    int righe;                                              // variabile per determinare il numero degli utenti

/*  Stampa delle matrici    */

/*    int righe=0;
    for(int i=0; i<vMax; i++)
    {
        if(nTel[i][i]!='\0')
        righe++;
    }

    cout <<righe;
*/

/*  Calcolo delle righe effettivamente occupate nella matrice */
    longN =0;                                               // inizializzazione a zero delle variabili
    righe=0;
    for(int i=0; i<vMax && nTel[i][i]!='\0'; i++)           // --->>> Perche' funziona con [i][i] ????????????????
    {
        if(strlen(nomi[i]) > longN)                         // calcolo lunghezza degli array "nomi" (da usare con setw)
        longN = strlen(nomi[i]);                            // memorizzo il numero di caratteri del nome più lungo
        righe++;
    }

    cout << "Finora ci sono "<< righe << " utenti nella rubrica" <<endl<<endl;


    for(int i=0; i<righe && nTel[i][i]!='\0'; i++)          // --->>> Perche' [i][i] ????????????????
    {                                                       // e perche' funzione anche togliendo - && nTel[i][i]!='\0' - ??
        cout <<setw(longN)<< nomi[i] <<" ";         // setw(2): funzione che stabilisce quanti caratteri (longN) far 
        cout << nTel[i] <<endl;                     // occupare alla stampa dei nomi
    }
    cout <<endl;

/*  prova aggiunta utente con relativo numero   */

    cout << "Aggiungo un nuovo utente: ";
    cin >> nome;
    cout << "Aggiungo un nuovo numero: ";
    cin >> numero;

    strcpy(nomi[righe], nome);
    strcpy(nTel[righe], numero);
    righe++;

    cout <<endl<<endl;

/*  ristampo le matrici */

    cout << "ristampa matrici" <<endl<<endl;

    for(int i=0; i<righe && nTel[i][i]!='\0'; i++)
    {
        cout <<setw(longN)<< nomi[i] <<" ";         // setw(2): funzione che stabilisce quanti caratteri (longN)
        cout << nTel[i] <<endl;                     // far occupare alla stampa dei nomi */
    }

/*  prova ricerca esatta nome   */

    //for(int i=0; i<righe && nTel[i][i]!='\0'; i++)










return 0;

}