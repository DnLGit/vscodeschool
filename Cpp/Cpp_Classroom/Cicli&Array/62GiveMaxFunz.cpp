#include<iostream>
#include<cstdlib>                                       // Libreria da cui utilizzare le funzioni "rand" e "srand"
#include<time.h>                                        // Inizializza correttamente il seme in random
using namespace std;

/*  Bergamasco Daniele 4As - 24.11.2019 >>>>>>>>>>>> Give Max ... con Funzioni

    62. Scrivere un programma che, letti due vettori numerici, ne inizializzi un terzo assegnando a ogni suo elemento 
        il massimo tra i corrispondenti elementi nei primi due vettori. Visualizzare infine i tre vettori.  */

void FillRand(int, int const, int[]);                   // Dichiarazione prototipo funzione 1: riempimento array
void FillComp(int const, int[], int[], int[]);          // Dichiarazione prototipo funzione 2: confronto array
void Print(int const, int[]);                           // Dichiarazione prototipo funzione 3: stampa array

int main()
{
    int const N = 10;                                   // variabile costante della dimensione dell'array
    int Vect1[N], Vect2[N], Vect3[N];                   // dichiarazione di tre array
    int range;                                          // variabile che determina l'estremo superiore numeri casuali

    cout <<"\n\n ***** Print Array Give Max ... con Funzioni ***** \n\n";

    cout <<"Decidi il range di valori casuali per generare l'array (0-range)!\n";
    cout <<"Inserisci il valore di range ->   ";
    cin >>range;
    cout <<endl;

    srand(time(0));                                     // Funzione numeri casuali (seme inizializzato a zero)

    FillRand(range, N, Vect1);
    FillRand(range, N, Vect2);
    FillComp(N, Vect1, Vect2, Vect3);

    Print(N, Vect1);
    cout <<"  Primo array generato con numeri casuali   (range: 0-"<<range<<")"<<endl;          // stampa dell'array 1

    Print(N, Vect2);
    cout <<"  Secondo array generato con numeri casuali (range: 0-"<<range<<")"<<endl<<endl;    // stampa dell'array 2

    Print(N, Vect3);
    cout <<"  Array con valori max tra i corrispettivi indici dei precedenti"<<endl;            // stampa dell'array risultante
  
    return 0;
}

/* ***************** Implementazione delle funzioni **************************** */


// funzione 1: da utilizzare per riempire l'array utilizzando la funzione rand
void FillRand(int R, int const dim, int array[])
{   
    for(int i=0; i<dim; i++)                // riempimento array
    {
        array[i]=rand()%R+1;                 // utilizzando numeri casuali tra zero ed R compreso.
    }                  
}

// funzione 2: da utilizzare per riempire l'array confrontanto altri array
void FillComp(int const dim, int array1[], int array2[], int array3[])
{
    for(int i=0; i<dim; i++)                // confronto array
    {
          if(array1[i]>array2[i])                           
        {
            array3[i]=array1[i];                          
        }
        else
        {
            array3[i]=array2[i];            // assegnazione del terzo array con i valori max tra i due
        }   
    }                  
}

// funzione 3: da utilizzare per la stampa dell' array
void Print(int const dim, int array[])
{
    for(int i=0; i<dim; i++)                // ciclo for di stampa array
    cout << array[i] <<"\t";
}