#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 22.11.2019 >>>>>>>>>>>> StampaPremi

    59. Dato un vettore contenente i premi pagati presso un'agenzia assicurativa, 
    stampare l'elenco dei premi compresi tra due valori prefissati.  */

int main()
{
    int clienti, i1, i2;    // Dichiarazione variabili. clienti = dimensione array, i1 e 12 = range di stampa

    cout <<"\n\n ***** Stampa parziale Premi assicurativi ***** \n\n";

    cout <<"Inserisci il numero di clienti -> ";
    cin >>clienti;

    int Premi[clienti];     // Dichiarazione array (dove inserire i valori dei premi)

    for(int i=0; i<clienti; i++)                    // ciclo for di riempimento array
    {
        cout <<"Inserisci il premio del "<<i+1<<"^ cliente -> ";
        cin >> Premi[i];
    }   
    cout <<endl;

    cout <<"Questo e' l'elenco dei premi dei tuoi clienti: \n";
    for(int i=0; i<clienti; i++)                     // ciclo for di stampa array
    {
        cout <<Premi[i]<<"\t";
    }
    cout <<endl<<endl;

    cout <<"Ora dimmi entra quale range vuoi la stampa dei premi\n";
    cout <<"indica per favore il valore piu' basso -> ";
    cin >>i1;
    cout <<"ora per favore il valore piu' alto    --> ";
    cin >>i2;

    cout <<endl<<endl;
    
    cout <<"Hai scelto un range di valori da "<<i1<<" a "<<i2<<endl;
    cout <<"Questa e' la stampa dei premi nel range indicato, valori compresi: \n";
    for(int i=0; i<clienti; i++)                    // ciclo for di stampa array parziale
    {
        if(Premi[i] >= i1 && Premi[i] <= i2)        // condizione per la stampa fra i1 e i2 compresi
        {
            cout << Premi[i] <<"\t";
        }
    }
    cout <<endl;
    return 0;
}