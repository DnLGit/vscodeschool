#include<iostream>
using namespace std;

//min2 ritorna il minimo tra due interi
int min2(int n, int m)
{
    if(n < m)
        return n;
    else
        return m;
}

bool ricercaSeq(int V[], int n, int e)
{
    bool trovato = false;
    for(int i=0; i<n && !trovato; i++)
    {
        if(V[i]==e)
            trovato = true;
    }
    return trovato;
}

/*  Bergamasco Daniele 4As - 24.10.2019

51. Siano dati due vettori di interi inseriti da tastiera. 
    La lunghezza dei due vettori è inserita dall’utente da tastiera. 
    I due vettori possono avere lunghezze diverse, ma possono contenere al massimo 30 numeri. 
    Si scriva un programma in linguaggio C per generare un terzo vettore che contiene l’intersezione tra due vettori. 
    Tale vettore deve contenere i numeri presenti in entrambi i vettori dati. 
    Ad esempio, si assuma che siano stati inseriti i due vettori: 
    1 6 15 20 
    25 2 20 18 6 
    Il programma deve visualizzare la sequenza 6 20.     */

    int main()
{
    int N1, N2;                                                         // Variabili: "N1" e N2" (valore lunghezza array da inserire)
    bool doppio;        
                                                      
    cout <<"\n\n  ****** Array 30 max ... con Tre Array ****** \n\n";

    do
    {
        cout <<"\n\nInserisci le lunghezze dei rispettivi array: \n";
        cout <<"Inserisci qui la prima lunghezza   -> ";
        cin >> N1;
        cout <<"Inserisci qui la seconda lunghezza -> ";
        cin >> N2;

    } 
    while ((N1<=1 || N1>=30) || (N2<=1 || N2>=30)); //Rimango dentro al DO-WHILE fintanto che almeno uno dei due tra N1 e N2 risulta fuori dal range

    int Vect1[N1], Vect2[N2], Vect3[min2(N1, N2)];//Nota l'intersezione (Vect3 sarà lunga al massimo come il minimo tra N1 e N2)
    
    cout <<"\nCreazione Primo Array!!!\n";
    
    for(int i=0; i<N1; i++)
    {
        cout <<"Inserisci il "<<(i+1)<<"^ numero -> ";                  // Inserimento valori nell'array 1
        cin >> Vect1[i];
    }

    cout << "\nQuesta la Sequenza dell'array 1 ->\t";                   // stampa array 1
    
    for(int i=0; i<N1; i++)
    {
        cout << Vect1[i]<<"\t";
    }

    cout <<"\n\nQui il Secondo Array!!!\n";
    for(int i=0; i<N2; i++)
    {
        cout <<"Inserisci il "<<(i+1)<<"^ numero -> ";                  // Inserimento valori nell'array 2
        cin >> Vect2[i];
    }

    cout << "\nQuesta la Sequenza dell'array 2 ->\t";                 // stampa array 2
    for(int i=0; i<N2; i++)
    {
        cout << Vect2[i]<<"\t";
    }

    cout<<endl;

    int counter3 = 0;//contatore di elementi in Vect3
    for(int i=0; i<N1; i++)
    {
        //ad ogni ciclo considero elemento Vect1[i] e vado a vedere se si trova anche nel secondo
        if(ricercaSeq(Vect2, N2, Vect1[i]))
        {
            //se si trova anche nel secondo, allora farà parte dell'intersezione -> lo aggiungo a vect3
            
            //a meno che non sia già stato inserito precedentemente in Vect3
            if(ricercaSeq(Vect3, counter3, Vect1[i]) == false)
            {              
                Vect3[counter3] = Vect1[i];
                counter3++;
            }
        
        }
    }

    cout << "\nQuesta è l'intersezione' ->\t";                 // stampa array 2
    for(int i=0; i<counter3; i++)
    {
        cout << Vect3[i]<<"\t";
    }

    cout<<endl;

    /*
        int k, N=0;

        for(int i=0; i<N1; i++)                                             // ciclo per creazione terzo array
        {
            for(int j=0; j<N2; j++)                                         // ciclo annidato per confronto
            {
                doppio==false;
                if(Vect1[i]==Vect2[j])                                     // confronto tra i due array
                {
                    for(int z=0; i<N; z++)
                    {
                        if(Vect3[N]==Vect2[i])
                        doppio = true;
                    }
                }
            if(doppio == true)
            break;
            {
                Vect3[N]=Vect2[j];                                      // riempimento terzo array
                N++;
                break;
            }
   
            }//N++; 
        }

        cout << "La dimensione del nuovo array e': " <<N<<endl;

        cout << "\nQuesta la Sequenza dell'array 3 ->\t";                    // stampa array 3

        for(int i=0; i<N; i++)                                               // ciclo stampa terzo array
        {
            cout << Vect3[i]<<"\t";
        }
        cout<<endl;

    */    
        

    return 0;
}

/* ************  Da completare evitando le doppie!!! *************************    */