#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 24.10.2019

51. Siano dati due vettori di interi inseriti da tastiera. 
    La lunghezza dei due vettori è inserita dall’utente da tastiera. 
    I due vettori possono avere lunghezze diverse, ma possono contenere al massimo 30 numeri. 
    Si scriva un programma in linguaggio C per generare un terzo vettore che contiene l’intersezione tra due vettori. 
    Tale vettore deve contenere i numeri presenti in entrambi i vettori dati. 
    Ad esempio, si assuma che siano stati inseriti i due vettori: 
    1 6 15 20 
    25 2 20 18 6 
    Il programma deve visualizzare la sequenza 6 20.     */

                                                   
    int N3min(int a, int b) // funzione per determinare poi la dimensione di Vect3 minima tra N1 e N2
{
    if(a<b)
        return a;
    else
        return b;
}                           // In alternativa si potrebbe porre qui il Prototipo. La funzione dopo il return 0, fuori dal main.


    int main()
{
    int N1, N2;                                                         // Variabili: "N1" e N2" (valore lunghezza Array da inserire)
    int countE=0;                                                       // countE: contatore di elementi nell'Array 3
    bool doppio;                                                        // Variabile booleana per controllo valori doppi sull'Array 3
       
                                                      
    cout <<"\n\n  ****** Array 30 max ... con Tre Array + break ****** \n";

    do
    {
        cout <<"\n\nInserisci le lunghezze dei rispettivi Array: \n";
        cout <<"Inserisci qui la prima lunghezza   -> ";
        cin >> N1;
        cout <<"Inserisci qui la seconda lunghezza -> ";
        cin >> N2;
        if((N1<=1 || N1>=30) || (N2<=1 || N2>=30))                      // Alert se N1 e/o N2 fuori range richiesto.
        {
            cout<<"\nAttenzione: dimensioni Array non valide!!! Riprova!";
        }

    } 
    while ((N1<=1 || N1>=30) || (N2<=1 || N2>=30));     // Rimango dentro al DO-WHILE fino almeno che N1 o N2 risulta fuori dal range

    int Vect1[N1], Vect2[N2], Vect3[N3min(N1, N2)];     // N3min: dimensione di Vect3: minimo tra N1 e N2 (vedi funzione sotto)
    
    cout <<"\nCreazione Primo Array!!!\n";
    
    for(int i=0; i<N1; i++)
    {
        cout <<"Inserisci il "<<(i+1)<<"^ numero -> ";              // Inserimento valori nell'Array 1
        cin >> Vect1[i];
    }

    cout << "\nQuesta la Sequenza dell'Array 1 ->\t";               // stampa Array 1
    
    for(int i=0; i<N1; i++)
    {
        cout << Vect1[i]<<"\t";
    }

    cout <<"\n\nCreazione Secondo Array!!!\n";
    for(int i=0; i<N2; i++)
    {
        cout <<"Inserisci il "<<(i+1)<<"^ numero -> ";                // Inserimento valori nell'Array 2
        cin >> Vect2[i];
    }

    cout << "\nQuesta la Sequenza dell'Array 2 ->\t";                 // stampa Array 2
    for(int i=0; i<N2; i++)
    {
        cout << Vect2[i]<<"\t";
    }
    cout<<endl;


    for(int i=0; i<N1; i++)                                            // ciclo for scansione primo Array
    {
        for(int j=0; j<N2; j++)                                        // ciclo for annidato scansione secondo Array
        {
            if(Vect1[i]==Vect2[j])                                     // confronto tra i due Array
            {
                doppio=false;
                for(int k=0; k<countE; k++)
                {
                    if(Vect3[k]==Vect1[i])
                    {
                        doppio=true;                                    // Se ci fossero numeri doppi "doppio" diventa "true" ed esco dal for
                        break;
                    }
                }
                if(doppio == true)                                      // Se "doppio" risulta "true" esco dal for
                {
                    break;
                }
                else                                                    // altrimenti:
                {
                    Vect3[countE]=Vect2[j];                             // riempimento terzo Array + contatore celle
                    countE++;
                }
            }
        }
    }

    cout << "\nLa dimensione reale del nuovo Array e': " <<countE<<endl;

    cout << "\nQuesta la Sequenza dell'Array 3 ->\t";                    // stampa Array 3

    for(int i=0; i<countE; i++)                                         // ciclo stampa Array 3
    {
        cout << Vect3[i]<<"\t";
    }
    cout<<endl;

    return 0;
}


