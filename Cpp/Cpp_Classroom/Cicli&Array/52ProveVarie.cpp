#include<iostream>
using namespace std;

int main()
{
    int N1, N2;                                                              // Variabile "N1" (valore lunghezza array)
    
    bool trovatoInVect2;
    int counter;                                            
                                                      


    cout <<"\n\nInserisci la lunghezza dell'array: ";
    cin >> N1;
    
                                                 
    int Vect1[N1], Vect2[N1];                                     // Inizializzazione array

    for(int i=0; i<N1; i++)
    {
        cout <<"Inserisci il "<<(i+1)<<"^ numero -> ";              // Inserimento valori nell'array
        cin >> Vect1[i];
        
    }

    
    
    cout << "\nQuesta la Sequenza dell'array 1 ->\t";               // stampa array
    for(int i=0; i<N1; i++)
    {
        cout << Vect1[i]<<"\t";
        //Vect2[i]=Vect1[i];
    }
    cout <<endl;


    N2=0;                               // lunghezza reale di Vect2 (in memoria rimane comunque di dim N1)
    for(int i=0; i<N1; i++)
    {
        trovatoInVect2 = false;
        for(int j=0; j<N2 && !trovatoInVect2; j++)
        {
            if(Vect1[i]==Vect2[j])
            {
                trovatoInVect2 = true;
            }
        }

        if(!trovatoInVect2)
        {
            // non c'è già in vect2 -> lo aggiungo a vect2 e aumento N2
            Vect2[N2] = Vect1[i];
            N2++;
        }
        
        
    }

    cout << "\ndoppio vale -> " <<trovatoInVect2<<endl;
    cout << "\ncounter vale -> " <<counter<<endl;
    cout << "\nQuesta la Sequenza dell'array 2 ->\t";
    for(int i=0; i<N2; i++)
    {
        cout << Vect2[i]<<"\t";
    }
    cout <<endl;


    for(int j=0; j<N2; j++)
    {
        counter = 0;
        for(int i=0; i<N1; i++)
        {
            if(Vect2[j]==Vect1[i])
            {
                counter++;
            }
        }
        cout <<"Il valore "<<Vect2[j]<<" e' stato trovato " <<counter<<" volte\n";
    }
    




    return 0;
}