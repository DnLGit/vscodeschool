﻿#include<iostream>
using namespace std;

/* Bergamasco Daniele 4As - 16.10.2019

44. Scrivere un programma in linguaggio C per la rappresentazione del triangolo di Floyd.
Il triangolo di Floyd è un triangolo rettangolo che contiene numeri naturali, definito riempiendo le righe del triangolo
con numeri consecutivi e partendo da 1 nell’angolo in alto a sinistra.
Si consideri ad esempio il caso N=5. Il triangolo di Floyd e’ il seguente:
1
2 3
4 5 6
7 8 9 10
11 12 13 14 15
Il programma riceve da tastiera un numero intero N. Il programma visualizza le prime N righe del triangolo di Floyd.*/

int main()
{
    cout <<"**** Stampa di un Triangolo di Floyd ****"<<endl<<endl;

    int Ncol=0, Nprint=1;           // variabili del numero colonne/righe del triangolo e numeri stampati
    cout << "inserisci qui il numero di colonne del triangolo --> ";
    cin >> Ncol;

    for(int i=0; i<=Ncol; i++)       // ciclo per stampare le colonne/righe.
    {                               // indice "i": mi rende qtà di "colonne e numeri" per colonna ad incrementare di 1
        for(int j=0; j<i; j++)
        {
            cout << Nprint++<<" ";
        }

    cout << "\n";
    }
    
   return 0;
}