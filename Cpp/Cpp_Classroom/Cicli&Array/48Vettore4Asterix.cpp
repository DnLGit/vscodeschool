#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 20.10.2019

48. Scrivere un programma in linguaggio C che riceve in ingresso una sequenza di N numeri interi. 
    Il valore N è inserito dall’utente. I numeri sono memorizzati in un vettore. 
    Terminato l’inserimento della sequenza di numeri, il programma deve visualizzare una riga di asterischi per ogni numero inserito. 
    Il numero di asterischi nella riga è pari al valore del numero inserito. 
    Ad esempio, dato il vettore 9 4 6 il programma deve visualizzare: 
    Elemento 1: 9 *********
    Elemento 2: 4 **** 
    Elemento 3: 6 ******    */


int main()
{
    int N;                                                        // Variabili:  "N" (valore lunghezza array da inserire) 
                                                       
    cout <<"\n\n  ****** Array di Asterix ****** \n\n";
    cout <<"Inserisci la lunghezza dell'array -> ";
    cin >> N;

    int Vect[N];
  
    for(int i=0; i<N; i++)
    {
        cout <<"Inserisci ora il "<<(i+1)<<"^ numero -> ";          // Inserimento valori nell'array
        cin >> Vect[i];
    }

    cout << "\nQuesta la Sequenza dell'array:\n";                   // stampa array
    for(int i=0; i<N; i++)                                          // ciclo per stampa numeri
    {
        cout <<"\nElemento "<< i+1 << ":  " <<Vect[i] <<" ";

        for(int j=0; j<Vect[i]; j++)                                // ciclo per stampa Asterix
        {
            cout << "*";
        }
    }
    cout <<endl;
      
    return 0;
}