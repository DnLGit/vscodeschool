#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 11.11.2019 >>>>>>>>>>>> Media voti scolastici

    56. Letti da input un certo numero (N) di voti, memorizzarli in un vettore e quindi calcolarne la media aritmetica. 
        Visualizzare infine il risultato.  */

int main()
{
    int const Nvoti = 7;            // costante che indica la quantità di materie/voti
    int voti[Nvoti];                // Array dove inserire i voti da tastiera
    float somma;                    // variabile decimale per la somma dei voti

    cout <<"\n\n ****** Media voti scolastici ****** \n\n";
    cout <<"\nInserisci i voti delle varie materie: \n";
    somma=0;
    for(int i=0; i<Nvoti; i++)      // Ciclo For che inserisce i voti nell'Array e li aggiunge alla variabile "somma"
    {
        cout<<"Qui il voto della "<<i+1<<"^ materia --> ";
        cin >> voti[i];
        if(voti[i]<1 || voti[i]>10)
        {
            
        }
        somma +=voti[i];
    } 

    cout <<"\nLa media dei voti e': "<< somma/Nvoti<<endl;  // operazione aritmetica per la media/voti

return 0;
}