#include<iostream>
using namespace std;

/* Bergamasco Daniele 4As - 22.10.2019

45. Scrivere un programma in linguaggio C che riceve in ingresso una sequenza di N numeri interi. 
    I numeri sono memorizzati in un vettore. Il valore N è inserito dall’utente, ma il vettore può contenere al massimo 30 numeri. 
    Terminato l’inserimento della sequenza di numeri, l’utente inserisce un valore di riferimento. 
    Il programma deve indicare se tale valore di riferimento è contenuto nel vettore.   */


int main()
{
   int N, E, Q=0, P=0;                                        // Variabili "N" (valore lunghezza array da inserire), "E" (elemento da da cercare)    
                                                              // "Q" (contatore iterazioni) e "P" (memoria posizione elemento da cercare)
    cout <<"\n\n  ****** Array 30 max ... con Do While ****** \n\n";

    int Vect[N];

    do
    {        
        cout <<"Inserisci la lunghezza dell'array -> ";
        cin >> N;
    }
    while (N<1 || N>=30);
    {
        for(int i=0; i<N; i++)
        {
            cout <<"Inserisci il "<<(i+1)<<"^ numero -> ";      // Inserimento valori nell'array
            cin >> Vect[i];
        }

        cout << "\nQuesta la Sequenza dell'array ->\t";         // stampa array
        for(int i=0; i<N; i++)
        {
            cout <<Vect[i] <<"\t";
        }

        cout <<"\n\nInserire ora il valore del riferimento -> "; // inserimento valore da cercare
        cin >> E;

        for(int i=0; i<N; i++)                                  // ciclo for controlla array
        {
            if(Vect[i]==E)
            {
                Q++;                                            // conteggio iterazioni vere
                P=i;                                            // memorizza posizione di "E"                
            }        
        }

        if(Q!=0)
        {
            cout <<"L'elemento inserito "<<E<<" e' nell'array in posizione "<<P+1<<"\n";
        }
        else
        {
            cout <<"L'elemento "<<E<<" non appartiene all'array\n";
        }
    }

return 0;
}