#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 07.11.2019 >>>>>>>>>>>> Fusion 2

    53.(esercizio simpatico). Scrivere un programma in linguaggio C che esegue la fusione di due vettori di interi ordinati in modo crescente.
        Il programma deve eseguire le seguenti operazioni:
        1.  leggere due vettori di N interi. Il numero N viene inserito dall’utente ed è minore di 20.
            I due vettori possono avere lunghezza diversa. I due vettori si suppongono già ordinati in maniera crescente.
        2.  Creare un terzo vettore di lunghezza pari alla somma delle lunghezze dei due vettori dati.
            Il vettore dovrà contenere i numeri contenuti nei due vettori di partenza.
            I numeri nel vettore devono essere ordinati in modo crescente.
        3.  Stampare il vettore generato. Ad esempio, si assuma che siano stati inseriti i due vettori
            1 6 15 20 25
            2 8 18 19.
            Il programma dovrà visualizzare la sequenza 1 2 6 8 15 18 19 20 25  */


int main()
{
    int N1, N2, N3;

    cout <<"\n\n ****** Array 20 max ... Fusione Array ****** \n";

    do
    {
        cout <<"\nArray 1: inserisci la lunghezza -->  ";
        cin >> N1;
        cout <<"\nArray 2: inserisci la lunghezza -->  ";
        cin >> N2;


        if(N1<=1 || N1>=20 || N2<=1 || N2>=20)              // Alert se N1 e/o N2 fuori dal range richiesto.
        {
            cout<<"\nAttenzione: dimensioni Array non valide!!! Riprova!";
        }
    }
    while (N1<=1 || N1>=20 || N2<=1 || N2>=20);             // Rimango dentro al DO-WHILE almeno fino a che N1/N2 risulta fuori dal range

    int Vect1[N1], Vect2[N2];

    cout<<endl;
    for(int i=0; i<N1; i++)                                 // Ciclo inserimento Array 1
        {
            cout <<"Array 1: inserisci "<<i+1<<"^ valore crescente -> ";
            cin >> Vect1[i];
        }

    cout <<"Array 1 --> ";
    for(int i=0; i<N1; i++)
        {
            cout <<Vect1[i]<<"\t";
        }
    cout <<endl<<endl;

    
    for(int j=0; j<N2; j++)                                 // Ciclo inserimento Array 2
        {
            cout <<"Array 2: inserisci "<<j+1<<"^ valore crescente -> ";
            cin >> Vect2[j];
        }
    cout <<"Array 2 --> ";
    for(int j=0; j<N2; j++)
        {
            cout <<Vect2[j]<<"\t";
        }
    cout <<endl<<endl;

    N3=N1+N2;
    int Vect3[N3];

    for(int k=0, i=0, j=0; k<N3 && i<N1 && j<N2; k++)
    {
        if(Vect1[i]<=Vect2[j])
        {
            Vect3[k]=Vect1[i];
            i++;
        }
        else
        {
            Vect3[k]=Vect2[j];
            j++;
        }                       // stampato da qui Array 3 risulta --> 1  2  6   8   15  18  19  0   0

        cout <<"iiiii vale: "<<i<<endl;
        cout <<"jeiii vale: "<<j<<endl;
        cout <<"kappa vale: "<<k<<endl;


        if(k<N3)                // se Array 3 e' incompleto verrà riempito con: 
        {
            if(i==N1)                               // i rimanenti di Vect2
            {
                int y=(k-i)+1;                      // y e' il nuovo indice di Vect2 (differenza indici Vect3/Vect1)
                for(int k=i+j; k<N3; k++, y++)      // il for parte dalla somma degli indici
                Vect3[k]=Vect2[y];                  
            }
            else                                    // oppure i rimanenti di Vect1
            {
                int x=(k-j)+1;                      // x e' il nuovo indice di Vect1 (differenza indici Vect3/Vect2)
                for(int k=i+j; k<N3; k++, x++)      // il for parte dalla somma degli indici
                Vect3[k]=Vect1[x];                  
                          
            }
            
        }
    }                               

    
    //Vect3[7]=Vect1[3];                              // test valori mancanti ad Array 3 --> ok, così l'array si riempie
    //Vect3[8]=Vect1[4];                              // test valori mancanti ad Array 3 --> ok, così l'array si riempie

    cout <<"Array 3 --> ";
    for(int k=0; k<N3; k++)
        {
            cout <<Vect3[k]<<"\t";
        }
    cout <<endl<<endl;

    return 0;
}