#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 21.10.2019

49.(esercizio simpatico) 
    Scrivere un programma in linguaggio C che riceve in ingresso una sequenza di N numeri interi. 
    Il valore N è inserito dall’utente. I numeri sono memorizzati in un vettore. 
    Il programma esegue le seguenti operazioni: 
    1. visualizza il vettore 
    2. esegue uno spostamento (shift) a sinistra di una posizione del contenuto del vettore. 
        Pertanto ogni elemento del vettore deve assumere il valore dell’elemento immediatamente successivo all’interno del vettore. 
        L’elemento di indice N-1 deve assumere il valore zero. 
        Ad esempio dato il vettore: 1 10 15 18 Il programma deve generare il vettore: 10 15 18 0 Il programma visualizza il vettore ottenuto. 
    3. esegue uno spostamento (shift) a destra di una posizione del contenuto del vettore ottenuto nel passo precedente. 
        Pertanto ogni elemento del vettore deve assumere il valore dell’elemento immediatamente precedente all’interno del vettore. 
        L’elemento di indice 0 deve assumere il valore zero. 
        Ad esempio dato il vettore: 10 15 18 0 Il programma deve generare il vettore: 0 10 15 18 Il programma visualizza il vettore ottenuto. 
        Nota. Nella definizione di “destra” e “sinistra” si immagini il vettore stampato orizzontalmente, a partire dalla cella di indice 0.        */

int main()
{
    int N;                                                        // Variabili:  "N" (valore lunghezza array da inserire) 
                                                       
    cout <<"\n\n  ****** Array ShiftShift ****** \n\n";
    cout <<"Inserisci la lunghezza dell'array -> ";
    cin >> N;

    int Vect[N];
  
    for(int i=0; i<N; i++)
    {
        cout <<"Inserisci ora il "<<(i+1)<<"^ numero -> ";          // Inserimento valori nell'array
        cin >> Vect[i];
    }


    cout << "\nQuesta la Sequenza dell'array              --->  ";  // stampa array
    for(int i=0; i<N; i++)                                          // ciclo per stampa numeri
    {
        cout <<Vect[i] <<"\t";
    }


    cout << "\nQuesta la Sequenza dell'array con shift a sx ->  ";   // stampa array
    for(int i=1; i<N; i++)                                          // ciclo per stampa numeri tranne i=0
    {
        cout <<Vect[i] <<"\t";
    }
    Vect[N-1]=0;                                                    // assegno "zero" all'ultimo elemento dell'array
    cout <<Vect[N-1] <<"\t";                                        // stampo separatamente questo elemento dopo il for


    cout << "\nQuesta la Sequenza dell'array con shift a dx ->  ";  // stampa array

    cout <<Vect[N-1] <<"\t";                                        // stampo separatamente questo elemento prima del for
    for(int i=0; i<N-1; i++)                                        // ciclo per stampa numeri tranne i=N-1
    {
        cout <<Vect[i] <<"\t";
    }
   
    cout <<endl;
      
    return 0;
}