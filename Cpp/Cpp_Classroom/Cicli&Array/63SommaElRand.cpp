#include<iostream>
#include<cstdlib>                                       // Libreria da cui utilizzare le funzioni "rand" e "srand"
#include<time.h>                                        // Inizializza correttamente il seme in random
using namespace std;

/*  Bergamasco Daniele 4As - 24.11.2019 >>>>>>>>>>>> Analizza Array Random

    63. Scrivere un programma che, letto un vettore int, determini il numero degli zeri, degli elementi positivi, 
        degli elementi negativi e la somma complessiva di quest'ultimi.  */

int main()
{
    int const N = 10;                                   // variabile costante della dimensione dell'array
    int range, zeri, pos, neg, TotNeg;
    int Vect[N];                                        // dichiarazione array

    cout <<"\n\n ***** analizza Array generato Random ***** \n\n";

    cout <<"Decidi il range di valori casuali per generare l'array (0-range)!\n";
    cout <<"Inserisci il valore di range ->   ";
    cin >>range;
    cout <<endl;

    srand(time(0));                                     // Funzione numeri casuali (seme inizializzato a zero)

    for(int i=0; i<N; i++)                              // Ciclo For di riempimento array con valori casuali
    {
        Vect[i]=rand()%range+1;                          // i valori memorizzati negli array vanno da 0 a "range"
    }

    for(int i=0; i<N; i++)
    {
        cout <<Vect[i]<<"\t";
    }
    cout <<"  Array generato con numeri casuali   (range: 0-"<<range<<")"<<endl<<endl;      // stampa dell'array
    cout <<"Ora analizziamo l'array!!!";

    zeri=0; pos=0; neg=0, TotNeg=0;
    for(int i=0; i<N; i++)
    {
        if(Vect[i]==0)
        {
            zeri++;
        }
        if(Vect[i]>0)
        {
            pos++;
        }
        if(Vect[i]<0)
        {
            neg++;
            TotNeg+=Vect[i];
        }
    }
    cout <<"In questo array ci sono: \n";
    cout <<zeri<<" cifre pari allo zero\n";
    cout <<pos<<" cifre con segno positivo\n";
    cout <<neg<<" cifre con segno negativo\n";
    cout <<"La somma di quest'ultimi e' "<<TotNeg<<endl;

return 0;
}