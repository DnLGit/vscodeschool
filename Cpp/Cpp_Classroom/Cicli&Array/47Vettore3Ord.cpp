#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 20.10.2019

47. Scrivere un programma in linguaggio C che riceve in ingresso una sequenza di N numeri interi. 
I numeri sono memorizzati in un vettore. Il valore N è inserito dall’utente, ma il vettore può contenere al massimo 30 numeri. 
Terminato l’inserimento della sequenza di numeri, il programma deve verificare se il vettore contiene una sequenza di numeri 
ordinata in modo strettamente crescente. */


int main()
{
    int N;
    bool B;                                                      // Variabili:  "N" (valore lunghezza array da inserire) "Q" (contatore)
    char ris = 'y';  
                                                       
    cout <<"\n\n  ****** Array 30 max ... la sequenza e' ordinata??? ****** \n\n";

    while(ris!='n')
    {
        int Q = 0;                                                      // Q = 0 -> all'interno del while riporta a zero il contatore.
        cout <<"\nInserisci la lunghezza dell'array -> ";
        cin >> N;

        int Vect[N];
        if(N>1 && N<=30)
        {
            for(int i=0; i<N; i++)
            {
                cout <<"Inserisci il "<<(i+1)<<"^ numero -> ";          // Inserimento valori nell'array
                cin >> Vect[i];
            }

            cout << "\nQuesta la Sequenza dell'array ->\t";             // stampa array
            for(int i=0; i<N; i++)
            {
                cout <<Vect[i] <<"\t";
            }
            
            cout << "\n\nConfrontando gli elementi dell'array, ";         // controlla se nell'array ci sono valori crescenti
            for(int i=0; i<N; i++)
            {
                if(Vect[i]<Vect[i+1])
                {
                    Q++;
                }
            }

            if (Q == N-1)
            {
                cout << "L'array risulta ordinato in modo crescente\n\n";
                cout << "   Vuoi continuare?? (rispondi y or n) -> ";
                cin >> ris;
            }
            else
            {
                cout << "L'array NON risulta ordinato in modo crescente\n\n";
                cout << "   Vuoi continuare?? (rispondi y or n) -> ";
                cin >> ris;
            }
        }
        else
        {
            if(N<1)
            {
            cout << "    Attenzione, valore troppo piccolo!!!"<<endl;
            cout << "    Vuoi riprovare?? (rispondi y or n) -> ";
            cin >> ris;
            }
            else
            {
            cout << "    Attenzione, valore troppo grande!!!"<<endl;
            cout << "    Vuoi riprovare?? (rispondi y or n) -> ";
            cin >> ris;
            }
        }
    }
    return 0;
}