#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 23.11.2019 >>>>>>>>>>>> StampaSommaIndici

    60. Scrivere un programma che permetta di caricare in a e b, due array monodimensionali della stessa lunghezza, n valori interi, 
        calcoli la somma degli elementi con lo stesso indice, la memorizzi nell'array c e visualizzi a,b,c.   */

int main()
{
    cout <<"\n\n ***** Stampa elementi a stesso indice ***** \n\n";

    int const N = 10;                   // dichiarazione dimensione costante dell'array 
    int VectA[N], VectB[N], VectC[N];   // dichiarazioni degli array 

    for(int i=0; i<N; i++)              // Ciclo for di riempimento array
    {
        VectA[i]=i;                     // riempimento array A
        VectB[i]=i*2;                   // riempimento array B (i*2 per "variegare" l'array)
        VectC[i]=VectA[i]+VectB[i];     // riempimento array C: somma elementi a stesso indice
    }
    cout <<endl;
    cout <<"Questo l'array A ->\t";
    for(int i=0; i<N; i++)              // Ciclo for di stampa array A
    {
        cout<<VectA[i]<<"\t";
    }
    cout <<endl;

    cout <<"Questo l'array B ->\t";
    for(int i=0; i<N; i++)              // Ciclo for di stampa array B
    {
        cout<<VectB[i]<<"\t";
    }
    cout <<endl;

    cout <<"E questo l'array C ->\t";
    for(int i=0; i<N; i++)              // Ciclo for di stampa array C
    {
        cout <<VectC[i]<<"\t";
    }
    cout <<endl;

    return 0;
}