#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 11.11.2019 >>>>>>>>>>>> Eta' maggiorenne

    54. Esercizio semplice: 
        Un vettore contiene le eta' di un gruppo di persone. 
        Si vogliono visualizzare le eta' delle persone maggiorenni e la loro posizione nel vettore(indice).  */

int main()
{
    int const N =10;
    int eta[N]{10, 25, 65, 14, 58, 15, 98, 18, 35, 17};

    cout <<"\n\n ****** Array di persone ... trova maggiorenni ****** \n\n";

    cout <<"\nQuesti i maggiorenni del gruppo: \n\n";
    for(int i=0; i<N; i++)
    {
        if(eta[i]>=18)
        {
            cout <<"Il "<< eta[i]<<"enne e' al "<<i+1<<" posto\n";
        }
    }

return 0;
}