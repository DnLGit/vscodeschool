#include<iostream>
#include<cstdlib>                                       // Libreria da cui utilizzare le funzioni "rand" e "srand"
#include<time.h>                                        // Inizializza correttamente il seme in random
using namespace std;

/*  Bergamasco Daniele 4As - 24.11.2019 >>>>>>>>>>>> Give Max

    62. Scrivere un programma che, letti due vettori numerici, ne inizializzi un terzo assegnando a ogni suo elemento 
        il massimo tra i corrispondenti elementi nei primi due vettori. Visualizzare infine i tre vettori.  */

int main()
{
    int const N = 10;                                   // variabile costante della dimensione dell'array
    int Vect1[N], Vect2[N], Vect3[N];                   // dichiarazione di tre array
    int range;                                          // variabile che determina l'estremo superiore numeri casuali

    cout <<"\n\n ***** Print Array Give Max ***** \n\n";

    cout <<"Decidi il range di valori casuali per generare l'array (0-range)!\n";
    cout <<"Inserisci il valore di range ->   ";
    cin >>range;
    cout <<endl;

    srand(time(0));                                     // Funzione numeri casuali (seme inizializzato a zero)

    for(int i=0; i<N; i++)                              // Ciclo For di riempimento array con valori casuali
    {
        Vect1[i]=rand()%range+1;                        // i valori memorizzati negli array vanno da 0 a "range"
        Vect2[i]=rand()%range+1;
        if(Vect1[i]>Vect2[i])                           // assegnazione del terzo array con i valori max tra i due
        {
            Vect3[i]=Vect1[i];                          
        }
        else
        {
            Vect3[i]=Vect2[i];
        }
    }

    for(int i=0; i<N; i++)
    {
        cout <<Vect1[i]<<"\t";
    }
    cout <<"  Primo array generato con numeri casuali   (range: 0-"<<range<<")"<<endl;          // stampa dell'array 1
    for(int i=0; i<N; i++)
    {
        cout <<Vect2[i]<<"\t";
    }
    cout <<"  Secondo array generato con numeri casuali (range: 0-"<<range<<")"<<endl<<endl;    // stampa dell'array 2
    for(int i=0; i<N; i++)
    {
        cout <<Vect3[i]<<"\t";
    }
    cout <<"  Array con valori max tra i corrispettivi indici dei precedenti"<<endl;           // stampa dell'array risultante
  
    return 0;
}