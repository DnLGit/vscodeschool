#include<iostream>
#include<iomanip>       /* Libreria per la funzione setw(n)*/
using namespace std;

/* Bergamasco Daniele 4As - 17.10.2019

44. Scrivere un programma in linguaggio C per la rappresentazione del triangolo di Floyd.
Il triangolo di Floyd è un triangolo rettangolo che contiene numeri naturali, definito riempiendo le righe del triangolo
con numeri consecutivi e partendo da 1 nell’angolo in alto a sinistra.
Si consideri ad esempio il caso N=5. Il triangolo di Floyd e’ il seguente:
1
2 3
4 5 6
7 8 9 10
11 12 13 14 15
Il programma riceve da tastiera un numero intero N. Il programma visualizza le prime N righe del triangolo di Floyd.*/

int main()
{
    cout <<"\n\n**** Stampa di un Triangolo di Floyd: versione 4 stampe ****"<<endl<<endl;

    int Ncol=0, Nprint=1;     // variabili del numero colonne/righe del triangolo e numeri stampati
    char print =' ';

    cout <<"Che tipo di stampa vuoi?\nmonospazio (m)\ndoppio spazio (d)\ntabulato (t)\nsettato (s)?\nscegli qui: ";
    cin >> print;

    cout << "inserisci qui il numero di colonne del triangolo --> ";
    cin >> Ncol;

    for(int i=0; i<=Ncol; i++)       // ciclo per stampare le colonne/righe. indice "i": mi rende qtà di "colonne e numeri" per colonna ad incrementare di 1
    {
        for(int j=0; j<i; j++)
        {
            switch (print)
            {
            case 'm':
                cout << Nprint++<<" ";
                break;

            case 'd':
            cout << Nprint++<<"  ";
            break;

            case 't':
            cout << Nprint++<<"\t";
            break;

            case 's':
            cout <<setw(2)<< Nprint++<<" ";     /* setw(2): funzione che stabilisce quanti caratteri (2 ora) far occupare ad Nprint */
            break;
            
            default:
                break;
            }
        
        }
        cout << "\n";
    }
   return 0;
}