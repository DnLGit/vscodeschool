#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 23.11.2019 >>>>>>>>>>>> StampaSommaIndici con Funzioni

    60. Scrivere un programma che permetta di caricare in a e b, due array monodimensionali della stessa lunghezza, n valori interi, 
        calcoli la somma degli elementi con lo stesso indice, la memorizzi nell'array c e visualizzi a,b,c.   */

void Fill(int const, int, int[]);                   // Dichiarazione prototipo funzione 1: riempimento array
void Print(int const, int[]);                       // Dichiarazione prototipo funzione 2: stampa array
void FillPlus(int const, int[], int[], int[]);      // Dichiarazione prototipo funzione 3: riempimento array tramite somma indici


int main()
{
    cout <<"\n\n ***** Stampa elementi a stesso indice (con le Funzioni) ***** \n\n";

    int const N = 10;                               // dichiarazione dimensione costante dell'array 
    int VectA[N], VectB[N], VectC[N];               // dichiarazioni degli array 

    Fill(N, 1,VectA);                               // Invocazione funzione "Fill": riempimento array A (x vale 1)
    Fill(N, 2,VectB);                               // Invocazione funzione "Fill": riempimento array B (x vale 2)
    FillPlus(N, VectA, VectB, VectC);               // Invocazione funzione "FillPlus": riempimento array C

    cout <<endl;
    cout <<"Questo l'array A ->\t";
    Print(N, VectA);                                // Invocazione funzione "Print": stampa array A
    cout <<endl;
    cout <<"Questo l'array B ->\t";
    Print(N, VectB);                                // Invocazione funzione "Print": stampa array B
    cout <<endl;
    cout <<"E questo l'array C ->\t";
    Print(N, VectC);                                // Invocazione funzione "Print": stampa array C

    cout <<endl;

    return 0;
}

/* ***************** Implementazione delle funzioni **************************** */


// funzione 1: da utilizzare per riempire l'array
void Fill(int const dim, int x, int array[])
{
    for(int i=0; i<dim; i++)                     // ciclo for di stampa array
    {
        array[i]=i*x;                            // x = eventuale modificatore dell'indice (altrimenti = 1)
    }                        
}

// funzione 2: da utilizzare per riempire un array sommando i valori degli indici di altri due.
void FillPlus(int const N, int array1[], int array2[], int array3[])
{
    for(int i=0; i<N; i++)
    {
        array3[i]=array1[i]+array2[i];
    }
}

// funzione 3: da utilizzare per la stampa dell'intero array
void Print(int const dim, int array[])
{
    for(int i=0; i<dim; i++)                    // ciclo for di stampa array
    cout << array[i] <<"\t";
}