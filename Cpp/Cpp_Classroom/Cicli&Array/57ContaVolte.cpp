#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 20.11.2019 >>>>>>>>>>>> ContaVolte

    57. Contare il numero di volte in cui compare un valore x ricevuto in ingresso 
        tra gli elementi di un vettore a di lunghezza n.  */

int main()
{
    int x, y, N, count; // x= valore da contare, y=l'estremo del range di numeri da ripetere nell'array (vedi if(j>y))
    
    cout <<"\n\n ***** ContaLeVolte ***** \n\n";

    cout <<"Inserisci la dimensione dell'array -> ";
    cin >>N;
    cout <<"Inserisci un range di valori da ripetere (es.: 1-5  inserisci 5) -> ";
    cin >>y;
    cout <<"Inserisci il valore da contare -> ";
    cin >>x;

    int superVect[N];
    cout <<endl;

    count=0;
    for(int i=0, j=1; i<N; i++)         // ciclo per riempire l'array e contare i valori doppi
    {
        superVect[j]=j;
        if(superVect[j]==x)
        {
            count++;
        }
        cout << superVect[j]<<"\t";
        j++;
        if(j>y)                         // condizione che fa ripartire i valori dall'inizio
        {
            j=1;
        }
    }

cout <<"\nIl valore inserito "<<x<<" e' apparso "<<count<<" volte nell'array\n";

    return 0;
}

/*  -> altra soluzione del for poteva essere:
    for(int i=0, j=1; i<N; i++, j++)
    {
        superVect[j]=j;
        cout << superVect[j]<<"\t";
        if(j>=y)
        {
            j=0;
        }
    }
    */
