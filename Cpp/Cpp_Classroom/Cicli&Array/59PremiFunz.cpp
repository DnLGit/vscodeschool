#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 22.11.2019 >>>>>>>>>>>> StampaPremi con Funzioni

    59F. Dato un vettore contenente i premi pagati presso un'agenzia assicurativa, 
    stampare l'elenco dei premi compresi tra due valori prefissati.  
    
    ---------- versione con funzioni ---------------------   */

void Insert(int, int[]);                    // Dichiarazione prototipo funzione 1: riempimento array
void Print(int, int[]);                     // Dichiarazione prototipo funzione 2: stampa array
void PrintRange(int, int, int, int[]);      // Dichiarazione prototipo funzione 3: stampa range array

int main()
{
    int clienti, i1, i2;    // Dichiarazione variabili. clienti = dimensione array, i1 e 12 = range di stampa

    cout <<"\n\n ***** Stampa parziale Premi assicurativi con Funzioni ***** \n\n";
    cout <<"Inserisci il numero di clienti -> ";
    cin >>clienti;

    int Premi[clienti];                         // Dichiarazione array (dove inserire i valori dei premi)

    Insert(clienti, Premi);                     // invocazione funzione 1: riempimento array

    cout <<endl;

    cout <<"Questo e' l'elenco dei premi dei tuoi clienti: \n";
    Print(clienti, Premi);                      // invocazione funzione 2: stampa array

    cout <<endl<<endl;

    cout <<"Ora dimmi entro quale range vuoi la stampa dei premi\n";
    cout <<"indica per favore il valore piu' basso -> ";
    cin >>i1;
    cout <<"ora per favore il valore piu' alto    --> ";
    cin >>i2;

    cout <<endl;
    
    cout <<"Hai scelto un range di valori da "<<i1<<" a "<<i2<<endl;
    cout <<"Questa e' la stampa dei premi nel range indicato, valori compresi: \n";

    PrintRange(clienti, i1, i2, Premi);         // invocazione funzione 3: stampa range array

    cout <<endl;
    return 0;
}



/* ***************** Implementazione delle funzioni **************************** */


// funzione 1: da utilizzare per riempire l'array
void Insert(int dim, int array[])
{
    for(int i=0; i<dim; i++)                     // ciclo for di stampa array
    {
        cout <<"Inserisci il premio del "<<i+1<<"^ cliente -> ";
        cin >> array[i];
    }                        
}

// funzione 2: da utilizzare per la stampa dell'intero array
void Print(int dim, int array[])
{
    for(int i=0; i<dim; i++)                    // ciclo for di stampa array
    cout << array[i] <<"\t";
}

// funzione 3: da utilizzare per l'ultima stampa tra i due valori i1 e i2
void PrintRange(int dim, int x, int y, int array[])
{
    for(int i=0; i<dim; i++)                    // ciclo for di stampa array parziale
    {
    if(array[i] >= x && array[i] <= y)          // condizione per la stampa fra i1 e i2 compresi
        {
            cout << array[i] <<"\t";
        }
    }
}