#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 24.11.2019 >>>>>>>>>>>> Confronta Array - con Funzioni

    61. Scrivere un programma, che rischiesti all'utente i valori di due vettori v1 e v2, entrambi di lunghezza 10, 
        determini se i due vettori sono uguali. Due vettori v1 e v2 sono uguali solo se v1[i]=V2[I] per ogni i compreso tra 1 e 10.  */


void Fill(int const, int[]);                        // Dichiarazione prototipo funzione 1: riempimento array
void Print(int const, int[]);                       // Dichiarazione prototipo funzione 2: stampa array

int main()
{
    int const N = 10;                                   // variabile costante della dimensione dell'array
    int Vect1[N], Vect2[N];                             // dichiarazione di due array
    bool uguali;                                        // variabile booleana per confronto valori

    cout <<"\n\n ***** Cofronta gli Array ---> Versione con Funzioni ***** \n\n";



    return 0;
}

/* ***************** Implementazione delle funzioni **************************** */


// funzione 1: da utilizzare per riempire l'array
void Fill(int const dim, int array[])
{
    cout <<"Inserisci i valori del primo array: \n";
    for(int i=0; i<dim; i++)                              // riempimento array
    {
        cout <<i+1<<"^ valore: ";
        cin >>array[i];
    }                  
}

// funzione 2: da utilizzare per la stampa dell'intero array
void Print(int const dim, int array[])
{
    for(int i=0; i<dim; i++)                    // ciclo for di stampa array
    cout << array[i] <<"\t";
}

