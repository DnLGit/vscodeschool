#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 24.11.2019 >>>>>>>>>>>> Analizza Array

    63. Scrivere un programma che, letto un vettore int, determini il numero degli zeri, degli elementi positivi, 
        degli elementi negativi e la somma complessiva di quest'ultimi.  */

int main()
{
    int const N = 10;                                           // variabile costante della dimensione dell'array
    int zero, pos, neg, TotNeg;
    int Vect[N]={-1, 10, 20, 50, 100, 0, -20, -50, 105, 15};    // dichiarazione di due array

    cout <<"\n\n ***** analizza Array ***** \n\n";

    zero=0; pos=0; neg=0, TotNeg=0;                             // Inizializzazione variabili prima del For.
    for(int i=0; i<N; i++)                                      // Ciclo For scansione array
    {
        if(Vect[i]==0)                                          // Condizione per trovare i valori zero presenti nell'array
        {
            zero++;                                             // incremento il contatore di zero
        }
        if(Vect[i]>0)                                           // Condizione per trovare i valori positivi presenti nell'array
        {
            pos++;                                              // incremento il contatore dei valori positivi
        }
        if(Vect[i]<0)                                           // Condizione per trovare i valori negativi presenti nell'array
        {
            neg++;                                              // incremento il contatore dei valori negativi
            TotNeg+=Vect[i];                                    // sommatoria di tutti i valori negativi su variabile 'TotNeg'.
        }
    }
    cout <<"In questo array ci sono: \n";
    cout <<zero<<" cifre pari allo zero\n";
    cout <<pos<<" cifre con segno positivo\n";
    cout <<neg<<" cifre con segno negativo\n";
    cout <<"La somma di quest'ultimi e' "<<TotNeg<<endl;

return 0;
}