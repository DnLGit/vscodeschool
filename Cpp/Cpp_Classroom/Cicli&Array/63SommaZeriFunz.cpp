#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 24.11.2019 >>>>>>>>>>>> analizza Array compresi 'zeri' intermedi utilizzando le Funzioni

    63. Scrivere un programma che, letto un vettore int, determini il numero degli zeri, degli elementi positivi, 
        degli elementi negativi e la somma complessiva di quest'ultimi.  */

int ContaZeri(int const, int, int, int[]);      // Dichiarazione prototipo funzione 1: conta gli zeri presenti nei numeri 
int ContaZero(int const, int, int[]);           // Dichiarazione prototipo funzione 2: conta i valori pari a zero
int ContaPos(int const, int, int[]);            // Dichiarazione prototipo funzione 3: conta i valori positivi
int ContaNeg(int const, int, int[]);            // Dichiarazione prototipo funzione 4: conta i valori negativi
int SumNeg(int const, int, int[]);              // Dichiarazione prototipo funzione 5: esegue la somma dei valori negativi
void Print(int const, int[]);                   // Dichiarazione prototipo funzione 6: stampa array

int main()
{
    int const N = 10;                                           // variabile costante della dimensione dell'array
    int zero, nzero, vtemp, pos, neg, TotNeg;
    int tmp, count;
    int Vect[N]={-1, 10, 20, 50, 100, 0, -20, -50, 105, 15};    // dichiarazione di due array

    cout <<"\n\n ***** analisi di un Array compresi 'zeri' intermedi - Utilizzando le Funzioni ***** \n\n";

    zero=ContaZeri(N, tmp, count, Vect);        // assegnazione e chiamata della funzione 1
    nzero=ContaZero(N, count, Vect);            // assegnazione e chiamata della funzione 2
    pos=ContaPos(N, count, Vect);               // assegnazione e chiamata della funzione 3
    neg=ContaNeg(N, count, Vect);               // assegnazione e chiamata della funzione 4
    TotNeg=SumNeg(N, count, Vect);              // assegnazione e chiamata della funzione 5

    cout <<"In questo array  ->\t";       // stampa di tutti i risultati
    Print(N, Vect);
    cout <<"<-  ci sono: \n\n";
    cout <<zero<<" cifre pari allo zero e ";
    cout <<nzero<<" valori pari allo zero\n";
    cout <<"  per cui complessivamente "<<(zero+nzero)<<" zeri presenti nell'array\n";
    cout <<"e poi\n";

    cout <<pos<<" cifre con segno positivo\n";
    cout <<neg<<" cifre con segno negativo\n";
    cout <<"e la somma di quest'ultimi e' "<<TotNeg<<endl;

    return 0;
}

/* ***************** Implementazione delle funzioni **************************** */


// funzione 1: da utilizzare per contare zeri di numeri
int ContaZeri(int const dim, int tmp, int count, int array[])
{ 
    for(int i=0; i<dim; i++)           // Ciclo For principale scansione array
    {
        tmp=array[i];                 // assegnamento temporaneo valori dell'array per conteggio degli zeri 
        for(int j=0; tmp!=0; j++)     // Ciclo For per conteggio zeri che formano i valori dell'array
        {                              // si rmane dentro fino a quando var1 sarà intero
            if(tmp%10==0)             // Condizione: se divisibile per 10 mi da resto 0 ho trovato uno zero!
            {
                count++;                // incremento il contatore di zeri 
            }
            tmp/=10;                  // divido per 10 il valore fino ai minimi termini per cercare altri zeri.
        }
    }
    return count;
}

// funzione 2: da utilizzare per contare elementi di valore zero
int ContaZero(int const dim, int count, int array[])
{
    for(int i=0; i<dim; i++)               // Ciclo For principale scansione array
    {
         if(array[i]==0)                  // Condizione per trovare i valori zero presenti nell'array
        {
            count++;                       // incremento il contatore di zero
        }
    }
    return count;
}

// funzione 3: da utilizzare per contare elementi positivi
int ContaPos(int const dim, int count, int array[])
{
    for(int i=0; i<dim; i++)              // Ciclo For principale scansione array
    {
         if(array[i]>0)                   // Condizione per trovare i valori positivi presenti nell'array
        {
            count++;                       // incremento il contatore dei valori positivi
        }
    }
    return count;
}

// funzione 4: da utilizzare per contare elementi negativi
int ContaNeg(int const dim, int count, int array[])
{
    for(int i=0; i<dim; i++)              // Ciclo For principale scansione array
    {
         if(array[i]<0)                   // Condizione per trovare i valori negativi presenti nell'array
        {
            count++;                       // incremento il contatore dei valori negativi
        }
    }
    return count;
}

// funzione 5: da utilizzare per sommare gli elementi negativi
int SumNeg(int const dim, int count, int array[])
{
    for(int i=0; i<dim; i++)              // Ciclo For principale scansione array
    {
         if(array[i]<0)                   // Condizione per trovare i valori negativi presenti nell'array
        {
            count+=array[i];               // assegnamento e somma dei valori negativi
        }
    }
    return count;
}

// funzione 6: da utilizzare per la stampa dell' array
void Print(int const dim, int array[])
{
    for(int i=0; i<dim; i++)                // ciclo for di stampa array
    cout << array[i] <<"\t";
}
