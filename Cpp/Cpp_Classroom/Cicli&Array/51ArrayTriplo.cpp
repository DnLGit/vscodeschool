#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 24.10.2019

51. Siano dati due vettori di interi inseriti da tastiera. 
    La lunghezza dei due vettori è inserita dall’utente da tastiera. 
    I due vettori possono avere lunghezze diverse, ma possono contenere al massimo 30 numeri. 
    Si scriva un programma in linguaggio C per generare un terzo vettore che contiene l’intersezione tra due vettori. 
    Tale vettore deve contenere i numeri presenti in entrambi i vettori dati. 
    Ad esempio, si assuma che siano stati inseriti i due vettori: 
    1 6 15 20 
    25 2 20 18 6 
    Il programma deve visualizzare la sequenza 6 20.     */

    int main()
{
    int N1, N2;                                                         // Variabili: "N1" e N2" (valore lunghezza array da inserire)
    int N3=0;                                                           // N3: contatore di elementi in Vect3
    bool doppio;                                                        // Variabile booleana per controllo valori doppi su array3
    int N3min (int, int);                                               // Prototipo funzione per determinare la dimensione del terzo array        
                                                      
    cout <<"\n\n  ****** Array 30 max ... con Tre Array + break ****** \n\n";

    do
    {
        cout <<"\n\nInserisci le lunghezze dei rispettivi array: \n";
        cout <<"Inserisci qui la prima lunghezza   -> ";
        cin >> N1;
        cout <<"Inserisci qui la seconda lunghezza -> ";
        cin >> N2;
        if((N1<=1 || N1>=30) || (N2<=1 || N2>=30))
        {
            cout<<"\nAttenzione: dimensioni array non valide!!! Riprova!";
        }

    } 
    while ((N1<=1 || N1>=30) || (N2<=1 || N2>=30));     // Rimango dentro al DO-WHILE fino almeno che N1 o N2 risulta fuori dal range

    int Vect1[N1], Vect2[N2], Vect3[N3min(N1, N2)];     // N3min: dimensione di Vect3: minimo tra N1 e N2 (vedi funzione sotto)
    
    cout <<"\nCreazione Primo Array!!!\n";
    
    for(int i=0; i<N1; i++)
    {
        cout <<"Inserisci il "<<(i+1)<<"^ numero -> ";              // Inserimento valori nell'array 1
        cin >> Vect1[i];
    }

    cout << "\nQuesta la Sequenza dell'array 1 ->\t";               // stampa array 1
    
    for(int i=0; i<N1; i++)
    {
        cout << Vect1[i]<<"\t";
    }

    cout <<"\n\nQui il Secondo Array!!!\n";
    for(int i=0; i<N2; i++)
    {
        cout <<"Inserisci il "<<(i+1)<<"^ numero -> ";                // Inserimento valori nell'array 2
        cin >> Vect2[i];
    }

    cout << "\nQuesta la Sequenza dell'array 2 ->\t";                 // stampa array 2
    for(int i=0; i<N2; i++)
    {
        cout << Vect2[i]<<"\t";
    }
    cout<<endl;


    for(int i=0; i<N1; i++)                                            // ciclo for scansione primo array
    {
        for(int j=0; j<N2; j++)                                        // ciclo for annidato per confronto
        {
            if(Vect1[i]==Vect2[j])                                     // confronto tra i due array
            {
                doppio=false;
                for(int k=0; k<N3; k++)
                {
                    if(Vect3[k]==Vect1[i])
                    {
                        doppio=true;
                        break;
                    }
                }
                if(doppio == true)
                {
                    break;
                }
                else
                {
                    Vect3[N3]=Vect2[j];                                      // riempimento terzo array
                    N3++;
                }
            }
        }
    }

    cout << "La dimensione del nuovo array e': " <<N3<<endl;

    cout << "\nQuesta la Sequenza dell'array 3 ->\t";                    // stampa array 3

    for(int i=0; i<N3; i++)                                               // ciclo stampa terzo array
    {
        cout << Vect3[i]<<"\t";
    }
    cout<<endl;

    return 0;
}

int N3min(int a, int b) // funzione per determinare poi la dimensione di Vect3 minima tra N1 e N2
{
    if(a<b)
        return a;
    else
        return b;
}
