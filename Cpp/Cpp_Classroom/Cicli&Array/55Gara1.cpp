#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 11.11.2019 >>>>>>>>>>>> Gara Podistica

    55. In un vettore e' contenuta la classifica di una gara podistica con l’iniziale dei nomi dei concorrenti in ordine di arrivo. 
        Data l’iniziale del nome di un concorrente, stampare la sua posizione di classifica. 
        Se il nome non compare tra quelli caricati nel vettore, visualizzare una segnalazione di errore..  */

int main()
{
    int const N =10;                                                // variabile costante della dimensione Array 
    char nomi[N]{'D', 'F', 'G', 'B', 'M', 'R', 'C', 'N', 'P', 'L'}; // Array di caratteri/iniziali classifica
    char iniziale;                                                  // variabile char: inserisce l'utente l'iniziale 
    bool trovato;                                                   // contatore booleano di confronto all'interno dell'Array        

    cout <<"\n\n ****** Array Classifica Gara Podistica ****** \n\n";

    cout <<"\nCerca un concorrente, inserisci l'iniziale -->  ";
    cin >> iniziale;
    cout <<endl;

    trovato=false;
    for(int i=0; i<N; i++)                                          // Ciclo For di ricerca nome all'interno dell'Array
    {
        if(iniziale == nomi[i])                                     // se il nome c'e' viene stampato e inizializza il boo a true
        {
            cout <<"Il concorrente "<< nomi[i]<<" e' arrivato al "<<i+1<<" ^ posto\n";
            trovato=true;
        }
    }
     if(trovato==false)                                             // altrimenti viene stampato un alert
        {
            cout <<"Il concorrente non e' di questa categoria\n";
        }

return 0;
}