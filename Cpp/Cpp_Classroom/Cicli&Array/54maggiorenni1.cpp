#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 10.11.2019 >>>>>>>>>>>> Eta' maggiorenne

    54. Esercizio semplice: 
        Un vettore contiene le eta' di un eta di persone. 
        Si vogliono visualizzare le eta' delle persone maggiorenni e la loro posizione nel vettore(indice).  */

int main()
{
    int Np;
    bool anni=true;

    cout <<"\n\n ****** Array di persone ... trova maggiorenni ****** \n\n";
    cout <<"Indica il numero di persone --> ";
    cin >>Np;
    int eta[Np];

    do
    {
        cout <<"Inserisci ora le loro eta' --> \n";
    } while (anni!=true);
    
    

    for(int i=0; i<Np; i++)
    {
        cout <<"dimmi gli anni della "<<i+1<<"^ persona: ";
        cin >>eta[i];
        if(eta[i] <1 || eta[i]>120)
        {
            cout <<"Attenzione: eta' non valida \n";
            anni = false;
            break;
        }
    }

    cout <<"\nIl gruppo e' composto da "<<Np<<" persone di\t";
    for(int i=0; i<Np; i++)
    {
        cout << eta[i] <<" ";
    }
    cout <<" anni\n\n";
    cout <<"E questi i maggiorenni: \n";

    for(int i=0; i<Np; i++)
    {
         if(eta[i]>=18)
         {
             cout <<"\tIl "<< eta[i]<<"enne e' al posto "<<i+1<<"\n";
         }
    }

    return 0;
}