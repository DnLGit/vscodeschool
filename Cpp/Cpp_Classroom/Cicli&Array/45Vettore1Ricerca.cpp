#include<iostream>
using namespace std;

/* Bergamasco Daniele 4As - 17.10.2019

45. Scrivere un programma in linguaggio C che riceve in ingresso una sequenza di N numeri interi. 
    I numeri sono memorizzati in un vettore. Il valore N è inserito dall’utente, ma il vettore può contenere al massimo 30 numeri. 
    Terminato l’inserimento della sequenza di numeri, l’utente inserisce un valore di riferimento. 
    Il programma deve indicare se tale valore di riferimento è contenuto nel vettore.   */


int RicercaSeq(int[], int, int);            // Prototipo della funzione di Ricerca Sequenziale per verificare
                                            // la presenza o meno dell'elemento di riferimento all'interno dell'array

int main()
{

int N, E;                                   // Variabili "N" (valore lunghezza array da inserire) e "E" (elemento di riferimento da cercare)    

cout <<"\n\n  ****** Array 30 max - Ricerca Sequenziale ****** \n\n";
cout <<"Inserisci la lunghezza dell'array -> ";
cin >> N;

int Vect[N];                                // Dichiarazione di un'array

if(N<=30)
{
    for(int i=0; i<N; i++)                  // ciclo for per inserimento numeri nell'array
    {
        cout <<"Inserisci il "<<(i+1)<<"^ numero -> ";
        cin >> Vect[i];
    }

    cout << "Questa la Sequenza dell'array ->\t";
    for(int i=0; i<N; i++)                  // ciclo for per la stampa dell'array
    {
        cout <<Vect[i] <<"\t";
    }
    cout <<"\n\nInserire ora il valore di riferimento -> ";
    cin >> E;

    int Rs = RicercaSeq (Vect, E, N);       // Assegnazione risultato della funzione "RicercaSeq"alla variabile "Rs" 
                                            // (indica la posizione nell'array dell'eventuale numero da cercare)     
    if(Rs>=0)
    {
        cout <<"Il numero "<<E<<" inserito e' presente nell'array ed e' in posizione "<<Rs+1<<"\n";
    }
    else
    {
         cout <<"Il numero "<<E<<" inserito non e' presente nell'array \n";
    }
}
else
{
    cout <<"Attenzione, valore troppo grande!!!"<<endl;
}

    return 0;
}


/* Implementazione della funzione RicercaSeq */

int RicercaSeq(int Vect[], int E, int N)
{
    int pos = -1;
    
    for(int i=0; i<N; i++)
    {
        if(Vect[i]==E)
        {
            pos = i;                            // memorizza l'ultima posizione dell'elemento, se presente.
        }
    }
    return pos;    
}