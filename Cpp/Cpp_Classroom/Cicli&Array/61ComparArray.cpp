#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 24.11.2019 >>>>>>>>>>>> Confronta Array

    61. Scrivere un programma, che rischiesti all'utente i valori di due vettori v1 e v2, entrambi di lunghezza 10, 
        determini se i due vettori sono uguali. Due vettori v1 e v2 sono uguali solo se v1[i]=V2[I] per ogni i compreso tra 1 e 10.  */

int main()
{
    int const N = 10;                                   // variabile costante della dimensione dell'array
    int Vect1[N], Vect2[N];                             // dichiarazione di due array
    bool uguali;                                        // variabile booleana per confronto valori

    cout <<"\n\n ***** Cofronta gli Array ***** \n\n";

    cout <<"Inserisci i valori del primo array: \n";
    for(int i=0; i<N; i++)                              // riempimento array 1
    {
        cout <<i+1<<"^ valore: ";
        cin >>Vect1[i];
    }

    cout <<"\nInserisci i valori del secondo array: \n";
    for(int i=0; i<N; i++)                              // riempimento array 2
    {
        cout <<i+1<<"^ valore: ";
        cin >>Vect2[i];
    }

    cout <<"\nQueste le stampe dei due array";

    cout <<"\nArray 1 ->\t";
    for(int i=0; i<N; i++)                              // Ciclo for di stampa array 1
    {
        cout <<Vect1[i]<<"\t";
    }
     cout <<"\nArray 2 ->\t";
    for(int i=0; i<N; i++)                              // Ciclo for di stampa array 2
    {
        cout <<Vect2[i]<<"\t";
    }

    for(int i=0; i<N; i++)                              // Ciclo for di confronto
    {   
        uguali=false;
        if(Vect1[i]==Vect2[i])
        {
            uguali=true;                                // se i valori dei rispettivi indici sono uguali 
        }                                               // la booleana viene inizializzata a true
    }
    cout <<endl;
    
    if(uguali)                                          // stampa delle rispettive risposte 
    {
        cout <<"\nEffettivamente i due array sono uguali";
    }
    else
    {
        cout <<"\nSi informa che i due non sono uguali";
    }
    
    cout <<endl;
    
    return 0;
}