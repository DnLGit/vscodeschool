#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 05.11.2019

    52. Scrivere un programma in linguaggio C che legge N numeri interi da tastiera e li memorizza in un vettore. 
    Il numero N viene inserito dall’utente ed è minore di 20. Il programma deve visualizzare, per ogni cifra contenuta nel vettore, il numero di occorrenze. 
    Ad esempio, si supponga N=7 e si consideri la sequenza di numeri 1 6 15 6 2 15 15. 
    Il programma deve visualizzare: 
    numero 1 occorrenze 1 
    numero 6 occorrenze 2 
    numero 15 occorrenze 3 
    numero 2 occorrenze 1 
    Suggerimento. 
    Per ogni numero presente nel vettore, il numero di occorrenze deve essere visualizzato una sola volta (ad esempio per i numeri 6 e 15). 
    Utilizzare un vettore di supporto per poter tenere traccia dei numeri nel vettore per cui sono già state calcolate le occorrenze.   */

 int main()
{
    int N1, N2;                                                              // Variabile "N1" (valore lunghezza array)
    
    //int N3 = 0;                                                         // N3: contatore di elementi in Vect3
    bool doppio;
    bool ricercaSeq(int[], int, int);                                   // Prototipo funzione di ricerca sequenziale
    int min2(int, int);                                                 // Prototipo funzione per determinare la dimensione del terzo array                                             
                                                      
    cout <<"\n\n  ****** Array 20 max ... calcolo Occorrenze con RicercaSeq ****** \n\n";

    do
    {
        cout <<"\n\nInserisci la lunghezza dell'array: ";
        cin >> N1;
    } 
    while (N1<=1 || N1>=20);    // Rimango dentro al DO-WHILE fino almeno N1 risulta fuori dal range

    N2 = N1;                                                        // Variabile "N2" (valore max iniziale del secondo array)
    int Vect1[N1], Vect2[N2];  // Inizializzazione array
    
    cout <<"\nCreazione Primo Array!!!\n";
    
    for(int i=0; i<N1; i++)
    {
        cout <<"Inserisci il "<<(i+1)<<"^ numero -> ";                  // Inserimento valori nell'array
        cin >> Vect1[i];
    }

    cout << "\nQuesta la Sequenza dell'array 1 ->\t";                   // stampa array
    
    
    for(int i=0; i<N1; i++)
    {
        cout << Vect1[i]<<"\t";

        //Vect2[i]=Vect1[i];
    }
    
    cout <<"\nVect1[0] e': "<<Vect1[0]<<endl;

    for(int i=0; i<N1; i++)                                         // ciclo for scansione array
    {
        doppio=false;
        for(int j=0; j<N2; j++)
        {
            if(Vect1[i]==Vect2[j])
            {
                doppio=true;
                break;
            }
            if(doppio==true)
            {
                break;
            }
            else
            {
                Vect2[j]=Vect1[i];
            }
        }
        
    }

    cout <<"\nN2 e': "<<N2<<endl;
    cout <<"\nVect1[0] e': "<<Vect1[0]<<endl;
    cout <<"\nVect2[0] e': "<<Vect2[0]<<endl;

    cout << "\nQuesta la Sequenza dell'array 2 ->\t";                   // stampa array 2
    for(int j=0; j<N2; j++)
    {
        cout << Vect2[j]<<"\t";
    }






    return 0;
}

bool ricercaSeq(int V[], int n, int e)      // funzione di ricerca elementi
{
    bool trovato = false;
    for(int i=0; i<n && !trovato; i++)
    {
        if(V[i]==e)
            trovato = true;
    }
    return trovato;
}