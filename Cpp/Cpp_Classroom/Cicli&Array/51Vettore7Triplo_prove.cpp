#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 24.10.2019

51. Siano dati due vettori di interi inseriti da tastiera. 
    La lunghezza dei due vettori è inserita dall’utente da tastiera. 
    I due vettori possono avere lunghezze diverse, ma possono contenere al massimo 30 numeri. 
    Si scriva un programma in linguaggio C per generare un terzo vettore che contiene l’intersezione tra due vettori. 
    Tale vettore deve contenere i numeri presenti in entrambi i vettori dati. 
    Ad esempio, si assuma che siano stati inseriti i due vettori: 
    1 6 15 20 
    25 2 20 18 6 
    Il programma deve visualizzare la sequenza 6 20.     */

    int main()
{
    int N1, N2;                                                         // Variabili: "N1" e N2" (valore lunghezza array da inserire)        
    char ris = 'y';                                                     // Variabile char: inizializzata con 'y' per entrare nel while      
    int count=0;                                                       
    cout <<"\n\n  ****** Array 30 max ... con Tre Array ****** \n\n";

    do
    {
        cout <<"\n\nInserisci le lunghezze dei rispettivi array: \n";
        cout <<"Inserisci qui la prima lunghezza   -> ";
        cin >> N1;
        cout <<"Inserisci qui la seconda lunghezza -> ";
        cin >> N2;

    } 
    while (N1<=1, N1>=30, N2<=1, N2>=30);

    int Vect1[N1]={}, Vect2[N2]={}, Vect3[]={};
    
    cout <<"\nCreazione Primo Array!!!\n";
        int i, j, k, N=0;
        for(i=0; i<N1; i++)
        {
            cout <<"Inserisci il "<<(i+1)<<"^ numero -> ";                  // Inserimento valori nell'array 1
            cin >> Vect1[i];
        }

        cout << "\nQuesta la Sequenza dell'array 1 ->\t";                   // stampa array 1
        
        for(i=0; i<N1; i++)
        {
            cout << Vect1[i]<<"\t";
        }

        cout <<"\n\nQui il Secondo Array!!!\n";
        for(j=0; j<N2; j++)
        {
            cout <<"Inserisci il "<<(j+1)<<"^ numero -> ";                  // Inserimento valori nell'array 2
            cin >> Vect2[j];
        }

        cout << "\n\nQuesta la Sequenza dell'array 2 ->\t";                 // stampa array 2
        for(j=0; j<N2; j++)
        {
            cout << Vect2[j]<<"\t";
        }

        for(i=0; i<N1; i++)
        {
            for(j=0; j<N2; j++)
            {
                if (Vect1[i]==Vect2[j])
                {
                //Vect3[0]=Vect2[j];
                //j++, k++, N++;
                count++;
                
                Vect3[i]=Vect2[j];
                //cout<<" qui count e': "<<count<<endl;
                }
                
            
            }N++;
        }
        

        //cout << "valori di J, k, N e count sono: " <<j<<", "<<k<<", "<<N<<" e "<<count<<endl;
        cout<<endl;
        cout << "valore di Vect1[i] e': " <<Vect1[i]<<endl;
        cout << "valore di Vect2[j] e': " <<Vect2[j]<<endl;
        cout << "valore di count e': " <<count<<endl;
        cout << "valore di N e': " <<N<<endl;

        for(i=0; i<N; i++)
        {
            cout << Vect3[i]<<"\t";
        }

        
        

    return 0;
}

/* ************  Da completare *************************    */