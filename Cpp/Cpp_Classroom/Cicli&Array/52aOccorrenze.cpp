#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 05.11.2019

    52. Scrivere un programma in linguaggio C che legge N numeri interi da tastiera e li memorizza in un vettore. 
    Il numero N viene inserito dall’utente ed è minore di 20. Il programma deve visualizzare, per ogni cifra contenuta nel vettore, il numero di occorrenze. 
    Ad esempio, si supponga N=7 e si consideri la sequenza di numeri 1 6 15 6 2 15 15. 
    Il programma deve visualizzare: 
    numero 1 occorrenze 1 
    numero 6 occorrenze 2 
    numero 15 occorrenze 3 
    numero 2 occorrenze 1 
    Suggerimento. 
    Per ogni numero presente nel vettore, il numero di occorrenze deve essere visualizzato una sola volta (ad esempio per i numeri 6 e 15). 
    Utilizzare un vettore di supporto per poter tenere traccia dei numeri nel vettore per cui sono già state calcolate le occorrenze.   */

 int main()
{
    int N1, N2;                                                              // Variabili "N1" ed "N2" (valore lunghezza array)
    
    bool trovatoInVect2;                                                    // booleana ricerca valori presenti più volte
    int counter;                                                            // contatore occorrenze

    cout <<"\n\n  ****** Array 20 max ... calcolo Occorrenze + break ****** \n";

    do
    {
        cout <<"\n\nInserisci la lunghezza dell'array: ";
        cin >> N1;
        if(N1<=1 || N1>=20)                                                 // Alert se N1 fuori range richiesto.
        {
            cout<<"\nAttenzione: dimensioni Array non valide!!! Riprova!";
        }
    } 
    while (N1<=1 || N1>=20);                                                // Rimango dentro al DO-WHILE fino almeno N1 risulta fuori dal range

    int Vect1[N1], Vect2[N1];                                               // Inizializzazione array (la dimensione del secondo al max e' N1)

    for(int i=0; i<N1; i++)
    {
        cout <<"Inserisci il "<<(i+1)<<"^ numero -> ";              // Inserimento valori nell'array
        cin >> Vect1[i];
        
    }

    cout << "\nQuesta la Sequenza dell'array 1 ->\t";               // stampa array
    for(int i=0; i<N1; i++)
    {
        cout << Vect1[i]<<"\t";
    }
    cout <<endl;

    N2=0;                                   // lunghezza reale di Vect2 (in memoria rimane comunque di dimensione N1)
    for(int i=0; i<N1; i++)
    {
        trovatoInVect2 = false;
        for(int j=0; j<N2; j++)
        {
            if(Vect1[i]==Vect2[j])
            {
                trovatoInVect2 = true;
                break;
            }
        }

        if(!trovatoInVect2)                  // non c'è già in vect2 -> lo aggiungo a vect2 e aumento N2
        {
            Vect2[N2] = Vect1[i];
            N2++;
        }
    }

    cout << "\nQuesta la Sequenza dell'array 2 di supporto ->\t";
    for(int i=0; i<N2; i++)
    {
        cout << Vect2[i]<<"\t";
    }
    cout <<endl<<endl;

    for(int j=0; j<N2; j++)                   // Qui ciclo for per controllo delle occorrenze
    {
        counter = 0;
        for(int i=0; i<N1; i++)
        {
            if(Vect2[j]==Vect1[i])
            {
                counter++;
            }
        }
        cout <<"Il valore "<<Vect2[j]<<" e' stato trovato " <<counter<<" volte\n";
    }
    return 0;
}