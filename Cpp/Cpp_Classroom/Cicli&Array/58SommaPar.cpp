#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 21.11.2019 >>>>>>>>>>>> SommaPar&Disp

    58. Calcolare la somma degli elementi pari e quella degli elementi dispari presenti in un vettore di interi a di lunghezza n.  */

int main()
{
     int pp, ddd, y, N; // pp= pari, ddd= dispari, y=l'estremo del range di numeri da ripetere nell'array (vedi if(j>y)), N=dim array.
    
    cout <<"\n\n ***** Somma di Pari & Dispari ***** \n\n";

    cout <<"Inserisci la dimensione dell'array -> ";
    cin >>N;
    cout <<"Inserisci un range di valori da ripetere (es.: 1-5  inserisci 5) -> ";
    cin >>y;

    int superVect[N];   // dichiarazione dell'array
    cout <<endl;

    pp=0, ddd=0;
    for(int i=0, j=1; i<N; i++, j++)
    {
        superVect[j]=j;
        if(j>y)
        {
            j=1;
        }
        if(superVect[j]%2==0)
        {
            pp+=superVect[j];
        }
        else
        {
            ddd+=superVect[j];
        }
        
        cout <<superVect[j]<<"\t";
    }
    cout <<endl<<endl;

    cout <<"Nell'array, la somma dei numeri pari e' "<<pp<<endl;
    cout <<"mentre quella dei numeri dispari e' "<<ddd<<endl;

    return 0;
}