#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 21.10.2019

50(esercizio simpatico). 
    Scrivere un programma in linguaggio C che legge N numeri interi da tastiera e li memorizza in un vettore. 
    Il numero N viene inserito dall’utente ed è minore di 20. 
    Il programma deve generare un secondo vettore che compatta i numeri contenuti nel primo vettore. 
    In particolare:
        • ogni numero che compare ripetuto nel primo vettore, deve comparire una sola volta nel secondo vettore 
        • ogni numero uguale a zero presente nel primo vettore non deve comparire nel secondo vettore.
    Il programma deve visualizzare il contenuto del secondo vettore. 
    Ad esempio, si supponga N=8 e si consideri la sequenza di numeri 1 18 3 0 24 3 6 0 inseriti da tastiera. 
    Il programma deve visualizzare 1 18 3 24 6.     */

int main()
{
    int N;                                                                  // Variabile: "N" (valore lunghezza array da inserire)        
    char ris = 'y';                                                         // Variabile char: inizializzata con 'y' per entrare nel while      
                                                       
    cout <<"\n\n  ****** Array 20 max ... con secondo Array ****** \n\n";

    while(ris == 'y')
    {
        cout <<"\nInserisci la lunghezza dell'array -> ";
        cin >> N;

        int Vect1[N]={}, Vect2[N]={};


        if(N<=20)
        {
            for(int i=0; i<N; i++)
            {
                cout <<"Inserisci il "<<(i+1)<<"^ numero -> ";                  // Inserimento valori nell'array
                cin >> Vect1[i];
            }

            cout << "\nQuesta la Sequenza dell'array ->\t";                      // stampa array 1
            int i, j;
            for(i=0; i<N; i++)
            {
                cout <<Vect1[i] <<"\t";
                if(Vect1[i]!=Vect1[i+1])
                {
                    Vect2[i]=Vect1[i];
                }
            }
 
            cout << "\nQuesta invece la Sequenza dell'array compattato ->\t";   // stampa array senza doppie e zeri
            for(i=0; i<N; i++)
            {
                if(Vect2[i]!=0)
                {
                    cout << Vect2[i] <<"\t";
                }            
            }   
            cout <<"\nVuoi continuare? (y/n): ";
            cin >> ris;    
        }
        else
        {
            cout <<"Attenzione, valore troppo grande!!!"<<endl;
            cout <<"Vuoi riprovare? (y/n): ";
            cin >> ris;
        }
    }
    return 0;
}