#include <string>
#include <cstring>                              // dovrebbe sostituire <string.h> e permettere di usare le funzioni di C in C++
#include<iostream>
using namespace std;

/* Bergamasco Daniele  28.12.19  - Stringhe - Esercizio n° 3

    Codifica di una parola

    Scrivere un programma in linguaggio C che legga una frase introdotta da tastiera. 
    La frase è terminata dall’introduzione del carattere di invio. 
    La frase contiene sia caratteri maiuscoli che caratteri minuscoli, e complessivamente al più 100 caratteri. 
    Il programma deve svolgere le seguenti operazioni: 
    * visualizzare la frase inserita; 
    * costruire una nuova frase tale che ogni lettera vocale presente nella frase di partenza sia seguita 
        dalla lettera ’f’ (se la vocale è minuscola) o 
        dalla lettera ’F’ (se la vocale è maiuscola) nella nuova frase. 
        Il programma deve memorizzare la nuova frase in una opportuna variabile. 
    * visualizzare la nuova frase.  */

#define CharMax 100                                                             // direttiva per il preProcessore

int main()
{
    cout << " \n***** Sostituisci vocali con f&F ****** \n " << endl;

    char fraseC[CharMax];                                                       // dichiarazione 1^ frase, da inserire da tastiera
    char fraseC2[CharMax];                                                      // dichiarazione 2^ frase,  da costruire successivamente

    cout << "Inserisci una frase: ";
    
    cin.getline(fraseC, CharMax);                                               // acquisizione di un array di caratteri con spazi (fgets prende anche \n)
    int longC = strlen(fraseC);                                                 // calcolo lunghezza della 1^ frase
    cout << "La frase inserita e': " << fraseC << ", lunga " << longC << " caratteri" <<endl;
    
    int voc = 0;                                                                 // contatore vocali
    for(int i=0; i<longC; i++)                                                   // ciclo for: scansione 1^ frase                                                  
    {
        fraseC2[i+voc] = fraseC[i];                                              // assegnazione valori 1^ frase alla 2^ frase
        if(fraseC[i] == 'i' || fraseC[i] == 'u' || fraseC[i] == 'o' || fraseC[i] == 'a' || fraseC[i] == 'e')
        {
            fraseC2[i+voc+1] ='f';                                                // se trovo una vocale minuscola aggiungo una f 
            voc++;                                                                // incremento contatore delle vocali minuscole  
        }
        if(fraseC[i] == 'I' || fraseC[i] == 'U' || fraseC[i] == 'O' || fraseC[i] == 'A' || fraseC[i] == 'E')
        {
            fraseC2[i+voc+1] ='F';                                                // se trovo una vocale maiuscola aggiungo una F 
            voc++;                                                                // incremento contatore delle vocali maiuscole
        }
    }
    
    int longC2 = longC+voc;                                                       // calcolo lunghezza della 2^ frase
    fraseC2[longC2] = '\0';                                                       // aggiungo tappo finale
    cout << "La frase nuova e': " << fraseC2 <<endl;                              // stampa 2^ frase
    cout << "La nuova frase e' lunga " << longC2 << " caratteri" <<endl;   

    return 0;
}