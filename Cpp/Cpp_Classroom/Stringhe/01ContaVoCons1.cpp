#include<iostream>
#include<cstring>        // in C -> string.h (verificare!!!??!!!)
#include<string.h>
#include<string>
using namespace std;

/*  Bergamasco Daniele 4As - 30.11.2019 >>>>>>>>>>>> Conta Vocali e Consonanti

    01. Scrivere un programma in linguaggio C che legga una frase introdotta da tastiera. 
        La frase è terminata dall’introduzione del carattere di invio. 
        La frase contiene sia caratteri maiuscoli che caratteri minuscoli, e complessivamente al più 100 caratteri. 
        Il programma dovrà stampare su schermo le seguenti informazioni: 
         • per ognuna delle lettere dell’alfabeto, il numero di volte che la lettera compare nella stringa 
         • il numero di consonanti presenti nella stringa 
         • il numero di vocali presenti nella stringa..  */

int main()
{

int Dim = 50;                   // dimensione dell'array per "frase"
int countL, countV, countC;     // contatori per la quantità di Lettere, Vocali e Consonanti
bool trovata;                   // booleana per la ricerca delle lettere diverse in "frase"
int Len1;                       // lunghezza effettiva di "frase"
int Len2;                       // lunghezza array dove memorizzare le "Lettere" diverse fra loro

char frase[Dim] = "Eccco\n";     // array di caratteri "frase"
Len1 = strlen(frase);            // calcolo lunghezza effettiva di "frase"
char Lettere[Len2];              // array di caratteri "Lettere"            

/* Info per Devis:    al momento sto provando con un array già dichiarato e inizializzato "Eccco\n" */
/* Info per Devis:    qui volevo solo creare un secondo array delle sole Lettere diverse presenti in "frase" */
/* info aggiunta:  in ASCII le maiuscole vanno dal 65 al 90, le minuscole dal 97 al 122 */

Len2=0;
for(int i=0; i<Len1; i++)
{
    trovata=false;
    for(int j=0; j<Len2; j++)
    {
        
        if(frase[i]==Lettere[j])
        {
            trovata=true;
            break;
        }
    }
    if(trovata){
        Lettere[Len2]=frase[i];
        Len2++;
    }
}

cout <<endl;
cout <<"frase: " << frase <<endl;
cout << "Len1: "<< Len1 <<endl;
cout << "Len2: "<< Len2 <<endl;
cout <<"count:" << countL <<endl;
cout <<"Lettere: " << Lettere <<endl;

    return 0;
}