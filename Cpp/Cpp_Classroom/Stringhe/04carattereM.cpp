#include <string>
#include <cstring>                              // dovrebbe sostituire <string.h> e permettere di usare le funzioni di C in C++
#include<iostream>
using namespace std;

/* Bergamasco Daniele  30.12.19  - Stringhe - Esercizio n° 4

    Primo carattere maiuscolo 
    
    Scrivere un programma in linguaggio C che legga una frase introdotta da tastiera. 
    La frase è terminata dall’introduzione del carattere di invio. 
    La frase contiene sia caratteri maiuscoli che caratteri minuscoli, e complessivamente al più 100 caratteri. 
    Il programma deve svolgere le seguenti operazioni: 
        • visualizzare la frase inserita 
        • costruire una nuova frase in cui il primo carattere di ciascuna parola nella frase di partenza è stato reso maiuscolo. 
           Tutti gli altri caratteri devono essere resi minuscoli. 
        Il programma deve memorizzare la nuova frase in una opportuna variabile 
        • visualizzare la nuova frase. 
           Ad esempio la frase cHe bElLA gIOrnaTa diviene Che Bella Giornata.  */
           

#define CharMax 100                                                        // direttiva per il preProcessore

int main()
{
    cout << " \n***** Sostituisci minuscoli e maiuscoli ****** \n " << endl;

    char fraseC[CharMax];                                                  // dichiarazione 1^ frase, da inserire da tastiera
    char fraseC2[CharMax];                                                 // dichiarazione 2^ frase,  da costruire successivamente

    cout << "Inserisci una frase: ";
    
    cin.getline(fraseC, CharMax);                                          // acquisizione di un array di caratteri con spazi (fgets prende anche \n)
    int longC = strlen(fraseC);                                            // calcolo lunghezza della 1^ frase
    cout << "La frase inserita e': " << fraseC << ", lunga " << longC << " caratteri" <<endl;

    int diffMm = 'a'-'A';                                                  // differenza su ASCII tra lettere maiuscole e minuscole

    if(fraseC[0] >='A' && fraseC[0] <='Z')                                 // verifico se la prima lettera della frase è maiuscola  
    {
        fraseC2[0] = fraseC[0];                                            // se così la copio nella 2^ frase
    }
    else
    {
        fraseC2[0] = (fraseC[0]-diffMm);                                    // altrimenti copio la rispettiva maiuscola
    }

    for(int i=1; i<longC; i++)                                              // ciclo for di controllo fraseC1 (prima frase)
    {
       if(fraseC[i] >='A' && fraseC[i] <='Z')                               // controllo le maiuscole
        {
            fraseC2[i] = (fraseC[i]+diffMm);                                // e copio le rispettive minuscole nella 2^ frase
        }
        else                                                                // tranne le lettere che già lo sono
        {
            fraseC2[i] = (fraseC[i]);                                       // e copio le rimanenti minuscole nella 2^ frase
        }

        if(fraseC[i-1] == ' ')                                              // cerco gli spazi tra le parole
        {
            if(fraseC[i] >='A' && fraseC[i] <='Z')
            {
                fraseC2[i] = (fraseC[i]);                                   // e copio le successive lettere maiuscole nella 2^ frase
            }
            else
            {
                fraseC2[i] = (fraseC[i]-diffMm);                            // oppure converto le minuscole in maiuscole e le copio nella 2^ frase
            }
        }
    }

    fraseC2[longC] = '\0';                                                  // aggiungo tappo finale

    cout << "La frase nuova e': " << fraseC2;                               // stampa 2^ frase                                            

    return 0;
}