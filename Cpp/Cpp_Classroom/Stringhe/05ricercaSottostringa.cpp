#include <string>
#include <cstring>                              // dovrebbe sostituire <string.h> e permettere di usare le funzioni di C in C++
#include<iostream>
using namespace std;

/* Bergamasco Daniele  01.01.20  - Stringhe - Esercizio n° 5

    Ricerca sottostringa 
    
    Si scriva un programma in linguaggio C che riceva in ingresso due parole inserite da tastiera. 
    Si consideri che ciascuna parola può contenere al massimo 30 caratteri. 
    Il programma deve verificare se la seconda parola inserita è contenuta almeno una volta all’interno della prima parola 
    (ossia se la seconda parola è una sottostringa della prima parola).  */


#define CharMax 30                                                              // direttiva per il preProcessore

int main()
{
    cout << " \n***** Ricerca sottostringa ****** \n " << endl;

    char parola1[CharMax];                                   // dichiarazione 1^ parola, da inserire da tastiera
    char parola2[CharMax];                                   // dichiarazione 2^ parola, da inserire da tastiera

    cout << "Inserisci due parole\n " << endl;

    cin.getline(parola1, CharMax);                           // essendo una sola "parole" era sufficiente anche il solo "cin"
    cin.getline(parola2, CharMax);                           // essendo una sola "parole" era sufficiente anche il solo "cin"

    int long1 = strlen(parola1);                             // calcolo la lunghezza delle due parole (strlen: funzione di C)
    int long2 = strlen(parola2);

    cout << "La prima parola: " << parola1 << " e' di " << long1 << " caratteri" <<endl;       // --> per es. -->   portofino
    cout << "La seconda parola: " << parola2 << " e' di " << long2 << " caratteri" <<endl;     // --> per es. -->   fino 
    cout <<endl;

    int trovate = 0;                                        // contatore inizializzato a zero
    for(int i=0; i<long1; i++)                              // ciclo for: scorro parola 1
    {
        if(parola1[i] == parola2[0])    // cerco nella prima parola una lettera uguale a quella iniziale della seconda parola
        {                               // -->  ipotizziamo che potrebbe "forse" essere l'inizio della sottostringa  <--
            int j;                              // dichiaro j prima del 2° for ... mi servirà anche fuori dal ciclo.
            for(j=1; j<long2 && parola1[i+j] == parola2[j]; j++)     
            { 
                                                // si esce dal for con j<long2 ma anche quando s1[i+j] e' diversa da s2[j],
                                                //  in pratica quando non viene garantita la sequenza delle ugualianze.
                            // ciclo for che mi serve solo per far iterare l'indice "j" fino alla fine della parola2 nel caso
            }               // in cui questa sia una sottostringa di parola1                 -->> nessuna istruzione quindi!!
            if(j == long2)                      // utilizzo le iterazioni come "alert" di fine parola trovata
                trovate++;                      // contatore delle volte in cui la parola trovata e' presente in parola1
        }
    }
    
    if(trovate)
    {
        cout << parola2 <<" e' sottostringa della prima parola!"<<endl;
        cout << "ed e' stata trovata ";
        if(trovate <2)
        {
            cout << trovate << " volta";
        }
        else
        {
            cout << trovate << " volte"; 
        }
    }
    else
    {
        cout << parola2 << " NON e' sottostringa della prima parola!" <<endl;
    }



/* ***************** Funzione trovata in rete ... ma non era questa probabilmente la richiesta del problema ... ******* */


        /* La funzione restituisce la posizione della prima occorrenza di "parola2" in "parola1", 
            se non trovato restituisce null   // ** trovata in rete: nella dispensa del prof. non se n'è parlato ...boh, c'è forse altro modo con cicli o funzioni conosciute?? */
/*

    if(strstr(parola1, parola2) != NULL)            
    {                                              
        cout << "La seconda parola e' contenuta nella prima parola \n";
    }
    else
    {
        cout << "La seconda parola non e' contenuta nella prima parola \n";
    }
    
   //string result = strstr(parola1, parola2);
    cout << "strstr risulta: " << strstr(parola1, parola2) <<endl;
    //cout << result <<endl;

*/



return 0;
}