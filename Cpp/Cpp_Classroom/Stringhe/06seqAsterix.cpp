#include <string>
#include <cstring>                           // dovrebbe sostituire <string.h> e permettere di usare le funzioni di C in C++      
#include<iostream>
using namespace std;

/* Bergamasco Daniele  02.01.20  - Stringhe - Esercizio n° 6

    Sostituisci sottostringa con Asterischi
    
    Si scriva un programma in linguaggio C che riceva in ingresso due parole inserite da tastiera. 
    Si consideri che ciascuna parola può contenere al massimo 30 caratteri. 
    Il programma deve sostituire ogni occorrenza della seconda parola nella prima parola con una sequenza di caratteri ’*’. 
    Ad esempio, inserite le parole abchdfffchdtlchd e chd, il programma deve visualizare la parola ab***fff***tl***.  */


#define CharMax 30                                  // direttiva per il preProcessore

int main()
{
    cout << " \n***** Sostituisci sottostringa con Asterischi ****** \n " << endl;                                                                                              

    char parola1[CharMax];                                                // dichiarazione 1^ parola, da inserire da tastiera
    char parola2[CharMax];                                                // dichiarazione 2^ parola, da inserire da tastiera

    cout << "Inserisci due parole\n " << endl;

    cin >> parola1;                                           // essendo una sola "parole" e' sufficiente anche il solo "cin"
    cin >> parola2;                                              

    int long1 = strlen(parola1);                             // calcolo la lunghezza delle due parole (strlen: funzione di C)
    int long2 = strlen(parola2);

    cout << "La prima parola: " << parola1 << " e' di " << long1 << " caratteri" <<endl;      // --> per es. -->   portafiori
    cout << "La seconda parola: " << parola2 << " e' di " << long2 << " caratteri" <<endl;    // --> per es. -->   fi 
    cout <<endl;

    

    for(int i=0; i<long1; i++)                                      // ciclo for: scorro parola 1
    {
        if(parola1[i] == parola2[0])         // cerco nella prima parola una lettera uguale all'iniziale della seconda parola
        {                                    // ipotizzando che potrebbe essere l'inizio della seconda parola
            for(int j=0; j<long2 && parola1[i+j] == parola2[j]; j++)
            parola1[i+j] = '*';              // se c'è continua sequenza di caratteri assegno a parola1 gli asterischi
        }
    }
    cout << "La nuova parola1 e' " << parola1 <<endl;

return 0;
}
