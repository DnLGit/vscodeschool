#include <iostream>
#include <string.h>                                     // Libreria per funzioni di C

using namespace std;

/*  Bergamasco Daniele  02.12.19  - Stringhe - Esercizio n° 2

    Sostituisci carattere

    Scrivere un programma in linguaggio C che legga una frase introdotta da tastiera.
    La frase è terminata dall’introduzione del carattere di invio e contiene complessivamente al più 100 caratteri.
    Il programma deve svolgere le seguenti operazioni:
    • visualizzare la frase inserita
    • costruire una nuova frase in cui tutte le occorrenze del carattere ’.’ sono sostituite con il carattere di ritorno di linea ’\n’.
    • visualizzare la nuova frase.  */

int main()
{
    cout << "\n *** Esercizio 2:  Cambia Carattere *** \n" << endl;

    const int dimax = 100;                                      // dimensione massima dell'array di caratteri
    char frase1[dimax];                                         // Array di caratteri
    cout << "\nInserisci una frase: ";                          // -> Anche oggi e' un Buongiorno <-

    cin.getline(frase1, dimax);                                 // cin(getline(): funzione di C++ per leggere array di caratteri con spazi (frase1)
    const int N1 = strlen(frase1);                              // strlen(): funzione di C per determinare la lunghezza dell'array
    cout <<"questa la prima frase:  "<<frase1<<endl;
    cout << "ed è di "<< N1 <<" caratteri\n";

    char frase2[dimax];                                         // Array di caratteri
    cout << "\nInserisci una seconda frase: ";                  // -> Eccomi. Ci vediamo domani. Sicuramente. A domani. <-

    cin.getline(frase2, dimax);                                 // cin(getline(): funzione di C++ per leggere array di caratteri con spazi (frase2)
    const int N2 = strlen(frase2);                              // strlen(): funzione di C per determinare la lunghezza dell'array
    cout <<"questa e' la seconda frase:  "<< frase2<<endl;
    cout << "ed è conposta di "<< N2 <<" caratteri";


    for(int i=0; i<N2; i++)                                    // Ciclo sostituzione carattere " punto " con " \n ".
        {
            if(frase2[i]=='.')
            {
                frase2[i]='\n';
            }
        }
    cout <<"\nquesta e' la seconda frase modificata:  \n "<< frase2;


    cout << "\n *** Esempio con le stringhe *** \n";

    string frase3;                                              // dichiarazione di una Stringa
    cout << " Inserisci la terza frase: ";                      // -> Totò disse: "E' la somma che fà U' totale!!". <-

    getline(cin, frase3);                                       // getline(cin, s): funzione di C++ per leggere stringhe con spazi (frase3)
    const int N3 = frase3.length();                             // s.length(): funzione di C++ per determinare la lunghezza della stringa
    cout <<"questa e' la terza frase:  "<< frase3<<endl;

    for(int i=0; i<dimax; i++)                                  // Ciclo sostituzione carattere " due punti " con "\n"
        {
            if(frase3[i]==':')
            {
                frase3[i]='\n';
            }
        }
    cout <<"\ne questa e' la terza frase modificata:  \n "<< frase3;
    cout << "\n ed è composta di "<< N3 <<" caratteri";

    return 0;
}
