#include<iostream>
#include<string>

using namespace std;

/*  Bergamasco Daniele 4As - 30.11.2019 >>>>>>>>>>>> Conta Vocali e Consonanti

    01. Scrivere un programma in linguaggio C che legga una frase introdotta da tastiera. 
        La frase è terminata dall’introduzione del carattere di invio. 
        La frase contiene sia caratteri maiuscoli che caratteri minuscoli, e complessivamente al più 100 caratteri. 
        Il programma dovrà stampare su schermo le seguenti informazioni: 
         • per ognuna delle lettere dell’alfabeto, il numero di volte che la lettera compare nella stringa 
         • il numero di consonanti presenti nella stringa 
         • il numero di vocali presenti nella stringa..  */

int main()
{
    cout <<"\n ***** Conta Lettere, Vocali e Consonanti ... con le Stringhe ***** \n";

    string frase;                       // Dichiarazione di una Stringa
    cout << "\nInserisci una frase:";
    getline(cin, frase);                // funzione per leggere la Stringa spazi compresi
    const int N = frase.length();       // funzione che determina la lunghezza di una Stringa

    cout << endl << "La frase inserita : \"" << frase << "\" \nha lunghezza EFFETTIVA di " << N << " caratteri\n" << endl;

    int countV = 0, countC = 0;                 // variabili contatori di Vocali e Consonanti 
    int alfaCountM[('Z'-'A'+1)];     // array contatore lettere alfabeto maiuscole (la dim è la differenza fra codici Ascii)
    int alfaCountm[('z'-'a'+1)];     // array contatore lettere alfabeto miniscole (la dim è la differenza fra codici Ascii)

    for(int i = 0; i < ('Z'-'A'+1); i++)        // Ciclo for per inizializzare a zero i contatori lettere 
    {                                           // (per sovrascrivere eventuali valori sconosciuti)
        alfaCountM[i] = 0;
        alfaCountm[i] = 0;
    }

    for(int i = 0; i < N; i++)                  // Ciclo for di ricerca lettere alfabeto all'interno della frase
    {
        if(frase[i] >= 'A' && frase[i] <= 'Z')  // se all'interno del range delle maiuscole ....
        {
            alfaCountM[frase[i]-'A']++;         // [frase[i]-'A'] -> e' la posizione nel contatore della lettera trovata 
                                                // (per esempio 'A'-'A' -> i=0)
            if(frase[i] == 'A' || frase[i] == 'E' || frase[i] == 'U' || frase[i] == 'O' || frase[i] == 'I') // se come le vocali M ...
            {
                countV++;                       // ... incrementa il contatore delle Vocali
            }
            else
            {
                countC++;                       // ... oppure incrementa il contatore delle Consonanti
            }
        }
        else if(frase[i] >= 'a' && frase[i] <= 'z') // ... altrimenti se all'interno del range delle minuscole ....
        {
            alfaCountm[frase[i]-'a']++;         // [frase[i]-'a'] -> e' la posizione nel contatore della lettera trovata ('c'-'a' -> i=2)
            if(frase[i] == 'a' || frase[i] == 'e' || frase[i] == 'u' || frase[i] == 'o' || frase[i] == 'i') // se come le vocali m ...
            {
                countV++;                       // ... incrementa il contatore delle Vocali
            }
            else
            {
                countC++;                       // ... oppure incrementa il contatore delle Consonanti
            } 
        }    
    }

    /*      Stampa finale dei risultati            */

    for(int i = 0; i < ('Z'-'A'+1); i++)        // Ciclo stampa occorrenze delle lettere nella frase
    {
        if(alfaCountM[i] > 0)                   // pero' non quelle non presenti (contatore a zero)
        {                                       
            cout << "La lettera " << (char)('A'+i) << " compare " << alfaCountM[i] <<" volte" << endl;
        }           // (char)('A'+i) --> casting per avere la stampa del carattere e non codice Ascii

        if(alfaCountm[i] > 0)
        {
            cout << "La lettera " << (char)('a'+i) << " compare " << alfaCountm[i] <<" volte" << endl;
        }
    }
    
    cout << endl << "Nella frase ci sono " << countV << " vocali e " << countC << " consonanti" << endl;

    return 0;
}