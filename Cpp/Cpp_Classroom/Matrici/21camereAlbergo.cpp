#include <iostream>
#include<string>
using namespace std;

/*  Bergamasco Daniele 4As - 02.01.2020 >>>>>>>>>>>> Esercizi con Funzioni e Matrici

    Esercizio n° 21 -  Gestione camere Albergo.
    Per gestire le camere di un albergo si hanno a disposizione gli array 
        CAMERE dove sono contenuti i numeri identificativi delle camere, 
        POSTI dove è contenuto il numero di posti (da 1 a 4) di ciascuna camera, e 
        LIBERO indica, per ogni camera, se è libera (=0) oppure occupata (=1). 
    Realizzare un'applicazione in grado di: 
        1) visualizzare la capienza totale dell'albergo (quante persone in tutto può ospitare) e quante persone sono ospitate nell'albergo; 
        2) visualizzare se la camera, il cui identificativo è inserito da input, è libera o occupata. Se la camera non esiste emettere la segnalazione di errore; 
        3) visualizzare il numero di camere da 1, da 2, da 3 e da 4 posti letto (sugg: utilizzare l'array NUMCAMERE di 4 componenti); 
        4) gestire una prenotazione: inserito in input il numero X di persone, cercare, 
            se esiste, una camera libera che possa ospitare tutte le persone. 
            Se esiste portare a 1 il relativo elemento dell'array LIBERO per indicare che ora la camera è occupata, 
            altrimenti dare una segnalazione di avviso   */

int main()
{
    cout << "\n******** 21 - Gestione camere Albergo ********\n\n";





return 0;

}