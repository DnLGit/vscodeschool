#include <iostream>
#include<string>
using namespace std;

/*  Bergamasco Daniele 4As - 02.12.2019 >>>>>>>>>>>> Esercizi con Funzioni e Matrici

    Esercizio n° 7 - Concorso di intelligenza 
        In un concorso di intelligenza, N giudici esprimono il loro giudizio su K candidati. 
        Il giudizio è un valore numerico tra 0 e 5. 
        Si scriva un programma in linguaggio C per determinare il candidato più intelligente, ed il giudice più severo. 
        Riempire manualmente le strutture con gli array bidimensionali */

int main()
{
    cout << "\n\n******** 7 - Concorso di intelligenza - soluzione 1 ********\n\n";

    int const G=3, K=5;                                     // dichiarazione e inizializzazione qta' Giudici e Kandidati
    int Jud[G][K]=  {{1, 2, 2, 4, 3},                       // dichiarazione e inizializzazione matrice con i voti
                     {4, 2, 5, 3, 1,},
                     {2, 2, 5, 3, 1,}};

    float somma, media, mediaLow, mediaHi;                  // dichiarazione variabili da calcolare
    int g, k;                                               // dichiarazione variabili per identificare giudici e kandidati

/*  Ciclo for per determinare la media piu' bassa e relativo giudice    */
    mediaLow=5;                                             // iniializzazione della "media piu' bassa" al valore massimo 
    for(int i=0; i<G; i++)                                  // scorrimento delle righe (Giudici) della matrice
    {   
        somma=0;                                            // iniializzazione a zero delle variabili da calcolare   
        media=0;
        cout <<i+1<<"^ Giudice: ";
        for(int j=0; j<K; j++)                              // scorrimento delle colonne/riga (voto Kandidati) della matrice
        {
            cout <<Jud[i][j]<<" ";                          // stampa righe della matrice (voti giudici + controllo media)
            somma+=Jud[i][j];                               // sommatoria dei voti di ogni riga della matrice
            media=somma/K;                                  // media dei voti delle righe della matrice
        }
        if(mediaLow>media)                                  // condizione per determinare la media piu' bassa
            {
                mediaLow=media;                             // aggiornamento variabile "media piu' bassa"
                g=i;                                        // salvataggio indice della riga dei giudici
            }
        cout <<"    (somma: "<<somma<<" media: "<<media<<" mediaLow ora e' -> "<<mediaLow<< ")\n";
    }
    cout <<endl;

/*  Ciclo for (invertito ordine matrice) per determinare la media piu' alta e relativo Kandidato    */
    mediaHi=0;                                              // iniializzazione della "media piu' alta" al valore minimo
    for(int i=0; i<K; i++)                                  // scorrimento delle colonne (Kandidati) della matrice
    {   
        somma=0;                                            // iniializzazione a zero delle variabili da calcolare
        media=0;
        cout <<i+1<<"^ Kandidato: ";
        for(int j=0; j<G; j++)                              // scorrimento delle righe (voto Giudici) della matrice
        {
            cout <<Jud[j][i]<<" ";                          // stampa colonne della matrice (invertito ordine righe/colonne)
            somma+=Jud[j][i];                               // sommatoria dei voti di ogni colonna della matrice
            media=somma/G;                                  // media dei voti delle colonne della matrice
        }
        if(mediaHi<media)                                   // condizione per determinare la media piu' alta
            {
                mediaHi=media;                              // aggiornamento variabile "media piu' alta"
                k=i;                                        // salvataggio indice della colonna dei kandidati
            }
        cout <<"    (somma -> "<<somma<<" media -> "<<media<<" mediaHi ora e' -> "<<mediaHi<< ")\n";
    }
    cout <<endl;

/*  Risultati finali    */
    cout <<"Con una media giudizio di "<<mediaLow<<" il giudice n^ "<<g+1<< " e' risultato il piu' severo "<<endl;
    cout <<"Con una media giudizio di "<<mediaHi<<" il candidato n^ "<<k+1<< " e' risultato il piu' intelligente "<<endl;

    return 0;
}