#include <iostream>
#include<string>

#include <string.h>

using namespace std;

/*  Bergamasco Daniele 4As - 02.12.2019 >>>>>>>>>>>> Esercizi con Funzioni e Matrici

    Esercizio n° 8 -  Statistiche Testo
        Si scriva un programma in C che acquisisca da tastiera un testo libero, composto da più righe (max 1000) di un numero di caratteri non superiore a 100 ciascuna. 
        L’inserimento termina quando l’utente inserirà una riga uguale a FINE. 
        Al termine dell’acquisizione del testo, il programma dovrà stampare le seguenti statistiche: 
        1. il numero totale di righe inserite;
        2. il numero totale di caratteri inseriti;
        3. il numero totale di caratteri alfanumerici inseriti;
        4. il numero totale di parole inserite. */

int main()
{
    cout <<"\n******** 8 - Statistiche Testo ********\n\n ";

    cout <<"Inserisci del testo libero:\n\n";

    int R = 1000;                           // dichiarazione variabile inizializzata RigheMax della matrice 
    int C = 100;                            // dichiarazione variabile inizializzata ColonneMax della matrice
    char Text[R][C];                        // dichiarazione matrice (array di ... array di caratteri)

    bool termine;                           // flag di fine testo
    int righe, caratteri, alphan, parole;   // contatori (alphan: corrisponde ai caratteri alfanumerici da trovare)

/*  Ciclo per inserire il testo nella matrice  */
    termine = false;                        // flag inizializzato a zero
    for(int i=0; i<R && termine!=true; i++)
    {
        cin.getline(Text[i], C);            // acquisizione del testo

        if(strcmp(Text[i], "FINE")==0)      // fino a quando la funzione strcmp ritorna zero trovando la parola FINE
            termine = true;                 // uscita dal ciclo //** ho sostituito break con un flag (funziona: e' ok cosi'?)
    }                                       

/*  Ciclo per stampare il testo della matrice  */
    cout << "\n\n Stampa Matrice:" << endl;

    termine = false;
    righe = 0; caratteri = 0;               // contatori inizializzati a zero
    for(int i=0; i<R && termine!=true; i++)
    {   
        int j;
        for(j=0; j<C && Text[i][j] != '\0'; j++)    // j<C ma solo fino "al tappo"
            cout << Text[i][j];

        righe=i;                            // ad ogni uscita dal for interno inizia una nuova riga
        caratteri+=j;                       // ed una nuova sommatoria di caratteri   
        cout << endl;
        if(strcmp(Text[i], "FINE")==0)      // la stampa termina all'incontro di "FINE"
            termine = true;                 ///** anche qui ho sostituito break con un flag ... 
    }

    int longF = strlen(Text[righe]);        // calcolo la lunghezza dell'ultima parola (FINE) da usare nei risultati
    Text[righe][longF+1] = '\0';            // e ci metto il tappo (... se mancante) ............ //** e' mancante? serve??


/*  Ciclo Ricerca caratteri alfanumerici  */
    alphan = 0;                             // contatore inizializzato a zero
    for(int i=0; i<=righe; i++)             // i<= perchè compresa la riga di FINE
    {
        for(int j=0; j<C && Text[i][j] !='\0'; j++)
        {
            if(isalnum(Text[i][j]))         // funzione per cercare i caratteri alfanumerici
            alphan++;                       // incremento caratteri alfanumerici trovati
        }
    }

/*  Ciclo Ricerca di tutte le parole  */
    parole = 0;                             // contatore inizializzato a zero
    for(int i=0; i<=righe; i++)
    {
        if(isalpha(Text[i][0]))             // cerco tutte le iniziali di ogni riga
            parole++;                       // incremento le prime parole trovate
        
        for(int j=0; j<C && Text[i][j] !='\0'; j++)
        {
            if(Text[i][j] == ' ' && isalpha(Text[i][j+1]))  // cerco le iniziali dopo gli spazi //** e' un problema j+1??
                parole++;                                   // incremento le restanti parole trovate
        }       
    }

    cout <<endl<<endl;
    cout << "ci sono " << righe+1 << " righe in questo testo" <<endl;                      // +1 perche' righe parte da zero!
    cout << "escludendo 'FINE' ci sono " << righe << " righe, in questo testo" <<endl<<endl;
    cout << "ci sono " << caratteri << " caratteri in questo testo" <<endl;
    cout << "escludendo 'FINE'ci sono " << caratteri-longF << " caratteri in questo testo" <<endl<<endl;
    cout << "ci sono " << alphan << " caratteri alfanumerici in questo testo" <<endl;
    cout << "ci sono " << parole << " parole in questo testo" <<endl; 

return 0;
}
