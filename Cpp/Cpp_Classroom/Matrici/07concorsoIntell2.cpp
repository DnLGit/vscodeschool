#include <iostream>
#include<string>
using namespace std;

/*  Bergamasco Daniele 4As - 02.12.2019 >>>>>>>>>>>> Esercizi con Funzioni e Matrici

    Esercizio n° 7 - Concorso di intelligenza
        In un concorso di intelligenza, N giudici esprimono il loro giudizio su K candidati. 
        Il giudizio è un valore numerico tra 0 e 5. 
        Si scriva un programma in linguaggio C per determinare il candidato più intelligente, ed il giudice più severo. 
        Riempire manualmente le strutture con gli array bidimensionali */

int main()
{
    cout << "\n******** 7 - Concorso di intelligenza - soluzione 2 ********\n\n";

    int const G=4, K=6;
    float Jud[G][K]={{1, 2, 2, 4, 3},{4, 2, 5, 3, 1,},{2, 2, 5, 3, 1,}};
 
    float somma, media, mediaLow, mediaHi;
    int g, k;

    mediaLow=5;
    for(int i=0; i<G-1; i++)
    {   
        somma=0;
        media=0;
        
        for(int j=0; j<K-1; j++)
        {
            cout <<Jud[i][j]<<" ";
            somma+=Jud[i][j];
            media=somma/(K-1);
            
        }
        if(mediaLow>media)
            {
                mediaLow=media;
                g=i;
            }

        Jud[i][K-1]=mediaLow;
        cout <<"    (somma -> "<<somma<<" media -> "<<media<<" e mediaLow ora e' -> "<<Jud[i][K-1]<< ")\n";
        cout <<endl;
        
    }
/*
    mediaHi=0;
    for(int i=0; i<K; i++)
    {   
        somma=0;
        media=0;
        
        for(int j=0; j<G; j++)
        {
            cout <<Jud[j][i];
            somma+=Jud[j][i];
            media=somma/G;
            
        }
        if(mediaHi<media)
            {
                mediaHi=media;
                k=i;
            }

        
        cout <<"    (somma -> "<<somma<<" media -> "<<media<<" mediaHi ora e' -> "<<mediaHi<< ")\n";
        cout <<endl;
        
    }

    cout <<"Con una media giudizio di "<<mediaLow<<" il giudice n^ "<<g+1<< " e' risultato il piu' severo "<<endl;
    cout <<"Con una media giudizio di "<<mediaHi<<" il candidato n^ "<<k+1<< " e' risultato il piu' intelligente "<<endl;

*/

    return 0;
}