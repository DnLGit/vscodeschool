#include <string>
#include <cstring>                            // dovrebbe sostituire <string.h> e permettere di usare le funzioni di C in C++
#include<iomanip>                                                                         // Libreria per la funzione setw(n)
#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 22.01.2020 >>>>>>>>>>>> Esercizi con Funzioni e Matrici

    Esercizio n° 10 -  Gestione Magazzino.
    Un’azienda deve tenere traccia dei beni presenti in un magazzino. 
    L’utente inserisce da tastiera dei “comandi” nel seguente formato: 
    bene EU quantità dove: 
        • bene è il nome di un bene; 
        • EU è la lettera ’E’ per entrata, ’U’ per uscita; 
        • quantità è la quantità di bene entrata o uscita. 
    L’utente termina il caricamento inserendo un comando pari a FINE. 
    In tal caso il programma deve stampare le quantità di beni presenti a magazzino...
    Esempio: 
        viti E 10 
        dadi E 50 
        viti U 5
        viti E 3 
        FINE    */

int main()
{
    cout << "\n******** 10 - Gestione Magazzino ********\n\n";

    cout << " ***************************\n ****** Menu' Magazzino ******\n ***************************\n";
    cout << " E) Carica materiale\n U) Scarica materiale\n 3) Ricerca approssimata per nome\n";
    cout << " 4) Ristampa completa rubrica\n 0) Esci dal programma\n\n";



return 0;

}