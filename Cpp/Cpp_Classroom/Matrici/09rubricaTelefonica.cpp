#include <string>
#include <cstring>                            // dovrebbe sostituire <string.h> e permettere di usare le funzioni di C in C++
#include<iomanip>                                                                         // Libreria per la funzione setw(n)
#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 02.01.2020 >>>>>>>>>>>> Esercizi con Funzioni e Matrici

    Esercizio n° 9 -  Rubrica Telefonica.
    Si realizzi un programma in linguaggio C in grado di gestire una rubrica di nomi e numeri telefonici. 
    La rubrica deve contenere fino a 100 voci diverse. 
    Ciascuna voce è composta da un nome (max 40 caratteri) e da un numero di telefono (max 20 caratteri). 
    Il programma deve fornire all’utente un menù di scelta, con le seguenti voci: 
    1) Aggiungi nuova voce in rubrica 
    2) Ricerca esatta per nome 
    3) Ricerca approssimata per nome 
    4) Stampa completa rubrica 
    0) Esci dal programma 
    Una volta che l’utente ha scelto l’operazione desiderata (1-4), 
    il programma acquisirà i dati necessari dall’utente ed eseguirà il comando. 
    Nota: nella rubrica non possono esistere due voci con lo stesso nome. */

#define vMax 100                                        // direttiva per il preProcessore: numero massimo di utenti (v=voci)
#define nMax 40                                       // direttiva per il preProcessore: lunghezza massima dei nomi (n=nomi)
#define tMax 20                                      // direttiva per il preProcessore: lunghezza massima dei numeri (t=tel)

// Prototitpi funzioni di ricerca precisa e approssimata
void ricercaContatto(char matrix1[][nMax], char matrix2[][tMax], char array[], int R);
void ricercaContattoApp(char matrix1[][nMax], char matrix2[][tMax], char array[], int R); 

int main()
{
    cout << "\n******** 9 - Rubrica Telefonica ********\n\n";

    char nomi[vMax][nMax] = {{"Carabinieri"},                   // dichiarazione e inizializzazione matrice dei nomi utenti
                                {"Polizia"},
                                {"Soccorso"},
                                {"Mare"}};
   
    char nTel[vMax][tMax] = {{"112"},                       // dichiarazione e inizializzazioni matrice dei numeri telefonici
                            {"113"},
                            {"118"},
                            {"1530"}};
    char nome[nMax];                                    // dichiarazione array nome utenti (da usare per la ricerca dei nomi)
    char nom[nMax];                                 // dichiarazione array nome utenti (da usare per la ricerca sottostringa)
    char numero[tMax];                              // dichiarazione array numero utente (da usare per la ricerca dei numeri)

    int longN;                                              // variabile in cui memorizzare la lunghezza del nome più lungo
    int longS;                                              // variabile in cui memorizzare la lunghezza della sottostringa
    int righe;                                                          // variabile per determinare il numero degli utenti

    int scelta;                                                                                    // variabile scelta menu' 

/*  Calcolo delle righe effettivamente occupate nella matrice */
    longN =0;                                                                   // inizializzazione a zero delle variabili
    righe=0;
    for(int i=0; i<vMax && strlen(nomi[i])>0; i++)
    {
        if(strlen(nomi[i]) > longN)                             // calcolo lunghezza degli array "nomi" (da usare con setw)
            longN = strlen(nomi[i]);                            // memorizzo il numero di caratteri del nome più lungo

        righe++;                                 // incremento valore righe della matrice (poteva essere anche "righe=i+1)")
    }

    cout << "Rubrica iniziale con "<< righe << " utenti" <<endl;
    cout << "il nome piu' lungo e' di " << longN << " caratteri!" <<endl<<endl;

    for(int i=0; i<righe; i++)  
    {
        cout <<setw(longN)<< nomi[i] <<" ";                 // setw(2): funzione che stabilisce quanti caratteri (longN) far 
        cout << nTel[i] <<endl;                                                             // occupare alla stampa dei nomi
    }
    cout <<endl;

    cout << " ***************************\n ****** Menu' Rubrica ******\n ***************************\n";
    cout << " 1) Aggiungi nuova voce\n 2) Ricerca esatta per nome\n 3) Ricerca approssimata per nome\n";
    cout << " 4) Ristampa completa rubrica\n 0) Esci dal programma\n\n";
    
    do 
    {
        cout << "Fai la tua scelta, inserisci il numero qui -> ";
        cin >> scelta;
        if(scelta <0 || scelta >4)
        cout << "Hai fatto una scelta sbagliata, riprova: \n";

        switch (scelta)
        {
            case 1:                                                             // prova aggiunta utente con relativo numero
                cout << "Aggiungo un nuovo utente: ";                           
                cin >> nome;                                                                        // es.: GuardiaDiFinanza
                if(strlen(nome)>longN)
                {
                    longN = (strlen(nome));                                     // memorizzo il numero di caratteri del nome)
                    cout << "Ora la parola piu' lunga e' di " << longN << " caratteri!" <<endl;        // se + lunga: stampo!
                }
                cout << "Aggiungo un nuovo numero: ";                                                             
                cin >> numero;                                                                                    // es.: 117

                strcpy(nomi[righe], nome);                       // memorizzo il nuovo nome e numero nell'ultima riga (vuota)
                strcpy(nTel[righe], numero);
                righe++;                                                                        // e aggiungo una nuova riga
                cout <<endl;
                break;
            
            case 2:
                cout <<"Cerca un nome: ";                                              // ricerca precisa nome (con funzione)
                cin >> nome;
                ricercaContatto(nomi, nTel, nome, righe);                          // chiamata della funzione ricerca precisa
                cout <<endl<<endl;
                break;
            
            case 3:                                                               // ricerca approssimata nome (con funzione)
                cout <<"Cerca un nome che inizia con: ";
                cin >> nom;
                ricercaContattoApp(nomi, nTel, nom, righe);                   // chiamata della funzione ricerca approssimata
                cout <<endl<<endl;
                break;
        
            case 4:                                                                                 //  ristampo le matrici
                cout << "ristampa matrici" <<endl<<endl;

                for(int i=0; i<righe; i++)
                {
                    cout <<setw(longN)<< nomi[i] <<" ";          // setw(2): funzione che stabilisce quanti caratteri (longN)
                    cout << nTel[i] <<endl;                                             // far occupare alla stampa dei nomi
                }
                cout <<endl;
                break;

            case 0:
                cout << "Hai scelto di uscire";
                break;        
    
            default:
                break;
        }
    } 
    while (scelta!=0);                                                    // condizione di uscita dal while: scelta = a zero.

    return 0;
}

/* ***************** Implementazione delle funzioni **************************** */


// Funzione ricerca di un nome preciso:

void ricercaContatto(char matrix1[][nMax], char matrix2[][tMax], char array[], int R)
{
    int trovato = -1;                                           // variabile dove memorizzare l'indice della stringa trovata
    for(int i=0; i<R && trovato == -1; i++)
    {
        if(strcmp(array, matrix1[i])==0)                                  // confronto nome cercato con i nomi della matrice
            trovato = i;                                                                    // se trovato memorizzo l'indice 
    }
    if(trovato > -1)                                                        // condizione per stampare risultato appropriato
        cout <<"Il nome " << array << " e' nella rubrica con numero " << matrix2[trovato];
    else
        cout <<"Il nome " << array << " NON e' nella rubrica!";
}



// Funzione ricerca di un nome approssimato:

void ricercaContattoApp(char matrix1[][nMax], char matrix2[][tMax], char array[], int R)
{   
    bool esiste = false;                       // flag che blocca il ciclo j appena trova la prima volta la sottostringa
    for(int i=0; i<R; i++)
    {
        for(int j=0; j<strlen(matrix1[i]) && !esiste; j++)
        {
            if(array[0] == matrix1[i][j])                  // se l'iniziale e' uguale ad una lettera di un nome della matrice
            {
                int k;                            // faccio un altro for per verificare se lo sono anche le successive [j+k] 
                for(k=1; k<strlen(array) && array[k] == matrix1[i][j+k] ; k++)
                {
                    
                }
                if(k == strlen(array))                   // se l'indice k e' uguale alla lunghezza della sottostringa cercata
                {
                    cout <<"la ricerca di " << array << " e' relativo a " << matrix1[i] 
                    <<" e il suo numero è " << matrix2[i] <<endl;
                    esiste = true;                                      // il nome e' stato trovato, stampo ed esco dal ciclo
                }
            }
        }
    }if (!esiste)                                                        // in caso contrario stampo messaggio di non trovato
        cout <<"Il nome " << array << " NON fa parte della rubrica!";
}



