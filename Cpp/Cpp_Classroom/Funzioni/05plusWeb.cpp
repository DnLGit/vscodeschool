/*
CLAUDIO CARIDNALE 3AI
Dato un elenco di prodotti con descrizione e prezzo, tramite una funzione viene calcolato l'incremento del prezzo  secondo una percentuale fornita da tastiera. 
Comunicare per ciascun prodotto la descrizione  e il prezzo incrementato.
*/
#include<iostream>
#include<stdio.h>
#define MAX 1000
#define LEN 100
float incremento(float percentuale, int prezzo);
char descrizione[MAX][LEN];
float prodotti[MAX];

int main(){
	//dichiarazioni
	
	int l,i;
	float perc;
	
	
	//acquisizione dati
	
	//numero prodotti
	
	do{
		printf("inserire il numero di prodotti : ");
		scanf("%d",&l);
	}while(l<1 || l>=MAX);
	
	//percentuale
	
	printf("inserire la percentale : ");
	scanf("%f",&perc);
	
	//caricamento vettori
	
	for(i=0;i<l;i++){
		printf("inserire la descrizione del prodotto, %d : ",i+1);
		scanf("%s",descrizione[i]);
		printf("inserire il prezzo del prodotto, %s : ",descrizione[i]);
		scanf("%f",&prodotti[i]);		
	}
	
	
	//chiamate di funzioni e visualizzazione risultati
	
	printf("prezzi incrementati :\n");
	//funzione incremento
	for(i=0;i<l;i++){
		prodotti[i] += incremento(perc,prodotti[i]);
		printf("%s = %f\n",descrizione[i],prodotti[i]);
	}
}


//funzioni

//funzione incremento
float incremento(float percentuale, int prezzo){
	return prezzo/100.0*percentuale;
}