#include <iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 01.12.2019 >>>>>>>>>>>> Esercizi con le Funzioni:  Calcolo Età.

    04. Scrivere la funzione che restituisca l'età di una persona, consocendo il suo anno di nascita. 
        (si consideri l'anno attua le come costante).. */


int age(int, int);                                                 // Dichiarazione prototipo funzione calcolo media

int main()
{
    int const year = 2019;                                          // costante dell'anno attuale
    int born;
   
    cout << "\n ******* Calcolo dell'eta' ******* \n";
        
    do
    {
        cout << "\n dimmi l'anno di nascita e ti dirò l'eta', inserisci qui l'anno -> ";    // Ciclo Do While di controllo
        cin >>born;
        if(born <1880 || born >2019)                                // anno di nascita non valido se supera un certo range
        {
            cout << "\n Attenzione, Inserita data non valida, riprova\n";
        }
    
    } while (born <1880 || born >2019);
 
    cout <<"\nse sei nato nel "<<born<< " hai "<<age(year, born)<<" anni";

    return 0;
}

/* ***************** Implementazione delle funzioni **************************** */

int age(int a, int b)               // Dichiarazione funzione e parametri
{                                 
      return (a-b);                 // Ritorno della funzione (differenza anni)                              
}   