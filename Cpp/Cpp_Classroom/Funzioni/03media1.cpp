#include <iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 01.12.2019 >>>>>>>>>>>> Esercizi con le Funzioni:  Calcolo Media.

    03. Scrivere una funzione che abbia come parametri 
        il numero dei valori e la loro somma e che calcoli la media aritmetica.. */

float Md(int, int[]);                                                 // Dichiarazione prototipo funzione calcolo media

int main()
{
    int N;
    int somma;
   
    cout << "\n ******* Calcolo della media ******* \n";

    cout << "\n decidi quanti valori considerare, inserisci qui la quantità -> ";
    cin >>N;
    int valori[N];


    cout <<"\nInserisci i valori da cui calcolare la media\n\n";
   
    for(int i=0; i<N; i++)                                                  // Ciclo for di riempimento array con i valori
    {
        cout <<i+1<<"^ valore: ";
        cin>>valori[i];
    }

    cout <<"\nquesta e' la media aritmetica dei valori: " <<Md(N, valori);  // chiamata della funzione
   

    return 0;
}

/* ***************** Implementazione delle funzioni **************************** */

float Md(int dim, int array[])          // Dichiarazione funzione e parametri
{
    float somma=array[0];             // variabile in cui memorizzare i valori                                 
    for(int i=0; i<dim; i++)
    somma+=array[i];
    return somma/dim;                 // Ritorno della funzione (somma valori diviso qtà valori)                              
}   