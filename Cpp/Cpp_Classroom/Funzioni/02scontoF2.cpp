#include <iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 30.11.2019 >>>>>>>>>>>> Esercizi con le Funzioni:  Prezzo scontato "decimale".

    02. Scrivere una funzione che abbia come parametri 
        il prezzo di vendita e la percentuale di sconto e che restituisca il prezzo scontato.. */

    float Sc (float, float);                                                 // Dichiarazione prototipo funzione prezzo scontato

int main()
{
    float listino;
    float sconto;
    cout << "\n ******* Calcolo dello sconto >>> con decimali ******* \n";
    cout <<"\nInserisci il prezzo di listino ->  ";
    cin >>listino;
    cout << "Ora la percentuale di sconto ->  ";
    cin >> sconto;

    //area=Area(lato);
    cout <<"Il prezzo di listino di " <<listino<< " €, scontato del ";
    cout <<sconto<< "% e' pari a "<<Sc(listino, sconto) <<"€"<<endl;    // Invocazione e stampa della funzione

    return 0;
}

/* ***************** Implementazione delle funzioni **************************** */

float Sc(float a, float b){                             // Dichiarazione funzione e parametri
    return a-(a*b/100);                                 // Ritorno della funzione (listino meno percentuale di sconto)
}