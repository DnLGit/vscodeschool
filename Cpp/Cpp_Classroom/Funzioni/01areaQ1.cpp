#include <iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 26.11.2019 >>>>>>>>>>>> Esercizi con le Funzioni: Area quadrato

    01. Organizzare una funzione che acquisisca da tastiera il lato di un quadrato e ne calcoli la misura dell'area e del perimetro. */

                                     

int main()
{int Area (int);// Dichiarazione prototipo funzione area quadrato
    int lato;
    cout << "\nCalcoliamo l'area di un quadrato\n\n";
    cout <<"Dammi il valore del lato ->  ";
    cin >>lato;

    //area=Area(lato);
    cout <<"L'area del quadrato di lato "<<lato<< " e' pari a "<<Area(lato)<<endl;  // Invocazione e stampa della funzione

    return 0;
}

/* ***************** Implementazione delle funzioni **************************** */

int Area(int a){                        // E' sufficiente un parametro solamente, il lato di un quadrato è uguale per tutti
    return a*a;                         // Ritorno della funzione (lato per lato ... in questo caso)
}