#include <iostream>
#include<string>
using namespace std;

/*  Bergamasco Daniele 4As - 02.12.2019 >>>>>>>>>>>> Esercizi con le Funzioni:  Calcolo Aumento prezzo.

    04. Dato un elenco di prodotti con descrizione e prezzo, tramite una funzione viene calcolato l'incremento del prezzo 
        secondo una percentuale fornita da tastiera. Comunicare per ciascun prodotto la descrizione e il prezzo incrementato. */

float listinoPlus(int, int);                                // Dichiarazione prototipo funzione prezzo tassato

int main()
{
    cout << "\n ***** Calcolo Aumento prezzo ***** \n";
    int qta, tax;

    cout << "\n Inserisci la quantità dei prodotti: ";
    cin >> qta;
    cout << " Inserisci ora la tassa da imporre: ";
    cin >> tax;
     
    string prodotti[qta];                                     // array di stringhe per nomi prodotti
    int listino[qta];                                         // array di interi per prezzi listino prodotti

    cout <<"\nInserisci ora in sequenza la lista prodotti e prezzo di listino netto\n";
    for(int i=0; i<qta; i++)                                  // riempimento dei due array (prodotti e listino)
    {
        cout << "Il prodotto n^ "<< i+1 <<" -> ";
        cin >> prodotti [i];
        cout << "listino -> ";
        cin >> listino[i];
    }
    cout << "\nApplicando la tassa del "<< tax << "%, i prezzi di vendita sono di: \n";
    for(int i=0; i<qta; i++)                                      // ciclo for per stampa nuovo listino
    {
        cout << listinoPlus(listino[i], tax) << " € per "<< prodotti[i] << "\n";
    }


    return 0;
}

/* ***************** Implementazione delle funzioni **************************** */

float listinoPlus(int a, int b)
{
    return a+(a*b/100);                                 // Dichiarazione funzione prezzo tassato
}
