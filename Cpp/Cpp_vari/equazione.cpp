// Example program
#include <iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 15.10.2019

    8. Data l'equazione ax + b = 0
    con "a" e "b" inseriti da tastiera, scrivere un programma in linguaggio C per determinare il valore di "x", se esiste, che risolve l'equazione. */

int main()
{
  float a, b;
    
  cout <<"Inserisci qui sotto i valori di a e b"<<endl;
  cout <<"inserisci a: ";
  cin >> a;
  cout <<"inserisci b: ";
  cin >> b;
  
  float x=-b/a;
  
  if(a==0)
  {
      cout <<"l'equazione risulta impossibile"<<endl;
  }
  else
  {
      cout <<"Il valore di x e': " << x <<endl;
  }
  
  return 0;
}