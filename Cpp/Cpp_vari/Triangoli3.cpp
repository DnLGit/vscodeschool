#include <iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 10.10.2019

    7. Si scriva un programma in linguaggio C che legga da tastiera i valori delle lunghezze dei tre lati di un triangolo (detti A, B e C), e determini:
    • se il triangolo è equilatero
    • se il triangolo è isoscele
    • se il triangolo è scaleno */

// Versione con Operatore Ternario (risultati con stringhe)

int main()
{
    int base, altezza, ipo;
    cout<<"Inserisci i valore dei lati di un triangolo"<<endl;
    cout<<"Inserisci la base: ";
    cin>> base;
    cout<<"Inserisci l'altezza: ";
    cin>> altezza;
    cout<<"Inserisci l'ipotenusa: ";
    cin>> ipo;
    
    string triangolo;
    
    triangolo = (base==altezza && base==ipo) ? "equilatero"  : (base==altezza) ? "isoscele" : "scaleno";  // Operatore ternario con 3 soluzioni

    cout << "Il triangolo e': " <<  triangolo <<endl;
   
    return 0;
}