#include<iostream>
#include<iomanip>                           /* Libreria per la funzione setw(n)*/
using namespace std;

/*  Bergamasco Daniele 4As - 07.01.2020 >>>>>>>>>>>> Esercizi con Funzioni e Matrici

    Esercizio libero in Laboratorio C (bunker VI ora) -  Calcolo diagonali.
    Realizzare un programma che calcoli la somma di ognuna delle due diagonali di una matrice quadrata. */


int main()
{
    cout << "\n*************** Calcolo diagonali di una matrice 4x4 *******************\n";

    int R=4;                                  // dichiarazione di righe e colonne
    int diag1, diag2;                         // dichiarazione variabili somma delle diagonali primo esempio
    int diag3, diag4;                         // dichiarazione variabili somma delle diagonali secondo esempio
    int diag5, diag6;                         // dichiarazione variabili somma delle diagonali terzo esempio
    
    int matrix[R][R]={{1,2,3,14},              // dichiarazione e inizializzazione matrice 4x4
                      {5,6,7,8},
                      {9,10,11,12},
                      {13,14,15,16}};

/*  stampa matrice  */
    cout << "\nStampa della matrice 4x4:\n";
    for(int i=0; i<R; i++)
    {
        for(int j=0; j<R; j++)
        cout <<setw(2) << matrix[i][j] <<" ";   // funzione "setw()": per incolonnare meglio le cifre singole con le doppie
        cout <<endl;
    }

    cout << "\nCalcolo delle diagonali ( Primo esempio):";
/*  calcolo prima diagonale */    
    diag1=0;                                  // inizializzazione a zero variabile da utilizzare per la sommatoria 
    for(int i=0; i<R; i++)                    // ciclo che scorre le righe della matrice
    {
        diag1+=matrix[i][i];                  // e somma ad ogni ciclo il valore trovato in [i][i]     
    }
    
    cout <<endl;
    cout <<"La diagonale 1  vale: "<<diag1<<endl;

/*  calcolo seconda diagonale */
    diag2=0;                                  // inizializzazione a zero variabile da utilizzare per la sommatoria
    for(int i=0; i<R; i++)                    // ciclo che scorre le righe della matrice
    {
        diag2+=matrix[(R-i)-1][i];                  // e somma ad ogni ciclo il valore trovato in [(R-i)-1][i]     
    }                                               // es.: al primo ciclo [(R-i)-1] vale (4-0)-1 = 3 ... poi 2 ...
    
    cout <<"La diagonale 2  vale: "<<diag2<<endl;
    

/*  Altro modo: unico for per entrambi le diagonali e con 2 indici   */
    cout << "\nCalcolo delle diagonali ( Secondo esempio):";

    diag3=0, diag4=0;                          // inizializzazione a zero variabili da utilizzare per le sommatorie 
    for(int i=0, j=R-1; i<R, j>=0; i++, j--)   // ciclo che scorre le righe della matrice in entrambi i versi
    {
        diag3+=matrix[i][i];                   // e somma ad ogni ciclo il valore trovato in [i][i]
        diag4+=matrix[j][i];                   // e somma ad ogni ciclo il valore trovato in [j][i]     
    }
    cout <<endl;
    cout << "Con unico ciclo for e due indici: " <<endl;
    cout << "La diagonale 3  vale " << diag3 << ", come diagonale 1." <<endl;
    cout << "La diagonale 4  vale " << diag4 << ", come diagonale 2." <<endl;


/*  Alternativa: unico for per entrambi le diagonali e unico indice   */
    cout << "\nCalcolo delle diagonali ( Terzo esempio):";

    diag5=0, diag6=0;                          // inizializzazione a zero variabili da utilizzare per le sommatorie 
    for(int i=0; i<R; i++)                     // ciclo che scorre le righe della matrice
    {
        diag5+=matrix[i][i];                   // e somma ad ogni ciclo il valore trovato in [i][i]
        diag6+=matrix[(R-i)-1][i];             // e somma ad ogni ciclo il valore trovato in [(R-i)-1][i]     
    }
    cout <<endl;
    cout << "Con unico ciclo for e unico indice: " <<endl;
    cout << "La diagonale 5  vale " << diag5 << ", come diagonale 1 e 3." <<endl;
    cout << "La diagonale 6  vale " << diag6 << ", come diagonale 2 e 4." <<endl;
    
    return 0;
}