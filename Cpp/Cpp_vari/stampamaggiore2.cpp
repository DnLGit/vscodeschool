#include<iostream>
using namespace std;

/* Bergamasco Daniele 4As - 16.10.2019

11. Si scriva un programma in linguaggio C che acquisisca tre numeri interi da tastiera e: 
• determini, stampando un messaggio opportuno quale dei tre numeri (il primo, il secondo o il terzo) sia maggiore 
• stampi il valore di tale numero.  Si trascuri il caso in cui i numeri siano uguali  
    -->    Versione con If/Else */

int main()
{
    int num1, num2, num3;
    cout <<"Inserisci tre numeri,\nqui il primo: ";
    cin >>num1;
    cout <<"qui il secondo numero: ";
    cin >> num2;
    cout <<"e qui il terzo numero: ";
    cin >> num3;

    if(num1==num2 && num2==num3)  // aggiunto lo stesso questa istruzione in caso di uguaglianza dei tre numeri
    {
        cout <<"i tre numeri che hai inserito, ("<< num1 <<") sono tutti uguali"<<endl;
    }
    else
    {
        if(num1>num2 && num2>num3)
        {
            cout <<"il primo numero che hai inserito, ("<< num1 <<") e' maggiore degli altri ("<<num2<<") e ("<<num3<<")"<<endl;
        }
        else
        {
            if(num1<num2 && num2>num3)
            {
                cout <<"il secondo numero che hai inserito, ("<< num2 <<") e' maggiore degli altri ("<<num1<<") e ("<<num3<<")"<<endl;
            }
            else
            {
                cout <<"il terzo numero che hai inserito, ("<< num3 <<") e' maggiore degli altri ("<<num1<<") e ("<<num2<<")"<<endl;
            }
        }
    }
    return 0;
}