#include<iostream>
using namespace std;

/* 03.10.2019 (Home) - Bergamasco Daniele   cl.: IV As

4)
Dato un numero intero tra 1 e 12, che rappresenta il mese corrente,
stampare il nome del mese per esteso ("Gennaio" ... "Dicembre") usando il costruttore switch. */

int main (){

    int mesi;
    cout << "Inserisci il numero di un mese del calendario Gregoriano \n";
    cin >> mesi;

    if(mesi>0 && mesi<13) // Aggiunto costruttore If: Condizione per stare all'interno dei 12 mesi
    {
        switch (mesi)
        {
        case 1:
            cout <<"Il numero "<< mesi <<" corrisponde al mese di Gennaio" <<endl;
            break;
      
        case 2:
            cout <<"Il numero "<< mesi <<" corrisponde al mese di Febbraio" <<endl;
            break;
        
        case 3:
            cout <<"Il numero "<< mesi <<" corrisponde al mese di Marzo" <<endl;
            break;
        
        case 4:
            cout <<"Il numero "<< mesi <<" corrisponde al mese di Aprile" <<endl;
            break;

        case 5:
            cout <<"Il numero "<< mesi <<" corrisponde al mese di Maggio" <<endl;
            break;

        case 6:
            cout <<"Il numero "<< mesi <<" corrisponde al mese di Giugno" <<endl;
            break;

        case 7:
            cout <<"Il numero "<< mesi <<" corrisponde al mese di Luglio" <<endl;
            break;

        case 8:
            cout <<"Il numero "<< mesi <<" corrisponde al mese di Agosto" <<endl;
            break;
        
        case 9:
            cout <<"Il numero "<< mesi <<" corrisponde al mese di Settembre" <<endl;
            break;
        
        case 10:
            cout <<"Il numero "<< mesi <<" corrisponde al mese di Ottobre" <<endl;
            break;
        
        case 11:
            cout <<"Il numero "<< mesi <<" corrisponde al mese di Novembre" <<endl;
            break;
        
        case 12:
            cout <<"Il numero "<< mesi <<" corrisponde al mese di Dicembre" <<endl;
            break;

        default:    // con l'IF all'inizio probabilmente questo "default" non serve a nulla!!!
            cout <<"Ops, qualcosa non va\n\n"; 
            break;
        }
    }

    else    // Interviene nel caso in cui il numero inserito esca dal range 1-12
    {
        cout << "Attenzione, il mese che hai inserito non appartiene al calendario Gregoriano " <<endl;
    }
        
return 0;
}