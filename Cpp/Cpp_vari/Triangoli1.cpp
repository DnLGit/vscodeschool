#include <iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 09.10.2019

    7. Si scriva un programma in linguaggio C che legga da tastiera i valori delle lunghezze dei tre lati di un triangolo (detti A, B e C), e determini:
    • se il triangolo è equilatero
    • se il triangolo è isoscele
    • se il triangolo è scaleno */

// Versione con If/else

int main()
{
  int base, altezza, ipo;
  cout<<"Inserisci i valore dei lati di un triangolo"<<endl;
  cout<<"Inserisci la base: ";
  cin>> base;
  cout<<"Inserisci l'altezza: ";
  cin>> altezza;
  cout<<"Inserisci l'ipotenusa: ";
  cin>> ipo;
  
  
  if(base==altezza && base==ipo)
  {
      cout<<"il triangolo e' equilatero"<<endl;
  }
  else
  {
      if(base==altezza)
      {
          cout<<"il triangolo e' isoscele"<<endl;          
      }
      
      else
      {
          cout<<"il triangolo e' scaleno"<<endl;
      }
  }
  
  return 0;
}