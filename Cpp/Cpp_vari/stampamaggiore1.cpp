#include<iostream>
using namespace std;

/* Bergamasco Daniele 4As - 16.10.2019

10.(per chi sente insicuro). 
Si scriva un programma in linguaggio C che acquisisca due numeri interi da tastiera e: 
• determini, stampando un messaggio opportuno quale dei due numeri (il primo o il secondo) sia maggiore 
• stampi il valore di tale numero.  */

int main()
{
    int num1, num2;
    cout <<"Inserisci due numeri,\nqui il primo: ";
    cin >>num1;
    cout <<"e qui il secondo numero: ";
    cin >> num2;

    if(num1==num2)  // aggiunto questa istruzione in caso di uguaglianza
    {
        cout <<"i due numeri che hai inserito, ("<< num1 <<") sono uguali"<<endl;
    }
    else
    {
        if(num1>num2)
        {
            cout <<"il primo numero che hai inserito, ("<< num1 <<") e' maggiore del secondo ("<<num2<<")"<<endl;
        }
        else
        {
            cout <<"il primo numero che hai inserito, ("<< num1 <<") e' minore del secondo ("<<num2<<")"<<endl;
        }
        
    }
    return 0;
}