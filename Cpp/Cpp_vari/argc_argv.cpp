#include <iostream>
using namespace std;

int main(int argc, char* argv[]) //argc = arguments counter; argv = arguments vector (IPOTESI Devis da verficare)
{
    //argc è un intero rappresentante la lunghezza di argv che è un vettore di stringhe
    cout << "Invocato il programma con " << argc << " parametri in ingresso"<< endl;

    //argv contiene stringa per stringa i parametri d'invocazione, dove in argv[0] ci sarà SEMPRE il nome del programma invocato
    for (int i=0; i < argc; i++)
        cout << "argv[" << i << "]: " << argv[i] << endl;

    return 0;
}