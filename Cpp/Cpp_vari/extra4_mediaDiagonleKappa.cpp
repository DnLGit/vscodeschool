#include<iostream>
#include<iomanip>                           /* Libreria per la funzione setw(n)*/
using namespace std;

/*  Bergamasco Daniele 4As - 15.01.2020 >>>>>>>>>>>> Esercizi con Funzioni e Matrici

    Esercizio libero in Laboratorio C (bunker III ora) -  Calcolo media diagonale.
    Realizzare un programma che calcoli la media dei valori della prima diagonale 
    sotto quella centrale di una matrice quadrata 4x4. */


    //#define N 4
    int const N = 4;
    float mediaDiagonaleK(int M[][N], int N, int k, bool top);

int main()
{   
    cout << "\n********* Calcolo media valori diagonale 'kappa' di una matrice 4x4 ***********\n";
    
    bool top;                                                       // booleana per determinare la posizione della diagonale
    int k;                                                          // variabile per modifica indice diagonale
    int matrix[N][N] = {{3,5,7,9}, {10,12,13,1},                    // matrice di 4 righe e 4 colonne
                        {-5,7,8,9}, {6,7,11,12}};

    // stampa matrice
    cout << "\nstampa matrice 4x4:\n";
    for(int i=0; i<N; i++)
    {   
        for(int j=0; j<N; j++)
        {
            cout << setw(2) << matrix[i][j] <<" ";                  // stampo matrice ordinata con setw()
        }cout <<endl;
    }
    cout <<endl;

    // scelta della diagonale da parte dell'utente
    cout << "scegli la diagonale indicando l'indice (ricorda che 0 = diagonale centrale)\n";
    cout << "inserisci qui il valore da 0 a " << N-1 <<" -> ";
    cin >> k;
    cout <<endl;

    if(k!=0)
    {
        cout << "scegli ora la posizione della diagonale, \n";
        cout << "inserisci qui, sopra (1) o sotto (0) -> ";
        cin >> top;
    }
    cout <<endl;
    float resultKappa = mediaDiagonaleK(matrix, N, k, top);      // salvo risultato della funzione invocata in una variabile 
    cout << "La media della diagonale scelta e': "<< resultKappa;                         // stampa risultato della funzione

    return 0;
}

/* ***************** Implementazione delle funzioni **************************** */

float mediaDiagonaleK(int M[][N], int N, int k, bool top)
{
    float media =0.0;                                               // variabile float dove memorizzare valori di media
    for(int i=0; i<N-k; i++)
    {   
        if(top)                                                     // se la scelta e' per la diagonale sopra (top)
        {
            media+=M[i][i+k];
        }
        else
        {
            media+=M[i+k][i];                                       // altrimenti e' per la diagonale sotto (!= top)
        }
    }
    return media/(N-k);
}