#include <iostream>
#include <string>
using namespace std;

/* Esercizi sulle stringhe da:   https://www.liceotosi.edu.it/inote/2017/02/19/teoria-stringhe-in-c/ 

Una stringa è semplicemente un array di caratteri, ovvero una sequenza di caratteri terminante con uno zero binario (carattere terminatore della stringa: \0).

La gestione delle stringhe in C++ è facilitato da una serie di funzioni richiamabili dalla libreria string.

Questo tipo di dato consente la semplificazione della gestione dei dati alfanumerici.

Vediamo quali sono le tipiche operazioni che si possono fare su una stringa:

#include <string>: libreria da richiamare per utilizzare le stringhe
string stringa: dichiarazione di una stringa
string stringa=”Daniele Corti”: dichiarazione e inizializzazione di una stringa
string stringa=”Daniele\nCorti”: multiriga
length(): restituisce la lunghezza della stringa
if(nomeStringa.empty()): restituisce TRUE se la stringa è vuota, altrimenti FALSE
stringa3=stringa1 + stringa2: concatenazione di stringhe con l’operatore (+)
if(stringa1 ==stringa2): restituisce TRUE se le due stringhe sono uguali
cout<<stringa1[i]: accesso alla i-esimo carattere della stringa
insert(n, stringa1): inserimento della stringa1 nella n-esima posizione della stringa2
substr(start, numCaratteri): estrazione da stringa1 di nCaratteri a partire da start carattere
erase(start, numCaratteri): cancellazione di numCaratteri a partire da start caratteri
replace(start, numCaratteri, stringa1): a partire da start carattere inserisco numCaratteri di stringa1 in stringa2
find(StringaDaCercare, start): restituisce la posizione del primo carattere della StringaDaCercare in stringa a partire da start esimo carattere.
cin>>stringa1: input stringa senza spazi
getline(stringa1, numeroCaratteri): input stringa con spazi
*/


int main(int argc, char *argv[])
{
	string s1, s2="Ciao", s3="Daniele", s4;
	cout<<s2<<endl;
	s2="Daniele\nCorti"; //multiriga
	cout<<s2<<endl;
	//restituisce la lunghezza della stringa
	int len=s2.length();
	cout<<"La stringa ha "<<len<<" caratteri (compresi gli spazi)"<<endl;
	if(s1.empty()) //verifico se la stringa è vuota
	cout<<"Lettura fallita!"<<endl;	
	//concateno le stringhe con l'operatore +
	s4=s2 + " " + s3;
	cout<<s4<<endl;
	s4=s2 + " " + s3 + '\n'; //e vado a capo
	cout<<s4;
	//confronto fra stringhe
	if(s2==s3)
	    cout<<"Stringhe uguali\n";
      else
        cout<<"Stringhe diverse\n";
   	//accesso agli elementi della stringa tramite l'operatore []
   	//stampo il primo carattere della stringa s2
    cout<<s2[0]<<endl;	  
    //inserire una stringa in un'altra
    string sa="Vittorio Secondo";
    string sb="Emanuele ";
    sa.insert(9,sb); //sa diviene "Vittorio Emanuele Secondo"
    cout<<sa<<endl;
    //estrazione di una sottostringa
    string frase="Daniele Corti";
    cout<<frase.substr(8,5)<<endl; //Corti
    //estrae dalla frase la parola Corti (inizio, quanti)
    //cancellazione di una sottostringa
    string frase2="Cara amica ti scrivo\n";
    frase2.erase(5,6); 
	cout<<frase2; // "caro ti scrivo"
    //cancello e sostituisco
    frase2.replace(0,4,"Anna");
    cout<<frase2<<endl; //Anna ti scrivo
    //ricerca sottostringa
    string nome="Maria Silvia";
	int startPos=nome.find("Silvia", 0);
    cout<<startPos<<endl;
    system("PAUSE");
    return EXIT_SUCCESS;
}