#include<iostream>
#include<cmath>
using namespace std;

/*  Bergamasco Daniele 4As - 16.10.2019

9.  Si scriva un programma in linguaggio C che implementi una semplice calcolatrice in grado di compiere le 5 operazioni (+ - × ÷ %) tra numeri interi. 
    Il programma presenti un semplice menù da cui l’utente indichi (con un numero tra 1 e 5) l’operazione da svolgere. 
    In seguito il programma acquisirà da tastiera i due operandi e stamperà il risultato dell’operazione.   */

int main()
{
    int x, y;
    float z;
    int operatore;
    char gioco = 's';
    cout <<"Vuoi giocare a far conti??? (s/n)"<<endl;
    cout << "inserisci qui la risposta: "; 
    cin >> gioco;

    while (gioco!='n')
    {
        cout <<"Scegli un operazione da fare tra\n somma (1)\n sottrazione (2)\n prodotto (3)\n divisione (4) o modulo (5)\n";
        cout <<"Inserisci qui la cifra corrispondente: ";
        cin >> operatore;

    
        if(operatore >0 && operatore <6)
        {
        cout <<"Inserisci ora i due operandi x e y\n";
        cout <<"x = ";
        cin >> x;
        cout <<"y = ";
        cin >> y;

        switch (operatore)
        {
        case 1:
            z=x+y;
            cout <<"hai scelto la somma, il risultato di "<<x<<" + "<<y<<" e': "<<z<<endl;
            cout <<"Continui?? (s/n)";
            cin >> gioco;
            break;

        case 2:
            z=x-y;
            cout <<"hai scelto la sottrazione, il risultato di "<<x<<" - "<<y<<" e': "<<z<<endl;
            cout <<"Continui?? (s/n)";
            cin >> gioco;
            break;
        
        case 3:
            z=x*y;
            cout <<"hai scelto il prodotto, il risultato di "<<x<<" x "<<y<<" e': "<<z<<endl;
            cout <<"Continui?? (s/n)";
            cin >> gioco;
            break;

        case 4:
            z=x/y;
            cout <<"hai scelto la divisione, il risultato di "<<x<<" : "<<y<<" e': "<<z<<endl;
            cout <<"Continui?? (s/n)";
            cin >> gioco;
            break;

        case 5:
            z=x%y;      // per questa operazione è stato necessario inserire la libreria "cmath"
                        // nota:  perchè il modulo non mi permette di usare il tipo float????
            cout <<"hai scelto di calcolare il modulo, il resto di "<<x<<" : "<<y<<" e': "<<z<<endl;
            cout <<"Continui?? (s/n)";
            cin >> gioco;
            break;

        default:
            break;
        }
        
        }
        else
        {
            cout<<"Attenzione, hai fatto una scelta sbagliata"<<endl;
            cout <<"Vuoi ritentare???\n se sì clicca s, altrimenti esci con n";
        }
    }
       
    return 0;
}