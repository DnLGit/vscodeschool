#include <iostream>

using namespace std;

/*      Bergamasco Daniele - 10.12.2019  -  Voti e media dei voti di una classe - versione 01
        Esempio: una classe di 32 studenti ha sostenuto durante l'anno 5 compiti in classe.
        Supponiamo di voler scrivere un programma che stampi per ogni studente la somma e la media dei voti ottenuti.   */

int main()
{
    int studenti = 5, compiti = 3;  // Righe e Colonne
    int voti[studenti][compiti];
    int somma=0;

    cout << "\n\n********** Voti e media dei voti di una classe - versione 01 *********\n";
    cout <<"\nInserisci qui sotto i voti agli studenti\n";

    for(int i=0; i<studenti; i++)                           // scansione righe "studenti" della matrice
    {
        cout << "\nStudente "<<i+1<<" :\n";
        for(int j=0; j<compiti; j++)                        // scansione colonne "compiti" della matrice                        
            {
                cout <<"-> Inserisci "<< j+1 <<"^ voto: ";
                cin >> voti[i][j];
            }
    }
    cout << endl;

    cout <<"\nDeterminiamo la media degli studenti\n";
    for(int i=0; i<studenti; i++)                           // scansione righe "studenti" della matrice
    {
        cout << "\nLo Studente "<<i+1;
        somma=0;                                            // reset della variabile somma dei voti
        for(int j=0; j<compiti; j++)                        // scansione colonne "compiti" della matrice
            {
                somma+= voti[i][j];                         // incremento della variabile somma dei voti
            }
        cout <<" ha somma dei voti di "<<somma;             // stampa della somma dei voti
        cout << " e una media di "<<somma/compiti;          // stampa della media dei voti
    }
    cout << endl;

    return 0;
}
