#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 14.10.2019

    extra 1) Input da tastiera:  
    • se il numero e' 2 o 4 stampare il numero
    • se il numero e' tra il valore 10 e 20 stampare il numero */

int main()
{
    int num;
    int val1=10;
    int val2=20;

    cout <<"Inserisci un numero qui: ";
    cin >> num;

    if(num==2 || num==4)
    {
        cout <<"il numero inserito e': " <<num<<endl;
    }

    else
    {
        if(num>=val1 && num<=val2)
    {
        cout <<"il numero"<<num<<" e' compreso tra il valore "<<val1<<" e "<<val2<<endl;
    }

        else
        {
            cout<<"il numero "<<num<<" non e' valido per questo gioco!!";
        }
    }
 
    return 0;
}

