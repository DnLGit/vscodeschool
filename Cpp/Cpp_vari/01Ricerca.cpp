#include <iostream>
using namespace std;


int ricercaSeq(int e, int vect[], int N);
int ricercaSeqEff(int e, int vect[], int N);

int main(int argc, char* argv[])
{
    const int N = 10;
    int array[N] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};

    int e1 = 70;
    int e2 = 55;

    int i1 = ricercaSeq(e1, array, N);
    if (i1 < 0)
        cout << "l'elemento e1 non e' stato trovato\n";
    else
        cout << "l'elemento e1 e' stato trovato nella posizione " << i1 << endl;

    int i2 = ricercaSeq(e2, array, N);
    if(i2 < 0) 
        cout << "l'elemento e2 non e' stato trovato\n";
    else
        cout << "l'elemento e2 e' stato trovato nella posizione " << i2 << endl;

    return 0;
}

int ricercaSeq(int e, int vect[], int N)
{
    int pos = -1;
    for(int i=0; i < N; i++)
    {
        if(e == vect[i])
            pos = i;

    }

    return pos;
}

int ricercaSeqEff(int e, int vect[], int N)
{
    for(int i=0; i < N; i++)
    {
        if(e == vect[i])
            return i;

    }

    return -1;
 }