#include <iostream>

using namespace std;

/*      Bergamasco Daniele - 10.12.2019  -  Voti e media dei voti di una classe - versione 02
        Esempio: una classe di 32 studenti ha sostenuto durante l'anno 5 compiti in classe.
        Supponiamo di voler scrivere un programma che stampi per ogni studente la somma e la media dei voti ottenuti.   */

int main()
{
    int studenti = 5, compiti = 4;  // Righe e Colonne (ultima cella della riga sarà riservata alla media)
    float voti[studenti][compiti];
    float somma=0;

    cout << "\n\n********** Voti e media dei voti di una classe - versione 02 *********\n";
    cout <<"\n\nInserisci qui sotto i voti agli studenti\n";

    for(int i=0; i<studenti; i++)                           // scansione righe "studenti" della matrice
    {
        cout << "\nStudente "<<i+1<<" :\n";
        somma=0;                                            // reset della variabile somma dei voti
        for(int j=0; j<(compiti-1); j++)                    // scansione colonne "compiti" della matrice
            {
                cout <<"-> Inserisci "<< j+1 <<"^ voto: ";
                cin >> voti[i][j];                          // Inserimento voti per ogni studente
                somma+= voti[i][j];                         // somma dei voti di ogni studente
            }
            voti[i][compiti]=somma/(compiti-1);             // memorizzazione media nell'ultima cella della riga rispettiva             
            cout <<"La somma dei voti dello studente "<<i+1<<" e': " <<somma<<endl;
            cout <<"mentre la sua media risulata essere di: "<<voti[i][compiti]<<endl;
    }
    cout << endl;

    return 0;
}
