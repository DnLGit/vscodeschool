#include<iostream>
using namespace std;

/*  03.10.2019 (Home) - Bergamasco Daniele   cl.: IV As

4. Dato il prezzo di un prodotto, calcolare il prezzo da pagare sapendo che se il prezzo è superiore a una
determinata cifra (costante) si applica il 6% di sconto, e se il pagamento è fatto in contanti si applica un
ulteriore sconto del 2%  */

int main ()
{
    double prezzo;
    const double listino = 350;
    const int sc1 = 6;
    const int sc2 = 2;
    bool contanti;
    

    cout << "Inserire il prezzo del prodotto: ";
    cin >> prezzo;

    if(prezzo>listino)
    {
        cout <<"pagherai "<< prezzo*(100-6) << "euro"<<endl;
    }
    else
    {
        cout << "pagherai prezzo pieno di " << prezzo<<endl;
    }

    if(prezzo>listino && contanti==1)
    {
        cout <<"pagherai "<< prezzo*(100-6) << "euro"<<endl;
    }




    return 0;
}