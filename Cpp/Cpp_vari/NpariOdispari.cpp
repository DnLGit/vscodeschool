#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 14.10.2019

    extra 2) Input di "N" numeri da tastiera:  
    • determinari quanti numeri pari ci sono 
    • (provare anche a stampare tutti i numeri inseriti - DnL) */

int main()
{
    int num, N;
    int pari=0;
    int dispari=0;

    cout <<"dimmi qui quanti numeri vuoi far inserire: ";
    cin >> N;


    for (int i=1; i<=N; i++)
    {
        cout <<"inserisci il numero: ";
        cin >> num;

        if(num%2==0)
        {
            pari++;
        }
        else
        {
            dispari++;
        }
    } 
    cout <<"Fra tutti i "<< N << "numeri inseriti ci sono \n"<< pari <<" numeri pari e \n"<< dispari <<" numeri dispari"<<endl;

return 0;
}

