#include <string>
#include <cstring>      // dovrebbe sostituire <string.h> e permettere di usare le funzioni di C in C++
#include <cctype>       // dovrebbe sostituire <ctype.h> e permettere di usare le funzioni di C in C++
#include<iomanip>       /* Libreria per la funzione setw(n)*/
#include<iostream>
using namespace std;

/*  Bergamasco Daniele 4As - 21/23.01.2020 >>>>>>>>>>>> Esercizi con Matrici e Stringhe

    Esercizio in classe e poi in Laboratorio C (bunker IV ora)
    Realizzare un programma in cui creare una matrice di stringhe 10x10 e: 
    - riempire da tastiera con delle parole e terminare (uscire) con la parola "FINE"
    - calcolare la somma della lunghezza totale delle parole
    - stampare assieme la 2°e penultima parola (es.:  ciao + mondo >> ciao mondo)
    - calcolare quanti charatteri alfabetici, quanti numerici e quanti simboli ci sono nella matrice    */


int main()
{
    
int const N = 10;
char matrix[N][N];
int i, lungS, righe, alfa, num, simb;

cout << "\n ***************************\n *** Matrice di Stringhe ***\n ***************************\n";
cout << "inserisci almeno 3 parole seguite da FINE per chiudere: \n\n";

/* 
cout <<"(con ciclo for)\n\n";
lungS=0; righe=0;
for(int i=0; i<N && strcmp(matrix[i-1], "FINE")!=0; i++)

    {   
        cout << i+1 << "^ insert: ";
        cin.getline(matrix[i], N);                                  // utilizzabile anche "cin" se non si inseriscono spazi
        lungS+=strlen(matrix[i]);                                   // sommatoria delle lunghezze di tutte le parole
        righe=i+1;                                                    // memorizzo il numero di righe della matrice
    } 
*/    
  
    cout <<"(con ciclo while)\n\n";
    i=0; lungS=0; righe=0; 
    do
    {
        cout << i+1 << "^ insert: ";
        cin.getline(matrix[i], N);                                  // utilizzabile anche "cin" se non si inseriscono spazi
        lungS+=strlen(matrix[i]);                                   // sommatoria delle lunghezze di tutte le parole
        
        i++;                                                        // incremento l'indice
        righe=i;                                                    // memorizzo il numero di righe della matrice
    }
    while(strcmp(matrix[i-1],"FINE")!=0);                           // esco dal while quando strcmp non sara' piu' !=0

    
    // stampa matrice (compreso FINE):
    cout << "\nStampa della matrice: \n";
    for(int i=0; i<righe; i++)
    {
        cout << matrix[i] <<"\n";
    }

    cout << "\nLa somma delle lunghezze delle parole e': " << lungS << " caratteri\n";
    strcat(matrix[1], " ");                                         // concateno uno spazio alla seconda parola
    strcat(matrix[1], matrix[righe-2]);                                 // concateno la penultima parola alla seconda
    cout << "\nL'insieme della seconda parola con la penultima e': "<< matrix[1] <<endl;    

    alfa=0; num=0; simb=0;
    for(int i=0; i<righe-1; i++)                                    // il ciclo di i si ferma a righe-1 per escludere "FINE"
    {
        for(int j=0; j<N && matrix[i][j]!='\0'; j++)                // scorro gli array fino al tappo
        {
            if(isalpha(matrix[i][j]))                               // controllo e conto le lettere
                alfa++;

            if(isdigit(matrix[i][j]))                               // controllo e conto le cifre
                num++;

            if(!isalnum(matrix[i][j]))                              // controllo e conto i simboli
                simb++;                                             // (tutto ciò che non e' alfanumerico)
        }
    }

    cout << "\nNella matrice ci sono: \n";
    cout << "\nlettere dell'alfabeto: " << alfa;
    cout << "\ncaratteri numerici e: " << num;
    cout << "\ncaratteri simbolici: " << simb <<endl;
    
	return 0;

}