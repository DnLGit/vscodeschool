#include<iostream>
using namespace std;

/* Bergamasco Daniele 4As - 16.10.2019

11. Si scriva un programma in linguaggio C che acquisisca tre numeri interi da tastiera e: 
• determini, stampando un messaggio opportuno quale dei tre numeri (il primo, il secondo o il terzo) sia maggiore 
• stampi il valore di tale numero.  Si trascuri il caso in cui i numeri siano uguali  
    -->    Versione con If/For */

int main()
{
    int num, max=0;
    cout <<"Inserisci tre numeri"<<endl;

    for(int i=1; i<4; i++)
    {
        cout <<"inserisci qui il " <<i<<"^ numero: ";
        cin >>num;
        if(num>max)
        {
            max=num;
        }
    }
    cout <<"Il numero maggiore dei tre e' "<<max<<endl;

    return 0;
}