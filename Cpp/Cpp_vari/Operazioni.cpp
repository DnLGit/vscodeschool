#include <iostream>
using namespace std;

/* Bergamasco Daniele  4As

    5. (per chi non si sente sicuro). 
    Si scriva un programma in linguaggio C/C++ capace di compiere le 4 operazioni (somma, sottrazione, moltiplicazione e divisione) tra due numeri reali inseriti da tastiera.
    Dopo che sono stati inseriti i due numeri, detti A e B, il programma dovrà visualizzare i quattro valori A+B, A-B, A*B, A/B. Si ipotizzi che sia B!=0. */

int main()
{
  int a, b;
  
  cout << "Inserisci due fattori a e b"<<endl;
  cout << "Inserisci a: ";
  cin >> a;
  cout << "Inserisci b: ";
  cin >> b;

  int piu = a+b;
  int meno = a-b;
  int per = a*b;
  int divi = a/b;
  
  cout << "La somma a+b e': " << piu << endl;
  cout << "La sottrazione a-b e': " << meno << endl;
  cout << "Il prodotto axb e': " << per << endl;
  cout << "La divisione a:b e': " << divi << endl;

  return 0;
  

}